package org.kareha.hareka.client;

import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.security.KeyManagementException;
import java.security.cert.X509Certificate;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.net.ssl.X509TrustManager;
import javax.xml.bind.JAXBException;

import org.kareha.hareka.key.SimpleTrustManager;

final class ConnectorTest {

	private static final Logger logger = Logger.getLogger(ConnectorTest.class.getName());
	private static final String host = "localhost";
	private static final int port = 2468;

	private ConnectorTest() {
		throw new AssertionError();
	}

	public static void main(final String[] args) throws IOException, JAXBException {
		final Context context = new Context(null);
		final X509TrustManager trustManager = new SimpleTrustManager(context.getSimpleKeyStore(), host) {
			@Override
			protected boolean acceptCertificate(final X509Certificate cert) {
				return true;
			}
		};
		try (final Socket socket = Connector.connectSecurely(host, port, trustManager)) {
			socket.close();
		} catch (final UnknownHostException e) {
			logger.log(Level.SEVERE, "", e);
			return;
		} catch (final SecurityException e) {
			logger.log(Level.SEVERE, "", e);
			return;
		} catch (final IOException e) {
			logger.log(Level.SEVERE, "", e);
			return;
		} catch (final KeyManagementException e) {
			logger.log(Level.SEVERE, "", e);
			return;
		}
	}

}
