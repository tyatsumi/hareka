package org.kareha.hareka.client;

import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.security.KeyManagementException;
import java.security.cert.X509Certificate;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.net.ssl.X509TrustManager;
import javax.xml.bind.JAXBException;

import org.kareha.hareka.key.SimpleTrustManager;
import org.kareha.hareka.protocol.HandlerTable;
import org.kareha.hareka.protocol.WorldClientPacketType;

final class ConnectorTest2 {

	private static final Logger logger = Logger.getLogger(ConnectorTest2.class.getName());
	private static final String host = "localhost";
	private static final int port = 2468;

	private ConnectorTest2() {
		throw new AssertionError();
	}

	@SuppressWarnings("resource")
	public static void main(final String[] args) throws IOException, JAXBException {
		final Context context = new Context(null);
		final X509TrustManager trustManager = new SimpleTrustManager(context.getSimpleKeyStore(), host) {
			@Override
			protected boolean acceptCertificate(final X509Certificate cert) {
				return true;
			}
		};
		final Socket socket;
		try {
			socket = Connector.connectSecurely(host, port, trustManager);
		} catch (final UnknownHostException e) {
			logger.log(Level.SEVERE, "", e);
			return;
		} catch (final SecurityException e) {
			logger.log(Level.SEVERE, "", e);
			return;
		} catch (final IOException e) {
			logger.log(Level.SEVERE, "", e);
			return;
		} catch (final KeyManagementException e) {
			logger.log(Level.SEVERE, "", e);
			return;
		}
		final HandlerTable<WorldClientPacketType, WorldSession> parserTable = new HandlerTable<>(
				"org.kareha.hareka.client.handler", WorldClientPacketType.class, "Handler");
		final ConnectionSettings.Entry connection = new ConnectionSettings.Entry("Test", host, port,
				"https://example.net/hareka/");
		final WorldSession session = WorldSession.newInstance(context, connection, socket, parserTable);
		session.start();
	}

}
