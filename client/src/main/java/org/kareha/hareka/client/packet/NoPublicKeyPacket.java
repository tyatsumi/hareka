package org.kareha.hareka.client.packet;

import org.kareha.hareka.client.protocol.WorldServerPacket;
import org.kareha.hareka.protocol.PacketType;
import org.kareha.hareka.protocol.WorldServerPacketType;

public final class NoPublicKeyPacket extends WorldServerPacket {

	public NoPublicKeyPacket(final int handlerId, final int version) {
		out.writeCompactUInt(handlerId);
		out.writeCompactUInt(version);
	}

	@Override
	protected PacketType getType() {
		return WorldServerPacketType.NO_PUBLIC_KEY;
	}

}
