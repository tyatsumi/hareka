package org.kareha.hareka.client.handler;

import java.io.IOException;

import org.kareha.hareka.LocalEntityId;
import org.kareha.hareka.client.WorldSession;
import org.kareha.hareka.protocol.HandlerException;
import org.kareha.hareka.protocol.ProtocolInput;
import org.kareha.hareka.protocol.Handler;
import org.kareha.hareka.protocol.ProtocolException;

public class SetItemCountHandler implements Handler<WorldSession> {

	@Override
	public void handle(final ProtocolInput in, final WorldSession session)
			throws IOException, ProtocolException, HandlerException {
		final LocalEntityId id = LocalEntityId.readFrom(in);
		final long count = in.readCompactULong();

		session.getMirrors().getInventoryMirror().handleSetCount(id, count);
	}

}
