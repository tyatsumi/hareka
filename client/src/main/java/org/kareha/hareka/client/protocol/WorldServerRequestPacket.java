package org.kareha.hareka.client.protocol;

import java.io.IOException;

import org.kareha.hareka.protocol.ProtocolOutput;
import org.kareha.hareka.protocol.ProtocolOutputStream;

public abstract class WorldServerRequestPacket extends WorldServerPacket {

	protected ProtocolOutput body = new ProtocolOutputStream();

	public WorldServerRequestPacket() {
		// do nothing
	}

	public void setRequestId(final int requestId) {
		out.writeCompactUInt(requestId);
		try {
			((ProtocolOutputStream) body).writeTo((ProtocolOutputStream) out);
		} catch (final IOException e) {
			throw new RuntimeException(e);
		}
		body = null;
	}

}
