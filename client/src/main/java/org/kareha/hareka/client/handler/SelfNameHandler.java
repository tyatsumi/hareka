package org.kareha.hareka.client.handler;

import java.io.IOException;

import org.kareha.hareka.LocalEntityId;
import org.kareha.hareka.client.WorldSession;
import org.kareha.hareka.client.chat.ChatEntity;
import org.kareha.hareka.game.Name;
import org.kareha.hareka.protocol.HandlerException;
import org.kareha.hareka.protocol.ProtocolInput;
import org.kareha.hareka.protocol.Handler;
import org.kareha.hareka.protocol.ProtocolException;

public class SelfNameHandler implements Handler<WorldSession> {

	@Override
	public void handle(final ProtocolInput in, final WorldSession session)
			throws IOException, ProtocolException, HandlerException {
		final LocalEntityId id = LocalEntityId.readFrom(in);
		final Name name = Name.readFrom(in);

		final ChatEntity entity = session.getMirrors().getSelfMirror().getChatEntity();
		if (entity == null) {
			return;
		}
		if (!id.equals(entity.getLocalId())) {
			return;
		}
		session.getMirrors().getSelfMirror().handleSetName(name);
	}

}
