package org.kareha.hareka.client.handler;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.kareha.hareka.LocalEntityId;
import org.kareha.hareka.LocalUserId;
import org.kareha.hareka.client.WorldSession;
import org.kareha.hareka.protocol.HandlerException;
import org.kareha.hareka.protocol.ProtocolInput;
import org.kareha.hareka.protocol.Handler;
import org.kareha.hareka.protocol.ProtocolException;

public class InspectionHandler implements Handler<WorldSession> {

	@Override
	public void handle(final ProtocolInput in, final WorldSession session)
			throws IOException, ProtocolException, HandlerException {
		final LocalEntityId chatLocalId = LocalEntityId.readFrom(in);
		final LocalEntityId fieldLocalId = LocalEntityId.readFrom(in);
		final int userCount = in.readCompactUInt();
		final List<LocalUserId> userIds = new ArrayList<>();
		for (int i = 0; i < userCount; i++) {
			userIds.add(LocalUserId.readFrom(in));
		}

		session.getMirrors().getGroupMirror().handleInspection(chatLocalId, fieldLocalId, userIds);
	}

}
