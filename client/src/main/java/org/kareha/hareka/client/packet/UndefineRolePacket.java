package org.kareha.hareka.client.packet;

import org.kareha.hareka.client.protocol.WorldServerPacket;
import org.kareha.hareka.protocol.PacketType;
import org.kareha.hareka.protocol.WorldServerPacketType;

public final class UndefineRolePacket extends WorldServerPacket {

	public UndefineRolePacket(final String roleId) {
		out.writeString(roleId);
	}

	@Override
	protected PacketType getType() {
		return WorldServerPacketType.UNDEFINE_ROLE;
	}

}
