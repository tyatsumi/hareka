package org.kareha.hareka.client.packet;

import org.kareha.hareka.client.protocol.WorldServerRequestPacket;
import org.kareha.hareka.protocol.PacketType;
import org.kareha.hareka.protocol.WorldServerPacketType;

public final class LoginUserPacket extends WorldServerRequestPacket {

	public LoginUserPacket() {
		// do nothing
	}

	@Override
	protected PacketType getType() {
		return WorldServerPacketType.LOGIN_USER;
	}

}
