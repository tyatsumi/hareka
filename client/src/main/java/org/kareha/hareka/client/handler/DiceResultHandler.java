package org.kareha.hareka.client.handler;

import java.io.IOException;
import java.util.logging.Logger;

import org.kareha.hareka.client.DiceRoll;
import org.kareha.hareka.client.WorldSession;
import org.kareha.hareka.protocol.HandlerException;
import org.kareha.hareka.protocol.ProtocolInput;
import org.kareha.hareka.protocol.Handler;
import org.kareha.hareka.protocol.ProtocolException;

public class DiceResultHandler implements Handler<WorldSession> {

	private static final Logger logger = Logger.getLogger(DiceResultHandler.class.getName());

	@Override
	public void handle(final ProtocolInput in, final WorldSession session)
			throws IOException, ProtocolException, HandlerException {
		final int id = in.readCompactUInt();
		final int value = in.readCompactUInt();
		final byte[] secret = in.readByteArray();

		final DiceRoll diceRoll = session.getDiceRollTable().getDiceRoll(id);
		if (diceRoll == null) {
			// TODO report error
			logger.warning("A nonexistent dice roll result: id=" + id);
			return;
		}
		diceRoll.setValue(value);
		diceRoll.setSecret(secret);
		if (!diceRoll.checkHash()) {
			// TODO report cheat
			logger.warning("A dice roll cheat: id=" + id);
			return;
		}
	}

}
