package org.kareha.hareka.client.packet;

import org.kareha.hareka.client.protocol.WorldServerPacket;
import org.kareha.hareka.protocol.PacketType;
import org.kareha.hareka.protocol.WorldServerPacketType;

public final class TeleportToCenterPacket extends WorldServerPacket {

	public TeleportToCenterPacket() {

	}

	@Override
	protected PacketType getType() {
		return WorldServerPacketType.TELEPORT_TO_CENTER;
	}

}
