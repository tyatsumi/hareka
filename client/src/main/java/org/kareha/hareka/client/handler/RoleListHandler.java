package org.kareha.hareka.client.handler;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.kareha.hareka.client.WorldSession;
import org.kareha.hareka.client.user.CustomRole;
import org.kareha.hareka.protocol.HandlerException;
import org.kareha.hareka.protocol.ProtocolInput;
import org.kareha.hareka.protocol.Handler;
import org.kareha.hareka.protocol.ProtocolException;
import org.kareha.hareka.user.Role;

public class RoleListHandler implements Handler<WorldSession> {

	@Override
	public void handle(final ProtocolInput in, final WorldSession session)
			throws IOException, ProtocolException, HandlerException {
		final int size = in.readCompactUInt();
		final List<Role> list = new ArrayList<>();
		for (int i = 0; i < size; i++) {
			final Role role = CustomRole.readFrom(in);
			list.add(role);
		}

		session.getMirrors().getAdminMirror().handleRoleList(list);
	}

}
