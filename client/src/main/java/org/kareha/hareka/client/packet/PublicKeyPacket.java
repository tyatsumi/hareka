package org.kareha.hareka.client.packet;

import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.Signature;
import java.security.SignatureException;

import org.kareha.hareka.client.protocol.WorldServerPacket;
import org.kareha.hareka.protocol.PacketType;
import org.kareha.hareka.protocol.WorldServerPacketType;

public final class PublicKeyPacket extends WorldServerPacket {

	public PublicKeyPacket(final int handlerId, final int version, final byte[] nonce, final KeyPair keyPair,
			final String signatureAlgorithm) {
		out.writeCompactUInt(handlerId);
		out.writeCompactUInt(version);
		// write public key
		out.writeKey(keyPair.getPublic());
		// write algorithm
		out.writeString(signatureAlgorithm);
		// write signature
		final Signature signature;
		try {
			signature = Signature.getInstance(signatureAlgorithm);
		} catch (final NoSuchAlgorithmException e) {
			throw new RuntimeException(e);
		}
		try {
			signature.initSign(keyPair.getPrivate());
		} catch (final InvalidKeyException e) {
			throw new RuntimeException(e);
		}
		try {
			signature.update(nonce);
		} catch (final SignatureException e) {
			throw new RuntimeException(e);
		}
		final byte[] s;
		try {
			s = signature.sign();
		} catch (final SignatureException e) {
			throw new RuntimeException(e);
		}
		out.writeByteArray(s);
	}

	@Override
	protected PacketType getType() {
		return WorldServerPacketType.PUBLIC_KEY;
	}

}
