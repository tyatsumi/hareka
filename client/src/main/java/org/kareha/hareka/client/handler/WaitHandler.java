package org.kareha.hareka.client.handler;

import java.io.IOException;

import org.kareha.hareka.client.WorldSession;
import org.kareha.hareka.protocol.HandlerException;
import org.kareha.hareka.protocol.ProtocolInput;
import org.kareha.hareka.protocol.Handler;
import org.kareha.hareka.protocol.ProtocolException;
import org.kareha.hareka.wait.Wait;

public class WaitHandler implements Handler<WorldSession> {

	@Override
	public void handle(final ProtocolInput in, final WorldSession session)
			throws IOException, ProtocolException, HandlerException {
		final Wait wait = Wait.readFrom(in);

		session.getMirrors().getSelfMirror().getWaitTable().put(wait);
	}

}
