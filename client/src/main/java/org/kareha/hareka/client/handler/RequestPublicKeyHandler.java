package org.kareha.hareka.client.handler;

import java.io.IOException;
import java.security.KeyPair;

import org.kareha.hareka.Constants;
import org.kareha.hareka.client.WorldSession;
import org.kareha.hareka.client.packet.NoPublicKeyPacket;
import org.kareha.hareka.client.packet.PublicKeyPacket;
import org.kareha.hareka.protocol.HandlerException;
import org.kareha.hareka.protocol.ProtocolInput;
import org.kareha.hareka.protocol.Handler;
import org.kareha.hareka.protocol.ProtocolException;

public class RequestPublicKeyHandler implements Handler<WorldSession> {

	@Override
	public void handle(final ProtocolInput in, final WorldSession session)
			throws IOException, ProtocolException, HandlerException {
		final int handlerId = in.readCompactUInt();
		final int version = in.readCompactUInt();
		final byte[] nonce = in.readByteArray();

		final KeyPair keyPair = session.getMirrors().getSystemMirror().getKeyPair(version);
		if (keyPair == null) {
			session.write(new NoPublicKeyPacket(handlerId, version));
		} else {
			session.write(new PublicKeyPacket(handlerId, version, nonce, keyPair, Constants.SIGNATURE_ALGORITHM));
		}
	}

}
