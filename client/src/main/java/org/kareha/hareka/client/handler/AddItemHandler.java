package org.kareha.hareka.client.handler;

import java.io.IOException;

import org.kareha.hareka.LocalEntityId;
import org.kareha.hareka.client.WorldSession;
import org.kareha.hareka.game.ItemType;
import org.kareha.hareka.protocol.HandlerException;
import org.kareha.hareka.protocol.ProtocolInput;
import org.kareha.hareka.protocol.Handler;
import org.kareha.hareka.protocol.ProtocolException;
import org.kareha.hareka.wait.WaitType;

public class AddItemHandler implements Handler<WorldSession> {

	@Override
	public void handle(final ProtocolInput in, final WorldSession session)
			throws IOException, ProtocolException, HandlerException {
		final LocalEntityId id = LocalEntityId.readFrom(in);
		final String shape = in.readString();
		final long count = in.readCompactULong();
		final ItemType type = ItemType.readFrom(in);
		final String name = in.readString();
		final int weight = in.readCompactUInt();
		final int reach = in.readCompactUInt();
		final WaitType waitType = WaitType.readFrom(in);
		final int wait = in.readCompactUInt();

		session.getMirrors().getInventoryMirror().handleAdd(id, shape, count, type, name, weight, reach, waitType,
				wait);
	}

}
