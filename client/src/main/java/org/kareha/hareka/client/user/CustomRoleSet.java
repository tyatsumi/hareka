package org.kareha.hareka.client.user;

import java.util.Collection;

import org.kareha.hareka.user.AbstractRoleSet;
import org.kareha.hareka.user.Role;
import org.kareha.hareka.user.RoleSet;

public class CustomRoleSet extends AbstractRoleSet {

	public CustomRoleSet(final Collection<Role> roles) {
		this.roles.addAll(roles);
	}

	public CustomRoleSet(final RoleSet original) {
		super(original);
	}

}
