package org.kareha.hareka.client.packet;

import org.kareha.hareka.client.chat.ChatEntity;
import org.kareha.hareka.client.protocol.WorldServerPacket;
import org.kareha.hareka.protocol.PacketType;
import org.kareha.hareka.protocol.WorldServerPacketType;

public final class InspectChatEntityPacket extends WorldServerPacket {

	public InspectChatEntityPacket(final ChatEntity target) {
		out.write(target.getLocalId());
	}

	@Override
	protected PacketType getType() {
		return WorldServerPacketType.INSPECT_CHAT_ENTITY;
	}

}
