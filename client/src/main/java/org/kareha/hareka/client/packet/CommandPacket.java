package org.kareha.hareka.client.packet;

import org.kareha.hareka.client.protocol.WorldServerPacket;
import org.kareha.hareka.protocol.PacketType;
import org.kareha.hareka.protocol.WorldServerPacketType;

public final class CommandPacket extends WorldServerPacket {

	public CommandPacket(final String[] args) {
		out.writeCompactUInt(args.length);
		for (int i = 0; i < args.length; i++) {
			out.writeString(args[i]);
		}
	}

	@Override
	protected PacketType getType() {
		return WorldServerPacketType.COMMAND;
	}

}
