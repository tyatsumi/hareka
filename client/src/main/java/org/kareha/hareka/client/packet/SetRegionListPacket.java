package org.kareha.hareka.client.packet;

import java.util.Collection;

import org.kareha.hareka.client.protocol.WorldServerPacket;
import org.kareha.hareka.field.Region;
import org.kareha.hareka.protocol.PacketType;
import org.kareha.hareka.protocol.WorldServerPacketType;

public final class SetRegionListPacket extends WorldServerPacket {

	public SetRegionListPacket(final Collection<Region> regions) {
		out.writeCompactUInt(regions.size());
		for (final Region region : regions) {
			out.write(region);
		}
	}

	@Override
	protected PacketType getType() {
		return WorldServerPacketType.SET_REGION_LIST;
	}

}
