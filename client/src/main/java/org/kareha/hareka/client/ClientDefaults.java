package org.kareha.hareka.client;

import java.io.File;

import org.kareha.hareka.annotation.SynchronizedWith;

public final class ClientDefaults {

	private ClientDefaults() {
		throw new AssertionError();
	}

	@SynchronizedWith("org.kareha.hareka.updater.UpdaterConstants")
	public static final File DATA_DIRECTORY = new File(System.getProperty("user.dir"), "hareka_data");

}
