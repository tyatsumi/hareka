package org.kareha.hareka.client.handler;

import java.io.IOException;
import java.math.BigInteger;

import org.kareha.hareka.client.WorldSession;
import org.kareha.hareka.protocol.HandlerException;
import org.kareha.hareka.protocol.ProtocolInput;
import org.kareha.hareka.protocol.Handler;
import org.kareha.hareka.protocol.ProtocolException;

public class RequestPowHandler implements Handler<WorldSession> {

	@Override
	public void handle(final ProtocolInput in, final WorldSession session)
			throws IOException, ProtocolException, HandlerException {
		final int handlerId = in.readCompactUInt();
		final BigInteger target = in.readBigInteger();
		final byte[] data = in.readByteArray();
		final String message = in.readString();

		session.getMirrors().getSystemMirror().handleRequestPow(handlerId, target, data, message);
	}

}
