package org.kareha.hareka.client.packet;

import org.kareha.hareka.client.protocol.WorldServerPacket;
import org.kareha.hareka.protocol.PacketType;
import org.kareha.hareka.protocol.WorldServerPacketType;

public final class DeleteRoleTokenPacket extends WorldServerPacket {

	public DeleteRoleTokenPacket(final long roleTokenId) {
		out.writeCompactULong(roleTokenId);
	}

	@Override
	protected PacketType getType() {
		return WorldServerPacketType.DELETE_ROLE_TOKEN;
	}

}
