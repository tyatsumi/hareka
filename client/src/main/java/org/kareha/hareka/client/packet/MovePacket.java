package org.kareha.hareka.client.packet;

import org.kareha.hareka.client.protocol.WorldServerPacket;
import org.kareha.hareka.field.Vector;
import org.kareha.hareka.protocol.PacketType;
import org.kareha.hareka.protocol.WorldServerPacketType;
import org.kareha.hareka.wait.Wait;

public final class MovePacket extends WorldServerPacket {

	public MovePacket(final Wait wait, final Vector position) {
		out.writeCompactULong(wait.getId());
		out.write(position);
	}

	@Override
	protected PacketType getType() {
		return WorldServerPacketType.MOVE;
	}

}
