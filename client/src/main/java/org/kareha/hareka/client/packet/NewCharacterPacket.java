package org.kareha.hareka.client.packet;

import org.kareha.hareka.client.protocol.WorldServerRequestPacket;
import org.kareha.hareka.protocol.PacketType;
import org.kareha.hareka.protocol.WorldServerPacketType;

public final class NewCharacterPacket extends WorldServerRequestPacket {

	public NewCharacterPacket() {
		// do nothing
	}

	@Override
	protected PacketType getType() {
		return WorldServerPacketType.NEW_CHARACTER;
	}

}
