package org.kareha.hareka.client;

import java.security.SecureRandom;
import java.util.HashMap;
import java.util.Map;

import org.kareha.hareka.annotation.GuardedBy;

public class DiceRollTable {

	@GuardedBy("this")
	private final Map<Integer, DiceRoll> map = new HashMap<>();
	private final SecureRandom random = new SecureRandom();

	public synchronized DiceRoll accept(final int id) {
		if (map.containsKey(id)) {
			return null;
		}
		final DiceRoll diceRoll = new DiceRoll(id, random);
		map.put(id, diceRoll);
		return diceRoll;
	}

	public synchronized DiceRoll getDiceRoll(final int id) {
		return map.get(id);
	}

}
