package org.kareha.hareka.client.handler;

import java.io.IOException;

import org.kareha.hareka.client.WorldSession;
import org.kareha.hareka.client.field.FieldEntity;
import org.kareha.hareka.protocol.HandlerException;
import org.kareha.hareka.protocol.ProtocolInput;
import org.kareha.hareka.protocol.Handler;
import org.kareha.hareka.protocol.ProtocolException;

public class TeleportHandler implements Handler<WorldSession> {

	@Override
	public void handle(final ProtocolInput in, final WorldSession session)
			throws IOException, ProtocolException, HandlerException {
		if (!session.getMirrors().getSelfMirror().isWaitTeleport()) {
			return;
		}
		final FieldEntity entity = session.getMirrors().getSelfMirror().getFieldEntity();
		if (entity != null) {
			try {
				Thread.sleep(entity.getMotionWait());
			} catch (final InterruptedException e) {
				Thread.currentThread().interrupt();
			}
		}
	}

}
