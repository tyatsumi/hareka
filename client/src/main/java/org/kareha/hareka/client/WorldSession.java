package org.kareha.hareka.client;

import java.net.Socket;
import java.security.PublicKey;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

import org.kareha.hareka.ManuallyClosable;
import org.kareha.hareka.annotation.GuardedBy;
import org.kareha.hareka.client.mirror.Mirrors;
import org.kareha.hareka.client.protocol.WorldServerPacket;
import org.kareha.hareka.client.protocol.WorldServerRequestPacket;
import org.kareha.hareka.client.server.ChatEntityEntry;
import org.kareha.hareka.client.server.Server;
import org.kareha.hareka.key.KeyGrabber;
import org.kareha.hareka.protocol.HandlerTable;
import org.kareha.hareka.protocol.ProtocolSocket;
import org.kareha.hareka.protocol.WorldClientPacketType;

public class WorldSession implements ManuallyClosable {

	private volatile ConnectionSettings.Entry connection;
	private volatile ProtocolSocket<WorldClientPacketType, WorldSession> packetSocket;
	private final Context context;
	@GuardedBy("this")
	private Locale locale = Locale.getDefault();
	private final AtomicInteger nextRequestId = new AtomicInteger();
	private final Map<Integer, ResponseHandler> responseHandlers = new ConcurrentHashMap<>();
	private final KeyGrabber keyGrabber = new KeyGrabber();
	@GuardedBy("this")
	private PublicKey peerKey;
	@GuardedBy("this")
	private Server server;
	private volatile Mirrors mirrors;
	private final DiceRollTable diceRollTable = new DiceRollTable();

	// not private for test
	WorldSession(final Context context) {
		this.context = context;
	}

	// not private for test
	ProtocolSocket<WorldClientPacketType, WorldSession> newPacketSocket(final Socket socket,
			final HandlerTable<WorldClientPacketType, WorldSession> parserTable) {
		return new ProtocolSocket<WorldClientPacketType, WorldSession>(socket, parserTable, this) {
			@Override
			public void finish() {
				getContext().getServers().logout();
			}
		};
	}

	// not private for test
	void initialize(final Socket socket, final HandlerTable<WorldClientPacketType, WorldSession> parserTable) {
		packetSocket = newPacketSocket(socket, parserTable);
		mirrors = new Mirrors(this);
	}

	public static WorldSession newInstance(final Context context, final ConnectionSettings.Entry connection,
			final Socket socket, final HandlerTable<WorldClientPacketType, WorldSession> parserTable) {
		final WorldSession session = new WorldSession(context);
		session.connection = connection;
		session.initialize(socket, parserTable);
		return session;
	}

	public void start() {
		packetSocket.start();
	}

	@Override
	public void close() {
		packetSocket.close();
		final Mirrors m = mirrors;
		if (m != null) {
			m.close();
		}
	}

	public ConnectionSettings.Entry getConnection() {
		return connection;
	}

	public void write(final WorldServerPacket packet) {
		packetSocket.write(packet);
	}

	public void write(final WorldServerRequestPacket packet, final ResponseHandler responseHandler) {
		final int i = nextRequestId.getAndIncrement();
		responseHandlers.put(i, responseHandler);
		packet.setRequestId(i);
		write(packet);
	}

	public Context getContext() {
		return context;
	}

	public synchronized Locale getLocale() {
		return locale;
	}

	public synchronized void setLocale(final Locale locale) {
		this.locale = locale;
	}

	public ResponseHandler removeResponseHandler(final int requestId) {
		return responseHandlers.remove(requestId);
	}

	public KeyGrabber getKeyGrabber() {
		return keyGrabber;
	}

	public synchronized PublicKey getPeerKey() {
		return peerKey;
	}

	public synchronized void setPeerKey(final PublicKey peerKey) {
		this.peerKey = peerKey;
	}

	public synchronized Server getServer() {
		return server;
	}

	public synchronized void setServer(final Server server) {
		for (final ChatEntityEntry entry : server.getChatEntityEntries()) {
			getMirrors().getEntityMirror().loadChatEntity(entry.getId(), entry.getName());
		}
		server.setSession(this);
		this.server = server;
	}

	public Mirrors getMirrors() {
		return mirrors;
	}

	public DiceRollTable getDiceRollTable() {
		return diceRollTable;
	}

}
