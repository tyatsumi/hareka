package org.kareha.hareka.client.packet;

import org.kareha.hareka.client.protocol.WorldServerPacket;
import org.kareha.hareka.field.TilePiece;
import org.kareha.hareka.protocol.PacketType;
import org.kareha.hareka.protocol.WorldServerPacketType;

public final class TilePacket extends WorldServerPacket {

	public TilePacket(final TilePiece tilePiece) {
		out.write(tilePiece);
	}

	@Override
	protected PacketType getType() {
		return WorldServerPacketType.TILE;
	}

}
