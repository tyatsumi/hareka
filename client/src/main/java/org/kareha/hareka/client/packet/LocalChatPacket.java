package org.kareha.hareka.client.packet;

import org.kareha.hareka.client.protocol.WorldServerPacket;
import org.kareha.hareka.protocol.PacketType;
import org.kareha.hareka.protocol.WorldServerPacketType;

public final class LocalChatPacket extends WorldServerPacket {

	public LocalChatPacket(final String content) {
		out.writeString(content);
	}

	@Override
	protected PacketType getType() {
		return WorldServerPacketType.LOCAL_CHAT;
	}

}
