package org.kareha.hareka.client.mirror;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import org.kareha.hareka.annotation.GuardedBy;
import org.kareha.hareka.client.field.ClientField;
import org.kareha.hareka.field.Vector;

public class FieldMirror {

	public interface Listener {

		void fieldCreated(ClientField field);

		void effectAdded(String effectId, Vector origin, Vector target);

	}

	private final List<Listener> listeners = new CopyOnWriteArrayList<>();
	@GuardedBy("this")
	private ClientField field;

	public void addListener(final Listener listener) {
		listeners.add(listener);
	}

	public boolean removeListener(final Listener listener) {
		return listeners.remove(listener);
	}

	public Collection<Listener> getListeners() {
		return new ArrayList<>(listeners);
	}

	public void newField(final int viewSize) {
		final ClientField f = new ClientField(viewSize);
		synchronized (this) {
			field = f;
		}
		for (final Listener listener : listeners) {
			listener.fieldCreated(f);
		}
	}

	public void handleAddEffect(final String effectId, final Vector origin, final Vector target) {
		for (final Listener listener : listeners) {
			listener.effectAdded(effectId, origin, target);
		}
	}

	public synchronized ClientField getField() {
		return field;
	}

}
