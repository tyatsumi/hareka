package org.kareha.hareka.client.mirror;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.logging.Logger;

import org.kareha.hareka.annotation.GuardedBy;
import org.kareha.hareka.client.user.CustomRoleSet;
import org.kareha.hareka.client.user.RoleToken;
import org.kareha.hareka.user.Role;
import org.kareha.hareka.user.RoleSet;
import org.kareha.hareka.user.UserRegistrationMode;

public class AdminMirror {

	private static final Logger logger = Logger.getLogger(AdminMirror.class.getName());

	public interface Listener {

		void roleListLoaded(Collection<Role> roles);

		void myRolesLoaded(RoleSet roleSet);

		void roleTokenListLoaded(Collection<RoleToken> roleTokens);

		void commandOutAdded(String result);

		void settingsChanged(UserRegistrationMode userRegistrationMode, boolean rescueMethodsEnabled);

	}

	private final List<Listener> listeners = new CopyOnWriteArrayList<>();
	@GuardedBy("this")
	private final Map<String, Role> roles = new HashMap<>();
	@GuardedBy("this")
	private RoleSet roleSet;
	@GuardedBy("this")
	private final List<RoleToken> roleTokens = new ArrayList<>();

	public void addListener(final Listener listener) {
		listeners.add(listener);
	}

	public boolean removeListener(final Listener listener) {
		return listeners.remove(listener);
	}

	public Collection<Listener> getListeners() {
		return new ArrayList<>(listeners);
	}

	public void handleRoleList(final Collection<Role> roles) {
		synchronized (this) {
			this.roles.clear();
			for (final Role role : roles) {
				this.roles.put(role.getId(), role);
			}
		}
		for (final Listener listener : listeners) {
			listener.roleListLoaded(roles);
		}
	}

	public void handleMyRoles(final Collection<String> roleIds) {
		final RoleSet pubRoleSet;
		synchronized (this) {
			final Set<Role> set = new HashSet<>();
			for (final String roleId : roleIds) {
				final Role role = roles.get(roleId);
				if (role == null) {
					logger.warning("Role not found: roleId=" + roleId);
					continue;
				}
				set.add(role);
			}
			roleSet = new CustomRoleSet(set);

			pubRoleSet = new CustomRoleSet(roleSet);
		}
		for (final Listener listener : listeners) {
			listener.myRolesLoaded(pubRoleSet);
		}
	}

	public void handleRoleTokenList(final Collection<RoleToken> roleTokens) {
		final List<RoleToken> pubRoleTokens;
		synchronized (this) {
			this.roleTokens.clear();
			this.roleTokens.addAll(roleTokens);

			pubRoleTokens = new ArrayList<>(roleTokens);
		}
		for (final Listener listener : listeners) {
			listener.roleTokenListLoaded(pubRoleTokens);
		}
	}

	public void handleCommandOut(final String result) {
		for (final Listener listener : listeners) {
			listener.commandOutAdded(result);
		}
	}

	public synchronized Collection<Role> getRoles() {
		return new ArrayList<>(roles.values());
	}

	public synchronized RoleSet getRoleSet() {
		if (roleSet == null) {
			return RoleSet.EMPTY;
		}
		return new CustomRoleSet(roleSet);
	}

	public synchronized Collection<RoleToken> getRoleTokens() {
		return new ArrayList<>(roleTokens);
	}

	public void handleSettings(final UserRegistrationMode userRegistrationMode, final boolean rescueMethodsEnabled) {
		for (final Listener listener : listeners) {
			listener.settingsChanged(userRegistrationMode, rescueMethodsEnabled);
		}

	}

}
