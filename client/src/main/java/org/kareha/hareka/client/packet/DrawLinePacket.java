package org.kareha.hareka.client.packet;

import org.kareha.hareka.client.protocol.WorldServerPacket;
import org.kareha.hareka.field.TilePattern;
import org.kareha.hareka.field.Vector;
import org.kareha.hareka.protocol.PacketType;
import org.kareha.hareka.protocol.WorldServerPacketType;

public final class DrawLinePacket extends WorldServerPacket {

	public DrawLinePacket(final Vector a, final Vector b, final TilePattern tilePattern, final boolean force) {
		out.write(a);
		out.write(b);
		out.write(tilePattern);
		out.writeBoolean(force);
	}

	@Override
	protected PacketType getType() {
		return WorldServerPacketType.DRAW_LINE;
	}

}
