package org.kareha.hareka.client.handler;

import java.io.IOException;
import java.util.logging.Logger;

import org.kareha.hareka.LocalEntityId;
import org.kareha.hareka.client.WorldSession;
import org.kareha.hareka.client.field.ClientField;
import org.kareha.hareka.client.field.FieldEntity;
import org.kareha.hareka.client.field.StatBar;
import org.kareha.hareka.field.Placement;
import org.kareha.hareka.protocol.HandlerException;
import org.kareha.hareka.protocol.ProtocolInput;
import org.kareha.hareka.protocol.Handler;
import org.kareha.hareka.protocol.ProtocolException;

public class AddFieldEntityHandler implements Handler<WorldSession> {

	private static final Logger logger = Logger.getLogger(AddFieldEntityHandler.class.getName());

	@Override
	public void handle(final ProtocolInput in, final WorldSession session)
			throws IOException, ProtocolException, HandlerException {
		final LocalEntityId id = LocalEntityId.readFrom(in);
		final Placement placement = Placement.readFrom(in);
		final String shape = in.readString();
		final boolean healthAlive = in.readBoolean();
		final int healthBar = in.readByte();
		final boolean magicAlive = in.readBoolean();
		final int magicBar = in.readByte();
		final long count = in.readCompactULong();

		final ClientField field = session.getMirrors().getFieldMirror().getField();
		final FieldEntity entity = new FieldEntity(id, field, placement, shape, count);
		entity.setHealthBar(new StatBar(healthAlive, healthBar));
		entity.setMagicBar(new StatBar(magicAlive, magicBar));
		if (!session.getMirrors().getEntityMirror().addFieldEntity(entity)) {
			logger.warning("An already existent field entity has been added: " + id);
		}
	}

}
