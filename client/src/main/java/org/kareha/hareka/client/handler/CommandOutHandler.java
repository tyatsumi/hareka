package org.kareha.hareka.client.handler;

import java.io.IOException;

import org.kareha.hareka.client.WorldSession;
import org.kareha.hareka.protocol.HandlerException;
import org.kareha.hareka.protocol.ProtocolInput;
import org.kareha.hareka.protocol.Handler;
import org.kareha.hareka.protocol.ProtocolException;

public class CommandOutHandler implements Handler<WorldSession> {

	private static final String lineSeparator = System.getProperty("line.separator");

	@Override
	public void handle(final ProtocolInput in, final WorldSession session)
			throws IOException, ProtocolException, HandlerException {
		final String result = in.readString();

		final String localResult = result.replaceAll("\n", lineSeparator);
		session.getMirrors().getAdminMirror().handleCommandOut(localResult);
	}

}
