package org.kareha.hareka.client.packet;

import org.kareha.hareka.client.DiceRoll;
import org.kareha.hareka.client.protocol.WorldServerPacket;
import org.kareha.hareka.protocol.PacketType;
import org.kareha.hareka.protocol.WorldServerPacketType;

public final class DiceTokenPacket extends WorldServerPacket {

	public DiceTokenPacket(final DiceRoll diceRoll) {
		out.writeCompactUInt(diceRoll.getId());
		out.writeByteArray(diceRoll.getToken());
	}

	@Override
	protected PacketType getType() {
		return WorldServerPacketType.DICE_TOKEN;
	}

}
