package org.kareha.hareka.client.packet;

import org.kareha.hareka.client.protocol.WorldServerPacket;
import org.kareha.hareka.protocol.PacketType;
import org.kareha.hareka.protocol.WorldServerPacketType;
import org.kareha.hareka.user.UserRegistrationMode;

public final class SetUserRegistrationModePacket extends WorldServerPacket {

	public SetUserRegistrationModePacket(final UserRegistrationMode mode) {
		out.write(mode);
	}

	@Override
	protected PacketType getType() {
		return WorldServerPacketType.SET_USER_REGISTRATION_MODE;
	}

}
