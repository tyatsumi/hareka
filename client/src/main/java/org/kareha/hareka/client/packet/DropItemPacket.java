package org.kareha.hareka.client.packet;

import org.kareha.hareka.LocalEntityId;
import org.kareha.hareka.client.protocol.WorldServerPacket;
import org.kareha.hareka.protocol.PacketType;
import org.kareha.hareka.protocol.WorldServerPacketType;

public final class DropItemPacket extends WorldServerPacket {

	public DropItemPacket(final LocalEntityId itemId, final long count) {
		out.write(itemId);
		out.writeCompactULong(count);
	}

	@Override
	protected PacketType getType() {
		return WorldServerPacketType.DROP_ITEM;
	}

}
