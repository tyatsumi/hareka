package org.kareha.hareka.client.control;

import java.util.Collection;

import org.kareha.hareka.LocalEntityId;
import org.kareha.hareka.client.ConnectionSettings;
import org.kareha.hareka.client.ResponseHandler;
import org.kareha.hareka.client.WorldSession;
import org.kareha.hareka.client.chat.ChatEntity;
import org.kareha.hareka.client.field.FieldEntity;
import org.kareha.hareka.client.field.PositionMemory;
import org.kareha.hareka.client.packet.CancelTokenPacket;
import org.kareha.hareka.client.packet.DeleteCharacterPacket;
import org.kareha.hareka.client.packet.LoginCharacterPacket;
import org.kareha.hareka.client.packet.LoginUserPacket;
import org.kareha.hareka.client.packet.LogoutCharacterPacket;
import org.kareha.hareka.client.packet.LogoutUserPacket;
import org.kareha.hareka.client.packet.NewCharacterPacket;
import org.kareha.hareka.client.packet.TokenPacket;
import org.kareha.hareka.client.server.Server;
import org.kareha.hareka.client.server.ServerId;

public class UserControl {

	private final WorldSession session;

	public UserControl(final WorldSession session) {
		this.session = session;
	}

	// Connection

	public ConnectionSettings.Entry getConnectionSettingsEntry() {
		return session.getConnection();
	}

	public boolean peerKeyExists() {
		return session.getPeerKey() != null;
	}

	public ServerId getServerId() {
		final Server server = session.getServer();
		if (server == null) {
			return null;
		}
		return server.getId();
	}

	// Token request

	public void rejectTokenRequest(final int handlerId) {
		session.write(new CancelTokenPacket(handlerId));
	}

	public void sendToken(final int handlerId, final byte[] token) {
		session.write(new TokenPacket(handlerId, token));
	}

	// User management

	public void loginUser(final ResponseHandler handler) {
		session.write(new LoginUserPacket(), handler);
	}

	public void logoutUser(final ResponseHandler handler) {
		session.write(new LogoutUserPacket(), handler);
	}

	// Character management

	public void newCharacter(final ResponseHandler handler) {
		session.write(new NewCharacterPacket(), handler);
	}

	public void deleteCharacter(final LocalEntityId id, final ResponseHandler handler) {
		session.write(new DeleteCharacterPacket(id), handler);
	}

	public void loginCharacter(final LocalEntityId id, final ResponseHandler handler) {
		session.write(new LoginCharacterPacket(id), handler);
	}

	public void logoutCharacter(final ResponseHandler handler) {
		session.write(new LogoutCharacterPacket(), handler);
	}

	// Identity

	public ChatEntity getChatEntity(final FieldEntity fieldEntity) {
		return session.getServer().getChatEntity(fieldEntity);
	}

	public FieldEntity getFieldEntity(final ChatEntity chatEntity) {
		return session.getServer().getFieldEntity(chatEntity);
	}

	// Nickname

	public String getNickname(final LocalEntityId id) {
		return session.getServer().getFieldNickname(id);
	}

	public void addNickname(final LocalEntityId id, final String nickname) {
		session.getServer().addFieldNickname(id, nickname);
	}

	public void removeNickname(final LocalEntityId id) {
		session.getServer().removeFieldNickname(id);
	}

	// Position memory

	public Collection<PositionMemory> getPositionMemoryList() {
		return session.getServer().getPositionMemories();
	}

	public void addPositionMemory(final int index, final PositionMemory memory) {
		session.getServer().addPositionMemory(index, memory);
	}

	public PositionMemory removePositionMemory(final int index) {
		return session.getServer().removePositionMemory(index);
	}

}
