package org.kareha.hareka.client.packet;

import org.kareha.hareka.client.protocol.WorldServerPacket;
import org.kareha.hareka.field.TilePattern;
import org.kareha.hareka.field.Vector;
import org.kareha.hareka.protocol.PacketType;
import org.kareha.hareka.protocol.WorldServerPacketType;

public final class DrawRingPacket extends WorldServerPacket {

	public DrawRingPacket(final Vector center, final int size, final TilePattern tilePattern, final boolean force) {
		out.write(center);
		out.writeCompactUInt(size);
		out.write(tilePattern);
		out.writeBoolean(force);
	}

	@Override
	protected PacketType getType() {
		return WorldServerPacketType.DRAW_RING;
	}

}
