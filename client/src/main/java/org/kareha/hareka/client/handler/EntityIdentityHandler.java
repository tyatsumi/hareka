package org.kareha.hareka.client.handler;

import java.io.IOException;

import org.kareha.hareka.LocalEntityId;
import org.kareha.hareka.client.WorldSession;
import org.kareha.hareka.protocol.HandlerException;
import org.kareha.hareka.protocol.ProtocolInput;
import org.kareha.hareka.protocol.Handler;
import org.kareha.hareka.protocol.ProtocolException;

public class EntityIdentityHandler implements Handler<WorldSession> {

	@Override
	public void handle(final ProtocolInput in, final WorldSession session)
			throws IOException, ProtocolException, HandlerException {
		final LocalEntityId chatEntityId = LocalEntityId.readFrom(in);
		final LocalEntityId fieldEntityId = LocalEntityId.readFrom(in);

		session.getServer().addEntityIdentity(chatEntityId, fieldEntityId);
	}

}
