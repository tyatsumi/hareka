package org.kareha.hareka.client.packet;

import org.kareha.hareka.client.protocol.WorldServerPacket;
import org.kareha.hareka.protocol.PacketType;
import org.kareha.hareka.protocol.WorldServerPacketType;

public final class ChangeNamePacket extends WorldServerPacket {

	public ChangeNamePacket(final String language, final String translation) {
		out.writeString(language);
		out.writeString(translation);
	}

	@Override
	protected PacketType getType() {
		return WorldServerPacketType.CHANGE_NAME;
	}

}
