package org.kareha.hareka.client.handler;

import java.io.IOException;

import org.kareha.hareka.client.WorldSession;
import org.kareha.hareka.client.field.FieldEntity;
import org.kareha.hareka.client.field.PositionMemory;
import org.kareha.hareka.protocol.HandlerException;
import org.kareha.hareka.protocol.ProtocolInput;
import org.kareha.hareka.protocol.Handler;
import org.kareha.hareka.protocol.ProtocolException;

public class PositionMemoryHandler implements Handler<WorldSession> {

	@Override
	public void handle(final ProtocolInput in, final WorldSession session)
			throws IOException, ProtocolException, HandlerException {
		final byte[] data = in.readByteArray();

		final FieldEntity entity = session.getMirrors().getSelfMirror().getFieldEntity();
		if (entity == null) {
			return;
		}
		final PositionMemory positionMemory = new PositionMemory(data, entity.getLocalId());
		session.getServer().addPositionMemory(positionMemory);
		session.getMirrors().getSystemMirror().handlePositionMemoryAdded(positionMemory);
	}

}
