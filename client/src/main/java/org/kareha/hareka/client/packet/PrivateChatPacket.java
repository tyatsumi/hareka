package org.kareha.hareka.client.packet;

import org.kareha.hareka.client.chat.ChatEntity;
import org.kareha.hareka.client.protocol.WorldServerPacket;
import org.kareha.hareka.protocol.PacketType;
import org.kareha.hareka.protocol.WorldServerPacketType;

public final class PrivateChatPacket extends WorldServerPacket {

	public PrivateChatPacket(final ChatEntity peer, final String content) {
		out.write(peer.getLocalId());
		out.writeString(content);
	}

	@Override
	protected PacketType getType() {
		return WorldServerPacketType.PRIVATE_CHAT;
	}

}
