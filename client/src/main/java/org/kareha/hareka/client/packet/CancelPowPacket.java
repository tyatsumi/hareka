package org.kareha.hareka.client.packet;

import org.kareha.hareka.client.protocol.WorldServerPacket;
import org.kareha.hareka.protocol.PacketType;
import org.kareha.hareka.protocol.WorldServerPacketType;

public final class CancelPowPacket extends WorldServerPacket {

	public CancelPowPacket(final int handlerId) {
		out.writeCompactUInt(handlerId);
	}

	@Override
	protected PacketType getType() {
		return WorldServerPacketType.CANCEL_POW;
	}

}
