package org.kareha.hareka.client.handler;

import java.io.IOException;
import java.util.logging.Logger;

import org.kareha.hareka.LocalEntityId;
import org.kareha.hareka.client.WorldSession;
import org.kareha.hareka.client.field.FieldEntity;
import org.kareha.hareka.field.Placement;
import org.kareha.hareka.protocol.HandlerException;
import org.kareha.hareka.protocol.ProtocolInput;
import org.kareha.hareka.protocol.Handler;
import org.kareha.hareka.protocol.ProtocolException;

public class PlaceFieldEntityHandler implements Handler<WorldSession> {

	private static final Logger logger = Logger.getLogger(PlaceFieldEntityHandler.class.getName());

	@Override
	public void handle(final ProtocolInput in, final WorldSession session)
			throws IOException, ProtocolException, HandlerException {
		final LocalEntityId id = LocalEntityId.readFrom(in);
		final Placement placement = Placement.readFrom(in);
		final int motionWait = in.readCompactUInt();

		final FieldEntity entity = session.getMirrors().getEntityMirror().getFieldEntity(id);
		if (entity == null) {
			logger.warning("A nonexistent field entity placed: id=" + id + " placement=" + placement + " motionWait="
					+ motionWait);
			return;
		}
		entity.place(placement, motionWait);
	}

}
