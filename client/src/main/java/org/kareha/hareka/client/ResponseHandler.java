package org.kareha.hareka.client;

public interface ResponseHandler {

	void rejected(String message);

	void accepted(String message);

}
