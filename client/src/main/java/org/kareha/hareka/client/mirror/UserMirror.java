package org.kareha.hareka.client.mirror;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import org.kareha.hareka.LocalEntityId;
import org.kareha.hareka.annotation.GuardedBy;
import org.kareha.hareka.client.WorldSession;

public class UserMirror {

	public interface Listener {

		void charactersLoaded(WorldSession session, Collection<CharacterEntry> characterEntries);

	}

	private final List<Listener> listeners = new CopyOnWriteArrayList<>();
	@GuardedBy("this")
	private final List<CharacterEntry> characterEntries = new ArrayList<>();

	public void addListener(final Listener listener) {
		listeners.add(listener);
	}

	public boolean removeListener(final Listener listener) {
		return listeners.remove(listener);
	}

	public Collection<Listener> getListeners() {
		return new ArrayList<>(listeners);
	}

	public void handleLoadCharacters(final WorldSession session, final Collection<CharacterEntry> characterEntries) {
		synchronized (this) {
			this.characterEntries.clear();
			this.characterEntries.addAll(characterEntries);
		}
		for (final Listener listener : listeners) {
			listener.charactersLoaded(session, characterEntries);
		}
	}

	public static class CharacterEntry {

		private final LocalEntityId id;
		private final String name;
		private final String shape;

		public CharacterEntry(final LocalEntityId id, final String name, final String shape) {
			this.id = id;
			this.name = name;
			this.shape = shape;
		}

		@Override
		public String toString() {
			return name;
		}

		public LocalEntityId getId() {
			return id;
		}

		public String getName() {
			return name;
		}

		public String getShape() {
			return shape;
		}

	}

}
