package org.kareha.hareka.client.field;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.kareha.hareka.LocalEntityId;
import org.kareha.hareka.annotation.GuardedBy;
import org.kareha.hareka.annotation.Private;
import org.kareha.hareka.annotation.ThreadSafe;

@ThreadSafe
@XmlJavaTypeAdapter(PositionMemory.Adapter.class)
public class PositionMemory {

	@Private
	final byte[] data;
	@Private
	final LocalEntityId entityId;
	@Private
	final long date;
	@GuardedBy("this")
	@Private
	String name;

	public PositionMemory(final byte[] data, final LocalEntityId entityId) {
		this(data, entityId, System.currentTimeMillis(), "");
	}

	@Private
	PositionMemory(final byte[] data, final LocalEntityId entityId, final long date, final String name) {
		this.data = data.clone();
		this.entityId = entityId;
		this.date = date;
		this.name = name;
	}

	@Override
	public synchronized String toString() {
		if (name == null) {
			return "(null)";
		} else if (name.isEmpty()) {
			return "(empty)";
		} else {
			return name;
		}
	}

	@XmlType(name = "positionMemory")
	@XmlAccessorType(XmlAccessType.NONE)
	private static class Adapted {

		@XmlElement
		private byte[] data;
		@XmlElement
		private LocalEntityId entityId;
		@XmlElement
		private long date;
		@XmlElement
		private String name;

		@SuppressWarnings("unused")
		private Adapted() {
			// used by JAXB
		}

		@Private
		Adapted(final PositionMemory v) {
			data = v.data.clone();
			entityId = v.entityId;
			date = v.date;
			synchronized (this) {
				name = v.name;
			}
		}

		@Private
		PositionMemory unmarshal() {
			return new PositionMemory(data, entityId, date, name);
		}

	}

	@Private
	Adapted marshal() {
		return new Adapted(this);
	}

	static class Adapter extends XmlAdapter<Adapted, PositionMemory> {

		@Override
		public Adapted marshal(final PositionMemory v) throws Exception {
			if (v == null) {
				return null;
			}
			return v.marshal();
		}

		@Override
		public PositionMemory unmarshal(final Adapted v) throws Exception {
			if (v == null) {
				return null;
			}
			return v.unmarshal();
		}

	}

	public byte[] getData() {
		return data.clone();
	}

	public LocalEntityId getEntityId() {
		return entityId;
	}

	public long getDate() {
		return date;
	}

	public synchronized String getName() {
		return name;
	}

	public synchronized void setName(final String name) {
		this.name = name;
	}

}
