package org.kareha.hareka.client;

import java.net.MalformedURLException;
import java.net.URL;

public final class Officials {

	private Officials() {
		throw new AssertionError();
	}

	public static final String SERVER_NAME = "Kareha's Lair";
	public static final String SERVER_HOST = "hareka.kareha.org";
	public static final int SERVER_PORT = 2468;
	public static final URL SITE;
	public static final URL SOURCE_CODE;
	public static final URL DOCUMENTS;

	static {
		try {
			SITE = new URL("https://hareka.kareha.org/");
			SOURCE_CODE = new URL("https://gitlab.com/tyatsumi/hareka");
			DOCUMENTS = new URL("https://tyatsumi.gitlab.io/hareka/");
		} catch (final MalformedURLException e) {
			throw new AssertionError(e);
		}
	}
}
