package org.kareha.hareka.client.packet;

import org.kareha.hareka.LocalEntityId;
import org.kareha.hareka.client.protocol.WorldServerRequestPacket;
import org.kareha.hareka.protocol.PacketType;
import org.kareha.hareka.protocol.WorldServerPacketType;

public final class DeleteCharacterPacket extends WorldServerRequestPacket {

	public DeleteCharacterPacket(final LocalEntityId localId) {
		body.write(localId);
	}

	@Override
	protected PacketType getType() {
		return WorldServerPacketType.DELETE_CHARACTER;
	}

}
