package org.kareha.hareka.client.server;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.kareha.hareka.LocalEntityId;
import org.kareha.hareka.annotation.GuardedBy;
import org.kareha.hareka.annotation.Private;
import org.kareha.hareka.annotation.ThreadSafe;

@ThreadSafe
@XmlJavaTypeAdapter(ChatEntityEntry.Adapter.class)
public class ChatEntityEntry {

	@Private
	final LocalEntityId id;
	@GuardedBy("this")
	@Private
	List<Note> names;
	@GuardedBy("this")
	@Private
	List<Note> nicknames;

	public ChatEntityEntry(final LocalEntityId id) {
		this.id = id;
	}

	@Private
	ChatEntityEntry(final LocalEntityId id, final List<Note> names, final List<Note> nicknames) {
		this.id = id;
		if (names != null) {
			this.names = new ArrayList<>(names);
		}
		if (nicknames != null) {
			this.nicknames = new ArrayList<>(nicknames);
		}
	}

	@XmlType(name = "chatEntityEntry")
	@XmlAccessorType(XmlAccessType.NONE)
	private static class Adapted {

		@XmlAttribute
		private LocalEntityId id;
		@XmlElement(name = "name")
		private List<Note> names;
		@XmlElement(name = "nickname")
		private List<Note> nicknames;

		@SuppressWarnings("unused")
		private Adapted() {
			// used by JAXB
		}

		@Private
		Adapted(final ChatEntityEntry v) {
			id = v.id;
			synchronized (v) {
				if (v.names != null && !v.names.isEmpty()) {
					names = new ArrayList<>(v.names);
				}
				if (v.nicknames != null && !v.nicknames.isEmpty()) {
					nicknames = new ArrayList<>(v.nicknames);
				}
			}
		}

		@Private
		ChatEntityEntry unmarshal() {
			return new ChatEntityEntry(id, names, nicknames);
		}

	}

	@Private
	Adapted marshal() {
		return new Adapted(this);
	}

	static class Adapter extends XmlAdapter<Adapted, ChatEntityEntry> {

		@Override
		public Adapted marshal(final ChatEntityEntry v) throws Exception {
			if (v == null) {
				return null;
			}
			return v.marshal();
		}

		@Override
		public ChatEntityEntry unmarshal(final Adapted v) throws Exception {
			if (v == null) {
				return null;
			}
			return v.unmarshal();
		}

	}

	public LocalEntityId getId() {
		return id;
	}

	public synchronized boolean isEmpty() {
		return (names == null || names.isEmpty()) && (nicknames == null || nicknames.isEmpty());
	}

	public synchronized String getName() {
		if (names == null || names.isEmpty()) {
			return null;
		}
		return names.get(names.size() - 1).getContent();
	}

	public synchronized void addName(final String name) {
		final String prev = getName();
		if (prev != null && prev.equals(name)) {
			return;
		}
		if (names == null) {
			names = new ArrayList<>();
		}
		names.add(new Note(name));
	}

	public synchronized String getNickname() {
		if (nicknames == null || nicknames.isEmpty()) {
			return null;
		}
		return nicknames.get(nicknames.size() - 1).getContent();
	}

	public synchronized void addNickname(final String nickname) {
		final String prev = getNickname();
		if (prev != null && prev.equals(nickname)) {
			return;
		}
		if (nicknames == null) {
			nicknames = new ArrayList<>();
		}
		nicknames.add(new Note(nickname));
	}

	public synchronized void clearNicknames() {
		nicknames = null;
	}

}
