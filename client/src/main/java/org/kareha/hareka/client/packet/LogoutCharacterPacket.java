package org.kareha.hareka.client.packet;

import org.kareha.hareka.client.protocol.WorldServerRequestPacket;
import org.kareha.hareka.protocol.PacketType;
import org.kareha.hareka.protocol.WorldServerPacketType;

public final class LogoutCharacterPacket extends WorldServerRequestPacket {

	public LogoutCharacterPacket() {
		// do nothing
	}

	@Override
	protected PacketType getType() {
		return WorldServerPacketType.LOGOUT_CHARACTER;
	}

}
