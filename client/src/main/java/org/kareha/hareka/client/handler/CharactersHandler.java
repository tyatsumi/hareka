package org.kareha.hareka.client.handler;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.kareha.hareka.LocalEntityId;
import org.kareha.hareka.client.WorldSession;
import org.kareha.hareka.client.mirror.UserMirror;
import org.kareha.hareka.protocol.HandlerException;
import org.kareha.hareka.protocol.ProtocolInput;
import org.kareha.hareka.protocol.Handler;
import org.kareha.hareka.protocol.ProtocolException;

public class CharactersHandler implements Handler<WorldSession> {

	@Override
	public void handle(final ProtocolInput in, final WorldSession session)
			throws IOException, ProtocolException, HandlerException {
		final int count = in.readCompactUInt();
		final List<UserMirror.CharacterEntry> entries = new ArrayList<>();
		for (int i = 0; i < count; i++) {
			final LocalEntityId id = LocalEntityId.readFrom(in);
			final String name = in.readString();
			final String shape = in.readString();
			entries.add(new UserMirror.CharacterEntry(id, name, shape));
		}

		session.getMirrors().getUserMirror().handleLoadCharacters(session, entries);
	}

}
