package org.kareha.hareka.client.user;

import java.io.IOException;
import java.util.Collection;
import java.util.EnumSet;
import java.util.Set;

import org.kareha.hareka.protocol.ProtocolInput;
import org.kareha.hareka.protocol.ProtocolException;
import org.kareha.hareka.user.AbstractRole;
import org.kareha.hareka.user.Permission;

public class CustomRole extends AbstractRole {

	public CustomRole(final String id, final int rank, final Collection<Permission> permissions) {
		super(id, rank, permissions);
	}

	public static CustomRole readFrom(final ProtocolInput in) throws IOException, ProtocolException {
		final String id = in.readString();
		final int rank = in.readCompactUInt();
		final int size = in.readCompactUInt();
		final Set<Permission> permissions = EnumSet.noneOf(Permission.class);
		for (int i = 0; i < size; i++) {
			final Permission permission = Permission.readFrom(in);
			if (permission == null) {
				throw new RuntimeException("Permission not found");
			}
			permissions.add(permission);
		}
		return new CustomRole(id, rank, permissions);
	}

}
