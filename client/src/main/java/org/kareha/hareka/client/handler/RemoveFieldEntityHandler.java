package org.kareha.hareka.client.handler;

import java.io.IOException;
import java.util.logging.Logger;

import org.kareha.hareka.LocalEntityId;
import org.kareha.hareka.client.WorldSession;
import org.kareha.hareka.protocol.HandlerException;
import org.kareha.hareka.protocol.ProtocolInput;
import org.kareha.hareka.protocol.Handler;
import org.kareha.hareka.protocol.ProtocolException;

public class RemoveFieldEntityHandler implements Handler<WorldSession> {

	private static final Logger logger = Logger.getLogger(RemoveFieldEntityHandler.class.getName());

	@Override
	public void handle(final ProtocolInput in, final WorldSession session)
			throws IOException, ProtocolException, HandlerException {
		final LocalEntityId id = LocalEntityId.readFrom(in);

		if (!session.getMirrors().getEntityMirror().removeFieldEntity(id)) {
			logger.warning("A nonexistent field entity has been removed: " + id);
		}
	}

}
