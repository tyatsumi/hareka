package org.kareha.hareka.client.mirror;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

import org.kareha.hareka.LocalEntityId;
import org.kareha.hareka.annotation.GuardedBy;
import org.kareha.hareka.client.WorldSession;
import org.kareha.hareka.client.chat.ChatEntity;
import org.kareha.hareka.client.field.ClientField;
import org.kareha.hareka.client.field.FieldEntity;

public class EntityMirror {

	public interface Listener {

		void fieldEntityAdded(FieldEntity entity);

		void fieldEntityRemoved(FieldEntity entity);

	}

	private final WorldSession session;
	private final List<Listener> listeners = new CopyOnWriteArrayList<>();
	@GuardedBy("this")
	private final Map<LocalEntityId, ChatEntity> chatEntityMap = new HashMap<>();
	@GuardedBy("this")
	private final Map<LocalEntityId, FieldEntity> fieldEntityMap = new HashMap<>();

	public EntityMirror(final WorldSession session) {
		this.session = session;
	}

	public void addListener(final Listener listener) {
		listeners.add(listener);
	}

	public boolean removeListener(final Listener listener) {
		return listeners.remove(listener);
	}

	public Collection<Listener> getListeners() {
		return new ArrayList<>(listeners);
	}

	public synchronized ChatEntity loadChatEntity(final LocalEntityId id, final String name) {
		final ChatEntity entity = chatEntityMap.get(id);
		if (entity != null) {
			entity.setName(name);
			return entity;
		}
		final ChatEntity newEntity = new ChatEntity(id, name);
		chatEntityMap.put(id, newEntity);
		return newEntity;
	}

	public synchronized ChatEntity updateChatEntity(final LocalEntityId id, final String name) {
		final ChatEntity entity = loadChatEntity(id, name);
		session.getServer().addChatName(id, name);
		return entity;
	}

	public synchronized ChatEntity getChatEntity(final LocalEntityId id) {
		return chatEntityMap.get(id);
	}

	public boolean addFieldEntity(final FieldEntity entity) {
		final FieldEntity prev;
		synchronized (this) {
			prev = fieldEntityMap.put(entity.getLocalId(), entity);
		}
		final ClientField field = session.getMirrors().getFieldMirror().getField();
		if (field != null) {
			field.addEntity(entity);
		}
		for (final Listener listener : listeners) {
			listener.fieldEntityAdded(entity);
		}
		return prev == null;
	}

	public boolean removeFieldEntity(final LocalEntityId localId) {
		final FieldEntity removed;
		synchronized (this) {
			removed = fieldEntityMap.remove(localId);
		}
		if (removed != null) {
			final ClientField field = session.getMirrors().getFieldMirror().getField();
			if (field != null) {
				field.removeEntity(removed);
			}
			for (final Listener listener : listeners) {
				listener.fieldEntityRemoved(removed);
			}
		}
		return removed != null;
	}

	public synchronized FieldEntity getFieldEntity(final LocalEntityId localId) {
		return fieldEntityMap.get(localId);
	}

	public synchronized Collection<FieldEntity> getFieldEntities() {
		return new ArrayList<>(fieldEntityMap.values());
	}

}
