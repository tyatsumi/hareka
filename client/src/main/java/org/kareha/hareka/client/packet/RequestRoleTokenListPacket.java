package org.kareha.hareka.client.packet;

import org.kareha.hareka.client.protocol.WorldServerPacket;
import org.kareha.hareka.protocol.PacketType;
import org.kareha.hareka.protocol.WorldServerPacketType;

public final class RequestRoleTokenListPacket extends WorldServerPacket {

	public RequestRoleTokenListPacket() {
		// do nothing
	}

	@Override
	protected PacketType getType() {
		return WorldServerPacketType.REQUEST_ROLE_TOKEN_LIST;
	}

}
