package org.kareha.hareka.client.handler;

import java.io.IOException;
import java.util.logging.Logger;

import org.kareha.hareka.client.DiceRoll;
import org.kareha.hareka.client.WorldSession;
import org.kareha.hareka.client.packet.DiceTokenPacket;
import org.kareha.hareka.protocol.HandlerException;
import org.kareha.hareka.protocol.ProtocolInput;
import org.kareha.hareka.protocol.Handler;
import org.kareha.hareka.protocol.ProtocolException;

public class RequestDiceTokenHandler implements Handler<WorldSession> {

	private static final Logger logger = Logger.getLogger(RequestDiceTokenHandler.class.getName());

	@Override
	public void handle(final ProtocolInput in, final WorldSession session)
			throws IOException, ProtocolException, HandlerException {
		final int id = in.readCompactUInt();

		final DiceRoll diceRoll = session.getDiceRollTable().accept(id);
		if (diceRoll == null) {
			// TODO report error
			logger.warning("A nonexistent dice roll: id=" + id);
			return;
		}
		session.write(new DiceTokenPacket(diceRoll));
	}

}
