package org.kareha.hareka.client.packet;

import org.kareha.hareka.client.protocol.WorldServerPacket;
import org.kareha.hareka.protocol.PacketType;
import org.kareha.hareka.protocol.WorldServerPacketType;

public final class TeleportToPositionMemoryPacket extends WorldServerPacket {

	public TeleportToPositionMemoryPacket(final byte[] positionMemory) {
		out.writeByteArray(positionMemory);
	}

	@Override
	protected PacketType getType() {
		return WorldServerPacketType.TELEPORT_TO_POSITION_MEMORY;
	}

}
