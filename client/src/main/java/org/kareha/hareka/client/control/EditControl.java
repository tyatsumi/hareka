package org.kareha.hareka.client.control;

import java.util.Collection;

import org.kareha.hareka.client.WorldSession;
import org.kareha.hareka.client.packet.AddDownstairsPacket;
import org.kareha.hareka.client.packet.AddGatePairPacket;
import org.kareha.hareka.client.packet.AddUpstairsPacket;
import org.kareha.hareka.client.packet.DrawLinePacket;
import org.kareha.hareka.client.packet.DrawRingPacket;
import org.kareha.hareka.client.packet.FillRangePacket;
import org.kareha.hareka.client.packet.NewFieldPacket;
import org.kareha.hareka.client.packet.ReduceToBackgroundTilesPacket;
import org.kareha.hareka.client.packet.RemoveOrphansPacket;
import org.kareha.hareka.client.packet.RemoveSpecialTilePacket;
import org.kareha.hareka.client.packet.RequestRegionListPacket;
import org.kareha.hareka.client.packet.SetDefaultTilePatternPacket;
import org.kareha.hareka.client.packet.SetMarkPacket;
import org.kareha.hareka.client.packet.SetRegionListPacket;
import org.kareha.hareka.client.packet.TilePacket;
import org.kareha.hareka.field.Region;
import org.kareha.hareka.field.TilePattern;
import org.kareha.hareka.field.TilePiece;
import org.kareha.hareka.field.Vector;

public class EditControl {

	private final WorldSession session;

	public EditControl(final WorldSession session) {
		this.session = session;
	}

	// Default

	public void setDefaultTilePattern(final TilePattern tilePattern) {
		session.write(new SetDefaultTilePatternPacket(tilePattern));
	}

	// Edit individual tiles

	public void setTile(final TilePiece tilePiece) {
		session.write(new TilePacket(tilePiece));
	}

	// Mark

	public void setMark() {
		session.write(new SetMarkPacket());
	}

	// Add or remove special tiles

	public void addDownstairs() {
		session.write(new AddDownstairsPacket());
	}

	public void addUpstairs() {
		session.write(new AddUpstairsPacket());
	}

	public void addGatePair() {
		session.write(new AddGatePairPacket());
	}

	public void removeSpecialTile() {
		session.write(new RemoveSpecialTilePacket());
	}

	// New field

	public void newField(final int size) {
		session.write(new NewFieldPacket(size));
	}

	// Region

	public void requestRegionList() {
		session.write(new RequestRegionListPacket());
	}

	public void setRegionList(final Collection<Region> regions) {
		session.write(new SetRegionListPacket(regions));
	}

	// Draw

	public void drawLine(final Vector a, final Vector b, final TilePattern tilePattern, final boolean force) {
		session.write(new DrawLinePacket(a, b, tilePattern, force));
	}

	public void drawRing(final Vector center, final int size, final TilePattern tilePattern, final boolean force) {
		session.write(new DrawRingPacket(center, size, tilePattern, force));
	}

	public void fillRange(final Vector center, final int size, final TilePattern tilePattern, final boolean force) {
		session.write(new FillRangePacket(center, size, tilePattern, force));
	}

	// Maintain

	public void reduceToBackgroundTiles() {
		session.write(new ReduceToBackgroundTilesPacket());
	}

	// XXX
	public void removeOrphans() {
		session.write(new RemoveOrphansPacket());
	}

}
