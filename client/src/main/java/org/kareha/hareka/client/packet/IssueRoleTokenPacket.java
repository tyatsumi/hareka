package org.kareha.hareka.client.packet;

import org.kareha.hareka.client.protocol.WorldServerPacket;
import org.kareha.hareka.protocol.PacketType;
import org.kareha.hareka.protocol.WorldServerPacketType;

public final class IssueRoleTokenPacket extends WorldServerPacket {

	public IssueRoleTokenPacket(final String roleId) {
		out.writeString(roleId);
	}

	@Override
	protected PacketType getType() {
		return WorldServerPacketType.ISSUE_ROLE_TOKEN;
	}

}
