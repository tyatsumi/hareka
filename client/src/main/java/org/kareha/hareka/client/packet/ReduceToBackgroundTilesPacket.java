package org.kareha.hareka.client.packet;

import org.kareha.hareka.client.protocol.WorldServerPacket;
import org.kareha.hareka.protocol.PacketType;
import org.kareha.hareka.protocol.WorldServerPacketType;

public final class ReduceToBackgroundTilesPacket extends WorldServerPacket {

	public ReduceToBackgroundTilesPacket() {

	}

	@Override
	protected PacketType getType() {
		return WorldServerPacketType.REDUCE_TO_BACKGROUND_TILES;
	}

}
