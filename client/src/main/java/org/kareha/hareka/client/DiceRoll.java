package org.kareha.hareka.client;

import java.nio.ByteBuffer;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import org.kareha.hareka.annotation.GuardedBy;

public class DiceRoll {

	private final int id;
	private final Random random;
	@GuardedBy("this")
	private byte[] token;
	@GuardedBy("this")
	private String hashAlgorithm;
	@GuardedBy("this")
	private byte[] hash;
	@GuardedBy("this")
	private int faces;
	@GuardedBy("this")
	private int rate;
	@GuardedBy("this")
	private int[] choice;
	@GuardedBy("this")
	private int value;
	@GuardedBy("this")
	private byte[] secret;

	public DiceRoll(final int id, final Random random) {
		this.id = id;
		this.random = random;
	}

	public int getId() {
		return id;
	}

	public synchronized byte[] getToken() {
		if (token == null) {
			token = new byte[32];
			random.nextBytes(token);
		}
		return token.clone();
	}

	public synchronized void setHash(final String algorithm, final byte[] hash) {
		hashAlgorithm = algorithm;
		this.hash = hash.clone();
	}

	public synchronized void setFaces(final int faces) {
		this.faces = faces;
	}

	public synchronized void setRate(final int rate) {
		this.rate = rate;
	}

	private int[] generateChoice() {
		// XXX use shuffle
		final List<Integer> list = new ArrayList<>();
		for (int i = 0; i < faces; i++) {
			list.add(i);
		}
		final int[] result = new int[rate];
		for (int i = 0; i < rate; i++) {
			final int index = random.nextInt(list.size());
			result[i] = list.remove(index);
		}
		return result;
	}

	public synchronized int[] getChoice() {
		if (choice == null) {
			choice = generateChoice();
		}
		return choice.clone();
	}

	public synchronized void setValue(final int value) {
		this.value = value;
	}

	public synchronized void setSecret(final byte[] secret) {
		this.secret = secret.clone();
	}

	@GuardedBy("this")
	private byte[] generateHash() {
		final MessageDigest md;
		try {
			md = MessageDigest.getInstance(hashAlgorithm);
		} catch (final NoSuchAlgorithmException e) {
			// TODO report error/cheat
			return null;
		}
		final byte[] hash;
		synchronized (md) {
			md.reset();
			md.update(intToByteArray(value));
			md.update(secret);
			hash = md.digest(token);
		}
		return hash;
	}

	private byte[] intToByteArray(final int v) {
		final ByteBuffer b = ByteBuffer.allocate(4);
		b.putInt(v);
		return b.array();
	}

	public synchronized boolean checkHash() {
		final byte[] hash = generateHash();
		if (hash == null) {
			// TODO report error/cheat
			return false;
		}
		return Arrays.equals(this.hash, hash);
	}

}
