package org.kareha.hareka.client.chat;

import org.kareha.hareka.annotation.ThreadSafe;
import org.kareha.hareka.client.WorldSession;
import org.kareha.hareka.client.packet.PrivateChatPacket;

@ThreadSafe
public class PrivateChatSession extends ChatSession {

	private final ChatEntity peer;
	private final WorldSession session;

	public PrivateChatSession(final ChatEntity peer, final WorldSession session) {
		this.peer = peer;
		this.session = session;
	}

	public ChatEntity getPeer() {
		return peer;
	}

	private void writePrivateChat(final ChatEntity peer, final String content) {
		session.write(new PrivateChatPacket(peer, content));
	}

	@Override
	public void sendChat(final String content) {
		if (session != null) {
			writePrivateChat(peer, content);
		}
	}

}
