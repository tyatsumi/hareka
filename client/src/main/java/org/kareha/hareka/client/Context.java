package org.kareha.hareka.client;

import java.io.File;
import java.io.IOException;
import java.security.Key;

import javax.xml.bind.JAXBException;

import org.kareha.hareka.client.server.ServerId;
import org.kareha.hareka.client.server.ServerIdPersistentFormat;
import org.kareha.hareka.client.server.ServerStatic;
import org.kareha.hareka.client.server.Servers;
import org.kareha.hareka.key.KeyPersistentFormat;
import org.kareha.hareka.key.KeyStack;
import org.kareha.hareka.key.SimpleKeyStore;
import org.kareha.hareka.persistent.FastPersistentHashTable;
import org.kareha.hareka.persistent.PersistentHashTable;

public class Context {

	private final File dataDirectory;
	private final KeyStack keyStack;
	private final SimpleKeyStore simpleKeyStore;
	private final ConnectionSettings connectionSettings;
	private final ServerStatic serverStatic;
	private final PersistentHashTable<Key, ServerId> serverIndex;
	private final Servers servers;

	// version can be null for fake
	// dataDirectory can be null for fake
	public Context(final File dataDirectory) throws IOException, JAXBException {
		this.dataDirectory = dataDirectory;
		keyStack = new KeyStack(new File(dataDirectory, "KeyStack.xml"));
		simpleKeyStore = new SimpleKeyStore(new File(dataDirectory, "KeyStore"));
		connectionSettings = new ConnectionSettings(new File(dataDirectory, "ConnectionSettings.xml"));
		serverStatic = new ServerStatic(new File(dataDirectory, "ServerStatic.xml"));
		serverIndex = new FastPersistentHashTable<>(new File(dataDirectory, "ServerIndex.dat"),
				new KeyPersistentFormat(), new ServerIdPersistentFormat());
		servers = new Servers(new File(dataDirectory, "server"), serverStatic, serverIndex);
	}

	public File getDataDirectory() {
		return dataDirectory;
	}

	public KeyStack getKeyStack() {
		return keyStack;
	}

	public SimpleKeyStore getSimpleKeyStore() {
		return simpleKeyStore;
	}

	public ConnectionSettings getConnectionSettings() {
		return connectionSettings;
	}

	public Servers getServers() {
		return servers;
	}

	public void save() throws JAXBException {
		serverStatic.save();
	}

}
