package org.kareha.hareka.client.packet;

import org.kareha.hareka.client.protocol.WorldServerPacket;
import org.kareha.hareka.protocol.PacketType;
import org.kareha.hareka.protocol.WorldServerPacketType;

public final class RequestSettingsPacket extends WorldServerPacket {

	public RequestSettingsPacket() {
		// do nothing
	}

	@Override
	protected PacketType getType() {
		return WorldServerPacketType.REQUEST_SETTINGS;
	}

}
