package org.kareha.hareka.client.server;

import java.io.File;
import java.io.IOException;
import java.security.Key;
import java.security.PublicKey;
import java.text.MessageFormat;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.bind.JAXBException;

import org.kareha.hareka.annotation.GuardedBy;
import org.kareha.hareka.persistent.PersistentHashTable;
import org.kareha.hareka.protocol.ProtocolException;
import org.kareha.hareka.util.FileUtil;

public class Servers {

	private static final Logger logger = Logger.getLogger(Servers.class.getName());

	private final File directory;
	private final ServerStatic idTable;
	private final PersistentHashTable<Key, ServerId> keyIndexTable;
	@GuardedBy("this")
	private Server server;

	public Servers(final File directory, final ServerStatic idTable, final PersistentHashTable<Key, ServerId> keyIndexTable)
			throws IOException {
		this.directory = directory;
		this.idTable = idTable;
		this.keyIndexTable = keyIndexTable;

		FileUtil.ensureDirectoryExists(directory);
	}

	public File getDirectory(final ServerId id) {
		return new File(directory, id.toString());
	}

	private boolean exists(final ServerId id) {
		final File d = getDirectory(id);
		return d.exists();
	}

	private ServerId nextId() {
		ServerId id = idTable.next();
		while (exists(id)) {
			logger.severe(MessageFormat.format("ID {0} already exists", id));
			id = idTable.next();
		}
		return id;
	}

	public synchronized Server createServer(final PublicKey publicKey) {
		try {
			if (keyIndexTable.get(publicKey) != null) {
				return null;
			}
		} catch (final IOException | ProtocolException e) {
			logger.log(Level.SEVERE, "", e);
			return null;
		}
		final ServerId id = nextId();
		final File d = getDirectory(id);
		if (d.exists()) {
			return null;
		}
		if (!d.mkdirs()) {
			return null;
		}

		final Server s = new Server(id, publicKey);
		try {
			s.save(d);
		} catch (final IOException e) {
			logger.log(Level.SEVERE, "", e);
			return null;
		} catch (final JAXBException e) {
			logger.log(Level.SEVERE, "", e);
			return null;
		}
		try {
			keyIndexTable.put(publicKey, id);
		} catch (final IOException | ProtocolException e) {
			logger.log(Level.SEVERE, "", e);
			return null;
		}
		server = s;
		return s;
	}

	@GuardedBy("this")
	protected Server load(final ServerId id) throws JAXBException {
		final File d = getDirectory(id);
		if (!d.isDirectory()) {
			return null;
		}
		final Server server = Server.load(d);
		if (server.getId() == null || server.getPublicKey() == null) {
			logger.warning("id or publicKey is null.");
			return null;
		}
		if (!server.getId().equals(id)) {
			logger.warning("id mismatch");
			return null;
		}
		return server;
	}

	@GuardedBy("this")
	protected void save(final Server server) throws IOException, JAXBException {
		final File d = getDirectory(server.getId());
		server.save(d);
	}

	public synchronized Server login(final PublicKey publicKey) {
		final ServerId id;
		try {
			id = keyIndexTable.get(publicKey);
		} catch (final IOException | ProtocolException e) {
			logger.log(Level.SEVERE, "", e);
			return null;
		}
		if (id == null) {
			return null;
		}
		final Server s;
		try {
			s = load(id);
		} catch (final JAXBException e) {
			logger.log(Level.SEVERE, "", e);
			return null;
		}
		if (s == null) {
			return null;
		}
		if (!publicKey.equals(s.getPublicKey())) {
			return null;
		}
		server = s;
		return s;
	}

	public synchronized boolean logout() {
		final Server loggedInServer = server;
		server = null;
		if (loggedInServer == null) {
			return false;
		}
		try {
			save(loggedInServer);
		} catch (final IOException | JAXBException e) {
			logger.log(Level.SEVERE, "", e);
		}
		return true;
	}

}
