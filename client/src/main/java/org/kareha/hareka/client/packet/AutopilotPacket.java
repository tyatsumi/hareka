package org.kareha.hareka.client.packet;

import org.kareha.hareka.client.protocol.WorldServerPacket;
import org.kareha.hareka.protocol.PacketType;
import org.kareha.hareka.protocol.WorldServerPacketType;

public final class AutopilotPacket extends WorldServerPacket {

	public AutopilotPacket(final boolean flag) {
		out.writeBoolean(flag);
	}

	@Override
	protected PacketType getType() {
		return WorldServerPacketType.AUTOPILOT;
	}

}
