package org.kareha.hareka.client.packet;

import org.kareha.hareka.client.protocol.WorldServerPacket;
import org.kareha.hareka.protocol.PacketType;
import org.kareha.hareka.protocol.WorldServerPacketType;

public final class TokenPacket extends WorldServerPacket {

	public TokenPacket(final int handlerId, final byte[] token) {
		out.writeCompactUInt(handlerId);
		out.writeByteArray(token);
	}

	@Override
	protected PacketType getType() {
		return WorldServerPacketType.TOKEN;
	}

}
