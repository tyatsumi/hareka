package org.kareha.hareka.client.server;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.kareha.hareka.LocalEntityId;
import org.kareha.hareka.annotation.GuardedBy;
import org.kareha.hareka.annotation.Private;
import org.kareha.hareka.annotation.ThreadSafe;

@ThreadSafe
@XmlJavaTypeAdapter(FieldEntityEntry.Adapter.class)
public class FieldEntityEntry {

	@Private
	final LocalEntityId id;
	@GuardedBy("this")
	@Private
	List<Note> nicknames;

	public FieldEntityEntry(final LocalEntityId id) {
		this.id = id;
	}

	@Private
	FieldEntityEntry(final LocalEntityId id, final List<Note> nicknames) {
		this.id = id;
		if (nicknames != null) {
			this.nicknames = new ArrayList<>(nicknames);
		}
	}

	@XmlType(name = "fieldEntityEntry")
	@XmlAccessorType(XmlAccessType.NONE)
	private static class Adapted {

		@XmlAttribute
		private LocalEntityId id;
		@XmlElement(name = "nickname")
		private List<Note> nicknames;

		@SuppressWarnings("unused")
		private Adapted() {
			// used by JAXB
		}

		@Private
		Adapted(final FieldEntityEntry v) {
			id = v.id;
			synchronized (v) {
				if (v.nicknames != null && !v.nicknames.isEmpty()) {
					nicknames = new ArrayList<>(v.nicknames);
				}
			}
		}

		@Private
		FieldEntityEntry unmarshal() {
			return new FieldEntityEntry(id, nicknames);
		}

	}

	@Private
	Adapted marshal() {
		return new Adapted(this);
	}

	static class Adapter extends XmlAdapter<Adapted, FieldEntityEntry> {

		@Override
		public Adapted marshal(final FieldEntityEntry v) throws Exception {
			if (v == null) {
				return null;
			}
			return v.marshal();
		}

		@Override
		public FieldEntityEntry unmarshal(final Adapted v) throws Exception {
			if (v == null) {
				return null;
			}
			return v.unmarshal();
		}

	}

	public LocalEntityId getId() {
		return id;
	}

	public synchronized boolean isEmpty() {
		return nicknames == null || nicknames.isEmpty();
	}

	public synchronized String getNickname() {
		if (nicknames == null || nicknames.isEmpty()) {
			return null;
		}
		return nicknames.get(nicknames.size() - 1).getContent();
	}

	public synchronized void addNickname(final String nickname) {
		if (nicknames == null) {
			nicknames = new ArrayList<>();
		}
		nicknames.add(new Note(nickname));
	}

	public synchronized void clearNicknames() {
		nicknames = null;
	}

}
