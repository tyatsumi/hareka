package org.kareha.hareka.client.handler;

import java.io.IOException;

import org.kareha.hareka.client.WorldSession;
import org.kareha.hareka.protocol.Handler;
import org.kareha.hareka.protocol.HandlerException;
import org.kareha.hareka.protocol.ProtocolException;
import org.kareha.hareka.protocol.ProtocolInput;
import org.kareha.hareka.user.UserRegistrationMode;

public class SettingsHandler implements Handler<WorldSession> {

	@Override
	public void handle(final ProtocolInput in, final WorldSession session)
			throws IOException, ProtocolException, HandlerException {
		final UserRegistrationMode userRegistrationMode = UserRegistrationMode.readFrom(in);
		final boolean rescueMethodsEnabled = in.readBoolean();

		session.getMirrors().getAdminMirror().handleSettings(userRegistrationMode, rescueMethodsEnabled);
	}

}
