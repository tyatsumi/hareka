package org.kareha.hareka.client.packet;

import org.kareha.hareka.client.protocol.WorldServerPacket;
import org.kareha.hareka.protocol.PacketType;
import org.kareha.hareka.protocol.WorldServerPacketType;

public final class PowPacket extends WorldServerPacket {

	public PowPacket(final int handlerId, final byte[] nonce) {
		out.writeCompactUInt(handlerId);
		out.writeByteArray(nonce);
	}

	@Override
	protected PacketType getType() {
		return WorldServerPacketType.POW;
	}

}
