package org.kareha.hareka.client.handler;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.kareha.hareka.LocalUserId;
import org.kareha.hareka.client.WorldSession;
import org.kareha.hareka.client.user.RoleToken;
import org.kareha.hareka.protocol.HandlerException;
import org.kareha.hareka.protocol.ProtocolInput;
import org.kareha.hareka.protocol.Handler;
import org.kareha.hareka.protocol.ProtocolException;

public class RoleTokenListHandler implements Handler<WorldSession> {

	@Override
	public void handle(final ProtocolInput in, final WorldSession session)
			throws IOException, ProtocolException, HandlerException {
		final int size = in.readCompactUInt();
		final List<RoleToken> roleTokens = new ArrayList<>();
		for (int i = 0; i < size; i++) {
			final long id = in.readCompactULong();
			final long issuedDate = in.readCompactLong();
			final LocalUserId issuer = LocalUserId.readFrom(in);
			final String roleId = in.readString();
			final RoleToken token = new RoleToken(id, issuedDate, issuer, roleId);
			roleTokens.add(token);
		}

		session.getMirrors().getAdminMirror().handleRoleTokenList(roleTokens);
	}

}
