package org.kareha.hareka.client.handler;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.kareha.hareka.client.WorldSession;
import org.kareha.hareka.field.Region;
import org.kareha.hareka.protocol.HandlerException;
import org.kareha.hareka.protocol.ProtocolInput;
import org.kareha.hareka.protocol.Handler;
import org.kareha.hareka.protocol.ProtocolException;

public class RegionListHandler implements Handler<WorldSession> {

	@Override
	public void handle(final ProtocolInput in, final WorldSession session)
			throws IOException, ProtocolException, HandlerException {
		final int size = in.readCompactUInt();
		final List<Region> regions = new ArrayList<>();
		for (int i = 0; i < size; i++) {
			regions.add(Region.readFrom(in));
		}

		session.getMirrors().getFieldMirror().getField().setRegions(regions);
	}

}
