package org.kareha.hareka.client.server;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.kareha.hareka.annotation.Immutable;
import org.kareha.hareka.annotation.Private;

@Immutable
@XmlJavaTypeAdapter(ServerId.Adapter.class)
public final class ServerId {

	@Private
	final long value;

	private ServerId(final long value) {
		this.value = value;
	}

	public static ServerId valueOf(final long value) {
		return new ServerId(value);
	}

	public static ServerId valueOf(final String s) {
		// Long.parseLong throws NumberFormatException
		final long value = Long.parseLong(s, 16);
		return valueOf(value);
	}

	@Override
	public boolean equals(final Object obj) {
		if (!(obj instanceof ServerId)) {
			return false;
		}
		final ServerId id = (ServerId) obj;
		return id.value == value;
	}

	@Override
	public int hashCode() {
		return (int) (value >>> 32 ^ value);
	}

	@Override
	public String toString() {
		return Long.toHexString(value);
	}

	@XmlType(name = "serverId")
	@XmlAccessorType(XmlAccessType.NONE)
	private static class Adapted {

		@XmlValue
		private String value;

		@SuppressWarnings("unused")
		private Adapted() {
			// used by JAXB
		}

		@Private
		Adapted(final ServerId v) {
			value = v.toString();
		}

		@Private
		ServerId unmarshal() {
			return valueOf(value);
		}

	}

	@Private
	Adapted marshal() {
		return new Adapted(this);
	}

	static class Adapter extends XmlAdapter<Adapted, ServerId> {

		@Override
		public Adapted marshal(final ServerId v) throws Exception {
			if (v == null) {
				return null;
			}
			return v.marshal();
		}

		@Override
		public ServerId unmarshal(final Adapted v) throws Exception {
			if (v == null) {
				return null;
			}
			return v.unmarshal();
		}

	}

	public long value() {
		return value;
	}

}
