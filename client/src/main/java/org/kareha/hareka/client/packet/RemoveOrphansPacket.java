package org.kareha.hareka.client.packet;

import org.kareha.hareka.client.protocol.WorldServerPacket;
import org.kareha.hareka.protocol.PacketType;
import org.kareha.hareka.protocol.WorldServerPacketType;

public final class RemoveOrphansPacket extends WorldServerPacket {

	public RemoveOrphansPacket() {

	}

	@Override
	protected PacketType getType() {
		return WorldServerPacketType.REMOVE_ORPHANS;
	}

}
