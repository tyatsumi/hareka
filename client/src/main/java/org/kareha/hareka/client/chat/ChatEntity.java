package org.kareha.hareka.client.chat;

import org.kareha.hareka.LocalEntityId;
import org.kareha.hareka.annotation.GuardedBy;
import org.kareha.hareka.annotation.ThreadSafe;

@ThreadSafe
public class ChatEntity {

	private final LocalEntityId localId;
	@GuardedBy("this")
	private String name;

	public ChatEntity(final LocalEntityId localId, final String name) {
		this.localId = localId;
		this.name = name;
	}

	@Override
	public boolean equals(final Object obj) {
		if (!(obj instanceof ChatEntity)) {
			return false;
		}
		final ChatEntity entity = (ChatEntity) obj;
		return entity.localId.equals(localId);
	}

	@Override
	public int hashCode() {
		return localId.hashCode();
	}

	@Override
	public String toString() {
		return getAlias();
	}

	public LocalEntityId getLocalId() {
		return localId;
	}

	public synchronized String getName() {
		return name;
	}

	public synchronized void setName(final String name) {
		this.name = name;
	}

	public String getAlias() {
		final String n = getName();
		if (n != null && !n.isEmpty()) {
			return n;
		}
		return "(Unknown)";
	}

}
