package org.kareha.hareka.client.handler;

import java.io.IOException;
import java.util.logging.Logger;

import org.kareha.hareka.client.ResponseHandler;
import org.kareha.hareka.client.WorldSession;
import org.kareha.hareka.protocol.HandlerException;
import org.kareha.hareka.protocol.ProtocolInput;
import org.kareha.hareka.protocol.Handler;
import org.kareha.hareka.protocol.ProtocolException;

public class RejectHandler implements Handler<WorldSession> {

	private static final Logger logger = Logger.getLogger(RejectHandler.class.getName());

	@Override
	public void handle(final ProtocolInput in, final WorldSession session)
			throws IOException, ProtocolException, HandlerException {
		final int requestId = in.readCompactUInt();
		final String message = in.readString();

		final ResponseHandler rh = session.removeResponseHandler(requestId);
		if (rh == null) {
			logger.warning("A nonexistent request has been rejected: " + requestId);
			return;
		}
		rh.rejected(message);
	}

}
