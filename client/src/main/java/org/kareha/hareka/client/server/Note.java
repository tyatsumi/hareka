package org.kareha.hareka.client.server;

import java.util.Date;
import java.util.logging.Logger;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.kareha.hareka.annotation.Immutable;
import org.kareha.hareka.annotation.Private;

@Immutable
@XmlJavaTypeAdapter(Note.Adapter.class)
public class Note {

	@Private
	static final Logger logger = Logger.getLogger(Note.class.getName());

	@Private
	final long date;
	@Private
	final String content;

	public Note(final String content) {
		date = System.currentTimeMillis();
		this.content = content;
	}

	@Private
	Note(final long date, final String content) {
		this.date = date;
		this.content = content;
	}

	@XmlType(name = "note")
	@XmlAccessorType(XmlAccessType.NONE)
	private static class Adapted {

		@XmlAttribute
		private Date date;
		@XmlValue
		private String content;

		@SuppressWarnings("unused")
		private Adapted() {
			// used by JAXB
		}

		@Private
		Adapted(final Note v) {
			date = new Date(v.date);
			content = v.content;
		}

		@Private
		Note unmarshal() {
			if (date == null) {
				final String c = content != null ? content.substring(0, Math.min(content.length(), 32)) : null;
				logger.warning("date is null. It is changed to epoch time. content=" + c);
			}
			return new Note(date != null ? date.getTime() : 0, content);
		}

	}

	@Private
	Adapted marshal() {
		return new Adapted(this);
	}

	static class Adapter extends XmlAdapter<Adapted, Note> {

		@Override
		public Adapted marshal(final Note v) throws Exception {
			if (v == null) {
				return null;
			}
			return v.marshal();
		}

		@Override
		public Note unmarshal(final Adapted v) throws Exception {
			if (v == null) {
				return null;
			}
			return v.unmarshal();
		}

	}

	public long getDate() {
		return date;
	}

	public String getContent() {
		return content;
	}

}
