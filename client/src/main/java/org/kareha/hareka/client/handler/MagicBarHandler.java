package org.kareha.hareka.client.handler;

import java.io.IOException;
import java.util.logging.Logger;

import org.kareha.hareka.LocalEntityId;
import org.kareha.hareka.client.WorldSession;
import org.kareha.hareka.client.field.FieldEntity;
import org.kareha.hareka.client.field.StatBar;
import org.kareha.hareka.protocol.HandlerException;
import org.kareha.hareka.protocol.ProtocolInput;
import org.kareha.hareka.protocol.Handler;
import org.kareha.hareka.protocol.ProtocolException;

public class MagicBarHandler implements Handler<WorldSession> {

	private static final Logger logger = Logger.getLogger(MagicBarHandler.class.getName());

	@Override
	public void handle(final ProtocolInput in, final WorldSession session)
			throws IOException, ProtocolException, HandlerException {
		final LocalEntityId localId = LocalEntityId.readFrom(in);
		final boolean alive = in.readBoolean();
		final int bar = in.readByte();

		final FieldEntity fieldEntity = session.getMirrors().getEntityMirror().getFieldEntity(localId);
		if (fieldEntity == null) {
			logger.warning("Unknown field entity: " + localId);
			return;
		}
		fieldEntity.setMagicBar(new StatBar(alive, bar));
	}

}
