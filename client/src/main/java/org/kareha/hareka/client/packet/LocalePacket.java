package org.kareha.hareka.client.packet;

import java.util.Locale;

import org.kareha.hareka.client.protocol.WorldServerPacket;
import org.kareha.hareka.protocol.PacketType;
import org.kareha.hareka.protocol.WorldServerPacketType;

public final class LocalePacket extends WorldServerPacket {

	public LocalePacket(final Locale locale) {
		out.writeLocale(locale);
	}

	@Override
	protected PacketType getType() {
		return WorldServerPacketType.LOCALE;
	}

}
