package org.kareha.hareka.client.packet;

import java.util.logging.Logger;

import org.kareha.hareka.client.field.FieldEntity;
import org.kareha.hareka.client.protocol.WorldServerPacket;
import org.kareha.hareka.field.Vector;
import org.kareha.hareka.game.ActiveSkillType;
import org.kareha.hareka.game.SkillTargetType;
import org.kareha.hareka.protocol.PacketType;
import org.kareha.hareka.protocol.WorldServerPacketType;
import org.kareha.hareka.wait.Wait;

public final class UseActiveSkillPacket extends WorldServerPacket {

	private static final Logger logger = Logger.getLogger(UseActiveSkillPacket.class.getName());

	public UseActiveSkillPacket(final Wait wait, final ActiveSkillType abilityType) {
		if (abilityType.getTargetType() != SkillTargetType.NULL) {
			logger.severe("Invalid ability type=" + abilityType);
		}
		out.writeCompactULong(wait.getId());
		out.write(abilityType);
	}

	public UseActiveSkillPacket(final Wait wait, final ActiveSkillType abilityType, final FieldEntity target) {
		if (abilityType.getTargetType() != SkillTargetType.FIELD_ENTITY) {
			logger.severe("Invalid ability type=" + abilityType);
		}
		out.writeCompactULong(wait.getId());
		out.write(abilityType);
		out.write(target.getLocalId());
	}

	public UseActiveSkillPacket(final Wait wait, final ActiveSkillType abilityType, final Vector target) {
		if (abilityType.getTargetType() != SkillTargetType.TILE) {
			logger.severe("Invalid ability type=" + abilityType);
		}
		out.writeLong(wait.getId());
		out.write(abilityType);
		out.write(target);
	}

	@Override
	protected PacketType getType() {
		return WorldServerPacketType.USE_ACTIVE_SKILL;
	}

}
