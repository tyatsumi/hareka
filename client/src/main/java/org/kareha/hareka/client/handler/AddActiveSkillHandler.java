package org.kareha.hareka.client.handler;

import java.io.IOException;

import org.kareha.hareka.client.WorldSession;
import org.kareha.hareka.game.ActiveSkillType;
import org.kareha.hareka.protocol.HandlerException;
import org.kareha.hareka.protocol.ProtocolInput;
import org.kareha.hareka.protocol.Handler;
import org.kareha.hareka.protocol.ProtocolException;
import org.kareha.hareka.wait.WaitType;

public class AddActiveSkillHandler implements Handler<WorldSession> {

	@Override
	public void handle(final ProtocolInput in, final WorldSession session)
			throws IOException, ProtocolException, HandlerException {
		final ActiveSkillType type = ActiveSkillType.readFrom(in);
		final int reach = in.readCompactUInt();
		final WaitType waitType = WaitType.readFrom(in);
		final int wait = in.readCompactUInt();

		session.getMirrors().getSkillMirror().handleAdd(type, reach, waitType, wait);
	}

}
