package org.kareha.hareka.client.packet;

import org.kareha.hareka.client.protocol.WorldServerPacket;
import org.kareha.hareka.protocol.PacketType;
import org.kareha.hareka.protocol.WorldServerPacketType;

public final class SetRescueMethodsEnabledPacket extends WorldServerPacket {

	public SetRescueMethodsEnabledPacket(final boolean enabled) {
		out.writeBoolean(enabled);
	}

	@Override
	protected PacketType getType() {
		return WorldServerPacketType.SET_RESCUE_METHODS_ENABLED;
	}

}
