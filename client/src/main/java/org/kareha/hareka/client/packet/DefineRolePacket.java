package org.kareha.hareka.client.packet;

import org.kareha.hareka.client.protocol.WorldServerPacket;
import org.kareha.hareka.protocol.PacketType;
import org.kareha.hareka.protocol.WorldServerPacketType;
import org.kareha.hareka.user.Role;

public final class DefineRolePacket extends WorldServerPacket {

	public DefineRolePacket(final Role role) {
		out.write(role);
	}

	@Override
	protected PacketType getType() {
		return WorldServerPacketType.DEFINE_ROLE;
	}

}
