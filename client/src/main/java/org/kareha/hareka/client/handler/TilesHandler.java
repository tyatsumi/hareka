package org.kareha.hareka.client.handler;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.kareha.hareka.client.WorldSession;
import org.kareha.hareka.field.TilePiece;
import org.kareha.hareka.protocol.HandlerException;
import org.kareha.hareka.protocol.ProtocolInput;
import org.kareha.hareka.protocol.Handler;
import org.kareha.hareka.protocol.ProtocolException;

public class TilesHandler implements Handler<WorldSession> {

	@Override
	public void handle(final ProtocolInput in, final WorldSession session)
			throws IOException, ProtocolException, HandlerException {
		final int size = in.readCompactUInt();
		final List<TilePiece> list = new ArrayList<>();
		for (int i = 0; i < size; i++) {
			final TilePiece tilePiece = TilePiece.readFrom(in);
			list.add(tilePiece);
		}

		session.getMirrors().getFieldMirror().getField().setTiles(list);
	}

}
