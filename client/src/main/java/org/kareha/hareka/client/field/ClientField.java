package org.kareha.hareka.client.field;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import org.kareha.hareka.field.HashTileField;
import org.kareha.hareka.field.Region;
import org.kareha.hareka.field.Tile;
import org.kareha.hareka.field.TilePattern;
import org.kareha.hareka.field.TilePiece;
import org.kareha.hareka.field.Vector;

public class ClientField {

	public interface Listener {

		void tilesSet(Collection<TilePiece> tiles);

		void regionsSet(Collection<Region> regions);

	}

	private final List<Listener> listeners = new CopyOnWriteArrayList<>();
	private final int viewSize;
	private final HashTileField hashField;
	private final EntityField entityField;
	private volatile List<Region> regions = new CopyOnWriteArrayList<>();

	public ClientField(final int viewSize) {
		this.viewSize = viewSize;
		hashField = new HashTileField(true, TilePattern.SOLID_NULL_0);
		entityField = new EntityField();
	}

	public void addListener(final Listener listener) {
		listeners.add(listener);
	}

	public boolean removeListener(final Listener listener) {
		return listeners.remove(listener);
	}

	public Collection<Listener> getListeners() {
		return new ArrayList<>(listeners);
	}

	public Tile getTile(final Vector position) {
		return hashField.getTile(position);
	}

	public void setTile(final Vector position, final Tile tile) {
		hashField.setTile(position, tile, true);
	}

	public void setTiles(final Collection<TilePiece> tiles) {
		for (final TilePiece tilePiece : tiles) {
			hashField.setTile(tilePiece.position(), tilePiece.tile(), true);
		}
		for (final Listener listener : listeners) {
			listener.tilesSet(tiles);
		}
	}

	public int getViewSize() {
		return viewSize;
	}

	public boolean addEntity(final FieldEntity entity) {
		return entityField.addEntity(entity);
	}

	public boolean removeEntity(final FieldEntity entity) {
		return entityField.removeEntity(entity);
	}

	public boolean moveEntity(final FieldEntity entity) {
		return entityField.moveEntity(entity);
	}

	public List<FieldEntity> getEntities(final Vector position) {
		return entityField.getEntities(position);
	}

	public Collection<Region> getRegions() {
		return regions;
	}

	public void setRegions(final Collection<Region> regions) {
		final List<Region> newRegions = new CopyOnWriteArrayList<>(regions);
		this.regions = newRegions;
		for (final Listener listener : listeners) {
			listener.regionsSet(newRegions);
		}
	}

}
