package org.kareha.hareka.client.handler;

import java.io.IOException;

import org.kareha.hareka.client.WorldSession;
import org.kareha.hareka.field.Vector;
import org.kareha.hareka.protocol.HandlerException;
import org.kareha.hareka.protocol.ProtocolInput;
import org.kareha.hareka.protocol.Handler;
import org.kareha.hareka.protocol.ProtocolException;

public class EffectHandler implements Handler<WorldSession> {

	@Override
	public void handle(final ProtocolInput in, final WorldSession session)
			throws IOException, ProtocolException, HandlerException {
		final String effectId = in.readString();
		final Vector origin = Vector.readFrom(in);
		final Vector target = Vector.readFrom(in);

		session.getMirrors().getFieldMirror().handleAddEffect(effectId, origin, target);
	}

}
