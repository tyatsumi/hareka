package org.kareha.hareka.client.packet;

import org.kareha.hareka.LocalEntityId;
import org.kareha.hareka.client.field.FieldEntity;
import org.kareha.hareka.client.protocol.WorldServerPacket;
import org.kareha.hareka.protocol.PacketType;
import org.kareha.hareka.protocol.WorldServerPacketType;
import org.kareha.hareka.wait.Wait;

public final class UseItemPacket extends WorldServerPacket {

	public UseItemPacket(final Wait wait, final LocalEntityId itemId, final FieldEntity target) {
		out.writeCompactULong(wait.getId());
		out.write(itemId);
		out.write(target.getLocalId());
	}

	@Override
	protected PacketType getType() {
		return WorldServerPacketType.USE_ITEM;
	}

}
