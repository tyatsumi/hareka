package org.kareha.hareka.client.chat;

import org.kareha.hareka.annotation.ThreadSafe;
import org.kareha.hareka.client.WorldSession;
import org.kareha.hareka.client.packet.LocalChatPacket;

@ThreadSafe
public class LocalChatSession extends ChatSession {

	private final WorldSession session;

	public LocalChatSession(final WorldSession session) {
		this.session = session;
	}

	private void writeLocalChat(final String content) {
		session.write(new LocalChatPacket(content));
	}

	@Override
	public void sendChat(final String content) {
		if (session != null) {
			writeLocalChat(content);
		}
	}

}
