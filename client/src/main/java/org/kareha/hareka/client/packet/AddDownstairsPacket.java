package org.kareha.hareka.client.packet;

import org.kareha.hareka.client.protocol.WorldServerPacket;
import org.kareha.hareka.protocol.PacketType;
import org.kareha.hareka.protocol.WorldServerPacketType;

public final class AddDownstairsPacket extends WorldServerPacket {

	public AddDownstairsPacket() {

	}

	@Override
	protected PacketType getType() {
		return WorldServerPacketType.ADD_DOWNSTAIRS;
	}

}
