package org.kareha.hareka.client.handler;

import java.io.IOException;
import java.security.PublicKey;
import java.util.logging.Logger;

import org.kareha.hareka.Constants;
import org.kareha.hareka.Version;
import org.kareha.hareka.annotation.Private;
import org.kareha.hareka.client.WorldSession;
import org.kareha.hareka.client.packet.LocalePacket;
import org.kareha.hareka.client.packet.RequestPublicKeyPacket;
import org.kareha.hareka.client.server.Server;
import org.kareha.hareka.key.KeyGrabber;
import org.kareha.hareka.protocol.Handler;
import org.kareha.hareka.protocol.HandlerException;
import org.kareha.hareka.protocol.ProtocolException;
import org.kareha.hareka.protocol.ProtocolInput;

public class VersionHandler implements Handler<WorldSession> {

	@Private
	static final Logger logger = Logger.getLogger(VersionHandler.class.getName());

	@Override
	public void handle(final ProtocolInput in, final WorldSession session)
			throws IOException, ProtocolException, HandlerException {
		final Version serverVersion = Version.readFrom(in);

		final Version clientVersion = Constants.VERSION;
		final boolean matched = serverVersion.equals(clientVersion);
		if (matched) {
			session.write(new LocalePacket(session.getLocale()));
			session.getMirrors().getSystemMirror().startEcho();

			final KeyGrabber.Handler handler = new KeyGrabber.Handler() {
				@Override
				public void handle(final int version, final PublicKey publicKey) {
					if (publicKey == null) {
						logger.severe("Server public key is null");
						return;
					}
					session.setPeerKey(publicKey);

					Server server = session.getContext().getServers().login(publicKey);
					if (server == null) {
						server = session.getContext().getServers().createServer(publicKey);
					}
					session.setServer(server);

					session.getMirrors().getSystemMirror().handlePeerKeyArrived(publicKey);
				}
			};

			final KeyGrabber.Entry entry = session.getKeyGrabber().add(handler);
			if (entry != null) {
				final byte[] keyNonce = entry.newNonce(Constants.NONCE_BYTE_LENGTH);
				session.write(new RequestPublicKeyPacket(entry.getId(), 0, keyNonce));
			}
		}
		session.getMirrors().getSystemMirror().handleCheckVersion(matched);
	}

}
