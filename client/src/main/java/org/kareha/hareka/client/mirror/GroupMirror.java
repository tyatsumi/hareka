package org.kareha.hareka.client.mirror;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import org.kareha.hareka.LocalEntityId;
import org.kareha.hareka.LocalUserId;
import org.kareha.hareka.annotation.GuardedBy;

public class GroupMirror {

	public interface Listener {

		void inspected(LocalEntityId chatLocalId, LocalEntityId fieldLocalId, List<LocalUserId> userIds);

	}

	private final List<Listener> listeners = new CopyOnWriteArrayList<>();

	@GuardedBy("this")

	public void addListener(final Listener listener) {
		listeners.add(listener);
	}

	public boolean removeListener(final Listener listener) {
		return listeners.remove(listener);
	}

	public Collection<Listener> getListeners() {
		return new ArrayList<>(listeners);
	}

	public void handleInspection(final LocalEntityId chatLocalId, final LocalEntityId fieldLocalId,
			final List<LocalUserId> userIds) {
		for (final Listener listener : listeners) {
			listener.inspected(chatLocalId, fieldLocalId, userIds);
		}
	}
}
