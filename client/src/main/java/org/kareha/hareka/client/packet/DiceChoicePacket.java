package org.kareha.hareka.client.packet;

import org.kareha.hareka.client.DiceRoll;
import org.kareha.hareka.client.protocol.WorldServerPacket;
import org.kareha.hareka.protocol.PacketType;
import org.kareha.hareka.protocol.WorldServerPacketType;

public final class DiceChoicePacket extends WorldServerPacket {

	public DiceChoicePacket(final DiceRoll diceRoll) {
		out.writeCompactUInt(diceRoll.getId());
		final int[] choice = diceRoll.getChoice();
		out.writeCompactUInt(choice.length);
		for (int i = 0; i < choice.length; i++) {
			out.writeCompactUInt(choice[i]);
		}
	}

	@Override
	protected PacketType getType() {
		return WorldServerPacketType.DICE_CHOICE;
	}

}
