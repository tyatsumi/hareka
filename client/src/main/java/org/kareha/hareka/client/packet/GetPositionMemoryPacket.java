package org.kareha.hareka.client.packet;

import org.kareha.hareka.client.protocol.WorldServerPacket;
import org.kareha.hareka.protocol.PacketType;
import org.kareha.hareka.protocol.WorldServerPacketType;

public final class GetPositionMemoryPacket extends WorldServerPacket {

	public GetPositionMemoryPacket() {

	}

	@Override
	protected PacketType getType() {
		return WorldServerPacketType.GET_POSITION_MEMORY;
	}

}
