package org.kareha.hareka.client.server;

import java.io.IOException;

import org.kareha.hareka.persistent.PersistentFormat;
import org.kareha.hareka.persistent.PersistentInput;
import org.kareha.hareka.persistent.PersistentOutput;

public class ServerIdPersistentFormat implements PersistentFormat<ServerId> {

	@Override
	public ServerId read(final PersistentInput in) throws IOException {
		return ServerId.valueOf(in.readLong());
	}

	@Override
	public void write(final PersistentOutput out, final ServerId v) throws IOException {
		out.writeLong(v.value());
	}

}
