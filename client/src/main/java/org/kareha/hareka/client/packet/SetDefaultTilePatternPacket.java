package org.kareha.hareka.client.packet;

import org.kareha.hareka.client.protocol.WorldServerPacket;
import org.kareha.hareka.field.TilePattern;
import org.kareha.hareka.protocol.PacketType;
import org.kareha.hareka.protocol.WorldServerPacketType;

public final class SetDefaultTilePatternPacket extends WorldServerPacket {

	public SetDefaultTilePatternPacket(final TilePattern tilePattern) {
		out.write(tilePattern);
	}

	@Override
	protected PacketType getType() {
		return WorldServerPacketType.SET_DEFAULT_TILE_PATTERN;
	}

}
