package org.kareha.hareka.client.packet;

import org.kareha.hareka.client.protocol.WorldServerPacket;
import org.kareha.hareka.protocol.PacketType;
import org.kareha.hareka.protocol.WorldServerPacketType;

public final class NewFieldPacket extends WorldServerPacket {

	public NewFieldPacket(final int size) {
		out.writeCompactUInt(size);
	}

	@Override
	protected PacketType getType() {
		return WorldServerPacketType.NEW_FIELD;
	}

}
