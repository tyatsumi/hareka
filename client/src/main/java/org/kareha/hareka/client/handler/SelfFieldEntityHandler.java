package org.kareha.hareka.client.handler;

import java.io.IOException;
import java.util.logging.Logger;

import org.kareha.hareka.LocalEntityId;
import org.kareha.hareka.client.WorldSession;
import org.kareha.hareka.client.field.FieldEntity;
import org.kareha.hareka.protocol.Handler;
import org.kareha.hareka.protocol.HandlerException;
import org.kareha.hareka.protocol.ProtocolException;
import org.kareha.hareka.protocol.ProtocolInput;

public class SelfFieldEntityHandler implements Handler<WorldSession> {

	private static final Logger logger = Logger.getLogger(SelfFieldEntityHandler.class.getName());

	@Override
	public void handle(final ProtocolInput in, final WorldSession session)
			throws IOException, ProtocolException, HandlerException {
		final LocalEntityId id = LocalEntityId.readFrom(in);

		final FieldEntity entity = session.getMirrors().getEntityMirror().getFieldEntity(id);
		if (entity == null) {
			logger.warning("A nonexistent field entity has been set to self: " + id);
			return;
		}
		session.getMirrors().getSelfMirror().setFieldEntity(entity);
	}

}
