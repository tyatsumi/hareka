package org.kareha.hareka.client.packet;

import org.kareha.hareka.client.protocol.WorldServerPacket;
import org.kareha.hareka.protocol.PacketType;
import org.kareha.hareka.protocol.WorldServerPacketType;

public final class ConsumeRoleTokenPacket extends WorldServerPacket {

	public ConsumeRoleTokenPacket(final byte[] token) {
		out.writeByteArray(token);
	}

	@Override
	protected PacketType getType() {
		return WorldServerPacketType.CONSUME_ROLE_TOKEN;
	}

}
