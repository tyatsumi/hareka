package org.kareha.hareka.client.mirror;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import org.kareha.hareka.annotation.GuardedBy;
import org.kareha.hareka.annotation.Private;
import org.kareha.hareka.client.WorldSession;
import org.kareha.hareka.client.field.FieldEntity;
import org.kareha.hareka.client.packet.UseActiveSkillPacket;
import org.kareha.hareka.field.Vector;
import org.kareha.hareka.game.ActiveSkillType;
import org.kareha.hareka.wait.Wait;
import org.kareha.hareka.wait.WaitTable;
import org.kareha.hareka.wait.WaitType;

public class ActiveSkillMirror {

	public class Entry {

		private final ActiveSkillType type;
		private final int reach;
		private final WaitType waitType;
		private final int wait;

		Entry(final ActiveSkillType type, final int reach, final WaitType waitType, final int wait) {
			this.type = type;
			this.reach = reach;
			this.waitType = waitType;
			this.wait = wait;
		}

		public ActiveSkillType getType() {
			return type;
		}

		public int getReach() {
			return reach;
		}

		public WaitType getWaitType() {
			return waitType;
		}

		public int getWait() {
			return wait;
		}

		public void use() {
			final Wait wait = waitTable.get(waitType);
			if (wait != null) {
				session.write(new UseActiveSkillPacket(wait, type));
			}
		}

		public void use(final FieldEntity target) {
			final Wait wait = waitTable.get(waitType);
			if (wait != null) {
				session.write(new UseActiveSkillPacket(wait, type, target));
			}
		}

		public void use(final Vector target) {
			final Wait wait = waitTable.get(waitType);
			if (wait != null) {
				session.write(new UseActiveSkillPacket(wait, type, target));
			}
		}

	}

	public interface Listener {

		void skillsCleared();

		void skillAdded(Entry entry);

	}

	private final List<Listener> listeners = new CopyOnWriteArrayList<>();
	@Private
	final WorldSession session;
	@Private
	final WaitTable waitTable;
	@GuardedBy("this")
	private final List<Entry> entries = new ArrayList<>();

	public ActiveSkillMirror(final WorldSession session, final WaitTable waitTable) {
		this.session = session;
		this.waitTable = waitTable;
	}

	public void addListener(final Listener listener) {
		listeners.add(listener);
	}

	public boolean removeListener(final Listener listener) {
		return listeners.remove(listener);
	}

	public Collection<Listener> getListeners() {
		return new ArrayList<>(listeners);
	}

	public void handleClear() {
		synchronized (this) {
			entries.clear();
		}
		for (final Listener listener : listeners) {
			listener.skillsCleared();
		}
	}

	public void handleAdd(final ActiveSkillType type, final int reach, final WaitType waitType, final int wait) {
		final Entry entry = new Entry(type, reach, waitType, wait);
		synchronized (this) {
			entries.add(entry);
		}
		for (final Listener listener : listeners) {
			listener.skillAdded(entry);
		}
	}

	public synchronized Entry getEntry(final ActiveSkillType type) {
		for (final Entry entry : entries) {
			if (entry.getType().equals(type)) {
				return entry;
			}
		}
		return null;
	}

	public synchronized Collection<Entry> getEntries() {
		return new ArrayList<>(entries);
	}

}
