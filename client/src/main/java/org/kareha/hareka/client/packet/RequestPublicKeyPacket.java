package org.kareha.hareka.client.packet;

import org.kareha.hareka.client.protocol.WorldServerPacket;
import org.kareha.hareka.protocol.PacketType;
import org.kareha.hareka.protocol.WorldServerPacketType;

public final class RequestPublicKeyPacket extends WorldServerPacket {

	public RequestPublicKeyPacket(final int handlerId, final int version, final byte[] nonce) {
		out.writeCompactUInt(handlerId);
		out.writeCompactUInt(version);
		out.writeByteArray(nonce);
	}

	@Override
	protected PacketType getType() {
		return WorldServerPacketType.REQUEST_PUBLIC_KEY;
	}

}
