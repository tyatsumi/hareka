package org.kareha.hareka.client.control;

import java.util.Collection;

import org.kareha.hareka.client.WorldSession;
import org.kareha.hareka.client.chat.ChatEntity;
import org.kareha.hareka.client.field.FieldEntity;
import org.kareha.hareka.client.packet.ConsumeRoleTokenPacket;
import org.kareha.hareka.client.packet.DefineRolePacket;
import org.kareha.hareka.client.packet.DeleteRoleTokenPacket;
import org.kareha.hareka.client.packet.InspectChatEntityPacket;
import org.kareha.hareka.client.packet.InspectFieldEntityPacket;
import org.kareha.hareka.client.packet.IssueInvitationTokenPacket;
import org.kareha.hareka.client.packet.IssueRoleTokenPacket;
import org.kareha.hareka.client.packet.RebootPacket;
import org.kareha.hareka.client.packet.RequestMyRolesPacket;
import org.kareha.hareka.client.packet.RequestRoleListPacket;
import org.kareha.hareka.client.packet.RequestRoleTokenListPacket;
import org.kareha.hareka.client.packet.RequestSettingsPacket;
import org.kareha.hareka.client.packet.SavePacket;
import org.kareha.hareka.client.packet.SetRescueMethodsEnabledPacket;
import org.kareha.hareka.client.packet.SetUserRegistrationModePacket;
import org.kareha.hareka.client.packet.ShutdownPacket;
import org.kareha.hareka.client.packet.UndefineRolePacket;
import org.kareha.hareka.client.user.RoleToken;
import org.kareha.hareka.user.Permission;
import org.kareha.hareka.user.Role;
import org.kareha.hareka.user.UserRegistrationMode;

public class AdminControl {

	private final WorldSession session;

	public AdminControl(final WorldSession session) {
		this.session = session;
	}

	// Inspect

	public void inspect(final ChatEntity chatEntity) {
		session.write(new InspectChatEntityPacket(chatEntity));
	}

	public void inspect(final FieldEntity fieldEntity) {
		session.write(new InspectFieldEntityPacket(fieldEntity));
	}

	// Consume role token

	public void consumeRoleToken(final byte[] token) {
		session.write(new ConsumeRoleTokenPacket(token));
	}

	// Server settings

	public void requestSettings() {
		session.write(new RequestSettingsPacket());
	}

	public void setUserRegistrationMode(final UserRegistrationMode mode) {
		session.write(new SetUserRegistrationModePacket(mode));
	}

	public void setRescueMethodsEnabled(final boolean enabled) {
		session.write(new SetRescueMethodsEnabledPacket(enabled));
	}

	// Issue invitation token

	public void issueInvitationToken() {
		session.write(new IssueInvitationTokenPacket());
	}

	// Issue role token

	public void issueRoleToken(final String roleId) {
		session.write(new IssueRoleTokenPacket(roleId));
	}

	// Role token list

	public void requestRoleTokenList() {
		session.write(new RequestRoleTokenListPacket());
	}

	public Collection<RoleToken> getRoleTokenList() {
		return session.getMirrors().getAdminMirror().getRoleTokens();
	}

	public void deleteRoleToken(final long roleTokenId) {
		session.write(new DeleteRoleTokenPacket(roleTokenId));
	}

	// Edit roles

	public void requestRoleList() {
		session.write(new RequestRoleListPacket());
	}

	public Collection<Role> getRoleList() {
		return session.getMirrors().getAdminMirror().getRoles();
	}

	// XXX
	public void requestMyRoles() {
		session.write(new RequestMyRolesPacket());
	}

	public void createRole(final Role role) {
		session.write(new DefineRolePacket(role));
	}

	public void deleteRole(final String roleId) {
		session.write(new UndefineRolePacket(roleId));
	}

	// XXX why is this required?
	public Role getHighestRole(final Permission permission) {
		return session.getMirrors().getAdminMirror().getRoleSet().getHighestRole(permission);
	}

	// Shutdown, reboot, save

	public void shutdownWorldServer() {
		session.write(new ShutdownPacket());
	}

	public void rebootWorldServer() {
		session.write(new RebootPacket());
	}

	public void saveWorldServerData() {
		session.write(new SavePacket());
	}

}
