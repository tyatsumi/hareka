package org.kareha.hareka.client.mirror;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

import org.kareha.hareka.annotation.GuardedBy;
import org.kareha.hareka.client.WorldSession;
import org.kareha.hareka.client.chat.ChatEntity;
import org.kareha.hareka.client.chat.ChatSession;
import org.kareha.hareka.client.chat.LocalChatSession;
import org.kareha.hareka.client.chat.PrivateChatSession;

public class ChatMirror {

	public interface Listener {

		void chatSessionAdded(ChatSession session);

		void chatSessionRemoved(ChatSession session);

	}

	private final List<Listener> listeners = new CopyOnWriteArrayList<>();
	private final WorldSession session;
	@GuardedBy("this")
	private LocalChatSession localChatSession;
	@GuardedBy("this")
	private final Map<ChatEntity, PrivateChatSession> privateSessionMap = new HashMap<>();

	public ChatMirror(final WorldSession session) {
		this.session = session;
	}

	public void addListener(final Listener listener) {
		listeners.add(listener);
	}

	public boolean removeListener(final Listener listener) {
		return listeners.remove(listener);
	}

	public Collection<Listener> getListeners() {
		return new ArrayList<>(listeners);
	}

	public LocalChatSession getLocalChatSession() {
		final LocalChatSession s;
		final boolean created;
		synchronized (this) {
			if (localChatSession == null) {
				s = new LocalChatSession(session);
				localChatSession = s;
				created = true;
			} else {
				s = localChatSession;
				created = false;
			}
		}
		if (created) {
			for (final Listener listener : listeners) {
				listener.chatSessionAdded(s);
			}
		}
		return s;
	}

	public PrivateChatSession getPrivateChatSession(final ChatEntity peer) {
		final PrivateChatSession s;
		final boolean created;
		synchronized (this) {
			final PrivateChatSession ts = privateSessionMap.get(peer);
			if (ts == null) {
				s = new PrivateChatSession(peer, session);
				privateSessionMap.put(peer, s);
				created = true;
			} else {
				s = ts;
				created = false;
			}
		}
		if (created) {
			for (final Listener listener : listeners) {
				listener.chatSessionAdded(s);
			}
		}
		return s;
	}

	public boolean removePrivateChatSession(final ChatEntity peer) {
		final PrivateChatSession session;
		synchronized (this) {
			session = privateSessionMap.remove(peer);
		}
		final boolean result = session != null;
		if (result) {
			for (final Listener listener : listeners) {
				listener.chatSessionRemoved(session);
			}
		}
		return result;
	}

}
