package org.kareha.hareka.client.handler;

import java.io.IOException;
import java.security.Key;
import java.security.PublicKey;
import java.util.Arrays;
import java.util.logging.Logger;

import org.kareha.hareka.Constants;
import org.kareha.hareka.client.WorldSession;
import org.kareha.hareka.key.KeyGrabber;
import org.kareha.hareka.protocol.HandlerException;
import org.kareha.hareka.protocol.ProtocolInput;
import org.kareha.hareka.protocol.Handler;
import org.kareha.hareka.protocol.ProtocolException;

public class PublicKeyHandler implements Handler<WorldSession> {

	private static final Logger logger = Logger.getLogger(PublicKeyHandler.class.getName());

	@Override
	public void handle(final ProtocolInput in, final WorldSession session)
			throws IOException, ProtocolException, HandlerException {
		final int handlerId = in.readCompactUInt();
		final int version = in.readCompactUInt();
		final Key key = in.readKey();
		final String signatureAlgorithm = in.readString();
		final byte[] signature = in.readByteArray();

		final KeyGrabber.Entry entry = session.getKeyGrabber().get(handlerId);
		if (entry == null) {
			// TODO handle possible attack
			return;
		}

		if (key == null) {
			// have been informed above
			return;
		}

		if (!Arrays.asList(Constants.getAcceptablePublicKeyAlgorithms()).contains(key.getAlgorithm())) {
			logger.severe("Algorithm not acceptable: " + key.getAlgorithm());
			return;
		}
		if (!Arrays.asList(Constants.getAcceptablePublicKeyFormats()).contains(key.getFormat())) {
			logger.severe("Format not acceptable: " + key.getFormat());
			return;
		}

		if (!(key instanceof PublicKey)) {
			logger.severe("Invalid key"); // TODO make informative
			return;
		}
		final PublicKey publicKey = (PublicKey) key;

		if (!entry.verify(publicKey, signatureAlgorithm, signature)) {
			logger.warning("Invalid signature"); // TODO make informative
			return;
		}
		entry.handle(version, publicKey);
	}

}
