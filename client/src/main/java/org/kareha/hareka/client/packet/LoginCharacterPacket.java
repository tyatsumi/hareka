package org.kareha.hareka.client.packet;

import org.kareha.hareka.LocalEntityId;
import org.kareha.hareka.client.protocol.WorldServerRequestPacket;
import org.kareha.hareka.protocol.PacketType;
import org.kareha.hareka.protocol.WorldServerPacketType;

public final class LoginCharacterPacket extends WorldServerRequestPacket {

	public LoginCharacterPacket(final LocalEntityId id) {
		body.write(id);
	}

	@Override
	protected PacketType getType() {
		return WorldServerPacketType.LOGIN_CHARACTER;
	}

}
