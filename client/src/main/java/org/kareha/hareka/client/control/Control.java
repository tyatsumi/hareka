package org.kareha.hareka.client.control;

import java.util.ArrayList;
import java.util.List;

import org.kareha.hareka.LocalEntityId;
import org.kareha.hareka.client.WorldSession;
import org.kareha.hareka.client.chat.ChatEntity;
import org.kareha.hareka.client.field.FieldEntity;
import org.kareha.hareka.client.mirror.ActiveSkillMirror;
import org.kareha.hareka.client.packet.AutopilotPacket;
import org.kareha.hareka.client.packet.CancelPowPacket;
import org.kareha.hareka.client.packet.ChangeNamePacket;
import org.kareha.hareka.client.packet.CommandPacket;
import org.kareha.hareka.client.packet.DeleteItemPacket;
import org.kareha.hareka.client.packet.DropItemPacket;
import org.kareha.hareka.client.packet.GetPositionMemoryPacket;
import org.kareha.hareka.client.packet.PowPacket;
import org.kareha.hareka.client.packet.TeleportToCenterPacket;
import org.kareha.hareka.client.packet.TeleportToPositionMemoryPacket;
import org.kareha.hareka.field.Placement;
import org.kareha.hareka.field.Vector;
import org.kareha.hareka.game.Name;

public class Control {

	private final WorldSession session;

	public Control(final WorldSession session) {
		this.session = session;
	}

	// XXX
	public String getLanguage() {
		return session.getLocale().getLanguage();
	}

	// Self entities

	public ChatEntity getSelfChatEntity() {
		return session.getMirrors().getSelfMirror().getChatEntity();
	}

	public FieldEntity getSelfFieldEntity() {
		return session.getMirrors().getSelfMirror().getFieldEntity();
	}

	public Placement getSelfPlacement() {
		return session.getMirrors().getSelfMirror().getFieldEntity().getPlacement();
	}

	// Autopilot

	public void setAutopilot(final boolean enabled) {
		session.write(new AutopilotPacket(enabled));
	}

	// Move

	public void move(final Vector position) {
		session.getMirrors().getSelfMirror().move(position);
	}

	// Skill

	public List<ActiveSkillMirror.Entry> getActiveSkillEntries() {
		return new ArrayList<>(session.getMirrors().getSkillMirror().getEntries());
	}

	// Item

	public void deleteItem(final LocalEntityId itemId, final long count) {
		session.write(new DeleteItemPacket(itemId, count));
	}

	public void dropItem(final LocalEntityId itemId, final long count) {
		session.write(new DropItemPacket(itemId, count));
	}

	// Name

	public Name getSelfName() {
		return session.getMirrors().getSelfMirror().getName();
	}

	public void setSelfName(final String language, final String name) {
		session.write(new ChangeNamePacket(language, name));
	}

	// Chat

	// XXX
	public void activateLocalChat() {
		session.getMirrors().getChatMirror().getLocalChatSession();
	}

	public void removePrivateChatSession(final ChatEntity chatEntity) {
		session.getMirrors().getChatMirror().removePrivateChatSession(chatEntity);
	}

	// Rescue methods

	public void teleportToCenter() {
		session.write(new TeleportToCenterPacket());
	}

	// PoW

	public void pow(final int handlerId, final byte[] nonce) {
		session.write(new PowPacket(handlerId, nonce));
	}

	public void cancelPow(final int handlerId) {
		session.write(new CancelPowPacket(handlerId));
	}

	// Position memory

	public void getPositionMemory() {
		session.write(new GetPositionMemoryPacket());
	}

	public void teleportToPositionMemory(final byte[] data) {
		session.write(new TeleportToPositionMemoryPacket(data));
	}

	// Command

	public void command(final String[] args) {
		session.write(new CommandPacket(args));
	}

}
