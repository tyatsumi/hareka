package org.kareha.hareka.client.packet;

import org.kareha.hareka.client.field.FieldEntity;
import org.kareha.hareka.client.protocol.WorldServerPacket;
import org.kareha.hareka.protocol.PacketType;
import org.kareha.hareka.protocol.WorldServerPacketType;

public final class InspectFieldEntityPacket extends WorldServerPacket {

	public InspectFieldEntityPacket(final FieldEntity target) {
		out.write(target.getLocalId());
	}

	@Override
	protected PacketType getType() {
		return WorldServerPacketType.INSPECT_FIELD_ENTITY;
	}

}
