package org.kareha.hareka.graphics.sprite;

public interface SpriteFrameTable {

	SpriteFrame getFrame(String id);

}
