package org.kareha.hareka.graphics;

public final class GraphicsConstants {

	private GraphicsConstants() {
		throw new AssertionError();
	}

	public static final String UNDEFINED = "Undefined";

	public static final int TILE_WIDTH = 48;
	public static final int TILE_HEIGHT = 24;
	public static final int TILE_DEPTH = 16;

	public static final int MODEL_Z_OFFSET = 8;
	public static final int EFFECT_Z_OFFSET = 9;
	public static final int BAR_Z_OFFSET = 0x08000000;
	public static final int CURSOR_Z_OFFSET = 0x08000001;

}
