package org.kareha.hareka.graphics.model;

import java.awt.GraphicsConfiguration;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.bind.JAXBException;

import org.kareha.hareka.client.field.FieldEntity;
import org.kareha.hareka.graphics.GraphicsConstants;
import org.kareha.hareka.graphics.sprite.SpriteArray;
import org.kareha.hareka.graphics.sprite.SpriteArrayLoader;
import org.kareha.hareka.graphics.sprite.SpriteFrameTable;

public class ModelLoader {

	private static final Logger logger = Logger.getLogger(ModelLoader.class.getName());

	private final String directory;
	private final SpriteArrayLoader spriteArrayLoader;
	private final Map<String, ModelTemplate> cache = new HashMap<>();

	public ModelLoader(final String directory, final GraphicsConfiguration gc) {
		this.directory = directory;
		spriteArrayLoader = new SpriteArrayLoader(directory, gc);
	}

	public Model createModel(final FieldEntity object) {
		final String shape = object.getShape();
		final ModelTemplate template = load(shape);
		final Collection<SpriteFrameTable> tables = spriteArrayLoader.load(shape);
		final SpriteArray spriteArray = new SpriteArray(tables, object);
		return new Model(template, spriteArray, object);
	}

	private ModelTemplate load(final String id) {
		ModelTemplate template = cache.get(id);
		if (template != null) {
			return template;
		}

		final String path = directory + "/" + id + "/" + "model.xml";
		try (final InputStream in = this.getClass().getClassLoader().getResourceAsStream(path)) {
			if (in == null) {
				if (id.equals(GraphicsConstants.UNDEFINED)) {
					throw new RuntimeException("Undefined is undefined");
				}
				template = load(GraphicsConstants.UNDEFINED);
				cache.put(id, template);
				return template;
			}
			try {
				template = ModelTemplate.load(in);
			} catch (final JAXBException e) {
				logger.log(Level.SEVERE, "", e);
				return null;
			}
			cache.put(id, template);
		} catch (final IOException e) {
			logger.log(Level.SEVERE, "", e);
			return null;
		}

		return template;
	}

}
