package org.kareha.hareka.graphics;

import java.awt.GraphicsConfiguration;
import java.awt.GraphicsEnvironment;

import org.kareha.hareka.graphics.model.EffectLoader;
import org.kareha.hareka.graphics.model.ModelLoader;
import org.kareha.hareka.graphics.tile.TileLoader;

public final class GraphicsLoader {

	private TileLoader tileLoader;
	private ModelLoader modelLoader;
	private EffectLoader effectLoader;
	private ImageLoader imageLoader;
	private IconPainterTable iconPainterTable;

	public GraphicsLoader() {
		load();
	}

	private void load() {
		final GraphicsConfiguration gc = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice()
				.getDefaultConfiguration();
		tileLoader = new TileLoader("org/kareha/hareka/graphics/resource/tile", gc);
		modelLoader = new ModelLoader("org/kareha/hareka/graphics/resource/object", gc);
		effectLoader = new EffectLoader("org/kareha/hareka/graphics/resource/effect", gc);
		imageLoader = new ImageLoader("org/kareha/hareka/graphics/resource/image");
		iconPainterTable = new IconPainterTable("org/kareha/hareka/graphics/resource/icon");
	}

	public void reload() {
		load();
	}

	public TileLoader tileLoader() {
		return tileLoader;
	}

	public ModelLoader getModelLoader() {
		return modelLoader;
	}

	public EffectLoader getEffectLoader() {
		return effectLoader;
	}

	public ImageLoader getImageLoader() {
		return imageLoader;
	}

	public IconPainterTable getIconPainterTable() {
		return iconPainterTable;
	}

}
