package org.kareha.hareka.graphics.tile;

import org.kareha.hareka.field.Vector;

public class ViewUtil {

	public static Vector toViewVector(final Vector vector) {
		return Vector.valueOf(vector.x() * 3, vector.y() * 3);
	}

	public static Vector toVector(final Vector vector) {
		return Vector.valueOf(Math.round(vector.x() / 3f), Math.round(vector.y() / 3f));
	}

}
