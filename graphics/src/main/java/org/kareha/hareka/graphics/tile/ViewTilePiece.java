package org.kareha.hareka.graphics.tile;

import org.kareha.hareka.field.Vector;

public class ViewTilePiece {

	private final Vector position;
	private final ViewTile tile;

	private ViewTilePiece(final Vector position, final ViewTile tile) {
		if (position == null || tile == null) {
			throw new IllegalArgumentException("null");
		}
		this.position = position;
		this.tile = tile;
	}

	public static ViewTilePiece valueOf(final Vector position, final ViewTile tile) {
		return new ViewTilePiece(position, tile);
	}

	public Vector position() {
		return position;
	}

	public ViewTile tile() {
		return tile;
	}

}
