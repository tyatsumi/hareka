package org.kareha.hareka.graphics.sprite;

import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;

public class ImageSpriteFrame implements SpriteFrame {

	private final BufferedImage image;
	private final int centerX;
	private final int centerY;
	private final int centerZ;

	public ImageSpriteFrame(final BufferedImage image, final int centerX, final int centerY, final int centerZ) {
		this.image = image;
		this.centerX = centerX;
		this.centerY = centerY;
		this.centerZ = centerZ;
	}

	@Override
	public int getCenterX() {
		return centerX;
	}

	@Override
	public int getCenterY() {
		return centerY;
	}

	@Override
	public int getCenterZ() {
		return centerZ;
	}

	@Override
	public void paint(final Graphics g, final int x, final int y) {
		g.drawImage(image, x - centerX, y - centerY, null);
	}

	@Override
	public boolean contains(final int x, final int y) {
		final int px = x + centerX;
		final int py = y + centerY;
		if (px < 0 || px >= image.getWidth() || py < 0 || py >= image.getHeight()) {
			return false;
		}
		return (image.getRGB(px, py) & 0xff000000) != 0;
	}

	@Override
	public Rectangle getBounds() {
		return new Rectangle(-centerX, -centerY, image.getWidth(), image.getHeight());
	}

}
