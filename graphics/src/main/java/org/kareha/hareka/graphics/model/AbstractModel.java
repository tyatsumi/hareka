package org.kareha.hareka.graphics.model;

import java.awt.Point;
import java.awt.Rectangle;

import org.kareha.hareka.field.Direction;
import org.kareha.hareka.field.Placement;
import org.kareha.hareka.field.Transformation;
import org.kareha.hareka.field.Vector;
import org.kareha.hareka.graphics.GraphicsConstants;
import org.kareha.hareka.graphics.sprite.SpriteArray;

abstract class AbstractModel {

	private static final Placement PLACEMENT_ZERO = Placement.valueOf(Vector.ZERO, Direction.NULL);

	protected final SpriteArray spriteArray;

	protected Placement placement = PLACEMENT_ZERO;
	protected Vector prevPosition = Vector.ZERO;
	protected int elevation;
	protected int prevElevation;
	protected int prevMotionWait;
	protected int motionWait;

	protected float phase;
	protected float walkRatio;

	protected String frameId;

	protected AbstractModel(final SpriteArray spriteArray) {
		this.spriteArray = spriteArray;
	}

	protected abstract int getZOffset();

	public Rectangle getBounds() {
		return spriteArray.getBounds();
	}

	public void transform(final Transformation v) {
		placement = placement.transform(v);
		prevPosition = prevPosition.transform(v);
		updatePosition();
	}

	public void setPlacement(final Placement p, final int h) {
		prevPosition = p.getPosition();
		prevElevation = h;
		placement = p;
		elevation = h;
		walkRatio = 1;
		updatePosition();
	}

	public void place(final Placement p, final int h, final int motionWait) {
		prevPosition = placement.getPosition();
		prevElevation = elevation;
		placement = p;
		elevation = h;
		walkRatio = 0;
		if (this.motionWait == 0) {
			prevMotionWait = motionWait;
		} else {
			prevMotionWait = this.motionWait;
		}
		this.motionWait = motionWait;
	}

	private Point toView(final Vector position) {
		int vx = GraphicsConstants.TILE_WIDTH * position.x() + GraphicsConstants.TILE_WIDTH / 2 * position.y();
		int vy = GraphicsConstants.TILE_HEIGHT * position.y();
		return new Point(vx, vy);
	}

	protected void updatePosition() {
		final Point prevp = toView(prevPosition);
		final Point p = toView(placement.getPosition());
		final int u = (int) (prevp.x + (p.x - prevp.x) * walkRatio);
		final int v = (int) (prevp.y + (p.y - prevp.y) * walkRatio);
		final int v2;
		if (p.y < prevp.y) {
			if (walkRatio < 1.0f) {
				v2 = prevp.y;
			} else {
				v2 = p.y;
			}
		} else {
			v2 = p.y;
		}

		final float wr;
		if (elevation < prevElevation) {
			if (walkRatio < 0.5f) {
				wr = 0;
			} else {
				wr = (walkRatio - 0.5f) * 2;
			}
		} else if (elevation == prevElevation) {
			wr = 0;
		} else {
			if (walkRatio < 0.5f) {
				wr = walkRatio * 2;
			} else {
				wr = 1;
			}
		}
		final float h = prevElevation + (elevation - prevElevation) * wr;
		final int el = (int) (GraphicsConstants.TILE_DEPTH * h);

		spriteArray.setPosition(u, v - el, v2 + el + getZOffset());
	}

	protected abstract String makeFrameId();

	public void animate(final int v) {
		final String id = makeFrameId();
		if (!id.equals(frameId)) {
			spriteArray.setFrame(id);
			frameId = id;
			updatePosition();
		}

		phase += v / 1000f;

		final int mw = Math.min(prevMotionWait, motionWait);
		if (walkRatio < 1) {
			if (mw == 0) {
				walkRatio = 1;
			} else {
				walkRatio += (float) v / mw;
			}
			if (walkRatio > 1) {
				walkRatio = 1;
			}
			updatePosition();
		}
	}

	Direction getVisualDirection(final WayType type) {
		switch (type) {
		default:
			return null;
		case ONE_WAY:
			return Direction.RIGHT;
		case TWO_WAY:
			switch (placement.getDirection()) {
			default:
				return Direction.RIGHT;
			case DOWN_LEFT:
			case LEFT:
			case UP_LEFT:
				return Direction.LEFT;
			}
		case SIX_WAY:
			return placement.getDirection();
		}
	}

	public float getWalkRatio() {
		return walkRatio;
	}

	public int getMinMotionWait() {
		return Math.min(prevMotionWait, motionWait);
	}

}
