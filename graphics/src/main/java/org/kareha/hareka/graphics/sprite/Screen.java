package org.kareha.hareka.graphics.sprite;

import java.awt.Graphics;
import java.awt.Point;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Screen {

	private static final Comparator<Sprite> Z_COMPARATOR = (o1, o2) -> {
		final int z1 = o1.getFrameZ();
		final int z2 = o2.getFrameZ();
		if (z1 < z2) {
			return -1;
		} else if (z1 == z2) {
			return 0;
		} else {
			return 1;
		}
	};

	private final List<Sprite> sprites = new ArrayList<>();
	private int viewportX;
	private int viewportY;

	public void setViewport(final int x, final int y) {
		viewportX = x;
		viewportY = y;
	}

	public Point getViewport() {
		return new Point(viewportX, viewportY);
	}

	public void add(final Sprite sprite) {
		sprites.add(sprite);
	}

	public void remove(final Sprite sprite) {
		sprites.remove(sprite);
	}

	public void paint(final Graphics g) {
		Collections.sort(sprites, Z_COMPARATOR);
		for (final Sprite sprite : sprites) {
			sprite.paint(g, viewportX, viewportY);
		}
	}

	public Collection<Sprite> detect(final int x, final int y) {
		final List<Sprite> list = new ArrayList<>();
		for (final Sprite sprite : sprites) {
			if (sprite.contains(viewportX + x, viewportY + y)) {
				list.add(sprite);
			}
		}
		return list;
	}

}
