package org.kareha.hareka.graphics.sprite;

import java.awt.GraphicsConfiguration;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.kareha.hareka.graphics.GraphicsConstants;

public class SpriteArrayLoader {

	private final String directory;
	private final GraphicsConfiguration gc;
	private final Map<String, Collection<SpriteFrameTable>> cache = new HashMap<>();

	public SpriteArrayLoader(final String directory, final GraphicsConfiguration gc) {
		this.directory = directory;
		this.gc = gc;
	}

	public Collection<SpriteFrameTable> load(final String id) {
		Collection<SpriteFrameTable> tables = cache.get(id);
		if (tables != null) {
			return tables;
		}

		tables = SpriteFrameTableLoader.loadTables(directory + "/" + id, "sprites.xml", gc);
		if (tables.isEmpty()) {
			if (!id.equals(GraphicsConstants.UNDEFINED)) {
				tables = load(GraphicsConstants.UNDEFINED);
			}
		}

		cache.put(id, tables);

		return tables;
	}

}
