package org.kareha.hareka.graphics.sprite;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;

import org.kareha.hareka.graphics.GraphicsConstants;

public class TileCursorSpriteFrame implements SpriteFrame {

	@Override
	public int getCenterX() {
		return 0;
	}

	@Override
	public int getCenterY() {
		return 0;
	}

	@Override
	public int getCenterZ() {
		return 0;
	}

	@Override
	public void paint(final Graphics g, final int x, final int y) {
		g.setColor(Color.BLACK);
		g.drawOval(x - GraphicsConstants.TILE_WIDTH / 2 - 1, y - GraphicsConstants.TILE_HEIGHT / 2 - 1,
				GraphicsConstants.TILE_WIDTH + 2, GraphicsConstants.TILE_HEIGHT + 2);
		g.setColor(Color.BLACK);
		g.drawOval(x - GraphicsConstants.TILE_WIDTH / 2 + 1, y - GraphicsConstants.TILE_HEIGHT / 2 + 1,
				GraphicsConstants.TILE_WIDTH - 2, GraphicsConstants.TILE_HEIGHT - 2);
		g.setColor(Color.YELLOW);
		g.drawOval(x - GraphicsConstants.TILE_WIDTH / 2, y - GraphicsConstants.TILE_HEIGHT / 2,
				GraphicsConstants.TILE_WIDTH, GraphicsConstants.TILE_HEIGHT);
	}

	@Override
	public boolean contains(final int x, final int y) {
		return false;
	}

	@Override
	public Rectangle getBounds() {
		return new Rectangle(-GraphicsConstants.TILE_WIDTH / 2, -GraphicsConstants.TILE_HEIGHT / 2,
				GraphicsConstants.TILE_WIDTH, GraphicsConstants.TILE_HEIGHT);
	}

}
