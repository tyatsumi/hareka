package org.kareha.hareka.graphics.sprite;

import java.awt.Rectangle;
import java.util.Collection;

public class SpriteArray {

	private final Sprite[] sprites;
	private int x;
	private int y;
	private int z;

	public SpriteArray(final Collection<SpriteFrameTable> tables, final Object object) {
		sprites = new Sprite[tables.size()];
		int i = 0;
		for (final SpriteFrameTable table : tables) {
			sprites[i] = new Sprite(table, object);
			i++;
		}
	}

	public SpriteArray(final SpriteArray original) {
		sprites = new Sprite[original.sprites.length];
		int i = 0;
		for (final Sprite sprite : original.sprites) {
			sprites[i] = new Sprite(sprite);
			i++;
		}
		x = original.x;
		y = original.y;
		z = original.z;
	}

	public int size() {
		return sprites.length;
	}

	public Sprite getSprite(final int i) {
		return sprites[i];
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public int getZ() {
		return z;
	}

	public void setPosition(final int x, final int y, final int z) {
		this.x = x;
		this.y = y;
		this.z = z;
		for (int i = 0; i < sprites.length; i++) {
			sprites[i].setPosition(x, y, z);
		}
	}

	public void setFrame(final String id) {
		for (int i = 0; i < sprites.length; i++) {
			sprites[i].setFrame(id);
		}
	}

	public Rectangle getBounds() {
		final Rectangle rectangle = new Rectangle(0, 0, -1, -1);
		for (int i = 0; i < sprites.length; i++) {
			rectangle.add(sprites[i].getBounds());
		}
		return rectangle;
	}

	public void addTo(final Screen screen) {
		for (final Sprite sprite : sprites) {
			screen.add(sprite);
		}
	}

	public void removeFrom(final Screen screen) {
		for (final Sprite sprite : sprites) {
			screen.remove(sprite);
		}
	}

}
