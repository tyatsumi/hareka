package org.kareha.hareka.graphics.sprite;

import java.awt.Graphics;
import java.awt.Rectangle;

public interface SpriteFrame {

	int getCenterX();

	int getCenterY();

	int getCenterZ();

	void paint(Graphics g, int x, int y);

	boolean contains(int x, int y);

	Rectangle getBounds();

}
