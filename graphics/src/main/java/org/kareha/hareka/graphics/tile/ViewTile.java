package org.kareha.hareka.graphics.tile;

import org.kareha.hareka.field.Tile;

public class ViewTile {

	public static final ViewTile NULL_0 = valueOf(ViewTileType.NULL, 0);

	private final ViewTileType type;
	private final int elevation;

	private ViewTile(final ViewTileType type, final int elevation) {
		if (type == null) {
			throw new IllegalArgumentException("null");
		}
		this.type = type;
		this.elevation = elevation;
	}

	public static ViewTile valueOf(final ViewTileType type, final int elevation) {
		return new ViewTile(type, elevation);
	}

	public static ViewTile valueOf(final Tile tile) {
		return valueOf(ViewTileType.valueOf(tile.type()), tile.elevation());
	}

	public ViewTileType type() {
		return type;
	}

	public int elevation() {
		return elevation;
	}

}
