package org.kareha.hareka.graphics.tile;

import org.kareha.hareka.field.Transformation;
import org.kareha.hareka.field.Vector;

public enum ViewDirection {

	DOWN, DOWN_LEFT, UP_LEFT, UP, UP_RIGHT, DOWN_RIGHT, NULL;

	private final Vector vector;

	private ViewDirection() {
		if (ordinal() == 6) {
			vector = Vector.ZERO;
		} else {
			vector = Vector.valueOf(-1, 2).rotate(ordinal());
		}
	}

	private static final ViewDirection[] values = values();

	public static ViewDirection valueOf(final int ordinal) {
		if (ordinal < 0 || ordinal >= values.length) {
			throw new IllegalArgumentException("Out of bounds");
		}
		return values[ordinal];
	}

	public Vector vector() {
		return vector;
	}

	public ViewDirection reflect() {
		if (this == NULL) {
			return NULL;
		}
		return valueOf((6 - ordinal()) % 6);
	}

	public ViewDirection reflect(final boolean flag) {
		if (!flag) {
			return this;
		}
		return reflect();
	}

	public ViewDirection rotate(final int amount) {
		if (this == NULL) {
			return NULL;
		}
		return valueOf(Math.floorMod(ordinal() + amount, 6));
	}

	public ViewDirection transform(final Transformation transformation) {
		return rotate(-transformation.rotation()).reflect(transformation.reflection());
	}

	public ViewDirection invert(final Transformation transformation) {
		return reflect(transformation.reflection()).rotate(transformation.rotation());
	}

}
