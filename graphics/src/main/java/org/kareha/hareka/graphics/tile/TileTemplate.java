package org.kareha.hareka.graphics.tile;

import java.io.InputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.kareha.hareka.util.JaxbUtil;

@XmlRootElement(name = "tile")
@XmlAccessorType(XmlAccessType.NONE)
public final class TileTemplate {

	private static final JAXBContext jaxb;

	static {
		try {
			jaxb = JAXBContext.newInstance(TileTemplate.class);
		} catch (final JAXBException e) {
			throw new RuntimeException(e.getMessage(), e);
		}
	}

	@XmlElement
	private Pattern pattern;

	@XmlElement
	private int frames;

	@XmlElement
	private int frequency;

	private TileTemplate() {
		// For JAXB
	}

	public static TileTemplate load(final InputStream in) throws JAXBException {
		return JaxbUtil.unmarshal(in, jaxb);
	}

	Pattern pattern() {
		return pattern;
	}

	int frames() {
		return frames;
	}

	int frequency() {
		return frequency;
	}

	int getFrameNumber(final float phase) {
		float p = phase * frequency;
		return (int) (frames * (p - (int) p));
	}

}
