package org.kareha.hareka.graphics.sprite;

import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Rectangle;

import org.kareha.hareka.ui.swing.SwingUtil;

public class NameSpriteFrame implements SpriteFrame {

	private static final int yOffset = -2;

	private String name;
	private int width;
	private int height;

	@Override
	public int getCenterX() {
		return 0;
	}

	@Override
	public int getCenterY() {
		return 0;
	}

	@Override
	public int getCenterZ() {
		return 0;
	}

	public void setName(final String name) {
		this.name = name;
	}

	@Override
	public void paint(final Graphics g, final int x, final int y) {
		if (name != null) {
			final FontMetrics fm = g.getFontMetrics();
			width = fm.stringWidth(name);
			SwingUtil.drawString(g, name, x - width / 2, y + yOffset);
			height = fm.getHeight();
		} else {
			width = 0;
			height = 0;
		}
	}

	@Override
	public boolean contains(final int x, final int y) {
		if (width == 0 || height == 0) {
			return false;
		}
		return x >= -width / 2 && x < width / 2 && y >= -height + yOffset && y < yOffset;
	}

	@Override
	public Rectangle getBounds() {
		if (width == 0 || height == 0) {
			return new Rectangle(0, 0, -1, -1);
		}
		return new Rectangle(-width / 2, -height + yOffset, width, height);
	}

}
