package org.kareha.hareka.graphics.tile;

public enum ViewTileTypeCategory {

	NULL,

	GROUND,

	INDOOR,

	UNDERGROUND,

	SKY,

	;

	private static final ViewTileTypeCategory[] values = values();

	public static ViewTileTypeCategory valueOf(final int ordinal) {
		if (ordinal < 0 || ordinal >= values.length) {
			throw new IllegalArgumentException("Out of bounds");
		}
		return values[ordinal];
	}

}
