package org.kareha.hareka.graphics;

import java.util.HashMap;
import java.util.Map;

public final class IconPainterTable {

	public static final String UNDEFINED = "UNDEFINED";

	private final String directory;
	private final Map<String, IconPainter> table = new HashMap<>();

	public IconPainterTable(final String directory) {
		this.directory = directory;
	}

	public void reload() {
		table.clear();
	}

	private IconPainter loadPainter(final String v) {
		final String filename = directory + "/" + v + ".png";
		return new IconPainter(filename);
	}

	public IconPainter get(final String v) {
		IconPainter p = table.get(v);
		if (p != null) {
			return p;
		}

		p = loadPainter(v);
		if (p != null) {
			table.put(v, p);
			return p;
		}

		p = table.get(UNDEFINED);
		if (p != null) {
			return p;
		}

		p = loadPainter(UNDEFINED);
		if (p != null) {
			table.put(v, p);
			return p;
		}

		return null;
	}

}
