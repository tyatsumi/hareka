package org.kareha.hareka.graphics.model;

import java.io.InputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.kareha.hareka.util.JaxbUtil;

@XmlRootElement(name = "effect")
@XmlAccessorType(XmlAccessType.NONE)
public final class EffectTemplate {

	private static final JAXBContext jaxb;

	static {
		try {
			jaxb = JAXBContext.newInstance(EffectTemplate.class);
		} catch (final JAXBException e) {
			throw new RuntimeException(e.getMessage(), e);
		}
	}

	@XmlElement
	private WayType wayType;
	@XmlElement
	private int frames;
	@XmlElement
	private float frequency;
	@XmlElement
	private int repeatCount;

	public static EffectTemplate load(final InputStream in) throws JAXBException {
		return JaxbUtil.unmarshal(in, jaxb);
	}

	public WayType getWayType() {
		return wayType;
	}

	public int getFrameNumber(final float phase) {
		float p = phase * frequency;
		return Math.round(frames * (p - (int) p));
	}

	public float getMaxPhase() {
		if (repeatCount == 0) {
			return Float.MAX_VALUE;
		}
		return repeatCount / frequency;
	}

}
