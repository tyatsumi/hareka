package org.kareha.hareka.graphics.sprite;

import java.util.HashMap;
import java.util.Map;

public class NormalSpriteFrameTable implements SpriteFrameTable {

	private final Map<String, SpriteFrame> frames = new HashMap<>();

	@Override
	public SpriteFrame getFrame(final String id) {
		final SpriteFrame frame = frames.get(id);
		if (frame != null) {
			return frame;
		}
		return NullSpriteFrame.INSTANCE;
	}

	public void putFrame(final String id, final SpriteFrame frame) {
		frames.put(id, frame);
	}

}
