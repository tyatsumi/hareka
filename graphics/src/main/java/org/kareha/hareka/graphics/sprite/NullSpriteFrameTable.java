package org.kareha.hareka.graphics.sprite;

public final class NullSpriteFrameTable implements SpriteFrameTable {

	public static final SpriteFrameTable INSTANCE = new NullSpriteFrameTable();

	private NullSpriteFrameTable() {
		// do nothing
	}

	@Override
	public SpriteFrame getFrame(final String id) {
		return NullSpriteFrame.INSTANCE;
	}

}
