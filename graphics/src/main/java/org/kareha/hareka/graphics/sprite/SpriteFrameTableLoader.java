package org.kareha.hareka.graphics.sprite;

import java.awt.Graphics;
import java.awt.GraphicsConfiguration;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.imageio.ImageIO;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.kareha.hareka.annotation.Private;
import org.kareha.hareka.graphics.GraphicsConstants;
import org.kareha.hareka.util.JaxbUtil;

public class SpriteFrameTableLoader {

	private static final Logger logger = Logger.getLogger(SpriteFrameTableLoader.class.getName());
	private static final JAXBContext jaxbSpriteFrameTableXml;
	private static final JAXBContext jaxbSpriteFrameTablesXml;

	static {
		try {
			jaxbSpriteFrameTableXml = JAXBContext.newInstance(SpriteFrameTableXml.class);
			jaxbSpriteFrameTablesXml = JAXBContext.newInstance(SpriteFrameTablesXml.class);
		} catch (final JAXBException e) {
			throw new RuntimeException(e.getMessage(), e);
		}
	}

	private SpriteFrameTableLoader() {
		throw new AssertionError();
	}

	private static SpriteFrameTable load(final SpriteFrameTableXml o, final String directory,
			final GraphicsConfiguration gc) throws JAXBException {
		final Map<String, BufferedImage> imageCache = new HashMap<>();
		final NormalSpriteFrameTable table = new NormalSpriteFrameTable();

		for (final SpriteFrameXml frameXml : o.getFrames()) {
			BufferedImage image = imageCache.get(frameXml.filename);
			if (image == null) {
				final BufferedImage i;
				try (final InputStream in = SpriteFrameTableLoader.class.getClassLoader()
						.getResourceAsStream(directory + "/" + frameXml.filename)) {
					i = ImageIO.read(in);
				} catch (final IOException e) {
					logger.log(Level.SEVERE, "", e);
					return NullSpriteFrameTable.INSTANCE;
				}

				image = gc.createCompatibleImage(i.getWidth(), i.getHeight(), i.getTransparency());
				final Graphics g = image.getGraphics();
				g.drawImage(i, 0, 0, null);
				g.dispose();

				imageCache.put(frameXml.filename, image);
			}

			final int sx1;
			final int sx2;
			if (frameXml.horizontalFlip) {
				sx1 = frameXml.x + frameXml.width;
				sx2 = frameXml.x;
			} else {
				sx1 = frameXml.x;
				sx2 = frameXml.x + frameXml.width;
			}
			final int sy1;
			final int sy2;
			if (frameXml.verticalFlip) {
				sy1 = frameXml.y + frameXml.height;
				sy2 = frameXml.y;
			} else {
				sy1 = frameXml.y;
				sy2 = frameXml.y + frameXml.height;
			}
			final BufferedImage subImage = gc.createCompatibleImage(frameXml.width, frameXml.height,
					image.getTransparency());
			final Graphics g = subImage.getGraphics();
			g.drawImage(image, 0, 0, frameXml.width, frameXml.height, sx1, sy1, sx2, sy2, null);
			g.dispose();

			final SpriteFrame frame = new ImageSpriteFrame(subImage, frameXml.centerX,
					frameXml.centerY + frameXml.levitation, frameXml.centerZ - frameXml.levitation);
			table.putFrame(frameXml.id, frame);
		}

		return table;
	}

	public static SpriteFrameTable loadTable(final String directory, final String filename,
			final GraphicsConfiguration gc) {
		final SpriteFrameTableXml o;
		try (final InputStream in = SpriteFrameTableLoader.class.getClassLoader()
				.getResourceAsStream(directory + "/" + filename)) {
			if (in == null) {
				return NullSpriteFrameTable.INSTANCE;
			}
			try {
				o = JaxbUtil.unmarshal(in, jaxbSpriteFrameTableXml);
			} catch (final JAXBException e) {
				logger.log(Level.SEVERE, "", e);
				return NullSpriteFrameTable.INSTANCE;
			}
		} catch (final IOException e) {
			logger.log(Level.SEVERE, "", e);
			return NullSpriteFrameTable.INSTANCE;
		}
		try {
			return load(o, directory, gc);
		} catch (final JAXBException e) {
			logger.log(Level.SEVERE, "", e);
			return NullSpriteFrameTable.INSTANCE;
		}
	}

	public static Collection<SpriteFrameTable> loadTables(final String directory, final String filename,
			final GraphicsConfiguration gc) {
		final SpriteFrameTablesXml o;
		try (final InputStream in = SpriteFrameTableLoader.class.getClassLoader()
				.getResourceAsStream(directory + "/" + filename)) {
			if (in == null) {
				return Collections.emptyList();
			}
			try {
				o = JaxbUtil.unmarshal(in, jaxbSpriteFrameTablesXml);
			} catch (final JAXBException e) {
				logger.log(Level.SEVERE, "", e);
				return Collections.emptyList();
			}
		} catch (final IOException e) {
			logger.log(Level.SEVERE, "", e);
			return Collections.emptyList();
		}
		final List<SpriteFrameTable> tables = new ArrayList<>();
		for (final SpriteFrameTableXml se : o.getTables()) {
			if (se.type == null) {
				try {
					tables.add(SpriteFrameTableLoader.load(se, directory, gc));
				} catch (final JAXBException e) {
					e.printStackTrace();
					continue;
				}
			} else {
				switch (se.type) {
				default:
					try {
						tables.add(SpriteFrameTableLoader.load(se, directory, gc));
					} catch (final JAXBException e) {
						e.printStackTrace();
						continue;
					}
					break;
				case WALL:
					tables.addAll(makeSolid(directory, gc, se));
					break;
				case CHARACTER:
					tables.addAll(makeSolidCharacter(directory, gc, se));
					break;
				}
			}
		}
		return tables;
	}

	private static Collection<SpriteFrameTable> makeSolid(final String directory, final GraphicsConfiguration gc,
			final SpriteFrameTableXml o) {
		int maxHeight = 0;
		for (final SpriteFrameXml f : o.frames) {
			if (f.height > maxHeight) {
				maxHeight = f.height;
			}
		}
		final List<SpriteFrameTable> tables = new ArrayList<>();
		for (int i = 0, bottom = maxHeight; bottom > 0; i++, bottom -= GraphicsConstants.TILE_DEPTH) {
			final SpriteFrameTableXml se = new SpriteFrameTableXml();
			se.frames = new ArrayList<>();
			for (final SpriteFrameXml f : o.frames) {
				final int tmpY = f.height - (i + 1) * GraphicsConstants.TILE_DEPTH;
				final SpriteFrameXml slice = new SpriteFrameXml(f);
				if (tmpY <= 0) {
					slice.y = f.y;
					slice.height = GraphicsConstants.TILE_DEPTH + tmpY;
					if (slice.height > 0) {
						slice.centerY = f.centerY - (slice.y - f.y);
						slice.centerZ = f.centerZ - GraphicsConstants.TILE_DEPTH * i;
						se.frames.add(slice);
					}
				} else {
					slice.y = f.y + tmpY;
					slice.height = GraphicsConstants.TILE_DEPTH;
					slice.centerY = f.centerY - (slice.y - f.y);
					slice.centerZ = f.centerZ - GraphicsConstants.TILE_DEPTH * i;
					se.frames.add(slice);
				}
			}
			try {
				tables.add(SpriteFrameTableLoader.load(se, directory, gc));
			} catch (final JAXBException e) {
				e.printStackTrace();
				continue;
			}
		}
		return tables;
	}

	private static Collection<SpriteFrameTable> makeSolidCharacter(final String directory,
			final GraphicsConfiguration gc, final SpriteFrameTableXml o) {
		int maxUpperHeight = 0;
		int maxLowerHeight = 0;
		for (final SpriteFrameXml f : o.frames) {
			if (f.centerY > maxUpperHeight) {
				maxUpperHeight = f.centerY;
			}
			if (f.height - f.centerY > maxLowerHeight) {
				maxLowerHeight = f.height - f.centerY;
			}
		}
		final List<SpriteFrameTable> tables = new ArrayList<>();
		for (int i = 0, bottom = maxUpperHeight; bottom > 0; i++, bottom -= GraphicsConstants.TILE_DEPTH) {
			final SpriteFrameTableXml se = new SpriteFrameTableXml();
			se.frames = new ArrayList<>();
			for (final SpriteFrameXml f : o.frames) {
				final int tmpY = f.centerY - (i + 1) * GraphicsConstants.TILE_DEPTH;
				final SpriteFrameXml slice = new SpriteFrameXml(f);
				if (tmpY <= 0) {
					slice.y = f.y;
					slice.height = GraphicsConstants.TILE_DEPTH + tmpY;
					if (slice.height > 0) {
						if (f.verticalFlip) {
							slice.centerY = f.centerY - f.height + tmpY;
						} else {
							slice.centerY = f.centerY;
						}
						slice.centerZ = f.centerZ - GraphicsConstants.TILE_DEPTH * i;
						se.frames.add(slice);
					}
				} else {
					slice.y = f.y + tmpY;
					slice.height = GraphicsConstants.TILE_DEPTH;
					if (f.verticalFlip) {
						slice.centerY = f.centerY - f.height + tmpY;
					} else {
						slice.centerY = f.centerY - tmpY;
					}
					slice.centerZ = f.centerZ - GraphicsConstants.TILE_DEPTH * i;
					se.frames.add(slice);
				}
			}
			try {
				tables.add(SpriteFrameTableLoader.load(se, directory, gc));
			} catch (final JAXBException e) {
				e.printStackTrace();
				continue;
			}
		}
		for (int i = 0, top = 0; top < maxLowerHeight; i++, top += GraphicsConstants.TILE_DEPTH) {
			final SpriteFrameTableXml se = new SpriteFrameTableXml();
			se.frames = new ArrayList<>();
			for (final SpriteFrameXml f : o.frames) {
				final int tmpY = f.centerY + i * GraphicsConstants.TILE_DEPTH;
				final SpriteFrameXml slice = new SpriteFrameXml(f);
				if (tmpY >= f.height - GraphicsConstants.TILE_DEPTH) {
					slice.y = f.y + tmpY;
					slice.height = f.height - tmpY;
					if (slice.height > 0) {
						if (f.verticalFlip) {
							slice.centerY = f.centerY - GraphicsConstants.TILE_DEPTH;
						} else {
							slice.centerY = f.centerY - tmpY;
						}
						slice.centerZ = f.centerZ - GraphicsConstants.TILE_DEPTH * (i + 1);
						se.frames.add(slice);
					}
				} else {
					slice.y = f.y + tmpY;
					slice.height = GraphicsConstants.TILE_DEPTH;
					if (f.verticalFlip) {
						slice.centerY = f.centerY - f.height + tmpY;
					} else {
						slice.centerY = f.centerY - tmpY;
					}
					slice.centerZ = f.centerZ - GraphicsConstants.TILE_DEPTH * (i + 1);
					se.frames.add(slice);
				}
			}
			try {
				tables.add(SpriteFrameTableLoader.load(se, directory, gc));
			} catch (final JAXBException e) {
				e.printStackTrace();
				continue;
			}
		}
		return tables;
	}

	@XmlRootElement(name = "sprites")
	@XmlAccessorType(XmlAccessType.NONE)
	private static class SpriteFrameTablesXml {

		@XmlElement(name = "sprite")
		private List<SpriteFrameTableXml> tables = new ArrayList<>();

		Collection<SpriteFrameTableXml> getTables() {
			return tables;
		}

	}

	private enum SpriteType {
		PLAIN, WALL, CHARACTER,
	}

	@XmlRootElement(name = "sprite")
	@XmlAccessorType(XmlAccessType.NONE)
	private static class SpriteFrameTableXml {

		@XmlElement
		SpriteType type;
		@XmlElement(name = "frame")
		List<SpriteFrameXml> frames;

		@Private
		SpriteFrameTableXml() {

		}

		Collection<SpriteFrameXml> getFrames() {
			return frames;
		}

	}

	@XmlAccessorType(XmlAccessType.NONE)
	private static class SpriteFrameXml {

		@SuppressWarnings("unused")
		private SpriteFrameXml() {
			// used by JAXB
		}

		SpriteFrameXml(final SpriteFrameXml original) {
			id = original.id;
			filename = original.filename;
			x = original.x;
			y = original.y;
			width = original.width;
			height = original.height;
			horizontalFlip = original.horizontalFlip;
			verticalFlip = original.verticalFlip;
			centerX = original.centerX;
			centerY = original.centerY;
			centerZ = original.centerZ;
			levitation = original.levitation;
		}

		@XmlElement
		@Private
		String id;
		@XmlElement
		@Private
		String filename;
		@XmlElement
		@Private
		int x;
		@XmlElement
		@Private
		int y;
		@XmlElement
		@Private
		int width;
		@XmlElement
		@Private
		int height;
		@XmlElement
		@Private
		boolean horizontalFlip;
		@XmlElement
		@Private
		boolean verticalFlip;
		@XmlElement
		@Private
		int centerX;
		@XmlElement
		@Private
		int centerY;
		@XmlElement
		@Private
		int centerZ;
		@XmlElement
		@Private
		int levitation;

	}

}
