package org.kareha.hareka.graphics.tile;

import java.awt.Point;

import org.kareha.hareka.field.Vector;
import org.kareha.hareka.graphics.GraphicsConstants;
import org.kareha.hareka.graphics.sprite.Screen;
import org.kareha.hareka.graphics.sprite.SpriteArray;

public final class TileSprite {

	public enum Part {
		TOP, WALL,
	}

	private TileTemplate template;
	private SpriteArray spriteArray;
	private Vector position;
	float phase;
	private String frameId;
	private final Part part;
	private final ViewDirection direction;

	private static int deltaDepth(final ViewDirection direction) {
		switch (direction) {
		default:
			return 0;
		case DOWN:
			return 12;
		case DOWN_LEFT:
		case DOWN_RIGHT:
			return 6;
		case UP_LEFT:
		case UP_RIGHT:
			return -6;
		case UP:
			return -12;
		}
	}

	public TileSprite(final TileTemplate template, final SpriteArray spriteArray, final Vector position,
			final int elevation, final Part part, final ViewDirection direction) {
		this.template = template;
		this.spriteArray = spriteArray;
		this.position = position;
		this.part = part;
		this.direction = direction;

		final Point p = toView(position);
		final int d = elevation * GraphicsConstants.TILE_DEPTH;
		spriteArray.setPosition(p.x, p.y - d, p.y + d - deltaDepth(direction));
		frameId = makeFrameId();
		spriteArray.setFrame(frameId);
	}

	private TileSprite(final TileTemplate template, final SpriteArray spriteArray, final Vector position,
			final int elevation, final Part part, final ViewDirection direction, final int bottom) {
		this.template = template;
		this.spriteArray = spriteArray;
		this.position = position;
		this.part = part;
		this.direction = direction;

		final Point p = toView(position);
		final int d = elevation * GraphicsConstants.TILE_DEPTH;
		final int b = bottom * GraphicsConstants.TILE_DEPTH;
		spriteArray.setPosition(p.x, p.y - d, p.y + b - deltaDepth(direction));
		frameId = makeFrameId();
		spriteArray.setFrame(frameId);
	}

	public TileSprite createWall(final int elevation, final int bottom) {
		return new TileSprite(template, new SpriteArray(spriteArray), position, elevation, Part.WALL, direction,
				bottom);
	}

	private int patternParity(final Pattern pattern, final Vector position) {
		switch (pattern) {
		default:
			return 0;
		case ONE_UNIFORM:
			return threeColorIndex(position);
		}
	}

	private int threeColorIndex(final Vector v) {
		return Math.floorMod(v.x() / 3 - v.y() / 3, 3);
	}

	public void animate(final int v) {
		final String id = makeFrameId();
		if (!id.equals(frameId)) {
			spriteArray.setFrame(id);
			frameId = id;
		}

		phase += v / 1000f;
	}

	private static Point toView(final Vector position) {
		int vx = GraphicsConstants.TILE_WIDTH * position.x() / 3 + GraphicsConstants.TILE_WIDTH / 2 * position.y() / 3;
		int vy = GraphicsConstants.TILE_HEIGHT * position.y() / 3;
		return new Point(vx, vy);
	}

	public void addTo(final Screen screen) {
		spriteArray.addTo(screen);
	}

	public void removeFrom(final Screen screen) {
		spriteArray.removeFrom(screen);
	}

	private String makeFrameId() {
		final String parity = Integer.toString(patternParity(template.pattern(), position));
		final int frameNo = template.getFrameNumber(phase);
		return part.name() + " " + direction.name() + " " + parity + " " + frameNo;
	}

	public SpriteArray getSpriteArray() {
		return spriteArray;
	}

}
