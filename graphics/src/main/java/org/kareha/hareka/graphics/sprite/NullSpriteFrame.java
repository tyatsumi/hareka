package org.kareha.hareka.graphics.sprite;

import java.awt.Graphics;
import java.awt.Rectangle;

public final class NullSpriteFrame implements SpriteFrame {

	public static final SpriteFrame INSTANCE = new NullSpriteFrame();

	private NullSpriteFrame() {
		// do nothing
	}

	@Override
	public int getCenterX() {
		return 0;
	}

	@Override
	public int getCenterY() {
		return 0;
	}

	@Override
	public int getCenterZ() {
		return 0;
	}

	@Override
	public void paint(final Graphics g, final int x, final int y) {
		// do nothing
	}

	@Override
	public boolean contains(final int x, final int y) {
		return false;
	}

	@Override
	public Rectangle getBounds() {
		return new Rectangle(0, 0, -1, -1);
	}

}
