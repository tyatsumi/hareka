package org.kareha.hareka.graphics.sprite;

import java.awt.Graphics;
import java.awt.Rectangle;

public class Sprite {

	private final SpriteFrameTable table;
	private final Object object;
	private SpriteFrame frame = NullSpriteFrame.INSTANCE;
	private int x;
	private int y;
	private int z;

	public Sprite(final SpriteFrameTable table, final Object object) {
		this.table = table;
		this.object = object;
	}

	public Sprite(final Sprite original) {
		table = original.table;
		object = original.object;
		frame = original.frame;
		x = original.x;
		y = original.y;
		z = original.z;
	}

	public SpriteFrameTable getFrameTable() {
		return table;
	}

	public Object getObject() {
		return object;
	}

	public SpriteFrame getFrame() {
		return frame;
	}

	public void setFrame(final String id) {
		frame = table.getFrame(id);
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public int getZ() {
		return z;
	}

	public void setPosition(final int x, final int y, final int z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}

	public int getFrameX() {
		return z - frame.getCenterX();
	}

	public int getFrameY() {
		return y - frame.getCenterY();
	}

	public int getFrameZ() {
		return z - frame.getCenterZ();
	}

	public void paint(final Graphics g, int ox, int oy) {
		frame.paint(g, x - ox, y - oy);
	}

	public boolean contains(final int x, final int y) {
		return frame.contains(x - this.x, y - this.y);
	}

	public Rectangle getBounds() {
		final Rectangle b = frame.getBounds();
		b.translate(x, y);
		return b;
	}

}
