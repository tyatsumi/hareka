package org.kareha.hareka.updater;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.kareha.hareka.util.Parameters;

public class UpdaterParameters {

	private final boolean update;
	private final File dataDirectory;
	private final File clientFile;
	private final URL clientUrl;
	private final List<String> clientArgs;

	public UpdaterParameters(final String[] args) throws MalformedURLException {
		final Parameters parameters = new Parameters(args);

		update = parameters.unnamed().contains("--update");

		final String datadirOption = parameters.named().get("datadir");
		if (datadirOption != null) {
			dataDirectory = new File(datadirOption);
		} else {
			dataDirectory = UpdaterDefaults.DATA_DIRECTORY;
		}

		final String pathOption = parameters.named().get("path");
		if (pathOption != null) {
			clientFile = new File(pathOption);
		} else {
			clientFile = UpdaterDefaults.CLIENT_FILE;
		}

		final String urlOption = parameters.named().get("url");
		if (urlOption != null) {
			clientUrl = new URL(urlOption);
		} else {
			clientUrl = UpdaterDefaults.CLIENT_URL;
		}

		clientArgs = new ArrayList<>(parameters.passingThroughRaw());
		if (clientArgs.isEmpty() && datadirOption != null) {
			clientArgs.add("--datadir=" + datadirOption);
		}
	}

	public boolean update() {
		return update;
	}

	public File dataDirectory() {
		return dataDirectory;
	}

	public File clientFile() {
		return clientFile;
	}

	public URL clientUrl() {
		return clientUrl;
	}

	public List<String> clientArgs() {
		return Collections.unmodifiableList(clientArgs);
	}

}
