package org.kareha.hareka.updater;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

import org.kareha.hareka.annotation.SynchronizedWith;

public final class UpdaterDefaults {

	private UpdaterDefaults() {
		throw new AssertionError();
	}

	@SynchronizedWith("org.kareha.hareka.client.ClientConstants")
	public static final File DATA_DIRECTORY = new File(System.getProperty("user.dir"), "hareka_data");

	public static final File CLIENT_FILE = new File(System.getProperty("user.dir"), "hareka.jar");
	public static final URL CLIENT_URL;

	static {
		try {
			CLIENT_URL = new URL("https://hareka.kareha.org/hareka.jar");
		} catch (final MalformedURLException e) {
			throw new AssertionError(e);
		}
	}

}
