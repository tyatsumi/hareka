package org.kareha.hareka.updater;

import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.SwingUtilities;

import org.kareha.hareka.logging.FileLogger;
import org.kareha.hareka.logging.SimpleLogger;
import org.kareha.hareka.ui.UiType;
import org.kareha.hareka.util.FileUtil;
import org.kareha.hareka.util.JarRunner;
import org.kareha.hareka.util.Locker;

final class Main {

	private static final Logger logger = Logger.getLogger(Main.class.getName());
	private static final Logger appRootLogger = Logger.getLogger("org.kareha.hareka");

	private Main() {
		throw new AssertionError();
	}

	public static void main(final String[] args) {
		try {
			final UpdaterParameters parameters = new UpdaterParameters(args);

			FileUtil.ensureDirectoryExistsOrExit(parameters.dataDirectory(), UiType.SWING);

			if (!parameters.update() && parameters.clientFile().isFile()) {
				logger.info("Run existing client JAR");
				JarRunner.runJar(parameters.clientFile(), parameters.clientArgs());
				return;
			}

			Locker.INSTANCE.lock(parameters.dataDirectory(), true, UiType.SWING);

			SimpleLogger.INSTANCE.addStream(System.err);
			FileLogger.connect(appRootLogger, new File(parameters.dataDirectory(), "log"), UiType.SWING);

			SwingUtilities.invokeLater(() -> {
				new UpdaterFrame(parameters).downloadAndRun();
			});
		} catch (final Exception e) {
			logger.log(Level.SEVERE, "", e);
			UiType.SWING.showMessage(e.getMessage());
			return;
		}
	}

}
