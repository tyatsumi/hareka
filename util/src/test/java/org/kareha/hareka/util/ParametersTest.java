package org.kareha.hareka.util;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

public class ParametersTest {

	private static final String[] raw = { "foo", "--bar=buz", "--qux", "--test=hello=world" };
	private static final String[] unnamed = { "foo", "--qux", };
	private static final Map<String, String> named;
	static {
		named = new HashMap<>();
		named.put("bar", "buz");
		named.put("test", "hello=world");
	}

	public static void main(final String[] args) {
		final Parameters parameters = new Parameters(raw);
		for (final String a : parameters.unnamed()) {
			System.out.println(a);
		}
	}

	@Test
	public void testRaw() {
		final Parameters parameters = new Parameters(raw);
		final String[] result = parameters.raw().toArray(new String[0]);
		assertArrayEquals(raw, result);
	}

	@Test
	public void testUnnamed() {
		final Parameters parameters = new Parameters(raw);
		final String[] result = parameters.unnamed().toArray(new String[0]);
		assertArrayEquals(unnamed, result);
	}

	@Test
	public void testNamed() {
		final Parameters parameters = new Parameters(raw);
		assertTrue(parameters.named().equals(named));
	}

	@Test
	public void testNull() {
		final String[] args = { null, };
		final Parameters parameters = new Parameters(args);
		assertTrue(parameters.raw().size() == 0);
	}

	@Test
	public void testEmpty() {
		final String[] args = { "", };
		final Parameters parameters = new Parameters(args);
		assertTrue(parameters.raw().size() == 1);
		assertTrue(parameters.raw().get(0).isEmpty());
	}

}
