package org.kareha.hareka.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.kareha.hareka.annotation.GuardedBy;
import org.kareha.hareka.ui.UiType;

public enum Locker {

	INSTANCE;

	private static final Logger logger = Logger.getLogger(Locker.class.getName());

	private enum BundleKey {
		AlreadyRunning,
	}

	private static final String filename = "lock";

	@GuardedBy("this")
	private boolean invoked;
	@GuardedBy("this")
	private FileLock lock;

	@SuppressWarnings("resource")
	public synchronized void lock(final File dataDirectory, final boolean wait, final UiType uiType) {
		if (dataDirectory == null || uiType == null) {
			throw new IllegalArgumentException("null");
		}
		if (invoked) {
			throw new IllegalStateException("Already invoked");
		}
		invoked = true;
		final File file = new File(dataDirectory, filename);
		final FileOutputStream out;
		try {
			out = new FileOutputStream(file);
		} catch (final FileNotFoundException e) {
			logger.log(Level.SEVERE, "", e);
			die(e.getMessage(), uiType);
			return;
		}
		final FileChannel channel = out.getChannel();
		if (wait) {
			try {
				lock = channel.lock();
			} catch (final IOException e) {
				logger.log(Level.SEVERE, "", e);
				die(e.getMessage(), uiType);
			}
		} else {
			try {
				lock = channel.tryLock();
			} catch (final IOException e) {
				logger.log(Level.SEVERE, "", e);
				die(e.getMessage(), uiType);
				return;
			}
		}
		if (lock != null) {
			return;
		}
		final ResourceBundle bundle = ResourceBundle.getBundle(Locker.class.getName());
		die(bundle.getString(BundleKey.AlreadyRunning.name()), uiType);
	}

	public synchronized void unlock() {
		if (!invoked) {
			throw new IllegalStateException("Not locked yet");
		}
		try {
			lock.close();
		} catch (final IOException e) {
			logger.log(Level.WARNING, "", e);
			return;
		}
	}

	private static void die(final String message, final UiType uiType) {
		uiType.showMessage(message);
		System.exit(1);
	}

}
