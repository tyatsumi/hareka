package org.kareha.hareka.util;

import java.io.File;
import java.io.IOException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.kareha.hareka.ui.UiType;

public class Wiper {

	private static final Logger logger = Logger.getLogger(Wiper.class.getName());

	private enum BundleKey {
		ReallyDeleteData,
	}

	public static void confirmAndWipe(final File dataDirectory, final UiType uiType) {
		final ResourceBundle bundle = ResourceBundle.getBundle(Wiper.class.getName());
		if (!uiType.confirm(bundle.getString(BundleKey.ReallyDeleteData.name()))) {
			return;
		}
		logger.info("Wiping data..");
		try {
			FileUtil.deleteRecursively(dataDirectory);
		} catch (final IOException e) {
			logger.log(Level.SEVERE, "Cannot delete data directory", e);
			return;
		}
		logger.info("Data wiped");
	}

}
