package org.kareha.hareka.util;

import java.io.File;
import java.io.IOException;
import java.nio.file.CopyOption;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.StandardCopyOption;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.PosixFilePermission;
import java.util.Comparator;
import java.util.EnumSet;
import java.util.ResourceBundle;
import java.util.Set;

import org.kareha.hareka.ui.UiType;

public final class FileUtil {

	private enum BundleKey {
		CannotCreateDirectory,
	}

	private FileUtil() {
		throw new AssertionError();
	}

	public static boolean ensureDirectoryExists(final File directory) throws IOException {
		if (directory.isDirectory()) {
			return false;
		}
		if (directory.exists()) {
			throw new IOException("Cannot create directory");
		}
		if (!directory.mkdirs()) {
			throw new IOException("Cannot create directory");
		}
		return true;
	}

	private static void helperExit(final UiType uiType) {
		final ResourceBundle bundle = ResourceBundle.getBundle(FileUtil.class.getName());
		uiType.showMessage(bundle.getString(BundleKey.CannotCreateDirectory.name()));
		System.exit(1);
	}

	public static void ensureDirectoryExistsOrExit(final File directory, UiType uiType) {
		try {
			ensureDirectoryExists(directory);
		} catch (final IOException e) {
			helperExit(uiType);
			return;
		}
	}

	public static boolean setPermissionReadableAndWritableOnlyByOwner(final File file) throws IOException {
		file.createNewFile();
		if (!file.isFile()) {
			return false;
		}
		try {
			final Path path = file.toPath();
			final Set<PosixFilePermission> perms = EnumSet.of(PosixFilePermission.OWNER_READ,
					PosixFilePermission.OWNER_WRITE);
			Files.setPosixFilePermissions(path, perms);
			final Set<PosixFilePermission> resultPerms = Files.getPosixFilePermissions(path);
			return resultPerms.size() == 2 && resultPerms.contains(PosixFilePermission.OWNER_READ)
					&& resultPerms.contains(PosixFilePermission.OWNER_WRITE);
		} catch (final UnsupportedOperationException e) {
			return false;
		}
	}

	public static void deleteRecursively(final File directory) throws IOException {
		Files.walk(directory.toPath()).sorted(Comparator.reverseOrder()).map(Path::toFile).forEach(File::delete);
	}

	private static class CustomFileVisitor extends SimpleFileVisitor<Path> {

		private Path source;
		private Path target;
		private CopyOption[] options;

		CustomFileVisitor(final Path source, final Path target, final CopyOption... options) {
			this.target = target;
			this.source = source;
			this.options = options;
		}

		@Override
		public FileVisitResult preVisitDirectory(final Path dir, final BasicFileAttributes attrs) throws IOException {
			Files.createDirectories(target.resolve(source.relativize(dir)));
			return FileVisitResult.CONTINUE;
		}

		@Override
		public FileVisitResult visitFile(final Path file, final BasicFileAttributes attrs) throws IOException {
			Files.copy(file, target.resolve(source.relativize(file)), options);
			return FileVisitResult.CONTINUE;
		}
	}

	public static void copyRecursively(final File source, final File target) throws IOException {
		final CustomFileVisitor customFileVisitor = new CustomFileVisitor(source.toPath(), target.toPath(),
				StandardCopyOption.REPLACE_EXISTING);
		Files.walkFileTree(source.toPath(), customFileVisitor);
	}

}
