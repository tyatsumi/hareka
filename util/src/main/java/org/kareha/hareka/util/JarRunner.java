package org.kareha.hareka.util;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.kareha.hareka.annotation.NotPure;

public final class JarRunner {

	private static final String JAVA_PATH = System.getProperty("java.home") + File.separator + "bin" + File.separator
			+ "java";

	private JarRunner() {
		throw new AssertionError();
	}

	@NotPure
	public static void runJar(final File file, final List<String> args) throws IOException {
		final List<String> list = new ArrayList<>();
		list.add(JAVA_PATH);
		list.add("-jar");
		list.add(file.getAbsolutePath());
		list.addAll(args);
		new ProcessBuilder(list).directory(new File(System.getProperty("user.dir"))).start();
	}

}
