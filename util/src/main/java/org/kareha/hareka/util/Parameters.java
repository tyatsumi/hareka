package org.kareha.hareka.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Parameters {

	private final List<String> args;

	private final List<String> raw = new ArrayList<>();
	private final List<String> unnamed = new ArrayList<>();
	private final Map<String, String> named = new HashMap<>();

	private final List<String> passingThroughRaw = new ArrayList<>();
	private final List<String> passingThroughUnnamed = new ArrayList<>();
	private final Map<String, String> passingThroughNamed = new HashMap<>();

	public Parameters(final String[] args) {
		this(Arrays.asList(args));
	}

	private Parameters(final List<String> args) {
		this.args = args;

		final Pattern pattern = Pattern.compile("^--(.+?)=(.*)$");
		boolean passThrough = false;

		for (final String a : args) {
			if (a == null) {
				continue;
			}

			if (a.equals("--")) {
				passThrough = true;
				continue;
			}

			if (passThrough) {
				passingThroughRaw.add(a);
				final Matcher m = pattern.matcher(a);
				if (m.matches()) {
					passingThroughNamed.put(m.group(1), m.group(2));
				} else {
					passingThroughUnnamed.add(a);
				}
			} else {
				raw.add(a);
				final Matcher m = pattern.matcher(a);
				if (m.matches()) {
					named.put(m.group(1), m.group(2));
				} else {
					unnamed.add(a);
				}
			}
		}
	}

	public List<String> args() {
		return args;
	}

	public List<String> raw() {
		return Collections.unmodifiableList(raw);
	}

	public List<String> unnamed() {
		return Collections.unmodifiableList(unnamed);
	}

	public Map<String, String> named() {
		return Collections.unmodifiableMap(named);
	}

	public List<String> passingThroughRaw() {
		return Collections.unmodifiableList(passingThroughRaw);
	}

	public List<String> passingThroughUnnamed() {
		return Collections.unmodifiableList(passingThroughUnnamed);
	}

	public Map<String, String> passingThroughNamed() {
		return Collections.unmodifiableMap(passingThroughNamed);
	}

}
