package org.kareha.hareka.ui;

import java.awt.HeadlessException;
import java.lang.reflect.InvocationTargetException;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import org.kareha.hareka.util.Parameters;

public enum UiType {

	SWING, CONSOLE,;

	private static final Logger logger = Logger.getLogger(UiType.class.getName());

	public static UiType parse(final Parameters parameters) {
		final String uiOption = parameters.named().get("ui");
		if (uiOption == null || uiOption.equals("swing")) {
			return UiType.SWING;
		} else if (uiOption.equals("console")) {
			return UiType.CONSOLE;
		} else {
			throw new IllegalArgumentException("Unknown UI option: ui=" + uiOption);
		}
	}

	public void showMessage(final String message) {
		switch (this) {
		default:
			throw new IllegalStateException("Unknown UI type");
		case SWING:
			try {
				SwingUtilities.invokeAndWait(() -> {
					JOptionPane.showMessageDialog(null, message);
				});
			} catch (final HeadlessException | InvocationTargetException e) {
				logger.log(Level.SEVERE, "", e);
			} catch (final InterruptedException e) {
				Thread.currentThread().interrupt();
				logger.log(Level.SEVERE, "", e);
			}
			break;
		case CONSOLE:
			System.out.println(message);
			break;
		}
	}

	public boolean confirm(final String message) {
		switch (this) {
		default:
			throw new IllegalStateException("Unknown UI tye");
		case SWING:
			final AtomicInteger result = new AtomicInteger(JOptionPane.NO_OPTION);
			try {
				SwingUtilities.invokeAndWait(() -> {
					result.set(JOptionPane.showConfirmDialog(null, message));
				});
			} catch (final HeadlessException | InvocationTargetException e) {
				logger.log(Level.SEVERE, "", e);
				return false;
			} catch (final InterruptedException e) {
				Thread.currentThread().interrupt();
				logger.log(Level.SEVERE, "", e);
				return false;
			}
			return result.get() == JOptionPane.YES_OPTION;
		case CONSOLE:
			// XXX not implemented yet
			return false;
		}
	}

}
