package org.kareha.hareka.logging;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import java.util.logging.StreamHandler;

import org.kareha.hareka.ui.UiType;
import org.kareha.hareka.util.FileUtil;

public final class FileLogger {

	private static final Logger logger = Logger.getLogger(FileLogger.class.getName());

	private static final String filenameExtension = ".txt";
	private static final String simpleFilenameExtension = "s.txt";
	// keep strong reference to keep the application root logger from being
	// garbage collected
	@SuppressWarnings("unused")
	private static Logger appRootLogger;
	private static final String DATE_FORMAT = "yyyyMMdd_HHmmss";

	private FileLogger() {
		throw new AssertionError();
	}

	private static class FileAndDate {
		File file;
		Date date;

		FileAndDate(final File file, final Date date) {
			this.file = file;
			this.date = date;
		}
	}

	private static FileAndDate createFile(final File directory, final Date date, final String postfix)
			throws InterruptedException {
		Date d = date;
		final DateFormat format = new SimpleDateFormat(DATE_FORMAT);
		File file;
		while (true) {
			file = new File(directory, format.format(d) + postfix);
			if (!file.exists()) {
				return new FileAndDate(file, d);
			}
			Thread.sleep(100);
			d = new Date();
		}
	}

	@SuppressWarnings("resource")
	public static void connect(final Logger appRootLogger, final File logDirectory, final UiType uiType) {
		FileLogger.appRootLogger = appRootLogger;
		try {
			FileUtil.ensureDirectoryExists(logDirectory);
		} catch (final IOException e) {
			logger.log(Level.WARNING, "", e);
			uiType.showMessage(e.getMessage());
			return;
		}

		//
		// for java.util.logging
		//

		final FileAndDate fileAndDate;
		try {
			fileAndDate = createFile(logDirectory, new Date(), filenameExtension);
		} catch (final InterruptedException e) {
			Thread.currentThread().interrupt();
			return;
		}

		final OutputStream out;
		try {
			out = new FileOutputStream(fileAndDate.file);
		} catch (final FileNotFoundException e) {
			logger.log(Level.WARNING, "", e);
			uiType.showMessage(e.getMessage());
			return;
		}
		final Handler handler = new StreamHandler(out, new SimpleFormatter()) {
			@Override
			public synchronized void publish(final LogRecord record) {
				super.publish(record);
				flush();
			}
		};
		final Level rootLevel = appRootLogger.getLevel();
		if (rootLevel != null) {
			handler.setLevel(rootLevel);
		}
		appRootLogger.addHandler(handler);

		//
		// for SimpleLogger
		//

		final FileAndDate xFileAndDate;
		try {
			xFileAndDate = createFile(logDirectory, fileAndDate.date, simpleFilenameExtension);
		} catch (final InterruptedException e) {
			Thread.currentThread().interrupt();
			return;
		}

		final PrintStream xout;
		try {
			xout = new PrintStream(new FileOutputStream(xFileAndDate.file));
		} catch (final FileNotFoundException e) {
			logger.log(Level.WARNING, "", e);
			uiType.showMessage(e.getMessage());
			return;
		}

		SimpleLogger.INSTANCE.addStream(xout);
	}

}
