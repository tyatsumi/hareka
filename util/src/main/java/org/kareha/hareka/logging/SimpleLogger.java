package org.kareha.hareka.logging;

import java.io.PrintStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import org.kareha.hareka.annotation.GuardedBy;
import org.kareha.hareka.annotation.Singleton;
import org.kareha.hareka.annotation.ThreadSafe;

/**
 * A simple logger to use when java.util.logging framework does not work.
 */
@ThreadSafe
@Singleton
public enum SimpleLogger {

	INSTANCE;

	private static class Logger {

		// OutputStream is synchronized with itself
		private final PrintStream out;
		// SimpleDateFormat is not thread-safe
		private final DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		Logger(final PrintStream out) {
			this.out = out;
		}

		@GuardedBy("out")
		private String header() {
			final StackTraceElement s = Thread.currentThread().getStackTrace()[3];
			return format.format(new Date()) + " " + s.getClassName() + " " + s.getMethodName();
		}

		void log(final String message) {
			synchronized (out) {
				out.println(header());
				out.println(message);
			}
			out.flush();
		}

		void log(final Throwable t) {
			synchronized (out) {
				out.println(header());
				t.printStackTrace(out);
			}
			out.flush();
		}

	}

	private final List<Logger> loggers = new CopyOnWriteArrayList<>();

	public void addStream(final PrintStream out) {
		loggers.add(new Logger(out));
	}

	public void log(final String message) {
		for (final Logger logger : loggers) {
			logger.log(message);
		}
	}

	public void log(final Throwable t) {
		for (final Logger logger : loggers) {
			logger.log(t);
		}
	}

}
