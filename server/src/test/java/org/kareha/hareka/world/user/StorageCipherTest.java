package org.kareha.hareka.world.user;

import static org.junit.Assert.assertArrayEquals;

import java.io.IOException;
import java.security.SecureRandom;

import org.junit.Test;
import org.kareha.hareka.protocol.ProtocolException;
import org.kareha.hareka.server.user.StorageCipher;

public class StorageCipherTest {

	@Test
	public void testEncrypt() throws IOException, ProtocolException {
		final StorageCipher sc = new StorageCipher();
		final byte[] plain = new byte[32];
		new SecureRandom().nextBytes(plain);
		final byte[] toBeEncrypted = new byte[32];
		new SecureRandom().nextBytes(toBeEncrypted);
		final byte[] encrypted = sc.encrypt(plain, toBeEncrypted);
		assertArrayEquals(plain, StorageCipher.getPlain(encrypted));
		final byte[] decrypted = sc.decrypt(encrypted);
		assertArrayEquals(toBeEncrypted, decrypted);
	}

}
