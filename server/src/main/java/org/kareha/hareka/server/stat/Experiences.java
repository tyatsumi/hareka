package org.kareha.hareka.server.stat;

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.kareha.hareka.annotation.GuardedBy;
import org.kareha.hareka.annotation.Private;
import org.kareha.hareka.annotation.ThreadSafe;

@ThreadSafe
@XmlJavaTypeAdapter(Experiences.Adapter.class)
public class Experiences {

	@GuardedBy("this")
	@Private
	final Map<ExperienceType, Experience> map;

	public Experiences() {
		this(null);
	}

	@Private
	Experiences(final Map<ExperienceType, Experience> map) {
		if (map == null) {
			this.map = new EnumMap<>(ExperienceType.class);
		} else {
			this.map = new EnumMap<>(map);
		}
		final Set<ExperienceType> missings = EnumSet.allOf(ExperienceType.class);
		missings.removeAll(this.map.keySet());
		for (final ExperienceType type : missings) {
			this.map.put(type, new Experience());
		}
	}

	@XmlType(name = "entry")
	private static class Entry {

		@XmlAttribute
		@Private
		ExperienceType type;
		@XmlValue
		private long value;

		@SuppressWarnings("unused")
		private Entry() {
			// used by JAXB
		}

		@Private
		Entry(final ExperienceType type, final Experience experience) {
			this.type = type;
			synchronized (experience) {
				value = experience.value;
			}
		}

		@Private
		Experience getExperience() {
			return new Experience(value);
		}

	}

	@XmlType(name = "experiences")
	@XmlAccessorType(XmlAccessType.NONE)
	private static class Adapted {

		@XmlElement(name = "experience")
		private List<Entry> experienceEntries;

		@SuppressWarnings("unused")
		private Adapted() {
			// used by JAXB
		}

		@Private
		Adapted(final Experiences v) {
			experienceEntries = new ArrayList<>();
			synchronized (v) {
				for (final Map.Entry<ExperienceType, Experience> entry : v.map.entrySet()) {
					experienceEntries.add(new Entry(entry.getKey(), entry.getValue()));
				}
			}
		}

		@Private
		Experiences unmarshal() {
			final Map<ExperienceType, Experience> map = new EnumMap<>(ExperienceType.class);
			for (final Entry entry : experienceEntries) {
				map.put(entry.type, entry.getExperience());
			}
			return new Experiences(map);
		}

	}

	@Private
	Adapted marshal() {
		return new Adapted(this);
	}

	static class Adapter extends XmlAdapter<Adapted, Experiences> {

		@Override
		public Adapted marshal(final Experiences v) throws Exception {
			if (v == null) {
				return null;
			}
			return v.marshal();
		}

		@Override
		public Experiences unmarshal(final Adapted v) throws Exception {
			if (v == null) {
				return null;
			}
			return v.unmarshal();
		}

	}

	public synchronized Experience get(final ExperienceType type) {
		return map.get(type);
	}

}
