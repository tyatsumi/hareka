package org.kareha.hareka.server.handler;

import java.io.IOException;

import org.kareha.hareka.LocalEntityId;
import org.kareha.hareka.protocol.Handler;
import org.kareha.hareka.protocol.HandlerException;
import org.kareha.hareka.protocol.ProtocolException;
import org.kareha.hareka.protocol.ProtocolInput;
import org.kareha.hareka.server.entity.EntityId;
import org.kareha.hareka.server.entity.ItemEntity;
import org.kareha.hareka.server.external.Session;
import org.kareha.hareka.server.field.ServerFieldObject;
import org.kareha.hareka.server.game.Inventory;
import org.kareha.hareka.server.game.Player;

public final class DropItemHandler implements Handler<Session> {

	@Override
	public void handle(final ProtocolInput in, final Session session)
			throws IOException, ProtocolException, HandlerException {
		// read arguments
		final LocalEntityId itemLocalId = LocalEntityId.readFrom(in);
		final long count = in.readCompactULong();

		final Player player = session.getPlayer();
		if (player == null) {
			return;
		}
		final ServerFieldObject fo = player.getEntity().getFieldObject();
		if (fo == null) {
			return;
		}

		player.getEntity().stopMotion();

		final EntityId itemId = session.getUser().getItemEntityIdCipher().decrypt(itemLocalId);
		final Inventory inventory = player.getEntity().getInventory();
		final ItemEntity dropItem = inventory.pick(itemId, count);
		if (dropItem == null) {
			return;
		}
		dropItem.mount(session.getContext().fields(), fo.getField().getId(), fo.getPlacement());
	}

}
