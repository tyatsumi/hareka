package org.kareha.hareka.server.relationship;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.kareha.hareka.annotation.GuardedBy;
import org.kareha.hareka.annotation.Private;
import org.kareha.hareka.annotation.ThreadSafe;

@ThreadSafe
@XmlJavaTypeAdapter(SpeciesRelationships.Adapter.class)
public class SpeciesRelationships {

	@GuardedBy("this")
	@Private
	Map<String, Relationship> map;

	public SpeciesRelationships() {

	}

	@Private
	SpeciesRelationships(final Map<String, Relationship> map) {
		if (map != null) {
			this.map = new HashMap<>(map);
		}
	}

	@XmlType(name = "relationshipEntry")
	@XmlAccessorType(XmlAccessType.NONE)
	private static class RelationshipEntry {

		@XmlAttribute
		@Private
		String id;
		@XmlElement
		private long love;
		@XmlElement
		private long hate;

		@SuppressWarnings("unused")
		private RelationshipEntry() {
			// used by JAXB
		}

		RelationshipEntry(final String id, final Relationship relationship) {
			this.id = id;
			synchronized (relationship) {
				love = relationship.getLove();
				hate = relationship.getHate();
			}
		}

		@Private
		Relationship getRelationship() {
			return new Relationship(love, hate);
		}

	}

	@XmlType(name = "speciesRelationships")
	@XmlAccessorType(XmlAccessType.NONE)
	private static class Adapted {

		@XmlElement(name = "relationship")
		private List<RelationshipEntry> entries;

		@SuppressWarnings("unused")
		private Adapted() {
			// used by JAXB
		}

		@Private
		Adapted(final SpeciesRelationships v) {
			synchronized (v) {
				if (v.map != null) {
					entries = new ArrayList<>();
					for (final Map.Entry<String, Relationship> entry : v.map.entrySet()) {
						entries.add(new RelationshipEntry(entry.getKey(), entry.getValue()));
					}
				}
			}
		}

		@Private
		SpeciesRelationships unmarshal() {
			final Map<String, Relationship> map = new HashMap<>();
			for (final RelationshipEntry entry : entries) {
				map.put(entry.id, entry.getRelationship());
			}
			return new SpeciesRelationships(map);
		}

	}

	@Private
	Adapted marshal() {
		return new Adapted(this);
	}

	static class Adapter extends XmlAdapter<Adapted, SpeciesRelationships> {

		@Override
		public Adapted marshal(final SpeciesRelationships v) throws Exception {
			if (v == null) {
				return null;
			}
			return v.marshal();
		}

		@Override
		public SpeciesRelationships unmarshal(final Adapted v) throws Exception {
			if (v == null) {
				return null;
			}
			return v.unmarshal();
		}

	}

	@GuardedBy("this")
	private Relationship get(final String id) {
		if (map == null) {
			return null;
		}
		return map.get(id);
	}

	@GuardedBy("this")
	private void put(final String id, final Relationship v) {
		if (map == null) {
			map = new HashMap<>();
		}
		map.put(id, v);
	}

	public synchronized long getLove(final String id) {
		final Relationship relationship = get(id);
		if (relationship == null) {
			return 0;
		}
		return relationship.getLove();
	}

	public synchronized void addLove(final String id, final long v) {
		final Relationship relationship = get(id);
		if (relationship == null) {
			final Relationship newRelationship = new Relationship();
			newRelationship.addLove(v);
			put(id, newRelationship);
			return;
		}
		relationship.addLove(v);
	}

	public synchronized long getHate(final String id) {
		final Relationship relationship = get(id);
		if (relationship == null) {
			return 0;
		}
		return relationship.getHate();
	}

	public synchronized void addHate(final String id, final long v) {
		final Relationship relationship = get(id);
		if (relationship == null) {
			final Relationship newRelationship = new Relationship();
			newRelationship.addHate(v);
			put(id, newRelationship);
			return;
		}
		relationship.addHate(v);
	}

}
