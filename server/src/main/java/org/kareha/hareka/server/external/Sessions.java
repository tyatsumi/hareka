package org.kareha.hareka.server.external;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Logger;

import org.kareha.hareka.annotation.GuardedBy;
import org.kareha.hareka.annotation.ThreadSafe;

@ThreadSafe
public class Sessions {

	private static final Logger logger = Logger.getLogger(Sessions.class.getName());

	@GuardedBy("this")
	private final Set<Session> sessions = new HashSet<>();

	public synchronized boolean addSession(final Session session) {
		final boolean b = sessions.add(session);
		if (b) {
			logger.info(session.getStamp() + "Connected");
		} else {
			logger.warning(session.getStamp() + "Already connected");
		}
		return b;
	}

	public synchronized boolean removeSession(final Session session) {
		final boolean b = sessions.remove(session);
		if (b) {
			logger.info(session.getStamp() + "Disconnected");
		} else {
			logger.warning(session.getStamp() + "Already disconnected");
		}
		return b;
	}

	public synchronized Collection<Session> getSessions() {
		if (sessions.isEmpty()) {
			return Collections.emptyList();
		}
		return new ArrayList<>(sessions);
	}

	public synchronized int sizeOfSessions() {
		return sessions.size();
	}

}
