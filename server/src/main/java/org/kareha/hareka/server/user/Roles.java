package org.kareha.hareka.server.user;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBException;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.kareha.hareka.annotation.GuardedBy;
import org.kareha.hareka.annotation.Private;
import org.kareha.hareka.annotation.ThreadSafe;
import org.kareha.hareka.user.Permission;
import org.kareha.hareka.user.Role;
import org.kareha.hareka.util.JaxbUtil;

@ThreadSafe
public class Roles {

	private static final String DEFAULT_ID = "Default";

	private final File file;
	@GuardedBy("this")
	@Private
	final Map<String, Role> map = new HashMap<>();

	public Roles(final File file) throws JAXBException {
		this.file = file;

		final Adapted adapted = load();
		if (adapted == null) {
			final Role adminRole = new CustomRole("Admin", 32768, EnumSet.allOf(Permission.class));
			map.put(adminRole.getId(), adminRole);
			final Role editorRole = new CustomRole("Editor", 16384, EnumSet.of(Permission.FORCE_EDIT_TILE_FIELDS));
			map.put(editorRole.getId(), editorRole);
			final Role defaultRole = new CustomRole(DEFAULT_ID, 0, EnumSet.of(Permission.EDIT_TILE_FIELDS));
			map.put(defaultRole.getId(), defaultRole);
		} else {
			if (adapted.roles != null) {
				for (final Role role : adapted.roles) {
					map.put(role.getId(), role);
				}
			}
		}

		map.put(Role.SUPER.getId(), Role.SUPER);
	}

	@XmlRootElement(name = "roles")
	@XmlAccessorType(XmlAccessType.NONE)
	private static class Adapted {

		@XmlElement(name = "role")
		@Private
		List<CustomRole> roles;

		@SuppressWarnings("unused")
		private Adapted() {
			// used by JAXB
		}

		@Private
		Adapted(final Roles v) {
			roles = new ArrayList<>();
			synchronized (v) {
				for (final Role r : v.map.values()) {
					if (!(r instanceof CustomRole)) {
						continue;
					}
					roles.add((CustomRole) r);
				}
			}
		}

	}

	private Adapted load() throws JAXBException {
		if (!file.isFile()) {
			return null;
		}
		return JaxbUtil.unmarshal(file, Adapted.class);
	}

	public void save() throws JAXBException {
		JaxbUtil.marshal(new Adapted(this), file);
	}

	public synchronized Role get(final String id) {
		return map.get(id);
	}

	public boolean add(final Role role) {
		if (role.getId().equals(Role.SUPER.getId())) {
			return false;
		}
		synchronized (this) {
			map.put(role.getId(), role);
		}
		return true;
	}

	public Role remove(final String roleId) {
		if (roleId.equals(Role.SUPER.getId())) {
			return null;
		}
		if (roleId.equals(DEFAULT_ID)) {
			return null;
		}
		synchronized (this) {
			return map.remove(roleId);
		}
	}

	public synchronized Collection<Role> getRoles() {
		return new ArrayList<>(map.values());
	}

}
