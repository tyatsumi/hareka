package org.kareha.hareka.server.stat;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.EnumSet;
import java.util.List;
import java.util.Set;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.kareha.hareka.annotation.GuardedBy;
import org.kareha.hareka.annotation.ThreadSafe;
import org.kareha.hareka.game.ActiveSkillType;
import org.kareha.hareka.game.Name;
import org.kareha.hareka.game.PassiveSkillType;
import org.kareha.hareka.server.motion.MotionType;
import org.kareha.hareka.server.relationship.SpeciesRelationships;

@ThreadSafe
@XmlJavaTypeAdapter(AbstractSpecies.Adapter.class)
public abstract class AbstractSpecies implements Species {

	public static abstract class Builder<T extends Builder<T>> {

		protected Name name;
		protected List<String> shapes;
		protected List<ActiveSkillType> activeSkills;
		protected List<PassiveSkillType> passiveSkills;
		protected SpeciesRelationships relationships;
		protected MotionType motion;
		protected VisualType visualType;

		protected abstract T self();

		public T name(final Name v) {
			name = new Name(v);
			return self();
		}

		public T shapes(final List<String> v) {
			shapes = v;
			return self();
		}

		public T activeSkills(final List<ActiveSkillType> v) {
			activeSkills = v;
			return self();
		}

		public T passiveSkills(final List<PassiveSkillType> v) {
			passiveSkills = v;
			return self();
		}

		public T relationships(final SpeciesRelationships v) {
			relationships = v;
			return self();
		}

		public T motion(final MotionType v) {
			motion = v;
			return self();
		}

		public T visualType(final VisualType v) {
			visualType = v;
			return self();
		}

		public abstract AbstractSpecies build(String id);

	}

	protected final String id;
	protected final Name name;
	@GuardedBy("this")
	protected final List<String> shapes;
	@GuardedBy("this")
	protected final Set<ActiveSkillType> activeSkills;
	@GuardedBy("this")
	protected final Set<PassiveSkillType> passiveSkills;
	@GuardedBy("this")
	protected final SpeciesRelationships relationships;
	protected final MotionType motion;
	protected final VisualType visualType;

	protected AbstractSpecies(final String id, final Builder<?> builder) {
		this.id = id;
		name = builder.name;
		shapes = builder.shapes;
		if (builder.activeSkills == null) {
			activeSkills = EnumSet.noneOf(ActiveSkillType.class);
		} else {
			activeSkills = EnumSet.copyOf(builder.activeSkills);
		}
		if (builder.passiveSkills == null) {
			passiveSkills = EnumSet.noneOf(PassiveSkillType.class);
		} else {
			passiveSkills = EnumSet.copyOf(builder.passiveSkills);
		}
		relationships = builder.relationships;
		motion = builder.motion;
		if (builder.visualType == null) {
			visualType = VisualType.NORMAL;
		} else {
			visualType = builder.visualType;
		}
	}

	@XmlRootElement(name = "abstractSpecies")
	@XmlSeeAlso({ NormalSpecies.Adapted.class })
	@XmlAccessorType(XmlAccessType.NONE)
	protected static abstract class Adapted {

		@XmlAttribute
		protected String id;
		@XmlElement
		protected Name name;
		@XmlElement
		protected List<String> shape;
		@XmlElement(name = "activeSkill")
		protected List<ActiveSkillType> activeSkills;
		@XmlElement(name = "passiveSkill")
		protected List<PassiveSkillType> passiveSkills;
		@XmlElement
		protected SpeciesRelationships relationships;
		@XmlElement
		protected MotionType motion;
		@XmlElement(name = "visual")
		protected VisualType visualType;

		protected Adapted() {
			// used by JAXB
		}

		protected Adapted(final AbstractSpecies v) {
			id = v.id;
			name = new Name(v.name);
			synchronized (v) {
				if (v.shapes != null) {
					shape = new ArrayList<>(v.shapes);
				}
				if (v.activeSkills != null) {
					activeSkills = new ArrayList<>(v.activeSkills);
				}
				if (v.passiveSkills != null) {
					passiveSkills = new ArrayList<>(v.passiveSkills);
				}
			}
			relationships = v.relationships;
			motion = v.motion;
			visualType = v.visualType;
		}

		protected abstract AbstractSpecies unmarshal();

	}

	protected abstract Adapted marshal();

	static class Adapter extends XmlAdapter<Adapted, AbstractSpecies> {

		@Override
		public Adapted marshal(final AbstractSpecies v) throws Exception {
			if (v == null) {
				return null;
			}
			return v.marshal();
		}

		@Override
		public AbstractSpecies unmarshal(final Adapted v) throws Exception {
			if (v == null) {
				return null;
			}
			return v.unmarshal();
		}

	}

	@Override
	public String getId() {
		return id;
	}

	@Override
	public Name getName() {
		return name;
	}

	@Override
	public synchronized Collection<ActiveSkillType> getActiveSkills() {
		if (activeSkills == null || activeSkills.isEmpty()) {
			return Collections.emptyList();
		}
		return new ArrayList<>(activeSkills);
	}

	@Override
	public synchronized boolean hasActiveSkill(final ActiveSkillType v) {
		if (activeSkills == null) {
			return false;
		}
		return activeSkills.contains(v);
	}

	@Override
	public synchronized Collection<PassiveSkillType> getPassiveSkills() {
		if (passiveSkills == null || passiveSkills.isEmpty()) {
			return Collections.emptyList();
		}
		return new ArrayList<>(passiveSkills);
	}

	@Override
	public synchronized boolean hasPassiveSkill(final PassiveSkillType v) {
		if (passiveSkills == null) {
			return false;
		}
		return passiveSkills.contains(v);
	}

	@Override
	public SpeciesRelationships getRelationships() {
		return relationships;
	}

	@Override
	public MotionType getMotion() {
		return motion;
	}

	@Override
	public VisualType getVisualType() {
		return visualType;
	}

	@Override
	public synchronized boolean isVisible(final VisualType visualType) {
		return passiveSkills.contains(visualType.passiveSkill());
	}

}
