package org.kareha.hareka.server.field;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

import org.kareha.hareka.annotation.Immutable;
import org.kareha.hareka.field.Vector;

@Immutable
public class NullBoundary extends Boundary {

	public static final NullBoundary INSTANCE = new NullBoundary();

	@XmlType(name = "nullBoundary")
	@XmlAccessorType(XmlAccessType.NONE)
	protected static class Adapted extends Boundary.Adapted {

		protected Adapted() {
			// used by JAXB
		}

		protected Adapted(final NullBoundary v) {
			super(v);
		}

		@Override
		protected NullBoundary unmarshal() {
			return new NullBoundary();
		}

	}

	@Override
	protected Adapted marshal() {
		return new Adapted(this);
	}

	@Override
	public Vector confine(final Vector position) {
		return position;
	}

}
