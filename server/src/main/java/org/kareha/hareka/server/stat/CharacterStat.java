package org.kareha.hareka.server.stat;

import java.util.Collection;

import org.kareha.hareka.field.TileType;
import org.kareha.hareka.game.ActiveSkillType;
import org.kareha.hareka.game.PassiveSkillType;
import org.kareha.hareka.server.relationship.IndividualRelationships;

public interface CharacterStat extends Stat {

	Species getSpecies();

	int getMotionWait(TileType tileType);

	int getAttackWait(TileType tileType);

	StatResult addHealthPoints(int v);

	StatResult addMagicPoints(int v);

	boolean revive();

	Collection<ActiveSkillType> getActiveSkills();

	boolean hasActiveSkill(ActiveSkillType v);

	Collection<PassiveSkillType> getPassiveSkills();

	boolean hasPassiveSkill(PassiveSkillType v);

	int getEffectiveSkill(ExperienceType type);

	int getWeaponClass();

	int getArmorClass();

	int getAttackPoints(int targetArmorClass);

	int getAccuracy(int targetDodge);

	long getWeightCapacity();

	Experiences getSkills();

	IndividualRelationships getRelationships();

	VisualType getVisualType();

	boolean isVisible(VisualType visualType);

}
