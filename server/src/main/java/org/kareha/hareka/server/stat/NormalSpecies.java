package org.kareha.hareka.server.stat;

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.kareha.hareka.annotation.ThreadSafe;
import org.kareha.hareka.math.IntRange;
import org.kareha.hareka.server.motion.MotionType;
import org.kareha.hareka.server.relationship.SpeciesRelationships;

@ThreadSafe
public class NormalSpecies extends AbstractSpecies {

	public static class Builder extends AbstractSpecies.Builder<Builder> {

		protected Map<ExperienceType, IntRange> skillExperienceRanges;

		public Builder skillExperienceRanges(final Map<ExperienceType, IntRange> v) {
			skillExperienceRanges = v;
			return this;
		}

		@Override
		protected Builder self() {
			return this;
		}

		@Override
		public NormalSpecies build(final String id) {
			if (skillExperienceRanges == null) {
				skillExperienceRanges = new EnumMap<>(ExperienceType.class);
			}
			final Set<ExperienceType> missings = EnumSet.allOf(ExperienceType.class);
			missings.removeAll(skillExperienceRanges.keySet());
			for (final ExperienceType type : missings) {
				skillExperienceRanges.put(type, new IntRange(0, 0));
			}
			if (relationships == null) {
				relationships = new SpeciesRelationships();
			}
			if (motion == null) {
				motion = MotionType.NEUTRAL;
			}
			return new NormalSpecies(id, this);
		}

	}

	protected final Map<ExperienceType, IntRange> experienceRanges;

	protected NormalSpecies(final String id, final Builder builder) {
		super(id, builder);
		experienceRanges = new EnumMap<>(builder.skillExperienceRanges);
	}

	@XmlType(name = "experienceRange")
	@XmlAccessorType(XmlAccessType.NONE)
	protected static class ExperienceRange {

		@XmlAttribute
		protected ExperienceType type;
		@XmlElement
		protected int min;
		@XmlElement
		protected int max;

		@SuppressWarnings("unused")
		private ExperienceRange() {
			// used by JAXB
		}

		protected ExperienceRange(final ExperienceType type, final IntRange range) {
			this.type = type;
			min = range.getMin();
			max = range.getMax();
		}

		protected ExperienceType getType() {
			return type;
		}

		protected IntRange getRange() {
			return new IntRange(min, max);
		}

	}

	@XmlRootElement(name = "normalSpecies")
	@XmlType(name = "normalSpecies")
	@XmlAccessorType(XmlAccessType.NONE)
	protected static class Adapted extends AbstractSpecies.Adapted {

		@XmlElement(name = "experienceRange")
		protected List<ExperienceRange> experienceRanges;

		protected Adapted() {
			// used by JAXB
		}

		protected Adapted(final NormalSpecies v) {
			super(v);
			experienceRanges = new ArrayList<>();
			for (final Map.Entry<ExperienceType, IntRange> entry : v.experienceRanges.entrySet()) {
				experienceRanges.add(new ExperienceRange(entry.getKey(), entry.getValue()));
			}
		}

		@Override
		protected NormalSpecies unmarshal() {
			final Builder builder = new Builder();
			builder.name(name);
			builder.shapes(shape);
			builder.activeSkills(activeSkills);
			builder.passiveSkills(passiveSkills);
			builder.relationships(relationships);
			builder.motion(motion);
			builder.visualType(visualType);

			final Map<ExperienceType, IntRange> map = new EnumMap<>(ExperienceType.class);
			for (final ExperienceRange range : experienceRanges) {
				map.put(range.getType(), range.getRange());
			}
			builder.skillExperienceRanges(map);
			return builder.build(id);
		}

	}

	@Override
	protected Adapted marshal() {
		return new Adapted(this);
	}

	@Override
	public IntRange getExperienceRange(final ExperienceType type) {
		return experienceRanges.get(type);
	}

}
