package org.kareha.hareka.server.handler;

import java.io.IOException;
import java.util.ResourceBundle;
import java.util.logging.Logger;

import org.kareha.hareka.field.Direction;
import org.kareha.hareka.field.Placement;
import org.kareha.hareka.field.Vector;
import org.kareha.hareka.protocol.Handler;
import org.kareha.hareka.protocol.HandlerException;
import org.kareha.hareka.protocol.ProtocolException;
import org.kareha.hareka.protocol.ProtocolInput;
import org.kareha.hareka.server.ServerConstants;
import org.kareha.hareka.server.entity.CharacterEntity;
import org.kareha.hareka.server.external.Session;
import org.kareha.hareka.server.field.FieldId;
import org.kareha.hareka.server.packet.CharactersPacket;
import org.kareha.hareka.server.user.User;

public final class NewCharacterHandler implements Handler<Session> {

	private static final Logger logger = Logger.getLogger(NewCharacterHandler.class.getName());

	private enum BundleKey {
		LoginUserFirst, CannotCreateCharacter, NewCharacterCreated,
	}

	@Override
	public void handle(final ProtocolInput in, final Session session)
			throws IOException, ProtocolException, HandlerException {
		// read arguments
		final int requestId = in.readCompactUInt();

		final ResourceBundle bundle = session.getBundle(NewCharacterHandler.class.getName());

		// check state
		final User user = session.getUser();
		if (user == null) {
			session.writeRejectPacket(requestId, bundle.getString(BundleKey.LoginUserFirst.name()));
			logger.fine(session.getStamp() + "Not logged in");
			return;
		}

		if (user.getCharacterEntriesSize() >= ServerConstants.MAX_CHARACTERS_PER_USER) {
			session.writeRejectPacket(requestId, bundle.getString(BundleKey.CannotCreateCharacter.name()),
					ServerConstants.MAX_CHARACTERS_PER_USER);
			logger.fine(session.getStamp() + "Too many characters");
			return;
		}

		// create character
		final FieldId fieldId = session.getContext().fields().getRootField().getId();
		final Placement placement = Placement.valueOf(Vector.ZERO, Direction.NULL);
		final CharacterEntity entity = session.getContext().entities().createSoulEntity(fieldId, placement);
		if (user.addCharacterEntry(entity.getId())) {
			entity.addOwner(user.getId());
		}

		session.writeAcceptPacket(requestId, bundle.getString(BundleKey.NewCharacterCreated.name()));
		logger.info(session.getStamp() + "Created character=" + entity.getId());

		session.write(new CharactersPacket(user.getCharacterEntries(), session.getContext().entities(),
				user.getChatEntityIdCipher(), session.getLocale().getLanguage()));
	}

}
