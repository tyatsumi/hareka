package org.kareha.hareka.server.field;

import java.io.IOException;

import org.kareha.hareka.field.Vector;
import org.kareha.hareka.protocol.ProtocolElement;
import org.kareha.hareka.protocol.ProtocolException;
import org.kareha.hareka.protocol.ProtocolInput;
import org.kareha.hareka.protocol.ProtocolInputStream;
import org.kareha.hareka.protocol.ProtocolOutput;
import org.kareha.hareka.protocol.ProtocolOutputStream;
import org.kareha.hareka.server.entity.EntityId;
import org.kareha.hareka.server.entity.FieldEntity;
import org.kareha.hareka.server.user.StorageCipher;

public class PositionMemory implements ProtocolElement {

	private static final String TYPE = PositionMemory.class.getSimpleName();

	private final FieldPosition position;
	private final EntityId entityId;
	private final long date;

	public PositionMemory(final FieldPosition position, final EntityId entityId, final long date) {
		this.position = position;
		this.entityId = entityId;
		this.date = date;
	}

	public PositionMemory(final ServerField field, final Vector position, final FieldEntity entity) {
		this(FieldPosition.valueOf(field.getId(), position), entity.getId(), System.currentTimeMillis());
	}

	public static PositionMemory readFrom(final ProtocolInput in, final long date)
			throws IOException, ProtocolException {
		final long fieldIdValue = in.readCompactULong();
		final Vector position = Vector.readFrom(in);
		final long entityIdValue = in.readCompactULong();

		final FieldPosition fieldPosition = FieldPosition.valueOf(FieldId.valueOf(fieldIdValue), position);
		final EntityId entityId = EntityId.valueOf(entityIdValue);

		return new PositionMemory(fieldPosition, entityId, date);
	}

	@Override
	public void writeTo(final ProtocolOutput out) {
		out.writeCompactULong(position.getFieldId().value());
		out.write(position.getPosition());
		out.writeCompactULong(entityId.value());
	}

	@SuppressWarnings("resource")
	public static PositionMemory decrypt(final StorageCipher cipher, final byte[] data)
			throws IOException, ProtocolException {
		final byte[] plain = StorageCipher.getPlain(data);
		final ProtocolInputStream plainIn = new ProtocolInputStream(plain);
		final String type = plainIn.readString();
		if (!TYPE.equals(type)) {
			return null;
		}
		final long date = plainIn.readCompactLong();

		final byte[] decrypted = cipher.decrypt(data);
		if (decrypted == null) {
			return null;
		}
		final ProtocolInputStream in = new ProtocolInputStream(decrypted);
		return readFrom(in, date);
	}

	public byte[] encrypt(final StorageCipher cipher) {
		@SuppressWarnings("resource")
		final ProtocolOutputStream plainOut = new ProtocolOutputStream();
		plainOut.writeString(TYPE);
		plainOut.writeCompactLong(date);
		try (final ProtocolOutputStream toBeEncryptedOut = new ProtocolOutputStream()) {
			writeTo(toBeEncryptedOut);
			return cipher.encrypt(plainOut.toByteArray(), toBeEncryptedOut.toByteArray());
		}
	}

	public FieldPosition getPosition() {
		return position;
	}

	public EntityId getEntityId() {
		return entityId;
	}

	public long getDate() {
		return date;
	}

}
