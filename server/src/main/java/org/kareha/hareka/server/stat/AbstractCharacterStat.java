package org.kareha.hareka.server.stat;

import java.util.Collection;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.kareha.hareka.annotation.GuardedBy;
import org.kareha.hareka.annotation.ThreadSafe;
import org.kareha.hareka.game.ActiveSkillType;
import org.kareha.hareka.game.PassiveSkillType;

@ThreadSafe
@XmlJavaTypeAdapter(AbstractCharacterStat.Adapter.class)
public abstract class AbstractCharacterStat implements CharacterStat {

	public static abstract class Builder<T extends Builder<T>> {

		protected Species species;
		protected String shape;

		protected abstract T self();

		public T species(final Species v) {
			species = v;
			return self();
		}

		public T shape(final String v) {
			shape = v;
			return self();
		}

		public abstract AbstractCharacterStat build();

	}

	protected final Species species;
	@GuardedBy("this")
	protected String shape;

	protected AbstractCharacterStat(final Builder<?> builder) {
		species = builder.species;
		shape = builder.shape;
	}

	@XmlType(name = "abstractCharacterStat")
	@XmlSeeAlso({ NormalCharacterStat.Adapted.class })
	@XmlAccessorType(XmlAccessType.NONE)
	protected static abstract class Adapted {

		@XmlElement
		protected String speciesId;
		@XmlElement
		protected String shape;

		protected Adapted() {
			// used by JAXB
		}

		protected Adapted(final AbstractCharacterStat v) {
			speciesId = v.species.getId();
			synchronized (v) {
				shape = v.shape;
			}
		}

		protected abstract AbstractCharacterStat unmarshal();

	}

	protected abstract Adapted marshal();

	static class Adapter extends XmlAdapter<Adapted, AbstractCharacterStat> {

		@Override
		public Adapted marshal(final AbstractCharacterStat v) throws Exception {
			if (v == null) {
				return null;
			}
			return v.marshal();
		}

		@Override
		public AbstractCharacterStat unmarshal(final Adapted v) throws Exception {
			if (v == null) {
				return null;
			}
			return v.unmarshal();
		}

	}

	@Override
	public synchronized String getShape() {
		return shape;
	}

	@Override
	public long getCount() {
		return 1;
	}

	@Override
	public Species getSpecies() {
		return species;
	}

	@Override
	public Collection<ActiveSkillType> getActiveSkills() {
		return species.getActiveSkills();
	}

	@Override
	public boolean hasActiveSkill(final ActiveSkillType v) {
		return species.hasActiveSkill(v);
	}

	@Override
	public Collection<PassiveSkillType> getPassiveSkills() {
		return species.getPassiveSkills();
	}

	@Override
	public boolean hasPassiveSkill(final PassiveSkillType v) {
		return species.hasPassiveSkill(v);
	}

	@Override
	public VisualType getVisualType() {
		return species.getVisualType();
	}

	@Override
	public boolean isVisible(final VisualType visualType) {
		return species.isVisible(visualType);
	}

}
