package org.kareha.hareka.server.packet;

import org.kareha.hareka.protocol.WorldClientPacketType;
import org.kareha.hareka.server.protocol.WorldClientPacket;
import org.kareha.hareka.protocol.PacketType;

public final class CommandOutPacket extends WorldClientPacket {

	public CommandOutPacket(final String result) {
		out.writeString(result);
	}

	@Override
	protected PacketType getType() {
		return WorldClientPacketType.COMMAND_OUT;
	}

}
