package org.kareha.hareka.server.entity;

import java.util.Collection;

import org.kareha.hareka.field.Placement;
import org.kareha.hareka.field.TileType;
import org.kareha.hareka.field.Transformation;
import org.kareha.hareka.server.field.FieldId;
import org.kareha.hareka.server.field.FieldPosition;
import org.kareha.hareka.server.field.Fields;
import org.kareha.hareka.server.field.ServerFieldObject;
import org.kareha.hareka.server.stat.Stat;
import org.kareha.hareka.server.stat.VisualType;

public interface FieldEntity extends Entity {

	String getShape();

	void setShape(String v);

	Transformation getTransformation();

	ServerFieldObject getFieldObject();

	void setFieldObject(ServerFieldObject v);

	int getMotionWait(TileType tileType);

	Stat getStat();

	boolean mount(Fields fieldTable);

	boolean mount(Fields fieldTable, FieldId fieldId, Placement placement);

	boolean unmount();

	boolean isMounted();

	boolean teleport(FieldPosition fieldPosition);

	void addFieldDriver(FieldDriver driver);

	boolean removeFieldDriver(FieldDriver driver);

	Collection<FieldDriver> getFieldDrivers();

	int getJumpDown();

	int getJumpUp();

	VisualType getVisualType();

	boolean isVisible(VisualType visualType);

}
