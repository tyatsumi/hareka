package org.kareha.hareka.server.packet;

import org.kareha.hareka.protocol.WorldClientPacketType;
import org.kareha.hareka.server.entity.ItemEntity;
import org.kareha.hareka.server.protocol.WorldClientPacket;
import org.kareha.hareka.server.user.IdCipher;
import org.kareha.hareka.protocol.PacketType;

public final class SetItemCountPacket extends WorldClientPacket {

	public SetItemCountPacket(final ItemEntity itemEntity, final IdCipher itemEntityIdCipher) {
		out.write(itemEntityIdCipher.encrypt(itemEntity.getId()));
		out.writeCompactULong(itemEntity.getStat().getCount());
	}

	@Override
	protected PacketType getType() {
		return WorldClientPacketType.SET_ITEM_COUNT;
	}

}
