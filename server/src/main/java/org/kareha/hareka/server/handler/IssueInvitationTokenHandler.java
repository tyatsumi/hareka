package org.kareha.hareka.server.handler;

import java.io.IOException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.bind.DatatypeConverter;
import javax.xml.bind.JAXBException;

import org.kareha.hareka.protocol.Handler;
import org.kareha.hareka.protocol.HandlerException;
import org.kareha.hareka.protocol.ProtocolException;
import org.kareha.hareka.protocol.ProtocolInput;
import org.kareha.hareka.server.external.Session;
import org.kareha.hareka.server.packet.TextPacket;
import org.kareha.hareka.server.user.InvitationToken;
import org.kareha.hareka.server.user.InvitationTokensLogRecord;
import org.kareha.hareka.server.user.User;
import org.kareha.hareka.user.Permission;
import org.kareha.hareka.user.RoleSet;

public final class IssueInvitationTokenHandler implements Handler<Session> {

	private static final Logger logger = Logger.getLogger(IssueInvitationTokenHandler.class.getName());

	private enum BundleKey {
		LoginUserFirst, YouCannotUseThisFunction,
	}

	private static void inform(final Session session, final String key) {
		final ResourceBundle bundle = session.getBundle(IssueRoleTokenHandler.class.getName());
		session.writeInformPacket(bundle.getString(key));
	}

	@Override
	public void handle(final ProtocolInput in, final Session session)
			throws IOException, ProtocolException, HandlerException {
		// read arguments

		// check state
		final User user = session.getUser();
		if (user == null) {
			inform(session, BundleKey.LoginUserFirst.name());
			logger.fine(session.getStamp() + "User not logged in");
			return;
		}
		final RoleSet roles = session.getContext().accessController().getRoleSet(user);
		if (!roles.isAbleTo(Permission.ISSUE_INVITATION_TOKENS)) {
			inform(session, BundleKey.YouCannotUseThisFunction.name());
			logger.fine(session.getStamp() + "User cannot use issue invitation token function");
			return;
		}

		// process
		final InvitationToken token = session.getContext().invitationTokens().issue(user);
		try {
			session.getContext().invitationTokensLogger().add(new InvitationTokensLogRecord(token));
		} catch (final IOException | JAXBException e) {
			logger.log(Level.SEVERE, "", e);
		}
		final String hex = DatatypeConverter.printHexBinary(token.getValue()).toLowerCase();
		session.write(new TextPacket("Invitation token: " + hex + "\n"));
	}

}
