package org.kareha.hareka.server.handler;

import java.io.IOException;
import java.util.ResourceBundle;

import org.kareha.hareka.field.Placement;
import org.kareha.hareka.protocol.Handler;
import org.kareha.hareka.protocol.HandlerException;
import org.kareha.hareka.protocol.ProtocolException;
import org.kareha.hareka.protocol.ProtocolInput;
import org.kareha.hareka.server.entity.CharacterEntity;
import org.kareha.hareka.server.external.Session;
import org.kareha.hareka.server.field.FieldPosition;
import org.kareha.hareka.server.field.ServerField;
import org.kareha.hareka.server.field.ServerFieldObject;
import org.kareha.hareka.server.game.Player;

public final class SetMarkHandler implements Handler<Session> {

	private enum BundleKey {
		MarkSet,
	}

	private static void inform(final Session session, final String key) {
		final ResourceBundle bundle = session.getBundle(SetMarkHandler.class.getName());
		session.writeInformPacket(bundle.getString(key));
	}

	@Override
	public void handle(final ProtocolInput in, final Session session)
			throws IOException, ProtocolException, HandlerException {
		final Player player = session.getPlayer();
		if (player == null) {
			return;
		}
		final CharacterEntity entity = player.getEntity();
		final ServerFieldObject fo = entity.getFieldObject();
		if (fo == null) {
			return;
		}
		final ServerField field = fo.getField();
		final Placement placement = fo.getPlacement();
		final FieldPosition mark = FieldPosition.valueOf(field.getId(), placement.getPosition());
		player.setMark(mark);
		inform(session, BundleKey.MarkSet.name());
	}

}
