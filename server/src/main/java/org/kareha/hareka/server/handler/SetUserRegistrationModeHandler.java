package org.kareha.hareka.server.handler;

import java.io.IOException;
import java.util.ResourceBundle;
import java.util.logging.Logger;

import org.kareha.hareka.protocol.Handler;
import org.kareha.hareka.protocol.HandlerException;
import org.kareha.hareka.protocol.ProtocolException;
import org.kareha.hareka.protocol.ProtocolInput;
import org.kareha.hareka.server.external.Session;
import org.kareha.hareka.server.user.User;
import org.kareha.hareka.user.Permission;
import org.kareha.hareka.user.RoleSet;
import org.kareha.hareka.user.UserRegistrationMode;

public final class SetUserRegistrationModeHandler implements Handler<Session> {

	private static final Logger logger = Logger.getLogger(SetUserRegistrationModeHandler.class.getName());

	private enum BundleKey {
		LoginUserFirst, YouCannotUseThisFunction, SettingChanged,
	}

	private static void inform(final Session session, final String key) {
		final ResourceBundle bundle = session.getBundle(SetUserRegistrationModeHandler.class.getName());
		session.writeInformPacket(bundle.getString(key));
	}

	@Override
	public void handle(final ProtocolInput in, final Session session)
			throws IOException, ProtocolException, HandlerException {
		final UserRegistrationMode mode = UserRegistrationMode.readFrom(in);

		final User user = session.getUser();
		if (user == null) {
			inform(session, BundleKey.LoginUserFirst.name());
			logger.fine(session.getStamp() + "User not logged in");
			return;
		}
		final RoleSet roles = session.getContext().accessController().getRoleSet(user);
		if (!roles.isAbleTo(Permission.MANAGE_SETTINGS)) {
			inform(session, BundleKey.YouCannotUseThisFunction.name());
			logger.fine(session.getStamp() + "User cannot use set user registration mode function");
			return;
		}

		session.getContext().settings().setUserRegistrationMode(mode);
		inform(session, BundleKey.SettingChanged.name());
		logger.fine(session.getStamp() + "User registration mode changed to " + mode);
	}

}
