package org.kareha.hareka.server;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.io.IOException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;
import javax.xml.bind.JAXBException;

import org.kareha.hareka.annotation.ConfinedTo;
import org.kareha.hareka.annotation.Private;
import org.kareha.hareka.server.net.ServerException;
import org.kareha.hareka.server.user.InvitationToken;
import org.kareha.hareka.server.user.RoleToken;
import org.kareha.hareka.ui.swing.LogPanel;

@ConfinedTo("Swing")
@SuppressWarnings("serial")
public class MainFrame extends JFrame {

	private enum BundleKey {

		Title,

		Start, Stop,

		CopyInvitationToken, InvitationTokenCopied, CopyRoleToken, RoleTokenCopied,

	}

	@Private
	static final Logger logger = Logger.getLogger(MainFrame.class.getName());

	@Private
	final ServerParameters parameters;
	private final JButton startButton;
	@Private
	final JButton stopButton;
	@Private
	final JButton copyInvitationTokenButton;
	@Private
	final JButton copyRoleTokenButton;

	@Private
	String invitationToken;
	@Private
	String roleToken;

	public MainFrame(final ServerParameters parameters) {
		this.parameters = parameters;

		final ResourceBundle bundle = ResourceBundle.getBundle(MainFrame.class.getName());

		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		setTitle(bundle.getString(BundleKey.Title.name()));

		final JPanel northPanel = new JPanel();
		northPanel.setLayout(new BoxLayout(northPanel, BoxLayout.Y_AXIS));

		final JPanel buttonPanel = new JPanel();
		startButton = new JButton(bundle.getString(BundleKey.Start.name()));
		startButton.addActionListener(e -> doStart());
		buttonPanel.add(startButton);
		stopButton = new JButton(bundle.getString(BundleKey.Stop.name()));
		stopButton.setEnabled(false);
		stopButton.addActionListener(e -> doStop());
		buttonPanel.add(stopButton);
		northPanel.add(buttonPanel);

		final JPanel copyPanel = new JPanel();
		copyInvitationTokenButton = new JButton(bundle.getString(BundleKey.CopyInvitationToken.name()));
		copyInvitationTokenButton.setEnabled(false);
		copyInvitationTokenButton.addActionListener(e -> {
			copyToClipboard(invitationToken);
			JOptionPane.showMessageDialog(this, bundle.getString(BundleKey.InvitationTokenCopied.name()));
		});
		copyPanel.add(copyInvitationTokenButton);
		copyRoleTokenButton = new JButton(bundle.getString(BundleKey.CopyRoleToken.name()));
		copyRoleTokenButton.setEnabled(false);
		copyRoleTokenButton.addActionListener(e -> {
			copyToClipboard(roleToken);
			JOptionPane.showMessageDialog(this, bundle.getString(BundleKey.RoleTokenCopied.name()));
		});
		copyPanel.add(copyRoleTokenButton);
		northPanel.add(copyPanel);

		add(northPanel, BorderLayout.NORTH);
		final LogPanel logPanel = LogPanel.newInstance();
		logPanel.setPreferredSize(new Dimension(640, 480));
		add(logPanel, BorderLayout.CENTER);

		pack();
		setLocationRelativeTo(null);

		startButton.requestFocus();
		if (Rebooter.INSTANCE.isRebooted()) {
			startButton.doClick();
		}
	}

	private void doStart() {
		startButton.setEnabled(false);

		final ServerParameters params = parameters;
		new Thread(() -> {
			try {
				Starter.INSTANCE.start(params);
			} catch (final IOException | JAXBException | ServerException e) {
				SwingUtilities.invokeLater(() -> {
					logger.log(Level.SEVERE, "", e);
					JOptionPane.showMessageDialog(MainFrame.this, e.getMessage());
				});
				return;
			}

			SwingUtilities.invokeLater(() -> {
				stopButton.setEnabled(true);
			});

			final InvitationToken invitationToken = Global.INSTANCE.context().invitationTokens().issueInitial();
			if (invitationToken != null) {
				SwingUtilities.invokeLater(() -> {
					MainFrame.this.invitationToken = invitationToken.toHexString();
					copyInvitationTokenButton.setEnabled(true);
				});
			}

			final RoleToken roleToken = Global.INSTANCE.context().roleTokens().issueInitial();
			if (roleToken != null) {
				SwingUtilities.invokeLater(() -> {
					MainFrame.this.roleToken = roleToken.toHexString();
					copyRoleTokenButton.setEnabled(true);
				});
			}
		}).start();
	}

	private void doStop() {
		stopButton.setEnabled(false);
		copyInvitationTokenButton.setEnabled(false);
		copyRoleTokenButton.setEnabled(false);

		new Thread(() -> {
			Saver.INSTANCE.save(true);

			SwingUtilities.invokeLater(() -> {
				startButton.setEnabled(true);
			});
		}).start();
	}

	private void copyToClipboard(final String string) {
		final StringSelection selection = new StringSelection(string);
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(selection, selection);
	}

}
