package org.kareha.hareka.server.handler;

import java.io.IOException;
import java.util.EnumSet;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.bind.JAXBException;

import org.kareha.hareka.protocol.Handler;
import org.kareha.hareka.protocol.HandlerException;
import org.kareha.hareka.protocol.ProtocolException;
import org.kareha.hareka.protocol.ProtocolInput;
import org.kareha.hareka.server.external.Session;
import org.kareha.hareka.server.packet.RoleListPacket;
import org.kareha.hareka.server.user.RolesLogRecord;
import org.kareha.hareka.server.user.User;
import org.kareha.hareka.user.Permission;
import org.kareha.hareka.user.Role;
import org.kareha.hareka.user.RoleSet;

public final class UndefineRoleHandler implements Handler<Session> {

	private static final Logger logger = Logger.getLogger(UndefineRoleHandler.class.getName());

	private enum BundleKey {
		LoginUserFirst, YouCannotUseThisFunction, InsufficientRank,

		PermissionCannotBeRemoved,
	}

	private static void inform(final Session session, final String key) {
		final ResourceBundle bundle = session.getBundle(UndefineRoleHandler.class.getName());
		session.writeInformPacket(bundle.getString(key));
	}

	@Override
	public void handle(final ProtocolInput in, final Session session)
			throws IOException, ProtocolException, HandlerException {
		// read arguments
		final String roleId = in.readString();

		// check state
		final User user = session.getUser();
		if (user == null) {
			inform(session, BundleKey.LoginUserFirst.name());
			return;
		}
		final RoleSet roleSet = session.getContext().accessController().getRoleSet(user);
		final Role manageRole = roleSet.getHighestRole(Permission.MANAGE_ROLES);
		if (manageRole == null) {
			inform(session, BundleKey.YouCannotUseThisFunction.name());
			return;
		}
		final Role oldRole = session.getContext().roles().get(roleId);
		if (oldRole == null) {
			return;
		}
		if (manageRole.getRank() <= oldRole.getRank()) {
			inform(session, BundleKey.InsufficientRank.name());
			return;
		}
		final Set<Permission> oldPermissions = EnumSet.copyOf(oldRole.getPermissions());
		for (final Permission permission : oldPermissions) {
			final Role myRole = roleSet.getHighestRole(permission);
			if (myRole == null) {
				inform(session, BundleKey.PermissionCannotBeRemoved.name());
				return;
			}
			if (myRole.getRank() <= oldRole.getRank()) {
				inform(session, BundleKey.PermissionCannotBeRemoved.name());
				return;
			}
		}

		// process
		if (session.getContext().roles().remove(roleId) != null) {
			try {
				session.getContext().rolesLogger().add(new RolesLogRecord(user.getId(), roleId));
			} catch (final IOException | JAXBException e) {
				logger.log(Level.SEVERE, "", e);
			}
			session.write(new RoleListPacket(session.getContext().roles(), user,
					session.getContext().accessController()));
		}
	}

}
