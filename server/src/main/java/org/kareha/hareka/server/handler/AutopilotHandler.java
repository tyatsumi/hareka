package org.kareha.hareka.server.handler;

import java.io.IOException;
import java.util.ResourceBundle;
import java.util.logging.Logger;

import org.kareha.hareka.protocol.Handler;
import org.kareha.hareka.protocol.HandlerException;
import org.kareha.hareka.protocol.ProtocolException;
import org.kareha.hareka.protocol.ProtocolInput;
import org.kareha.hareka.server.external.Session;
import org.kareha.hareka.server.field.ServerFieldObject;
import org.kareha.hareka.server.game.Player;

public final class AutopilotHandler implements Handler<Session> {

	private static final Logger logger = Logger.getLogger(AutopilotHandler.class.getName());

	private enum BundleKey {
		LoginCharacterFirst,
	}

	private static void inform(final Session session, final String key) {
		final ResourceBundle bundle = session.getBundle(AutopilotHandler.class.getName());
		session.writeInformPacket(bundle.getString(key));
	}

	@Override
	public void handle(final ProtocolInput in, final Session session)
			throws IOException, ProtocolException, HandlerException {
		// read arguments
		final boolean flag = in.readBoolean();

		// check state
		final Player player = session.getPlayer();
		if (player == null) {
			inform(session, BundleKey.LoginCharacterFirst.name());
			logger.fine(session.getStamp() + "Character not logged in");
			return;
		}
		final ServerFieldObject fo = player.getEntity().getFieldObject();
		if (fo == null) {
			logger.warning(session.getStamp() + "FieldObject not found");
			return;
		}

		if (flag) {
			player.getEntity().startMotion();
		} else {
			player.getEntity().stopMotion();
		}
	}

}
