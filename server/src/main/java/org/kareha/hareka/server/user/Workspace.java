package org.kareha.hareka.server.user;

import java.io.File;

import javax.xml.bind.JAXBException;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.kareha.hareka.annotation.Private;
import org.kareha.hareka.annotation.ThreadSafe;
import org.kareha.hareka.util.JaxbUtil;

@ThreadSafe
public class Workspace {

	private final File file;
	@Private
	final IdCipher userIdCipher;

	public Workspace(final File file) throws JAXBException {
		this.file = file;

		final Adapted adapted = load();
		if (adapted == null) {
			userIdCipher = new IdCipher();
		} else {
			userIdCipher = adapted.userIdCipher;
		}
	}

	@XmlRootElement(name = "workspace")
	@XmlAccessorType(XmlAccessType.NONE)
	private static class Adapted {

		@XmlElement
		@Private
		IdCipher userIdCipher;

		@SuppressWarnings("unused")
		private Adapted() {
			// used by JAXB
		}

		@Private
		Adapted(final Workspace v) {
			userIdCipher = v.userIdCipher;
		}

	}

	private Adapted load() throws JAXBException {
		if (!file.isFile()) {
			return null;
		}
		return JaxbUtil.unmarshal(file, Adapted.class);
	}

	public void save() throws JAXBException {
		JaxbUtil.marshal(new Adapted(this), file);
	}

	public IdCipher getUserIdCipher() {
		return userIdCipher;
	}

}
