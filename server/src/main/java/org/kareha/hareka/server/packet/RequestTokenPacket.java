package org.kareha.hareka.server.packet;

import org.kareha.hareka.protocol.WorldClientPacketType;
import org.kareha.hareka.server.protocol.WorldClientPacket;
import org.kareha.hareka.protocol.PacketType;

public final class RequestTokenPacket extends WorldClientPacket {

	public RequestTokenPacket(final int handlerId, final String message) {
		out.writeCompactUInt(handlerId);
		out.writeString(message);
	}

	@Override
	protected PacketType getType() {
		return WorldClientPacketType.REQUEST_TOKEN;
	}

}
