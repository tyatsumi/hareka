package org.kareha.hareka.server.motion;

import java.util.Random;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

import org.kareha.hareka.annotation.GuardedBy;
import org.kareha.hareka.field.Direction;
import org.kareha.hareka.field.FieldObject;
import org.kareha.hareka.field.Placement;
import org.kareha.hareka.field.Tile;
import org.kareha.hareka.game.ActiveSkillType;
import org.kareha.hareka.game.PassiveSkillType;
import org.kareha.hareka.server.Global;
import org.kareha.hareka.server.control.Control;
import org.kareha.hareka.server.control.NotMountedException;
import org.kareha.hareka.server.control.TargetNotMountedException;
import org.kareha.hareka.server.entity.CharacterEntity;
import org.kareha.hareka.server.entity.FieldEntity;
import org.kareha.hareka.server.field.ServerFieldObject;
import org.kareha.hareka.wait.WaitResult;

public class NormalMotion extends AbstractMotion implements Runnable {

	private static final int CHASE_DISTANCE = 4;
	private static final int BAR_TO_RUN = 64;
	private static final int RUN_DISTANCE = 8;
	private static final int MIN_ACTION_HP = 96;

	private enum Mode {
		STOP, WALK, CHASE, ATTACK, RUN, PURSUE, HEAL,
	}

	private final Control control;
	@GuardedBy("this")
	private ScheduledFuture<?> future;
	@GuardedBy("this")
	private Mode mode = Mode.STOP;
	@GuardedBy("this")
	private Direction direction = Direction.NULL;
	@GuardedBy("this")
	private int count;
	@GuardedBy("this")
	private Interests interests;

	public NormalMotion(final CharacterEntity characterEntity) {
		control = new Control(characterEntity);
	}

	private Random random() {
		return ThreadLocalRandom.current();
	}

	@Override
	public synchronized void startMotion() {
		if (future == null) {
			if (!Global.INSTANCE.isMotionEnabled()) {
				return;
			}
			try {
				future = Global.INSTANCE.scheduledExecutor().schedule(this, control.getMotionWait(),
						TimeUnit.MILLISECONDS);
			} catch (final NotMountedException e) {
				return;
			}
		}
	}

	@Override
	public synchronized void stopMotion() {
		if (future != null) {
			future.cancel(false);
			future = null;
		}
	}

	@Override
	public synchronized void addLove(final CharacterEntity entity, final int value) {
		if (interests == null) {
			interests = new Interests();
		}
		interests.addLove(entity, value);
	}

	@Override
	public synchronized void addHate(final CharacterEntity entity, final int value) {
		if (interests == null) {
			interests = new Interests();
		}
		interests.addHate(entity, value);
	}

	private synchronized Mode getMode() {
		return mode;
	}

	private synchronized void setMode(final Mode v) {
		mode = v;
	}

	private synchronized Direction getDirection() {
		return direction;
	}

	private synchronized void setDirection(final Direction v) {
		direction = v;
	}

	private synchronized int getCount() {
		return count;
	}

	private synchronized void setCount(final int v) {
		count = v;
	}

	private synchronized void decrementCount() {
		count--;
	}

	private synchronized CharacterEntity decideLoveTarget() {
		if (interests == null) {
			return null;
		}
		return interests.decideLoveTarget();
	}

	private synchronized CharacterEntity decideHateTarget() {
		if (interests == null) {
			return null;
		}
		return interests.decideHateTarget();
	}

	private synchronized void removeLoveEntity(final CharacterEntity entity) {
		if (interests == null) {
			return;
		}
		interests.removeLoveEntity(entity);
		if (interests.isEmpty()) {
			interests = null;
		}
	}

	private synchronized void removeHateEntity(final CharacterEntity entity) {
		if (interests == null) {
			return;
		}
		interests.removeHateEntity(entity);
		if (interests.isEmpty()) {
			interests = null;
		}
	}

	@Override
	public void run() {
		synchronized (this) {
			if (future == null) {
				return;
			}
		}

		try {
			CharacterEntity target;
			CharacterEntity loveTarget;
			Mode m;
			while (true) {
				target = decideHateTarget();
				loveTarget = decideLoveTarget();
				m = getMode();
				if (target == null && loveTarget == null) {
					break;
				}

				if (target != null) {
					try {
						final int distance = control.getDistance(target);
						if (!target.getStat().getHealthPoints().isAlive()) {
							removeHateEntity(target);
							setMode(Mode.STOP);
						} else if (!control.isSameField(target)) {
							removeHateEntity(target);
							setMode(Mode.STOP);
						} else if (m == Mode.RUN && distance > RUN_DISTANCE) {
							removeHateEntity(target);
							setMode(Mode.STOP);
						} else if (m == Mode.CHASE && distance > CHASE_DISTANCE) {
							removeHateEntity(target);
							setMode(Mode.STOP);
						} else {
							if (control.hasActiveSkill(ActiveSkillType.ATTACK)) {
								if (control.getHealthBar() < BAR_TO_RUN) {
									setMode(Mode.RUN);
								} else if (distance > 1) {
									setMode(Mode.CHASE);
								} else {
									setMode(Mode.ATTACK);
								}
							} else {
								setMode(Mode.RUN);
							}
							break;
						}
					} catch (final TargetNotMountedException e) {
						removeHateEntity(target);
						setMode(Mode.STOP);
					}
				}

				if (loveTarget != null) {
					try {
						final int distance = control.getDistance(loveTarget);
						if (loveTarget.getStat().getHealthPoints().isAlive()) {
							removeLoveEntity(loveTarget);
							setMode(Mode.STOP);
						} else if (!control.isSameField(loveTarget)) {
							removeLoveEntity(loveTarget);
							setMode(Mode.STOP);
						} else if (m == Mode.PURSUE && distance > CHASE_DISTANCE) {
							removeLoveEntity(loveTarget);
							setMode(Mode.STOP);
						} else {
							if (control.hasActiveSkill(ActiveSkillType.REVIVE)) {
								if (distance > 1) {
									setMode(Mode.PURSUE);
								} else {
									setMode(Mode.HEAL);
								}
							} else {
								removeLoveEntity(loveTarget);
								setMode(Mode.STOP);
							}
							break;
						}
					} catch (final TargetNotMountedException e) {
						removeLoveEntity(loveTarget);
						setMode(Mode.STOP);
					}
				}
			}

			m = getMode();

			if (m == Mode.STOP || m == Mode.WALK) {
				if (getCount() <= 0) {
					if (control.getHealthBar() < MIN_ACTION_HP) {
						setMode(Mode.STOP);
					} else if (random().nextInt(2) < 1) {
						setMode(Mode.STOP);
					} else {
						setMode(Mode.WALK);
						setDirection(Direction.valueOf(random().nextInt(6)));
					}
					setCount(1 + random().nextInt(8));
				}
				decrementCount();
			}

			switch (mode) {
			case STOP: {
				// nothing to do
			}
				break;
			case WALK: {
				if (!checkMotionWait()) {
					break;
				}
				if (control.isStuck()) {
					break;
				}
				final Placement placement = control.getPlacement();
				final Direction d = getDirection();
				if (!helperWalk(d)) {
					if (control.getTile(placement).type().isStackable()) {
						control.move(placement);
					}
					setCount(0);
				}
				if (control.getMotion() == MotionType.AGGRESSIVE) {
					for (final FieldObject o : control.getInViewObjects()) {
						if (control.isSameFieldObject(o)) {
							continue;
						}
						if (!control.isVisible(o)) {
							continue;
						}
						final FieldEntity entity = ((ServerFieldObject) o).getFieldEntity();
						if (entity instanceof CharacterEntity) {
							final CharacterEntity ce = (CharacterEntity) entity;
							if (!ce.getStat().hasPassiveSkill(PassiveSkillType.IMMUNE_TO_ATTACK)) {
								if (control.getDistance(o.getPlacement()) <= CHASE_DISTANCE) {
									addHate(ce, 1);
								}
							}
						}
					}
				}
				if (control.hasActiveSkill(ActiveSkillType.REVIVE)) {
					for (final FieldObject o : control.getInViewObjects()) {
						if (control.isSameFieldObject(o)) {
							continue;
						}
						final FieldEntity entity = ((ServerFieldObject) o).getFieldEntity();
						if (entity instanceof CharacterEntity) {
							final CharacterEntity ce = (CharacterEntity) entity;
							if (!ce.getStat().getHealthPoints().isAlive()) {
								if (control.getDistance(o.getPlacement()) <= CHASE_DISTANCE) {
									addLove(ce, 1);
								}
							}
						}
					}
				}
			}
				break;
			case CHASE: {
				if (!checkMotionWait()) {
					break;
				}
				if (control.isStuck()) {
					break;
				}
				try {
					final Direction d = control.getDirection(target);
					if (!helperWalk(d)) {
						setMode(Mode.WALK);
					}
				} catch (final TargetNotMountedException e) {
					setMode(Mode.STOP);
				}
			}
				break;
			case ATTACK: {
				if (!checkAttackWait()) {
					break;
				}
				if (control.isStuck()) {
					break;
				}

				final Tile tile = control.getTile();
				final Tile targetTile;
				try {
					targetTile = control.getTile(target);
				} catch (final TargetNotMountedException e) {
					setMode(Mode.STOP);
					break;
				}
				final int delta = targetTile.elevation() - tile.elevation();
				if (delta >= 0) {
					if (delta > control.getJumpUp()) {
						break;
					}
				} else {
					if (-delta > control.getJumpDown()) {
						break;
					}
				}

				control.attack(target);
			}
				break;
			case RUN: {
				if (!checkMotionWait()) {
					break;
				}
				if (control.isStuck()) {
					break;
				}
				try {
					final Direction d = control.getInverseDirection(target);
					if (!helperWalk(d)) {
						final int distance = control.getDistance(target);
						if (distance < 2) {
							setMode(Mode.ATTACK);

							final Tile tile = control.getTile();
							final Tile targetTile = control.getTargetTile(target);
							final int delta = targetTile.elevation() - tile.elevation();
							if (delta >= 0) {
								if (delta > control.getJumpUp()) {
									break;
								}
							} else {
								if (-delta > control.getJumpDown()) {
									break;
								}
							}

							control.attack(target);
						}
					}
				} catch (final TargetNotMountedException e) {
					setMode(Mode.STOP);
				}
			}
				break;
			case HEAL: {
				if (control.isStuck()) {
					break;
				}

				final Tile tile = control.getTile();
				final Tile targetTile;
				try {
					targetTile = control.getTile(loveTarget);
				} catch (final TargetNotMountedException e) {
					setMode(Mode.STOP);
					break;
				}
				final int delta = targetTile.elevation() - tile.elevation();
				if (delta >= 0) {
					if (delta > control.getJumpUp()) {
						break;
					}
				} else {
					if (-delta > control.getJumpDown()) {
						break;
					}
				}

				control.revive(loveTarget);
				removeLoveEntity(loveTarget);
				setMode(Mode.WALK);
			}
				break;
			case PURSUE: {
				if (!checkMotionWait()) {
					break;
				}
				if (control.isStuck()) {
					break;
				}
				try {
					final Direction d = control.getDirection(loveTarget);
					if (!helperWalk(d)) {
						setMode(Mode.WALK);
					}
				} catch (final TargetNotMountedException e) {
					setMode(Mode.STOP);
				}
			}
				break;
			}

			final int wait;
			m = getMode();
			if (m == Mode.ATTACK) {
				wait = control.getAttackWait();
			} else {
				wait = control.getMotionWait();
			}

			synchronized (this) {
				if (future == null) {
					return;
				}
				future = Global.INSTANCE.scheduledExecutor().schedule(this, wait, TimeUnit.MILLISECONDS);
			}
		} catch (final NotMountedException e) {
			synchronized (this) {
				future = null;
			}
			return;
		}
	}

	private boolean checkMotionWait() throws NotMountedException {
		final WaitResult result = control.getNextMotionWait();
		if (!result.isSuccess()) {
			return false;
		}
		control.sendWait(result);
		return true;
	}

	private boolean checkAttackWait() throws NotMountedException {
		final WaitResult result = control.getNextAttackWait();
		if (!result.isSuccess()) {
			return false;
		}
		control.sendWait(result);
		return true;
	}

	private boolean helperWalk(final Direction d) throws NotMountedException {
		final Placement placement = control.getPlacement();
		Direction direction = d;
		Placement newPlacement = Placement.valueOf(placement.getPosition().neighbor(d), d);
		Placement oldPlacement = control.move(newPlacement);
		if (oldPlacement == null) {
			if (ThreadLocalRandom.current().nextBoolean()) {
				direction = d.rotate(1);
				newPlacement = Placement.valueOf(placement.getPosition().neighbor(direction), direction);
				oldPlacement = control.move(newPlacement);
				if (oldPlacement == null) {
					direction = d.rotate(-1);
					newPlacement = Placement.valueOf(placement.getPosition().neighbor(direction), direction);
					oldPlacement = control.move(newPlacement);
					if (oldPlacement == null) {
						return false;
					}
				}
			} else {
				direction = d.rotate(-1);
				newPlacement = Placement.valueOf(placement.getPosition().neighbor(direction), direction);
				oldPlacement = control.move(newPlacement);
				if (oldPlacement == null) {
					direction = d.rotate(1);
					newPlacement = Placement.valueOf(placement.getPosition().neighbor(direction), direction);
					oldPlacement = control.move(newPlacement);
					if (oldPlacement == null) {
						return false;
					}
				}
			}
		}
		control.incrementWalkExperience();
		if (!control.isFieldDriversEmpty()) {
			control.trySpawn(direction, newPlacement);
		}
		return true;
	}

}
