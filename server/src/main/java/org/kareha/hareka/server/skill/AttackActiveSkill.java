package org.kareha.hareka.server.skill;

import org.kareha.hareka.field.TileType;
import org.kareha.hareka.game.ActiveSkillType;
import org.kareha.hareka.server.entity.CharacterEntity;
import org.kareha.hareka.server.entity.FieldEntity;
import org.kareha.hareka.wait.WaitType;

public class AttackActiveSkill implements ActiveSkill {

	@Override
	public ActiveSkillType getType() {
		return ActiveSkillType.ATTACK;
	}

	@Override
	public int getReach() {
		return 1;
	}

	@Override
	public WaitType getWaitType() {
		return WaitType.ATTACK;
	}

	@Override
	public int getWait(final CharacterEntity entity, final TileType tileType) {
		return entity.getStat().getAttackWait(tileType);
	}

	@Override
	public void use(final CharacterEntity entity, final FieldEntity target) {
		if (target == entity) {
			return;
		}
		if (!(target instanceof CharacterEntity)) {
			return;
		}
		if (entity.isStuck()) {
			return;
		}
		entity.attack((CharacterEntity) target);
	}

}
