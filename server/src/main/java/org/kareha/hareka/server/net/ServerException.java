package org.kareha.hareka.server.net;

@SuppressWarnings("serial")
public class ServerException extends Exception {

	public ServerException(final String message) {
		super(message);
	}

	public ServerException(final String message, final Throwable cause) {
		super(message, cause);
	}

}
