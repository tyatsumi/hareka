package org.kareha.hareka.server.handler;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.logging.Logger;

import org.kareha.hareka.Constants;
import org.kareha.hareka.protocol.Handler;
import org.kareha.hareka.protocol.HandlerException;
import org.kareha.hareka.protocol.ProtocolException;
import org.kareha.hareka.protocol.ProtocolInput;
import org.kareha.hareka.server.entity.CharacterEntity;
import org.kareha.hareka.server.external.Session;
import org.kareha.hareka.server.game.Player;
import org.kareha.hareka.server.packet.SelfNamePacket;
import org.kareha.hareka.util.Utf8Util;

public final class ChangeNameHandler implements Handler<Session> {

	private static final Logger logger = Logger.getLogger(ChangeNameHandler.class.getName());

	private enum BundleKey {
		LoginCharacterFirst, InvalidLanguage, TooShort, TooLong,
	}

	private static void inform(final Session session, final String key) {
		final ResourceBundle bundle = session.getBundle(ChangeNameHandler.class.getName());
		session.writeInformPacket(bundle.getString(key));
	}

	@Override
	public void handle(final ProtocolInput in, final Session session)
			throws IOException, ProtocolException, HandlerException {
		// read arguments
		final String language = in.readString();
		final String translation = in.readString();

		// check state
		final Player player = session.getPlayer();
		if (player == null) {
			inform(session, BundleKey.LoginCharacterFirst.name());
			logger.fine(session.getStamp() + "No player");
			return;
		}

		// validate input
		final List<String> languages = Arrays.asList(Locale.getISOLanguages());
		if (!languages.contains(language)) {
			inform(session, BundleKey.InvalidLanguage.name());
			return;
		}
		final int length = Utf8Util.length(translation);
		if (length < Constants.MIN_NAME_LENGTH) {
			inform(session, BundleKey.TooShort.name());
			return;
		}
		if (length > Constants.MAX_NAME_LENGTH) {
			inform(session, BundleKey.TooLong.name());
			return;
		}

		// process
		final CharacterEntity entity = player.getEntity();
		entity.setName(language, translation);
		session.write(new SelfNamePacket(entity, session.getUser().getChatEntityIdCipher()));
	}

}
