package org.kareha.hareka.server.field;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.kareha.hareka.annotation.Immutable;
import org.kareha.hareka.annotation.Private;

@Immutable
@XmlJavaTypeAdapter(Gate.Adapter.class)
public class Gate {

	@Private
	final FieldPosition source;
	@Private
	final FieldPosition destination;

	private Gate(final FieldPosition source, final FieldPosition destination) {
		if (source == null || destination == null) {
			throw new IllegalArgumentException("null");
		}
		this.source = source;
		this.destination = destination;
	}

	public static Gate valueOf(final FieldPosition source, final FieldPosition destination) {
		return new Gate(source, destination);
	}

	@XmlType(name = "gate")
	@XmlAccessorType(XmlAccessType.NONE)
	private static class Adapted {

		@XmlElement
		private FieldPosition source;
		@XmlElement
		private FieldPosition destination;

		@SuppressWarnings("unused")
		private Adapted() {
			// used by JAXB
		}

		@Private
		Adapted(final Gate v) {
			source = v.source;
			destination = v.destination;
		}

		@Private
		Gate unmarshal() {
			return valueOf(source, destination);
		}

	}

	@Private
	Adapted marshal() {
		return new Adapted(this);
	}

	static class Adapter extends XmlAdapter<Adapted, Gate> {

		@Override
		public Adapted marshal(final Gate v) throws Exception {
			if (v == null) {
				return null;
			}
			return v.marshal();
		}

		@Override
		public Gate unmarshal(final Adapted v) throws Exception {
			if (v == null) {
				return null;
			}
			return v.unmarshal();
		}

	}

	public FieldPosition getSource() {
		return source;
	}

	public FieldPosition getDestination() {
		return destination;
	}

}
