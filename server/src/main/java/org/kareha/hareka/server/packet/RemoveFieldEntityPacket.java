package org.kareha.hareka.server.packet;

import org.kareha.hareka.protocol.WorldClientPacketType;
import org.kareha.hareka.server.entity.FieldEntity;
import org.kareha.hareka.server.protocol.WorldClientPacket;
import org.kareha.hareka.server.user.IdCipher;
import org.kareha.hareka.protocol.PacketType;

public final class RemoveFieldEntityPacket extends WorldClientPacket {

	public RemoveFieldEntityPacket(final FieldEntity entity, final IdCipher fieldEntityIdCipher) {
		out.write(fieldEntityIdCipher.encrypt(entity.getId()));
	}

	@Override
	protected PacketType getType() {
		return WorldClientPacketType.REMOVE_FIELD_ENTITY;
	}

}
