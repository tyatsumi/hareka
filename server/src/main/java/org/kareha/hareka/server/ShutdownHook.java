package org.kareha.hareka.server;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.kareha.hareka.logging.SimpleLogger;
import org.kareha.hareka.util.JarRunner;

public class ShutdownHook extends Thread {

	private final List<String> args;

	public ShutdownHook(final List<String> args) {
		this.args = args;
	}

	@Override
	// Logger does not work at shutdown. Use SimpleLogger instead.
	public void run() {
		SimpleLogger.INSTANCE.log("Shutdown..");

		Saver.INSTANCE.save(true);

		SimpleLogger.INSTANCE.log("Shutdown finished.");
		if (Rebooter.INSTANCE.isRebooting()) {
			SimpleLogger.INSTANCE.log("To be rebooted..");
			try {
				JarRunner.runJar(new File("harekaserv.jar"), args);
			} catch (final IOException e) {
				SimpleLogger.INSTANCE.log("Failed to reboot");
				e.printStackTrace();
			}
		}
	}

}
