package org.kareha.hareka.server.handler;

import java.io.IOException;
import java.security.Key;
import java.security.PublicKey;
import java.util.Arrays;
import java.util.logging.Logger;

import org.kareha.hareka.Constants;
import org.kareha.hareka.key.KeyGrabber;
import org.kareha.hareka.protocol.Handler;
import org.kareha.hareka.protocol.HandlerException;
import org.kareha.hareka.protocol.ProtocolException;
import org.kareha.hareka.protocol.ProtocolInput;
import org.kareha.hareka.server.external.Session;

public final class PublicKeyHandler implements Handler<Session> {

	private static final Logger logger = Logger.getLogger(PublicKeyHandler.class.getName());

	@Override
	public void handle(final ProtocolInput in, final Session session)
			throws IOException, ProtocolException, HandlerException {
		final int handlerId = in.readCompactUInt();
		final int version = in.readCompactUInt();
		final Key key = in.readKey();
		final String signatureAlgorithm = in.readString();
		final byte[] signature = in.readByteArray();

		final KeyGrabber.Entry entry = session.getKeyGrabber().get(handlerId);
		if (entry == null) {
			logger.fine(session.getStamp() + "Not found handlerId=" + handlerId);
			return;
		}

		if (key == null) {
			// have been informed above
			return;
		}

		if (!Arrays.asList(Constants.getAcceptablePublicKeyAlgorithms()).contains(key.getAlgorithm())) {
			logger.fine(session.getStamp() + "Not acceptable algorithm=" + key.getAlgorithm());
			return;
		}
		if (!Arrays.asList(Constants.getAcceptablePublicKeyFormats()).contains(key.getFormat())) {
			logger.fine(session.getStamp() + "Not acceptable format=" + key.getFormat());
			return;
		}

		if (!(key instanceof PublicKey)) {
			logger.fine(session.getStamp() + "Not a PublicKey");
			return;
		}
		final PublicKey publicKey = (PublicKey) key;

		if (!entry.verify(publicKey, signatureAlgorithm, signature)) {
			logger.fine(session.getStamp() + "Invalid signature");
			return;
		}
		entry.handle(version, publicKey);
	}

}
