package org.kareha.hareka.server.game;

import java.nio.ByteBuffer;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;

import org.kareha.hareka.annotation.GuardedBy;

public class DiceRoll {

	private final int id;
	private final int faces;
	private final int rate;
	private final int value;
	@GuardedBy("this")
	private final byte[] secret;
	private final String hashAlgorithm;
	@GuardedBy("this")
	private byte[] token;
	@GuardedBy("this")
	private byte[] hash;
	@GuardedBy("this")
	private int[] choice;
	@GuardedBy("this")
	private Boolean opponentWin;

	public DiceRoll(final int id, final int faces, final int rate, final Random random, final String hashAlgorithm) {
		if (faces < 1) {
			throw new IllegalArgumentException("faces must be positive");
		}
		if (rate < 0 || rate > faces) {
			throw new IllegalArgumentException("rate out of range");
		}
		this.id = id;
		this.faces = faces;
		this.rate = rate;
		value = random.nextInt(faces);
		secret = new byte[32];
		random.nextBytes(secret);
		try {
			MessageDigest.getInstance(hashAlgorithm);
		} catch (final NoSuchAlgorithmException e) {
			throw new IllegalArgumentException(e);
		}
		this.hashAlgorithm = hashAlgorithm;
	}

	public int getId() {
		return id;
	}

	public int getFaces() {
		return faces;
	}

	public int getRate() {
		return rate;
	}

	public int getValue() {
		return value;
	}

	public synchronized byte[] getSecret() {
		return secret.clone();
	}

	public synchronized void setToken(final byte[] token) {
		this.token = token.clone();
	}

	@GuardedBy("this")
	private void createHash() {
		final MessageDigest md;
		try {
			md = MessageDigest.getInstance(hashAlgorithm);
		} catch (final NoSuchAlgorithmException e) {
			throw new AssertionError(e);
		}
		final byte[] hash;
		synchronized (md) {
			md.reset();
			md.update(intToByteArray(value));
			md.update(secret);
			hash = md.digest(token);
		}
		this.hash = hash;
	}

	private byte[] intToByteArray(final int v) {
		final ByteBuffer b = ByteBuffer.allocate(4);
		b.putInt(v);
		return b.array();
	}

	public String getHashAlgorithm() {
		return hashAlgorithm;
	}

	public synchronized byte[] getHash() {
		if (hash == null) {
			createHash();
		}
		return hash.clone();
	}

	public synchronized boolean setChoice(final int[] choice) {
		if (choice.length > rate) {
			return false;
		}
		this.choice = choice.clone();
		return true;
	}

	@GuardedBy("this")
	private boolean checkOpponentWin() {
		for (final int c : choice) {
			if (c == value) {
				return true;
			}
		}
		return false;
	}

	public synchronized boolean isOpponentWinner() {
		if (opponentWin == null) {
			opponentWin = checkOpponentWin();
		}
		return opponentWin;
	}

}
