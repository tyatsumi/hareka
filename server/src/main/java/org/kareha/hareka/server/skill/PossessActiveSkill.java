package org.kareha.hareka.server.skill;

import org.kareha.hareka.field.TileType;
import org.kareha.hareka.game.ActiveSkillType;
import org.kareha.hareka.game.PassiveSkillType;
import org.kareha.hareka.server.Global;
import org.kareha.hareka.server.entity.CharacterEntity;
import org.kareha.hareka.server.entity.FieldDriver;
import org.kareha.hareka.server.entity.FieldEntity;
import org.kareha.hareka.server.external.Session;
import org.kareha.hareka.server.game.Player;
import org.kareha.hareka.server.user.User;
import org.kareha.hareka.wait.WaitType;

public class PossessActiveSkill implements ActiveSkill {

	@Override
	public ActiveSkillType getType() {
		return ActiveSkillType.POSSESS;
	}

	@Override
	public int getReach() {
		return 1;
	}

	@Override
	public WaitType getWaitType() {
		return WaitType.ATTACK;
	}

	@Override
	public int getWait(final CharacterEntity entity, final TileType tileType) {
		return entity.getStat().getAttackWait(tileType);
	}

	@Override
	public void use(final CharacterEntity entity, final FieldEntity target) {
		if (target == entity) {
			return;
		}
		if (!(target instanceof CharacterEntity)) {
			return;
		}
		final CharacterEntity targetChar = (CharacterEntity) target;
		if (!targetChar.getStat().getHealthPoints().isAlive()) {
			return;
		}

		if (targetChar.getStat().hasPassiveSkill(PassiveSkillType.IMMUNE_TO_POSSESSION)) {
			return;
		}

		for (final FieldDriver driver : entity.getFieldDrivers()) {
			if (!(driver instanceof Player)) {
				continue;
			}
			final Player player = (Player) driver;
			final Session session = player.getSession();
			final User user = player.getUser();
			session.logoutCharacter();
			final Player newPlayer = new Player(targetChar, session, user);
			if (!session.setPlayer(newPlayer)) {
				continue;
			}
			if (user.removeCharacterEntry(entity.getId())) {
				entity.removeOwner(user.getId());
				if (user.addCharacterEntry(targetChar.getId())) {
					targetChar.addOwner(user.getId());
				}
			}
			newPlayer.synchronize();
		}

		if (entity.getStat().hasPassiveSkill(PassiveSkillType.VOLATILE)) {
			entity.unmount();
			Global.INSTANCE.context().entities().destroyEntity(entity);
		}

	}

}
