package org.kareha.hareka.server.name;

import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.kareha.hareka.annotation.Private;
import org.kareha.hareka.annotation.ThreadSafe;
import org.kareha.hareka.game.Name;
import org.kareha.hareka.util.JaxbUtil;

@ThreadSafe
public class NameTable {

	@Private
	final Map<String, Name> map = new ConcurrentHashMap<>();

	@XmlRootElement(name = "nameTable")
	@XmlAccessorType(XmlAccessType.NONE)
	private static class Adapted {

		@XmlElement
		@Private
		Name[] name;

		@SuppressWarnings("unused")
		private Adapted() {
			// used by JAXB
		}

		@Private
		Adapted(final NameTable v) {
			name = new Name[v.map.size()];
			int i = 0;
			for (final Name n : v.map.values()) {
				name[i] = n;
				i++;
			}
		}

	}

	public void load(final File file) throws IOException, JAXBException {
		final JAXBContext jaxbContext;
		try {
			jaxbContext = JAXBContext.newInstance(Adapted.class);
		} catch (final JAXBException e) {
			throw new RuntimeException(e);
		}
		final Adapted adapted = JaxbUtil.unmarshal(file, jaxbContext);
		map.clear();
		for (final Name name : adapted.name) {
			map.put(name.get("en"), name);
		}
	}

	public void save(final File file) throws IOException, JAXBException {
		final JAXBContext jaxbContext;
		try {
			jaxbContext = JAXBContext.newInstance(Adapted.class);
		} catch (final JAXBException e) {
			throw new RuntimeException(e);
		}
		JaxbUtil.marshal(new Adapted(this), file, jaxbContext);
	}

	public Name get(final String enTranslation) {
		return map.get(enTranslation);
	}

}
