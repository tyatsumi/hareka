package org.kareha.hareka.server.skill;

import org.kareha.hareka.field.TileType;
import org.kareha.hareka.field.Vector;
import org.kareha.hareka.game.ActiveSkillType;
import org.kareha.hareka.server.entity.CharacterEntity;
import org.kareha.hareka.server.entity.FieldEntity;
import org.kareha.hareka.wait.WaitType;

public interface ActiveSkill {

	ActiveSkillType getType();

	int getReach();

	WaitType getWaitType();

	int getWait(CharacterEntity entity, TileType tileType);

	default void use(CharacterEntity entity) {
		throw new RuntimeException("Unexpected method has been called");
	}

	default void use(CharacterEntity entity, FieldEntity target) {
		throw new RuntimeException("Unexpected method has been called");
	}

	default void use(CharacterEntity entity, Vector target) {
		throw new RuntimeException("Unexpected method has been called");
	}

}
