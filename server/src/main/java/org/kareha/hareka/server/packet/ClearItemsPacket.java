package org.kareha.hareka.server.packet;

import org.kareha.hareka.protocol.WorldClientPacketType;
import org.kareha.hareka.server.protocol.WorldClientPacket;
import org.kareha.hareka.protocol.PacketType;

public final class ClearItemsPacket extends WorldClientPacket {

	public ClearItemsPacket() {
		// do nothing
	}

	@Override
	protected PacketType getType() {
		return WorldClientPacketType.CLEAR_ITEMS;
	}

}
