package org.kareha.hareka.server.packet;

import org.kareha.hareka.field.TileType;
import org.kareha.hareka.protocol.WorldClientPacketType;
import org.kareha.hareka.server.entity.CharacterEntity;
import org.kareha.hareka.server.entity.ItemEntity;
import org.kareha.hareka.server.item.Item;
import org.kareha.hareka.server.protocol.WorldClientPacket;
import org.kareha.hareka.server.user.IdCipher;
import org.kareha.hareka.protocol.PacketType;

public final class AddItemPacket extends WorldClientPacket {

	public AddItemPacket(final ItemEntity itemEntity, final IdCipher itemEntityIdCipher, final String language,
			final CharacterEntity entity) {
		out.write(itemEntityIdCipher.encrypt(itemEntity.getId()));
		out.writeString(itemEntity.getShape());
		out.writeCompactULong(itemEntity.getStat().getCount());
		final Item item = itemEntity.getStat().getBrand();
		out.write(item.getType());
		out.writeString(item.getName().get(language));
		out.writeCompactUInt(item.getWeight());
		out.writeCompactUInt(item.getReach());
		out.write(item.getWaitType());
		out.writeCompactUInt(item.getWait(entity, TileType.NULL));
	}

	@Override
	protected PacketType getType() {
		return WorldClientPacketType.ADD_ITEM;
	}

}
