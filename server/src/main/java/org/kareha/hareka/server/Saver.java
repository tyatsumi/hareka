package org.kareha.hareka.server;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.bind.JAXBException;

import org.kareha.hareka.logging.SimpleLogger;
import org.kareha.hareka.server.external.Session;
import org.kareha.hareka.server.net.ServerException;

public enum Saver {

	INSTANCE;

	private static final Logger logger = Logger.getLogger(Saver.class.getName());

	private enum BundleKey {
		ServerShutdown, DataSaving,
	}

	private void forceExitAllConnections(final boolean shutdown) throws InterruptedException {
		final Context context = Global.INSTANCE.context();
		final Collection<Session> sessions = context.sessions().getSessions();
		for (final Session s : sessions) {
			final ResourceBundle bundle = s.getBundle(Saver.class.getName());
			if (shutdown) {
				s.writeExitPacket(bundle.getString(BundleKey.ServerShutdown.name()));
			} else {
				s.writeExitPacket(bundle.getString(BundleKey.DataSaving.name()));
			}
		}
		Thread.sleep(500);
		for (final Session s : sessions) {
			s.close();
		}
		while (context.sessions().sizeOfSessions() > 0) {
			Thread.sleep(100);
		}
	}

	public synchronized void save(final boolean shutdown) {
		final Context context = Global.INSTANCE.context();

		if (shutdown) {
			SimpleLogger.INSTANCE.log("Data saving..");
		} else {
			logger.info("Data saving..");
		}

		// Stop listening
		try {
			context.server().stop();
		} catch (final IOException e) {
			if (shutdown) {
				SimpleLogger.INSTANCE.log(e);
			} else {
				logger.log(Level.SEVERE, "", e);
			}
			return;
		} catch (final InterruptedException e) {
			Thread.currentThread().interrupt();
			if (shutdown) {
				SimpleLogger.INSTANCE.log(e);
			} else {
				logger.log(Level.SEVERE, "", e);
			}
			return;
		}

		// Stop motion
		context.entities().stopMotion();

		// Kick all clients
		try {
			forceExitAllConnections(shutdown);
		} catch (final InterruptedException e) {
			Thread.currentThread().interrupt();
			if (shutdown) {
				SimpleLogger.INSTANCE.log(e);
			} else {
				logger.log(Level.SEVERE, "", e);
			}
			return;
		}

		// Save data
		try {
			context.save();
		} catch (final JAXBException e) {
			logger.log(Level.SEVERE, "Failed to save", e);
			return;
		}

		if (shutdown) {
			// Close transaction
			final File crashedFile = new File(context.parameters().liveDirectory(), ServerConstants.CRASHED_FILENAME);
			if (!crashedFile.delete()) {
				SimpleLogger.INSTANCE.log("Cannot delete crashed file");
				return;
			}
			return;
		}

		// Start motion
		context.entities().startMotion();

		// Start listening
		try {
			context.server().start();
		} catch (final IOException | ServerException e) {
			logger.log(Level.SEVERE, "", e);
			return;
		}
	}

}
