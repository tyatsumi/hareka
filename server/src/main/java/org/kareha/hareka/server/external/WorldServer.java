package org.kareha.hareka.server.external;

import java.io.File;
import java.io.IOException;
import java.net.Socket;
import java.text.MessageFormat;
import java.util.logging.Logger;

import org.kareha.hareka.protocol.HandlerTable;
import org.kareha.hareka.protocol.WorldServerPacketType;
import org.kareha.hareka.server.Context;
import org.kareha.hareka.server.ServerConstants;
import org.kareha.hareka.server.net.PlainServer;
import org.kareha.hareka.server.net.SecureServer;
import org.kareha.hareka.server.net.Server;
import org.kareha.hareka.server.net.ServerException;
import org.kareha.hareka.server.packet.VersionPacket;

public class WorldServer implements Server {

	private static final Logger logger = Logger.getLogger(WorldServer.class.getName());

	private final Server server;

	public WorldServer(final Context context, final int port, final File keyStoreFile) {
		final HandlerTable<WorldServerPacketType, Session> parserTable = new HandlerTable<>(
				"org.kareha.hareka.server.handler", WorldServerPacketType.class, "Handler");
		class Accepter {
			void accepted(final Socket clientSocket) {
				final Session session = Session.newInstance(context, clientSocket, parserTable);
				context.sessions().addSession(session);
				session.start();

				session.write(new VersionPacket());
			}
		}
		final Accepter accepter = new Accepter();

		if (keyStoreFile == null) {
			server = new PlainServer(port) {
				@Override
				protected void accepted(final Socket clientSocket) {
					accepter.accepted(clientSocket);
				}
			};
		} else {
			server = new SecureServer(port, keyStoreFile, ServerConstants.KEY_STORE_PASSWORD.toCharArray(),
					ServerConstants.KEY_PASSWORD.toCharArray()) {
				@Override
				protected void accepted(final Socket clientSocket) {
					accepter.accepted(clientSocket);
				}
			};
		}
	}

	@Override
	public int port() {
		return server.port();
	}

	@Override
	public boolean start() throws IOException, ServerException {
		if (!server.start()) {
			logger.warning("Failed to start server; Server is already running.");
			return false;
		}
		logger.info(MessageFormat.format("Listening to port {0}", port()));
		logger.info("Waiting for connections..");
		return true;
	}

	@Override
	public boolean stop() throws IOException, InterruptedException {
		logger.info("Stop Listening..");
		if (!server.stop()) {
			logger.warning("Failed to stop server; Server has already been stopped");
			return false;
		}
		logger.info("Listening stopped");
		return true;
	}

}
