package org.kareha.hareka.server.packet;

import org.kareha.hareka.protocol.WorldClientPacketType;
import org.kareha.hareka.server.entity.ChatEntity;
import org.kareha.hareka.server.protocol.WorldClientPacket;
import org.kareha.hareka.server.user.IdCipher;
import org.kareha.hareka.protocol.PacketType;

public final class PrivateChatPacket extends WorldClientPacket {

	public PrivateChatPacket(final ChatEntity peer, final String content, final IdCipher chatEntityIdCipher,
			final String language) {
		out.write(chatEntityIdCipher.encrypt(peer.getId()));
		out.writeString(peer.getName(language));
		out.writeString(content);
	}

	@Override
	protected PacketType getType() {
		return WorldClientPacketType.PRIVATE_CHAT;
	}

}
