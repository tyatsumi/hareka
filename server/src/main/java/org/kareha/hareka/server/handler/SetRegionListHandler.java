package org.kareha.hareka.server.handler;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Logger;

import org.kareha.hareka.field.Region;
import org.kareha.hareka.field.SimpleRegion;
import org.kareha.hareka.protocol.Handler;
import org.kareha.hareka.protocol.HandlerException;
import org.kareha.hareka.protocol.ProtocolException;
import org.kareha.hareka.protocol.ProtocolInput;
import org.kareha.hareka.server.entity.CharacterEntity;
import org.kareha.hareka.server.external.Session;
import org.kareha.hareka.server.field.ServerField;
import org.kareha.hareka.server.field.ServerFieldObject;
import org.kareha.hareka.server.field.ServerSimpleRegion;
import org.kareha.hareka.server.game.Player;
import org.kareha.hareka.server.user.User;
import org.kareha.hareka.user.Permission;
import org.kareha.hareka.user.RoleSet;

public final class SetRegionListHandler implements Handler<Session> {

	private static final Logger logger = Logger.getLogger(SetRegionListHandler.class.getName());

	private enum BundleKey {
		YouCannotUseThisFunction,
	}

	private static void inform(final Session session, final String key) {
		final ResourceBundle bundle = session.getBundle(SetRegionListHandler.class.getName());
		session.writeInformPacket(bundle.getString(key));
	}

	@Override
	public void handle(final ProtocolInput in, final Session session)
			throws IOException, ProtocolException, HandlerException {
		final int size = in.readCompactUInt();
		final Region[] regions = new Region[size];
		for (int i = 0; i < size; i++) {
			regions[i] = Region.readFrom(in);
		}

		final User user = session.getUser();
		if (user == null) {
			return;
		}
		final RoleSet roles = session.getContext().accessController().getRoleSet(user);
		if (!roles.isAbleTo(Permission.EDIT_REGIONS)) {
			inform(session, BundleKey.YouCannotUseThisFunction.name());
			logger.fine(session.getStamp() + "User cannot edit tile fields");
			return;
		}

		final Player player = session.getPlayer();
		if (player == null) {
			return;
		}
		final CharacterEntity entity = player.getEntity();
		final ServerFieldObject fo = entity.getFieldObject();
		if (fo == null) {
			return;
		}
		final ServerField field = fo.getField();

		final List<Region> realRegions = new ArrayList<>();
		for (final Region region : regions) {
			Region realRegion = region.invert(player.getTransformation());
			if (realRegion instanceof SimpleRegion) {
				realRegion = new ServerSimpleRegion((SimpleRegion) realRegion, field);
			}
			realRegions.add(realRegion);
		}
		field.setRegions(realRegions);

		for (final Session s : session.getContext().sessions().getSessions()) {
			final Player p = s.getPlayer();
			if (p == null) {
				continue;
			}
			final ServerFieldObject o = p.getEntity().getFieldObject();
			if (o == null) {
				continue;
			}
			if (o.getField() != field) {
				continue;
			}
			p.synchronize();
		}
	}

}
