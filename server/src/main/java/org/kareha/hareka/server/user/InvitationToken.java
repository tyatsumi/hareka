package org.kareha.hareka.server.user;

import java.security.SecureRandom;
import java.util.Date;

import javax.xml.bind.DatatypeConverter;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.kareha.hareka.annotation.Private;
import org.kareha.hareka.annotation.ThreadSafe;
import org.kareha.hareka.user.UserId;

@ThreadSafe
@XmlJavaTypeAdapter(InvitationToken.Adapter.class)
public class InvitationToken {

	@Private
	final long id;
	@Private
	final long issuedDate;
	@Private
	final UserId issuer;
	@Private
	final byte[] value;

	public InvitationToken(final long id, final UserId issuer) {
		this.id = id;
		issuedDate = System.currentTimeMillis();
		this.issuer = issuer;
		value = new byte[32];
		new SecureRandom().nextBytes(value);
	}

	public InvitationToken(final long id, final long issuedDate, final UserId issuer, final byte[] value) {
		this.id = id;
		this.issuedDate = issuedDate;
		this.issuer = issuer;
		this.value = value.clone();
	}

	@XmlType(name = "invitationToken")
	@XmlAccessorType(XmlAccessType.NONE)
	private static class Adapted {

		@XmlElement
		private String id;
		@XmlElement
		private Date issuedDate;
		@XmlElement
		private UserId issuer;
		@XmlElement
		private byte[] value;

		@SuppressWarnings("unused")
		private Adapted() {
			// used by JAXB
		}

		@Private
		Adapted(final InvitationToken v) {
			id = Long.toHexString(v.id);
			issuedDate = new Date(v.issuedDate);
			issuer = v.issuer;
			value = v.value.clone();
		}

		@Private
		InvitationToken unmarshal() {
			return new InvitationToken(Long.parseLong(id, 16), issuedDate.getTime(), issuer, value);
		}

	}

	@Private
	Adapted marshal() {
		return new Adapted(this);
	}

	static class Adapter extends XmlAdapter<Adapted, InvitationToken> {

		@Override
		public Adapted marshal(final InvitationToken v) throws Exception {
			if (v == null) {
				return null;
			}
			return v.marshal();
		}

		@Override
		public InvitationToken unmarshal(final Adapted v) throws Exception {
			if (v == null) {
				return null;
			}
			return v.unmarshal();
		}

	}

	public long getId() {
		return id;
	}

	public long getIssuedDate() {
		return issuedDate;
	}

	public UserId getIssuer() {
		return issuer;
	}

	public byte[] getValue() {
		return value.clone();
	}

	public String toHexString() {
		return DatatypeConverter.printHexBinary(value).toLowerCase();
	}

}
