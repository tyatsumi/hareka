package org.kareha.hareka.server.field;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import org.kareha.hareka.annotation.Immutable;
import org.kareha.hareka.annotation.Private;
import org.kareha.hareka.field.Vector;

@Immutable
public class PeriodicBoundary extends Boundary {

	@Private
	final int size;
	@Private
	final boolean parity;
	private final Vector[] centers = new Vector[6];

	public PeriodicBoundary(final int size, final boolean parity) {
		this.size = size;
		this.parity = parity;
		final Vector center = Vector.valueOf(2 * size + 1, -size - 1);
		for (int i = 0; i < 6; i++) {
			centers[i] = center.rotate(i).reflect(parity);
		}
	}

	@XmlType(name = "periodicBoundary")
	@XmlAccessorType(XmlAccessType.NONE)
	protected static class Adapted extends Boundary.Adapted {

		@XmlElement
		protected int size;
		@XmlElement
		protected boolean parity;

		protected Adapted() {
			// used by JAXB
		}

		protected Adapted(final PeriodicBoundary v) {
			super(v);
			size = v.size;
			parity = v.parity;
		}

		@Override
		protected PeriodicBoundary unmarshal() {
			return new PeriodicBoundary(size, parity);
		}

	}

	@Override
	protected Adapted marshal() {
		return new Adapted(this);
	}

	public int getSize() {
		return size;
	}

	@Override
	public Vector confine(final Vector position) {
		final int length = position.distance();
		if (length <= size) {
			return position;
		} else if (length <= size * 2 + 1) {
			for (final Vector center : centers) {
				if (position.distance(center) <= size) {
					return position.subtract(center);
				}
			}
			throw new AssertionError();
		} else {
			Vector p = position;
			do {
				Vector nearest = centers[0];
				int minDistance = p.distance(nearest);
				if (minDistance <= size) {
					return p.subtract(nearest);
				}
				for (int i = 1; i < 6; i++) {
					final Vector center = centers[i];
					final int distance = p.distance(center);
					if (distance <= size) {
						return p.subtract(center);
					}
					if (distance < minDistance) {
						minDistance = distance;
						nearest = center;
					}
				}
				p = p.subtract(nearest);
			} while (true);
		}
	}

}
