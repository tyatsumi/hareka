package org.kareha.hareka.server.control;

@SuppressWarnings("serial")
public class TargetNotMountedException extends Exception {

	public TargetNotMountedException() {
		// do nothing
	}

	public TargetNotMountedException(final String message) {
		super(message);
	}

	public TargetNotMountedException(final String message, final Throwable cause) {
		super(message, cause);
	}

}
