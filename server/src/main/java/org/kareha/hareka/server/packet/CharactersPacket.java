package org.kareha.hareka.server.packet;

import java.util.Collection;

import org.kareha.hareka.protocol.WorldClientPacketType;
import org.kareha.hareka.server.entity.CharacterEntity;
import org.kareha.hareka.server.entity.Entities;
import org.kareha.hareka.server.entity.Entity;
import org.kareha.hareka.server.protocol.WorldClientPacket;
import org.kareha.hareka.server.user.IdCipher;
import org.kareha.hareka.server.user.User;
import org.kareha.hareka.protocol.PacketType;

public class CharactersPacket extends WorldClientPacket {

	public CharactersPacket(final Collection<User.CharacterEntry> entries, final Entities entityTable,
			final IdCipher chatEntityIdCipher, final String language) {
		out.writeCompactUInt(entries.size());
		for (final User.CharacterEntry entry : entries) {
			out.write(chatEntityIdCipher.encrypt(entry.getId()));
			final Entity entity = entityTable.get(entry.getId());
			if (!(entity instanceof CharacterEntity)) {
				out.writeString("(null)");
				out.writeString("Undefined");
			} else {
				final CharacterEntity ce = (CharacterEntity) entity;
				final String name = ce.getName(language);
				if (name == null) {
					out.writeString("Undefined");
				} else {
					out.writeString(name);
				}
				final String shape = ce.getShape();
				if (shape == null) {
					out.writeString("Undefined");
				} else {
					out.writeString(shape);
				}
			}
		}
	}

	@Override
	protected PacketType getType() {
		return WorldClientPacketType.CHARACTERS;
	}

}
