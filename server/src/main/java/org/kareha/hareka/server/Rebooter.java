package org.kareha.hareka.server;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.kareha.hareka.annotation.GuardedBy;

public enum Rebooter {

	INSTANCE;

	private static final Logger logger = Logger.getLogger(Rebooter.class.getName());

	@GuardedBy("this")
	private boolean initialized;
	@GuardedBy("this")
	private File file;
	@GuardedBy("this")
	private boolean rebooted;
	@GuardedBy("this")
	private boolean rebooting;
	@GuardedBy("this")
	private boolean shutdownStarted;

	public synchronized void initialize(final File file) {
		if (initialized) {
			throw new IllegalStateException("Already initialized");
		}
		initialized = true;

		this.file = file;
		rebooted = file.exists();
		if (rebooted) {
			if (!file.delete()) {
				logger.warning("Cannot delete " + file);
			}
		}
	}

	public synchronized boolean isRebooted() {
		return rebooted;
	}

	private void doShutdown() {
		// System.exit never returns
		new Thread(() -> System.exit(0)).start();
	}

	public void shutdown() {
		synchronized (this) {
			if (shutdownStarted) {
				return;
			}
			shutdownStarted = true;

			if (file.exists()) {
				if (!file.delete()) {
					logger.warning("Cannot delete " + file);
				}
			}
			rebooting = false;
		}
		doShutdown();
	}

	public void reboot() {
		synchronized (this) {
			if (shutdownStarted) {
				return;
			}
			shutdownStarted = true;

			if (!file.exists()) {
				try {
					file.createNewFile();
				} catch (final IOException e) {
					logger.log(Level.WARNING, "", e);
				}
			}
			rebooting = true;
		}
		doShutdown();
	}

	public synchronized boolean isRebooting() {
		return rebooting;
	}

}
