package org.kareha.hareka.server.stat;

public interface Stat {

	String getShape();

	StatPoints getHealthPoints();

	StatPoints getMagicPoints();

	long getCount();

	boolean isExclusive();

	int getJumpDown();

	int getJumpUp();

}
