package org.kareha.hareka.server.packet;

import org.kareha.hareka.field.Field;
import org.kareha.hareka.protocol.WorldClientPacketType;
import org.kareha.hareka.server.protocol.WorldClientPacket;
import org.kareha.hareka.protocol.PacketType;

public final class FieldPacket extends WorldClientPacket {

	public FieldPacket(final Field field) {
		out.writeCompactUInt(field.getViewSize());
	}

	@Override
	protected PacketType getType() {
		return WorldClientPacketType.FIELD;
	}

}
