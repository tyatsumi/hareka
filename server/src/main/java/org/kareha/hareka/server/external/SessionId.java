package org.kareha.hareka.server.external;

import org.kareha.hareka.annotation.Immutable;

@Immutable
public class SessionId {

	private final long value;

	private SessionId(final long value) {
		this.value = value;
	}

	public static SessionId valueOf(final long value) {
		return new SessionId(value);
	}

	public static SessionId valueOf(final String s) {
		// Long.parseLong throws NumberFormatException
		final long value = Long.parseLong(s, 16);
		return valueOf(value);
	}

	@Override
	public boolean equals(final Object obj) {
		if (!(obj instanceof SessionId)) {
			return false;
		}
		final SessionId id = (SessionId) obj;
		return id.value == value;
	}

	@Override
	public int hashCode() {
		return (int) (value >>> 32 ^ value);
	}

	@Override
	public String toString() {
		return Long.toHexString(value);
	}

	public long value() {
		return value;
	}

}
