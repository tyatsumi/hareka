package org.kareha.hareka.server.packet;

import org.kareha.hareka.protocol.WorldClientPacketType;
import org.kareha.hareka.server.entity.ChatEntity;
import org.kareha.hareka.server.protocol.WorldClientPacket;
import org.kareha.hareka.server.user.IdCipher;
import org.kareha.hareka.protocol.PacketType;

public final class LocalChatPacket extends WorldClientPacket {

	public LocalChatPacket(final ChatEntity speaker, final String content, final IdCipher chatEntityIdCipher,
			final String language) {
		out.write(chatEntityIdCipher.encrypt(speaker.getId()));
		out.writeString(speaker.getName(language));
		out.writeString(content);
	}

	@Override
	protected PacketType getType() {
		return WorldClientPacketType.LOCAL_CHAT;
	}

}
