package org.kareha.hareka.server.handler;

import java.io.IOException;

import org.kareha.hareka.LocalEntityId;
import org.kareha.hareka.field.Placement;
import org.kareha.hareka.field.Tile;
import org.kareha.hareka.field.TileType;
import org.kareha.hareka.field.Vector;
import org.kareha.hareka.game.ActiveSkillType;
import org.kareha.hareka.protocol.Handler;
import org.kareha.hareka.protocol.HandlerException;
import org.kareha.hareka.protocol.ProtocolException;
import org.kareha.hareka.protocol.ProtocolInput;
import org.kareha.hareka.server.entity.Entity;
import org.kareha.hareka.server.entity.EntityId;
import org.kareha.hareka.server.entity.FieldEntity;
import org.kareha.hareka.server.external.Session;
import org.kareha.hareka.server.field.ServerFieldObject;
import org.kareha.hareka.server.game.Player;
import org.kareha.hareka.server.packet.WaitPacket;
import org.kareha.hareka.server.skill.ActiveSkill;
import org.kareha.hareka.wait.WaitResult;

public final class UseActiveSkillHandler implements Handler<Session> {

	@Override
	public void handle(final ProtocolInput in, final Session session)
			throws IOException, ProtocolException, HandlerException {
		// read arguments
		final long waitId = in.readCompactULong();
		final ActiveSkillType abilityType = ActiveSkillType.readFrom(in);
		final LocalEntityId targetLocalId;
		final Vector targetLocalPosition;
		switch (abilityType.getTargetType()) {
		default:
			return;
		case NULL:
			targetLocalId = null;
			targetLocalPosition = null;
			break;
		case FIELD_ENTITY:
			targetLocalId = LocalEntityId.readFrom(in);
			targetLocalPosition = null;
			break;
		case TILE:
			targetLocalId = null;
			targetLocalPosition = Vector.readFrom(in);
			break;
		}

		final Player player = session.getPlayer();
		if (player == null) {
			return;
		}
		if (!player.getEntity().getStat().hasActiveSkill(abilityType)) {
			return;
		}
		final ActiveSkill ability = session.getContext().activeSkills().get(abilityType);
		if (ability == null) {
			return;
		}

		final ServerFieldObject fo = player.getEntity().getFieldObject();
		final TileType tileType = fo.getField().getTile(fo.getPlacement().getPosition()).type();
		final WaitResult result = player.getEntity().getWaitGenerator().next(waitId, ability.getWaitType(),
				ability.getWait(player.getEntity(), tileType), player.getWaitPenalty());
		if (!result.isSuccess()) {
			session.write(new WaitPacket(result.getWait()));
			return;
		}
		player.getEntity().sendWait(result.getWait());

		if (player.getEntity().isStuck()) {
			return;
		}
		player.getEntity().stopMotion();

		switch (abilityType.getTargetType()) {
		default:
			return;
		case NULL: {
			ability.use(player.getEntity());
		}
			return;
		case FIELD_ENTITY: {
			final EntityId targetEntityId = session.getUser().getFieldEntityIdCipher().decrypt(targetLocalId);
			final Entity targetEntity = session.getContext().entities().get(targetEntityId);
			if (!(targetEntity instanceof FieldEntity)) {
				return;
			}
			final FieldEntity targetFieldEntity = (FieldEntity) targetEntity;
			final ServerFieldObject targetFo = targetFieldEntity.getFieldObject();
			if (targetFo == null) {
				return;
			}

			if (fo.getField() != targetFo.getField()) {
				return;
			}
			final Placement placement = fo.getPlacement();
			final Placement targetPlacement = targetFo.getPlacement();
			if (fo.getField().getBoundary().distance(placement.getPosition(), targetPlacement.getPosition()) > ability
					.getReach()) {
				return;
			}

			final Tile tile = fo.getField().getTile(placement.getPosition());
			final Tile targetTile = targetFo.getField().getTile(targetPlacement.getPosition());
			final int delta = targetTile.elevation() - tile.elevation();
			if (delta >= 0) {
				if (delta > player.getEntity().getJumpUp()) {
					return;
				}
			} else {
				if (-delta > player.getEntity().getJumpDown()) {
					return;
				}
			}

			ability.use(player.getEntity(), targetFieldEntity);
		}
			return;
		case TILE: {
			final Placement placement = fo.getPlacement();
			@SuppressWarnings("null")
			final Vector targetPosition = targetLocalPosition.invert(player.getTransformation());
			if (fo.getField().getBoundary().distance(placement.getPosition(), targetPosition) > ability.getReach()) {
				return;
			}
			ability.use(player.getEntity(), targetPosition);
		}
			return;
		}

	}

}
