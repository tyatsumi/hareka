package org.kareha.hareka.server.packet;

import org.kareha.hareka.protocol.WorldClientPacketType;
import org.kareha.hareka.server.entity.ChatEntity;
import org.kareha.hareka.server.protocol.WorldClientPacket;
import org.kareha.hareka.server.user.IdCipher;
import org.kareha.hareka.protocol.PacketType;

public final class SelfChatEntityPacket extends WorldClientPacket {

	public SelfChatEntityPacket(final ChatEntity entity, final IdCipher chatEntityIdCipher, final String language) {
		out.write(chatEntityIdCipher.encrypt(entity.getId()));
		out.writeString(entity.getName(language));
	}

	@Override
	protected PacketType getType() {
		return WorldClientPacketType.SELF_CHAT_ENTITY;
	}

}
