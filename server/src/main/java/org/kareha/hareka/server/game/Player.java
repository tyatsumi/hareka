package org.kareha.hareka.server.game;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.kareha.hareka.annotation.GuardedBy;
import org.kareha.hareka.field.Direction;
import org.kareha.hareka.field.FieldObject;
import org.kareha.hareka.field.Placement;
import org.kareha.hareka.field.TilePiece;
import org.kareha.hareka.field.TileType;
import org.kareha.hareka.field.Transformation;
import org.kareha.hareka.field.Vector;
import org.kareha.hareka.field.Vectors;
import org.kareha.hareka.game.ActiveSkillType;
import org.kareha.hareka.server.entity.CharacterEntity;
import org.kareha.hareka.server.entity.ChatDriver;
import org.kareha.hareka.server.entity.ChatEntity;
import org.kareha.hareka.server.entity.FieldDriver;
import org.kareha.hareka.server.entity.FieldEntity;
import org.kareha.hareka.server.entity.ItemEntity;
import org.kareha.hareka.server.external.Session;
import org.kareha.hareka.server.field.FieldPosition;
import org.kareha.hareka.server.field.ServerField;
import org.kareha.hareka.server.field.ServerFieldObject;
import org.kareha.hareka.server.packet.AddActiveSkillPacket;
import org.kareha.hareka.server.packet.AddFieldEntityPacket;
import org.kareha.hareka.server.packet.AddItemPacket;
import org.kareha.hareka.server.packet.ClearActiveSkillsPacket;
import org.kareha.hareka.server.packet.ClearItemsPacket;
import org.kareha.hareka.server.packet.EffectPacket;
import org.kareha.hareka.server.packet.EntityIdentityPacket;
import org.kareha.hareka.server.packet.FieldPacket;
import org.kareha.hareka.server.packet.HealthBarPacket;
import org.kareha.hareka.server.packet.LocalChatPacket;
import org.kareha.hareka.server.packet.MagicBarPacket;
import org.kareha.hareka.server.packet.PlaceFieldEntityPacket;
import org.kareha.hareka.server.packet.PrivateChatEchoPacket;
import org.kareha.hareka.server.packet.PrivateChatPacket;
import org.kareha.hareka.server.packet.RefreshPacket;
import org.kareha.hareka.server.packet.RemoveFieldEntityPacket;
import org.kareha.hareka.server.packet.SelfChatEntityPacket;
import org.kareha.hareka.server.packet.SelfFieldEntityPacket;
import org.kareha.hareka.server.packet.SelfNamePacket;
import org.kareha.hareka.server.packet.TilesPacket;
import org.kareha.hareka.server.packet.WaitPacket;
import org.kareha.hareka.server.packet.WeightBarPacket;
import org.kareha.hareka.server.skill.ActiveSkill;
import org.kareha.hareka.server.stat.StatPoints;
import org.kareha.hareka.server.user.User;
import org.kareha.hareka.wait.Wait;
import org.kareha.hareka.wait.WaitPenalty;

public class Player implements ChatDriver, FieldDriver {

	private final CharacterEntity entity;
	private final Session session;
	private final User user;
	private final WaitPenalty waitPenalty;
	private volatile FieldPosition mark;
	@GuardedBy("this")
	private Vector position = Vector.ZERO;
	@GuardedBy("this")
	private Vector startPosition = Vector.ZERO;

	public Player(final CharacterEntity entity, final Session session, final User user) {
		this.entity = entity;
		this.session = session;
		this.user = user;
		waitPenalty = new WaitPenalty();
	}

	public CharacterEntity getEntity() {
		return entity;
	}

	public Session getSession() {
		return session;
	}

	public User getUser() {
		return user;
	}

	public WaitPenalty getWaitPenalty() {
		return waitPenalty;
	}

	public FieldPosition getMark() {
		return mark;
	}

	public void setMark(final FieldPosition v) {
		mark = v;
	}

	public synchronized Vector getPosition() {
		return position;
	}

	public synchronized Transformation getTransformation() {
		final Transformation t = getEntity().getTransformation();
		return Transformation.valueOf(startPosition, t.rotation(), t.reflection(), t.elevation());
	}

	@Override
	public void handleReceiveLocalChat(final ChatEntity speaker, final String content) {
		if (speaker instanceof FieldEntity) {
			final FieldEntity fieldEntity = (FieldEntity) speaker;
			if (entity.getFieldObject().containsInView(fieldEntity.getFieldObject())
					&& entity.isVisible(fieldEntity.getVisualType())) {
				session.write(new EntityIdentityPacket(speaker, user.getChatEntityIdCipher(), fieldEntity,
						user.getFieldEntityIdCipher()));
			}
		}
		session.write(
				new LocalChatPacket(speaker, content, user.getChatEntityIdCipher(), session.getLocale().getLanguage()));
	}

	@Override
	public void handleSendPrivateChatEcho(final ChatEntity peer, final String content) {
		session.write(new PrivateChatEchoPacket(peer, content, user.getChatEntityIdCipher(),
				session.getLocale().getLanguage()));
	}

	@Override
	public void handleReceivePrivateChat(final ChatEntity peer, final String content) {
		session.write(
				new PrivateChatPacket(peer, content, user.getChatEntityIdCipher(), session.getLocale().getLanguage()));
	}

	@Override
	public synchronized void handleAddInViewObject(final ServerFieldObject v) {
		session.write(new AddFieldEntityPacket(v.getFieldEntity(), user.getFieldEntityIdCipher(), getTransformation(),
				this.position));
	}

	@Override
	public synchronized void handleRemoveInViewObject(final ServerFieldObject v) {
		session.write(new RemoveFieldEntityPacket(v.getFieldEntity(), user.getFieldEntityIdCipher()));
	}

	@Override
	public synchronized void handleInViewObjectMoved(final ServerFieldObject v, final Placement placement) {
		final TileType tileType = v.getField().getTile(placement.getPosition()).type();
		session.write(new PlaceFieldEntityPacket(v.getFieldEntity(), placement, tileType, user.getFieldEntityIdCipher(),
				getTransformation(), this.position));
	}

	@Override
	public synchronized void handleMove(final Placement prevPlacement, final Placement newPlacement) {
		final Vector prevPosition = this.position;
		this.position = getEntity().getFieldObject().getField().getBoundary().confine(newPlacement.getPosition(),
				prevPosition);

		final int distance = newPlacement.getPosition().distance(prevPlacement.getPosition());
		if (distance == 0) {
			return;
		} else if (distance == 1) {
			final ServerFieldObject fo = entity.getFieldObject();
			if (fo == null) {
				return;
			}
			final ServerField field = fo.getField(); // XXX field may be old
			if (field == null) {
				return;
			}
			final Direction direction = newPlacement.getPosition().subtract(prevPlacement.getPosition()).direction();
			final Vector[] edge = Vectors.edge(this.position, field.getViewSize(), direction); // Notice: this.position
			final List<Vector> list = Arrays.asList(edge);
			Collections.shuffle(list);
			final Transformation t = getTransformation();
			final List<TilePiece> tiles = new ArrayList<>();
			for (final Vector point : list) {
				final TilePiece tilePiece = TilePiece.valueOf(point, field.getTile(point));
				tiles.add(tilePiece);
			}
			session.write(new TilesPacket(tiles, t, position, field.getBoundary()));
		} else {
			final ServerFieldObject fo = entity.getFieldObject();
			if (fo == null) {
				return;
			}
			final ServerField field = fo.getField(); // XXX field may be old
			if (field == null) {
				return;
			}
			final Set<Vector> prevSet = new HashSet<>(Arrays.asList(Vectors.range(prevPosition, field.getViewSize())));
			final Set<Vector> newSet = new HashSet<>(Arrays.asList(Vectors.range(this.position, field.getViewSize())));
			newSet.removeAll(prevSet);
			final List<Vector> list = new ArrayList<>(newSet);
			Collections.shuffle(list);
			final Transformation t = getTransformation();
			final List<TilePiece> tiles = new ArrayList<>();
			for (final Vector point : list) {
				final TilePiece tilePiece = TilePiece.valueOf(point, field.getTile(point));
				tiles.add(tilePiece);
			}
			session.write(new TilesPacket(tiles, t, position, field.getBoundary()));
		}
	}

	@Override
	public synchronized void handleAddEffect(String effectId, Vector origin, Vector target) {
		final Transformation t = getTransformation();
		session.write(new EffectPacket(effectId, origin, target, t, this.position,
				getEntity().getFieldObject().getField().getBoundary()));
	}

	@Override
	public void handleHealthPointsChanged(final ServerFieldObject v, final StatPoints points) {
		session.write(new HealthBarPacket(v.getFieldEntity(), user.getFieldEntityIdCipher(), points));
	}

	@Override
	public void handleMagicPointsChanged(final ServerFieldObject v, final StatPoints points) {
		session.write(new MagicBarPacket(v.getFieldEntity(), user.getFieldEntityIdCipher(), points));
	}

	@Override
	public void handleSetTile(final TilePiece tilePiece) {
		final Transformation t = getTransformation();
		session.write(
				new TilesPacket(tilePiece, t, this.position, getEntity().getFieldObject().getField().getBoundary()));
	}

	public synchronized void synchronize() {
		session.write(
				new SelfChatEntityPacket(entity, user.getChatEntityIdCipher(), session.getLocale().getLanguage()));
		session.write(new SelfNamePacket(entity, user.getChatEntityIdCipher()));

		final ServerFieldObject fo = getEntity().getFieldObject();
		if (fo != null) {
			this.position = fo.getPlacement().getPosition();
			this.startPosition = this.position;

			session.write(new FieldPacket(fo.getField()));

			// send tiles in view
			final List<Vector> points = Arrays.asList(Vectors.range(this.position, fo.getField().getViewSize()));
			Collections.shuffle(points);
			final Transformation t = getTransformation();
			final List<TilePiece> tiles = new ArrayList<>();
			for (final Vector point : points) {
				final TilePiece tilePiece = TilePiece.valueOf(point, fo.getField().getTile(point));
				tiles.add(tilePiece);
			}
			session.write(new TilesPacket(tiles, t, this.position, fo.getField().getBoundary()));

			// send field entities in view
			final List<FieldObject> list = new ArrayList<>(fo.getInViewObjects());
			Collections.shuffle(list);
			for (final FieldObject fo2 : list) {
				final ServerFieldObject sfo2 = (ServerFieldObject) fo2;
				if (!fo.isVisible(sfo2)) {
					continue;
				}
				session.write(new AddFieldEntityPacket(sfo2.getFieldEntity(), user.getFieldEntityIdCipher(), t,
						this.position));
			}

			// identify self
			session.write(new SelfFieldEntityPacket(entity, user.getFieldEntityIdCipher()));
		}

		// ability
		session.write(new ClearActiveSkillsPacket());
		for (final ActiveSkillType type : entity.getStat().getActiveSkills()) {
			final ActiveSkill ability = session.getContext().activeSkills().get(type);
			if (ability == null) {
				continue;
			}
			session.write(new AddActiveSkillPacket(ability, entity));
		}

		// item
		session.write(new ClearItemsPacket());
		for (final ItemEntity itemEntity : entity.getInventory().getItems()) {
			session.write(new AddItemPacket(itemEntity, user.getItemEntityIdCipher(), session.getLocale().getLanguage(),
					entity));
		}
		session.write(new WeightBarPacket(entity.getWeightPoints()));

		// wait
		for (final Wait wait : entity.getWaitGenerator().getWaits()) {
			session.write(new WaitPacket(wait));
		}

		// refresh
		session.write(new RefreshPacket());
	}

}
