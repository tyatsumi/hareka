package org.kareha.hareka.server.handler;

import java.io.IOException;
import java.util.ResourceBundle;
import java.util.logging.Logger;

import org.kareha.hareka.protocol.Handler;
import org.kareha.hareka.protocol.HandlerException;
import org.kareha.hareka.protocol.ProtocolException;
import org.kareha.hareka.protocol.ProtocolInput;
import org.kareha.hareka.server.external.Session;
import org.kareha.hareka.server.packet.VersionPacket;
import org.kareha.hareka.server.user.User;

public final class LogoutUserHandler implements Handler<Session> {

	private static final Logger logger = Logger.getLogger(LogoutUserHandler.class.getName());

	private enum BundleKey {
		LoginUserFirst, LogoutUserSuccess,
	}

	@Override
	public void handle(final ProtocolInput in, final Session session)
			throws IOException, ProtocolException, HandlerException {
		// read arguments
		final int requestId = in.readCompactUInt();

		final ResourceBundle bundle = session.getBundle(LogoutUserHandler.class.getName());

		// check state
		final User user = session.getUser();
		if (user == null) {
			session.writeRejectPacket(requestId, bundle.getString(BundleKey.LoginUserFirst.name()));
			logger.fine(session.getStamp() + "Not logged in");
			return;
		}

		// process
		session.logoutUser();
		session.writeAcceptPacket(requestId, bundle.getString(BundleKey.LogoutUserSuccess.name()));

		session.write(new VersionPacket());
	}

}
