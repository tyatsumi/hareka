package org.kareha.hareka.server.user;

import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import org.kareha.hareka.annotation.GuardedBy;
import org.kareha.hareka.logging.xml.XmlLogRecord;
import org.kareha.hareka.user.UserId;

@XmlRootElement(name = "record")
@XmlAccessorType(XmlAccessType.NONE)
public class InvitationTokensLogRecord implements XmlLogRecord {

	public enum Type {
		ISSUE, CONSUME,
	}

	@XmlAttribute
	@GuardedBy("this")
	private long id;
	@XmlAttribute
	@GuardedBy("this")
	private Date date;
	@GuardedBy("this")
	private Type type;
	@GuardedBy("this")
	private InvitationToken token;
	@GuardedBy("this")
	private UserId user;

	@SuppressWarnings("unused")
	private InvitationTokensLogRecord() {
		// used by JAXB
	}

	public InvitationTokensLogRecord(final InvitationToken token) {
		type = Type.ISSUE;
		this.token = token;
	}

	public InvitationTokensLogRecord(final InvitationToken token, final UserId user) {
		type = Type.CONSUME;
		this.token = token;
		this.user = user;
	}

	@Override
	public synchronized long getId() {
		return id;
	}

	@Override
	public synchronized void setId(final long id) {
		this.id = id;
	}

	@Override
	public synchronized long getDate() {
		return date.getTime();
	}

	@Override
	public synchronized void setDate(final long date) {
		this.date = new Date(date);
	}

	public synchronized Type getType() {
		return type;
	}

	public synchronized InvitationToken getToken() {
		return token;
	}

	public synchronized UserId getUser() {
		return user;
	}

}
