package org.kareha.hareka.server.packet;

import org.kareha.hareka.protocol.WorldClientPacketType;
import org.kareha.hareka.server.game.DiceRoll;
import org.kareha.hareka.server.protocol.WorldClientPacket;
import org.kareha.hareka.protocol.PacketType;

public final class RequestDiceChoicePacket extends WorldClientPacket {

	public RequestDiceChoicePacket(final DiceRoll diceRoll) {
		out.writeCompactUInt(diceRoll.getId());
		final byte[] hash = diceRoll.getHash();
		out.writeString(diceRoll.getHashAlgorithm());
		out.writeByteArray(hash);
		out.writeCompactUInt(diceRoll.getFaces());
		out.writeCompactUInt(diceRoll.getRate());
	}

	@Override
	protected PacketType getType() {
		return WorldClientPacketType.REQUEST_DICE_CHOICE;
	}

}
