package org.kareha.hareka.server.handler;

import java.io.IOException;
import java.util.ResourceBundle;
import java.util.logging.Logger;

import org.kareha.hareka.protocol.Handler;
import org.kareha.hareka.protocol.HandlerException;
import org.kareha.hareka.protocol.ProtocolException;
import org.kareha.hareka.protocol.ProtocolInput;
import org.kareha.hareka.server.external.Session;
import org.kareha.hareka.server.user.User;
import org.kareha.hareka.user.Permission;
import org.kareha.hareka.user.RoleSet;

public final class SetRescueMethodsEnabledHandler implements Handler<Session> {

	private static final Logger logger = Logger.getLogger(SetRescueMethodsEnabledHandler.class.getName());

	private enum BundleKey {
		LoginUserFirst, YouCannotUseThisFunction, SettingChanged,
	}

	private static void inform(final Session session, final String key) {
		final ResourceBundle bundle = session.getBundle(SetRescueMethodsEnabledHandler.class.getName());
		session.writeInformPacket(bundle.getString(key));
	}

	@Override
	public void handle(final ProtocolInput in, final Session session)
			throws IOException, ProtocolException, HandlerException {
		final boolean enabled = in.readBoolean();

		final User user = session.getUser();
		if (user == null) {
			inform(session, BundleKey.LoginUserFirst.name());
			logger.fine(session.getStamp() + "User not logged in");
			return;
		}
		final RoleSet roles = session.getContext().accessController().getRoleSet(user);
		if (!roles.isAbleTo(Permission.MANAGE_SETTINGS)) {
			inform(session, BundleKey.YouCannotUseThisFunction.name());
			logger.fine(session.getStamp() + "User cannot use set user registration mode function");
			return;
		}

		session.getContext().settings().setRescueMethodsEnabled(enabled);
		inform(session, BundleKey.SettingChanged.name());
		logger.fine(session.getStamp() + "Rescue methods enabled: " + enabled);
	}

}
