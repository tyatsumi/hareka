package org.kareha.hareka.server.user;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Arrays;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.Mac;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.kareha.hareka.annotation.Immutable;
import org.kareha.hareka.annotation.Private;
import org.kareha.hareka.protocol.ProtocolException;
import org.kareha.hareka.protocol.ProtocolInputStream;
import org.kareha.hareka.protocol.ProtocolOutputStream;

@Immutable
@XmlJavaTypeAdapter(StorageCipher.Adapter.class)
public class StorageCipher {

	private static final String ALGORITHM = "AES/CBC/PKCS5Padding";
	private static final String MAC_ALGORITHM = "HmacSHA256";

	@Private
	final byte[] key;
	@Private
	final byte[] macKey;

	private static byte[] generate(final int size) {
		final byte[] b = new byte[size];
		new SecureRandom().nextBytes(b);
		return b;
	}

	public StorageCipher() {
		this(generate(16), generate(32));
	}

	public StorageCipher(final byte[] key, final byte[] macKey) {
		if (key == null || macKey == null) {
			throw new IllegalArgumentException("null");
		}
		if (key.length != 16) {
			throw new IllegalArgumentException("key.length must be 16");
		}
		if (macKey.length != 32) {
			throw new IllegalArgumentException("macKey.length must be 32");
		}
		this.key = key.clone();
		this.macKey = macKey.clone();
	}

	@XmlType(name = "storageCipher")
	@XmlAccessorType(XmlAccessType.NONE)
	private static class Adapted {

		@XmlElement
		private byte[] key;
		@XmlElement
		private byte[] macKey;

		@SuppressWarnings("unused")
		private Adapted() {
			// used by JAXB
		}

		@Private
		Adapted(final StorageCipher v) {
			key = v.key.clone();
			macKey = v.macKey.clone();
		}

		@Private
		StorageCipher unmarshal() {
			return new StorageCipher(key, macKey);
		}

	}

	@Private
	Adapted marshal() {
		return new Adapted(this);
	}

	static class Adapter extends XmlAdapter<Adapted, StorageCipher> {

		@Override
		public Adapted marshal(final StorageCipher v) throws Exception {
			if (v == null) {
				return null;
			}
			return v.marshal();
		}

		@Override
		public StorageCipher unmarshal(final Adapted v) throws Exception {
			if (v == null) {
				return null;
			}
			return v.unmarshal();
		}

	}

	private Cipher getEncrypter(final byte[] iv) {
		final Cipher encrypter;
		try {
			encrypter = Cipher.getInstance(ALGORITHM);
		} catch (final NoSuchAlgorithmException e) {
			throw new AssertionError();
		} catch (final NoSuchPaddingException e) {
			throw new AssertionError();
		}
		final SecretKeySpec encryptSpec = new SecretKeySpec(key, "AES");
		final IvParameterSpec encryptIv = new IvParameterSpec(iv);
		try {
			encrypter.init(Cipher.ENCRYPT_MODE, encryptSpec, encryptIv);
		} catch (final InvalidKeyException e) {
			throw new AssertionError();
		} catch (final InvalidAlgorithmParameterException e) {
			throw new AssertionError();
		}
		return encrypter;
	}

	private Cipher getDecrypter(final byte[] iv) {
		final Cipher decrypter;
		try {
			decrypter = Cipher.getInstance(ALGORITHM);
		} catch (final NoSuchAlgorithmException e) {
			throw new AssertionError();
		} catch (final NoSuchPaddingException e) {
			throw new AssertionError();
		}
		final SecretKeySpec decryptSpec = new SecretKeySpec(key, "AES");
		final IvParameterSpec decryptIv = new IvParameterSpec(iv);
		try {
			decrypter.init(Cipher.DECRYPT_MODE, decryptSpec, decryptIv);
		} catch (final InvalidKeyException e) {
			throw new AssertionError();
		} catch (final InvalidAlgorithmParameterException e) {
			throw new AssertionError();
		}
		return decrypter;
	}

	public byte[] encrypt(final byte[] plain, final byte[] toBeEncrypted) {
		try {
			final byte[] iv = generate(16);
			final byte[] encrypted = getEncrypter(iv).doFinal(toBeEncrypted);

			@SuppressWarnings("resource")
			final ProtocolOutputStream out = new ProtocolOutputStream();
			out.writeByteArray(plain);
			out.write(iv);
			out.write(encrypted);
			final byte[] body = out.toByteArray();

			final Mac mac = Mac.getInstance(MAC_ALGORITHM);
			final SecretKeySpec secretKey = new SecretKeySpec(macKey, MAC_ALGORITHM);
			mac.init(secretKey);
			final byte[] macBytes = mac.doFinal(body);

			out.write(macBytes);

			return out.toByteArray();
		} catch (final NoSuchAlgorithmException e) {
			throw new RuntimeException(e);
		} catch (final InvalidKeyException e) {
			throw new RuntimeException(e);
		} catch (final IllegalBlockSizeException e) {
			throw new RuntimeException(e);
		} catch (final BadPaddingException e) {
			throw new RuntimeException(e);
		}
	}

	public static byte[] getPlain(final byte[] data) throws IOException, ProtocolException {
		@SuppressWarnings("resource")
		final ProtocolInputStream in = new ProtocolInputStream(data);
		final byte[] plain = in.readByteArray();
		return plain;
	}

	public byte[] decrypt(final byte[] data) throws IOException, ProtocolException {
		try {
			@SuppressWarnings("resource")
			final ProtocolInputStream in = new ProtocolInputStream(data);
			@SuppressWarnings("unused")
			final byte[] plain = in.readByteArray();
			final byte[] iv = new byte[16];
			in.read(iv);

			final Mac mac = Mac.getInstance(MAC_ALGORITHM);
			final SecretKeySpec secretKey = new SecretKeySpec(macKey, MAC_ALGORITHM);
			mac.init(secretKey);

			final byte[] encrypted = new byte[in.available() - mac.getMacLength()];
			in.read(encrypted);
			final byte[] macBytes = new byte[mac.getMacLength()];
			in.read(macBytes);

			final byte[] body = Arrays.copyOf(data, data.length - mac.getMacLength());
			final byte[] macBytesRecalc = mac.doFinal(body);
			if (!Arrays.equals(macBytes, macBytesRecalc)) {
				return null;
			}

			return getDecrypter(iv).doFinal(encrypted);
		} catch (final NoSuchAlgorithmException e) {
			throw new ProtocolException("No such algorithm", e);
		} catch (final InvalidKeyException e) {
			throw new ProtocolException("Invalid key", e);
		} catch (final IllegalBlockSizeException e) {
			throw new ProtocolException("Illegal block size", e);
		} catch (final BadPaddingException e) {
			throw new ProtocolException("Bad padding", e);
		}
	}

}
