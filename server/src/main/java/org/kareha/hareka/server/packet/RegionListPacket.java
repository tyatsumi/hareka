package org.kareha.hareka.server.packet;

import java.util.Collection;

import org.kareha.hareka.field.Region;
import org.kareha.hareka.field.Transformation;
import org.kareha.hareka.protocol.WorldClientPacketType;
import org.kareha.hareka.server.protocol.WorldClientPacket;
import org.kareha.hareka.protocol.PacketType;

public final class RegionListPacket extends WorldClientPacket {

	public RegionListPacket(final Collection<Region> regions, final Transformation transformation) {
		out.writeCompactUInt(regions.size());
		for (final Region region : regions) {
			out.write(region.transform(transformation));
		}
	}

	@Override
	protected PacketType getType() {
		return WorldClientPacketType.REGION_LIST;
	}

}
