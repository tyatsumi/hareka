package org.kareha.hareka.server.packet;

import org.kareha.hareka.protocol.WorldClientPacketType;
import org.kareha.hareka.server.game.DiceRoll;
import org.kareha.hareka.server.protocol.WorldClientPacket;
import org.kareha.hareka.protocol.PacketType;

public final class DiceResultPacket extends WorldClientPacket {

	public DiceResultPacket(final DiceRoll diceRoll) {
		out.writeCompactUInt(diceRoll.getId());
		out.writeCompactUInt(diceRoll.getValue());
		out.writeByteArray(diceRoll.getSecret());
	}

	@Override
	protected PacketType getType() {
		return WorldClientPacketType.DICE_RESULT;
	}

}
