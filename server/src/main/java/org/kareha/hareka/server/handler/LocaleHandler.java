package org.kareha.hareka.server.handler;

import java.io.IOException;
import java.util.IllformedLocaleException;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.logging.Logger;

import org.kareha.hareka.protocol.Handler;
import org.kareha.hareka.protocol.HandlerException;
import org.kareha.hareka.protocol.ProtocolException;
import org.kareha.hareka.protocol.ProtocolInput;
import org.kareha.hareka.server.external.Session;

public final class LocaleHandler implements Handler<Session> {

	private static final Logger logger = Logger.getLogger(LocaleHandler.class.getName());

	private enum BundleKey {
		BadLocaleFormat,
	}

	@Override
	public void handle(final ProtocolInput in, final Session session)
			throws IOException, ProtocolException, HandlerException {
		// read arguments
		// check format
		final Locale locale;
		try {
			locale = in.readLocale();
		} catch (final IllformedLocaleException e) {
			final ResourceBundle bundle = session.getBundle(LocaleHandler.class.getName());
			session.writeInformPacket(bundle.getString(BundleKey.BadLocaleFormat.name()));
			logger.fine(session.getStamp() + "Illformed");
			return;
		}

		// process
		session.setLocale(locale);
	}

}
