package org.kareha.hareka.server.entity;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.kareha.hareka.annotation.GuardedBy;
import org.kareha.hareka.annotation.ThreadSafe;
import org.kareha.hareka.field.Direction;
import org.kareha.hareka.field.Placement;
import org.kareha.hareka.field.Transformation;
import org.kareha.hareka.field.Vector;
import org.kareha.hareka.server.Global;
import org.kareha.hareka.server.field.FieldId;
import org.kareha.hareka.server.field.FieldPosition;
import org.kareha.hareka.server.field.Fields;
import org.kareha.hareka.server.field.ServerField;
import org.kareha.hareka.server.field.ServerFieldObject;
import org.kareha.hareka.server.game.Player;
import org.kareha.hareka.server.packet.TeleportPacket;
import org.kareha.hareka.util.JaxbUtil;

@ThreadSafe
@XmlJavaTypeAdapter(AbstractEntity.Adapter.class)
public abstract class AbstractEntity implements FieldEntity {

	private static final Logger logger = Logger.getLogger(AbstractEntity.class.getName());

	private static final String UNDEFINED_SHAPE = "UNDEFINED";
	private static final JAXBContext jaxbContext;

	static {
		try {
			jaxbContext = JAXBContext.newInstance(Adapted.class);
		} catch (final JAXBException e) {
			logger.log(Level.SEVERE, "", e);
			throw new RuntimeException(e.getMessage(), e);
		}
	}

	protected static abstract class Builder<T extends Builder<T>> {

		protected FieldId fieldId;
		protected Placement placement;
		protected String shape;
		protected Transformation transformation = Transformation.IDENTITY;
		protected boolean mounted;

		public Builder() {

		}

		public Builder(final AbstractEntity entity) {
			synchronized (entity) {
				fieldId = entity.fieldId;
				placement = entity.placement;
				shape = entity.shape;
				transformation = entity.transformation;
				mounted = entity.mounted;
			}
		}

		protected abstract T self();

		public T fieldId(final FieldId v) {
			fieldId = v;
			return self();
		}

		public T placement(final Placement v) {
			placement = v;
			return self();
		}

		public T shape(final String v) {
			shape = v;
			return self();
		}

		public T transformation(final Transformation v) {
			transformation = v;
			return self();
		}

		public T mounted(final boolean v) {
			mounted = v;
			return self();
		}

		public abstract AbstractEntity build(EntityId id);

	}

	protected final EntityId id;
	@GuardedBy("this")
	protected boolean persistent;
	@GuardedBy("this")
	protected FieldId fieldId;
	@GuardedBy("this")
	protected Placement placement;
	@GuardedBy("this")
	protected String shape;
	@GuardedBy("this")
	protected ServerFieldObject fieldObject;
	@GuardedBy("this")
	protected Transformation transformation;
	@GuardedBy("this")
	protected boolean mounted;
	protected final List<FieldDriver> fieldDrivers = new CopyOnWriteArrayList<>();

	protected AbstractEntity(final EntityId id, final Builder<?> builder) {
		this.id = id;
		fieldId = builder.fieldId;
		placement = builder.placement;
		shape = builder.shape;
		transformation = builder.transformation;
		mounted = builder.mounted;
	}

	@XmlRootElement(name = "abstractEntity")
	@XmlSeeAlso({ ItemEntity.Adapted.class, CharacterEntity.Adapted.class })
	@XmlAccessorType(XmlAccessType.NONE)
	protected static abstract class Adapted {

		@XmlAttribute
		protected EntityId id;
		@XmlElement
		protected FieldId fieldId;
		@XmlElement
		protected Placement placement;
		@XmlElement
		protected String shape;
		@XmlElement
		protected Transformation transformation;
		@XmlElement
		protected boolean mounted;

		protected Adapted() {
			// used by JAXB
		}

		protected Adapted(final AbstractEntity v) {
			id = v.id;
			synchronized (v) {
				shape = v.shape;
				fieldId = v.fieldId;
				placement = v.placement;
				transformation = v.transformation;
				mounted = v.mounted;
			}
		}

		protected abstract AbstractEntity unmarshal();

	}

	protected abstract Adapted marshal();

	static class Adapter extends XmlAdapter<Adapted, AbstractEntity> {

		@Override
		public Adapted marshal(final AbstractEntity v) throws Exception {
			if (v == null) {
				return null;
			}
			return v.marshal();
		}

		@Override
		public AbstractEntity unmarshal(final Adapted v) throws Exception {
			if (v == null) {
				return null;
			}
			return v.unmarshal();
		}

	}

	public static AbstractEntity load(final File file) throws IOException, JAXBException {
		final Adapted adapted = JaxbUtil.unmarshal(file, jaxbContext);
		final AbstractEntity entity = adapted.unmarshal();
		entity.setPersistent(true);
		return entity;
	}

	public void save(final File file) throws JAXBException {
		JaxbUtil.marshal(marshal(), file, jaxbContext);
		setPersistent(true);
	}

	@Override
	public EntityId getId() {
		return id;
	}

	@Override
	public synchronized boolean isPersistent() {
		return persistent;
	}

	@Override
	public synchronized void setPersistent(final boolean persistent) {
		this.persistent = persistent;
	}

	public synchronized FieldId getFieldId() {
		return fieldId;
	}

	public synchronized Placement getPlacement() {
		return placement;
	}

	@Override
	public synchronized String getShape() {
		if (shape == null) {
			return UNDEFINED_SHAPE;
		}
		return shape;
	}

	@Override
	public synchronized void setShape(final String shape) {
		this.shape = shape;
	}

	@Override
	public synchronized ServerFieldObject getFieldObject() {
		return fieldObject;
	}

	@Override
	public synchronized void setFieldObject(final ServerFieldObject fieldObject) {
		this.fieldObject = fieldObject;
	}

	@Override
	public synchronized Transformation getTransformation() {
		if (transformation == null) {
			transformation = Transformation.random(Vector.ZERO);
		}
		return transformation;
	}

	@Override
	public synchronized boolean isMounted() {
		return mounted;
	}

	public boolean isFieldDriversEmpty() {
		return fieldDrivers.isEmpty();
	}

	@Override
	public void addFieldDriver(final FieldDriver driver) {
		fieldDrivers.add(driver);
	}

	@Override
	public boolean removeFieldDriver(final FieldDriver driver) {
		return fieldDrivers.remove(driver);
	}

	@Override
	public Collection<FieldDriver> getFieldDrivers() {
		return new ArrayList<>(fieldDrivers);
	}

	@Override
	public synchronized boolean mount(final Fields fieldTable) {
		if (fieldObject != null) {
			return false;
		}
		if (fieldId == null) {
			return false;
		}
		final ServerField field = fieldTable.getField(fieldId);
		if (field == null) {
			return false;
		}
		if (placement == null) {
			return false;
		}
		fieldObject = new ServerFieldObject(field, placement, this);
		fieldObject.mount();
		mounted = true;
		return true;
	}

	@Override
	public synchronized boolean mount(final Fields fieldTable, final FieldId fieldId, final Placement placement) {
		if (fieldObject != null) {
			return false;
		}
		if (fieldId == null) {
			return false;
		}
		final ServerField field = fieldTable.getField(fieldId);
		if (field == null) {
			return false;
		}
		if (placement == null) {
			return false;
		}
		fieldObject = new ServerFieldObject(field, placement, this);
		fieldObject.mount();
		this.fieldId = fieldId;
		this.placement = placement;
		mounted = true;
		return true;
	}

	// used in Entities
	synchronized boolean helperUnmount() {
		if (fieldObject == null) {
			return false;
		}
		final Placement p = fieldObject.unmount();
		if (p == null) {
			return false;
		}
		fieldId = fieldObject.getField().getId();
		placement = p;
		fieldObject = null;
		return true;
	}

	@Override
	public synchronized boolean unmount() {
		final boolean result = helperUnmount();
		if (result) {
			mounted = false;
		}
		return result;
	}

	@Override
	public boolean teleport(final FieldPosition fieldPosition) {
		for (final FieldDriver driver : getFieldDrivers()) {
			if (!(driver instanceof Player)) {
				continue;
			}
			final Player player = (Player) driver;
			player.getSession().write(new TeleportPacket());
		}
		synchronized (this) {
			if (Global.INSTANCE.context().fields().getField(fieldPosition.getFieldId()) == null) {
				return false;
			}
			if (!helperUnmount()) {
				return false;
			}
			if (!mount(Global.INSTANCE.context().fields(), fieldPosition.getFieldId(),
					Placement.valueOf(fieldPosition.getPosition(), Direction.NULL))) {
				return false;
			}
		}
		for (final FieldDriver driver : getFieldDrivers()) {
			if (!(driver instanceof Player)) {
				continue;
			}
			final Player player = (Player) driver;
			player.synchronize();
		}
		return true;
	}

	public abstract boolean isMotionEnabled();

	public abstract void startMotion();

	public abstract void stopMotion();

	public abstract void startHealthRegeneration();

	public abstract void stopHealthRegeneration();

	public abstract void startMagicRegeneration();

	public abstract void stopMagicRegeneration();

}
