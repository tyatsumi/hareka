package org.kareha.hareka.server.packet;

import java.util.Collection;

import org.kareha.hareka.protocol.WorldClientPacketType;
import org.kareha.hareka.server.protocol.WorldClientPacket;
import org.kareha.hareka.server.user.AccessController;
import org.kareha.hareka.server.user.User;
import org.kareha.hareka.protocol.PacketType;
import org.kareha.hareka.user.Role;

public final class MyRolesPacket extends WorldClientPacket {

	public MyRolesPacket(final User user, final AccessController accessController) {
		final Collection<Role> roles = accessController.getRoleSet(user).getRoles();
		out.writeCompactUInt(roles.size());
		for (final Role role : roles) {
			out.writeString(role.getId());
		}
	}

	@Override
	protected PacketType getType() {
		return WorldClientPacketType.MY_ROLES;
	}

}
