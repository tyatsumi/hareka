package org.kareha.hareka.server.packet;

import org.kareha.hareka.protocol.WorldClientPacketType;
import org.kareha.hareka.server.entity.ChatEntity;
import org.kareha.hareka.server.entity.FieldEntity;
import org.kareha.hareka.server.protocol.WorldClientPacket;
import org.kareha.hareka.server.user.IdCipher;
import org.kareha.hareka.protocol.PacketType;

public final class EntityIdentityPacket extends WorldClientPacket {

	public EntityIdentityPacket(final ChatEntity chatEntity, final IdCipher chatEntityIdCipher,
			final FieldEntity fieldEntity, final IdCipher fieldEntityIdCipher) {
		out.write(chatEntityIdCipher.encrypt(chatEntity.getId()));
		out.write(fieldEntityIdCipher.encrypt(fieldEntity.getId()));
	}

	@Override
	protected PacketType getType() {
		return WorldClientPacketType.ENTITY_IDENTITY;
	}

}
