package org.kareha.hareka.server.packet;

import org.kareha.hareka.protocol.WorldClientPacketType;
import org.kareha.hareka.server.protocol.WorldClientPacket;
import org.kareha.hareka.protocol.PacketType;
import org.kareha.hareka.wait.Wait;

public final class WaitPacket extends WorldClientPacket {

	public WaitPacket(final Wait wait) {
		out.write(wait);
	}

	@Override
	protected PacketType getType() {
		return WorldClientPacketType.WAIT;
	}

}
