package org.kareha.hareka.server.packet;

import org.kareha.hareka.Constants;
import org.kareha.hareka.Version;
import org.kareha.hareka.protocol.WorldClientPacketType;
import org.kareha.hareka.server.protocol.WorldClientPacket;
import org.kareha.hareka.protocol.PacketType;

public class VersionPacket extends WorldClientPacket {

	public VersionPacket() {
		this(Constants.VERSION);
	}

	public VersionPacket(final Version version) {
		out.write(version);
	}

	@Override
	protected PacketType getType() {
		return WorldClientPacketType.VERSION;
	}

}
