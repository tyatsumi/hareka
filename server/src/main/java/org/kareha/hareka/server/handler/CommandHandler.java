package org.kareha.hareka.server.handler;

import java.io.IOException;

import org.kareha.hareka.protocol.Handler;
import org.kareha.hareka.protocol.HandlerException;
import org.kareha.hareka.protocol.ProtocolException;
import org.kareha.hareka.protocol.ProtocolInput;
import org.kareha.hareka.server.external.Session;
import org.kareha.hareka.server.packet.CommandOutPacket;

public final class CommandHandler implements Handler<Session> {

	@Override
	public void handle(final ProtocolInput in, final Session session)
			throws IOException, ProtocolException, HandlerException {
		// read arguments
		final int length = in.readCompactUInt();
		final String[] args = new String[length];
		for (int i = 0; i < length; i++) {
			args[i] = in.readString();
		}

		session.write(new CommandOutPacket("hello\n"));
		// TODO handle command
	}

}
