package org.kareha.hareka.server.packet;

import org.kareha.hareka.protocol.WorldClientPacketType;
import org.kareha.hareka.server.protocol.WorldClientPacket;
import org.kareha.hareka.protocol.PacketType;

public final class NoPublicKeyPacket extends WorldClientPacket {

	public NoPublicKeyPacket(final int handlerId, final int version) {
		out.writeCompactUInt(handlerId);
		out.writeCompactUInt(version);
	}

	@Override
	protected PacketType getType() {
		return WorldClientPacketType.NO_PUBLIC_KEY;
	}

}
