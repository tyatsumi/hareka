package org.kareha.hareka.server.entity;

public interface Entity {

	EntityId getId();

	boolean isPersistent();

	void setPersistent(boolean persistent);

}
