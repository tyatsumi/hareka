package org.kareha.hareka.server.packet;

import org.kareha.hareka.field.Transformation;
import org.kareha.hareka.field.Vector;
import org.kareha.hareka.protocol.WorldClientPacketType;
import org.kareha.hareka.server.field.Boundary;
import org.kareha.hareka.server.protocol.WorldClientPacket;
import org.kareha.hareka.protocol.PacketType;

public final class EffectPacket extends WorldClientPacket {

	public EffectPacket(final String effectId, final Vector origin, final Vector target,
			final Transformation transformation, final Vector position, final Boundary boundary) {
		out.writeString(effectId);
		out.write(boundary.confine(origin, position).transform(transformation));
		out.write(boundary.confine(target, position).transform(transformation));
	}

	@Override
	protected PacketType getType() {
		return WorldClientPacketType.EFFECT;
	}

}
