package org.kareha.hareka.server.relationship;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.kareha.hareka.annotation.GuardedBy;
import org.kareha.hareka.annotation.Private;
import org.kareha.hareka.annotation.ThreadSafe;
import org.kareha.hareka.server.entity.EntityId;

@ThreadSafe
@XmlJavaTypeAdapter(IndividualRelationships.Adapter.class)
public class IndividualRelationships {

	@GuardedBy("this")
	@Private
	Map<EntityId, Relationship> map;

	public IndividualRelationships() {

	}

	@Private
	IndividualRelationships(final Map<EntityId, Relationship> map) {
		if (map != null) {
			this.map = new HashMap<>(map);
		}
	}

	@XmlType(name = "relationshipEntry")
	@XmlAccessorType(XmlAccessType.NONE)
	private static class RelationshipEntry {

		@XmlAttribute
		@Private
		EntityId id;
		@XmlElement
		private long love;
		@XmlElement
		private long hate;

		@SuppressWarnings("unused")
		private RelationshipEntry() {
			// used by JAXB
		}

		@Private
		RelationshipEntry(final EntityId id, final Relationship relationship) {
			this.id = id;
			synchronized (relationship) {
				love = relationship.getLove();
				hate = relationship.getHate();
			}
		}

		@Private
		Relationship getRelationship() {
			return new Relationship(love, hate);
		}

	}

	@XmlType(name = "individualRelationships")
	@XmlAccessorType(XmlAccessType.NONE)
	private static class Adapted {

		@XmlElement(name = "relationship")
		private List<RelationshipEntry> entries;

		@SuppressWarnings("unused")
		private Adapted() {
			// used by JAXB
		}

		@Private
		Adapted(final IndividualRelationships v) {
			synchronized (v) {
				if (v.map != null) {
					entries = new ArrayList<>();
					for (final Map.Entry<EntityId, Relationship> entry : v.map.entrySet()) {
						entries.add(new RelationshipEntry(entry.getKey(), entry.getValue()));
					}
				}
			}
		}

		@Private
		IndividualRelationships unmarshal() {
			final Map<EntityId, Relationship> map = new HashMap<>();
			for (final RelationshipEntry entry : entries) {
				map.put(entry.id, entry.getRelationship());
			}
			return new IndividualRelationships(map);
		}

	}

	@Private
	Adapted marshal() {
		return new Adapted(this);
	}

	static class Adapter extends XmlAdapter<Adapted, IndividualRelationships> {

		@Override
		public Adapted marshal(final IndividualRelationships v) throws Exception {
			if (v == null) {
				return null;
			}
			return v.marshal();
		}

		@Override
		public IndividualRelationships unmarshal(final Adapted v) throws Exception {
			if (v == null) {
				return null;
			}
			return v.unmarshal();
		}

	}

	@GuardedBy("this")
	private Relationship get(final EntityId id) {
		if (map == null) {
			return null;
		}
		return map.get(id);
	}

	@GuardedBy("this")
	private void put(final EntityId id, final Relationship v) {
		if (map == null) {
			map = new HashMap<>();
		}
		map.put(id, v);
	}

	public synchronized long getLove(final EntityId id) {
		final Relationship relationship = get(id);
		if (relationship == null) {
			return 0;
		}
		return relationship.getLove();
	}

	public synchronized void addLove(final EntityId id, final long v) {
		final Relationship relationship = get(id);
		if (relationship == null) {
			final Relationship newRelationship = new Relationship();
			newRelationship.addLove(v);
			put(id, newRelationship);
			return;
		}
		relationship.addLove(v);
	}

	public synchronized long getHate(final EntityId id) {
		final Relationship relationship = get(id);
		if (relationship == null) {
			return 0;
		}
		return relationship.getHate();
	}

	public synchronized void addHate(final EntityId id, final long v) {
		final Relationship relationship = get(id);
		if (relationship == null) {
			final Relationship newRelationship = new Relationship();
			newRelationship.addHate(v);
			put(id, newRelationship);
			return;
		}
		relationship.addHate(v);
	}

}
