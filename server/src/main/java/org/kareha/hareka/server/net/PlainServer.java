package org.kareha.hareka.server.net;

import java.io.IOException;
import java.net.ServerSocket;

public abstract class PlainServer extends AbstractServer {

	public PlainServer(final int port) {
		super(port);
	}

	@Override
	protected ServerSocket createServerSocket() throws IOException {
		return new ServerSocket(port());
	}

}
