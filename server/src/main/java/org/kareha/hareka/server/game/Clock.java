package org.kareha.hareka.server.game;

import java.io.File;
import java.util.Date;

import javax.xml.bind.JAXBException;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.kareha.hareka.annotation.Private;
import org.kareha.hareka.annotation.ThreadSafe;
import org.kareha.hareka.util.JaxbUtil;

@ThreadSafe
public class Clock {

	private final File file;
	@Private
	final long epochTime;

	public Clock(final File file) throws JAXBException {
		this.file = file;
		if (file.isFile()) {
			final Adapted adapted = JaxbUtil.unmarshal(file, Adapted.class);
			epochTime = adapted.epochTime.getTime();
		} else {
			epochTime = System.currentTimeMillis();
		}
	}

	@XmlRootElement(name = "clock")
	@XmlAccessorType(XmlAccessType.NONE)
	private static class Adapted {

		@XmlElement
		@Private
		Date epochTime;

		@SuppressWarnings("unused")
		private Adapted() {
			// used by JAXB
		}

		@Private
		Adapted(final Clock v) {
			epochTime = new Date(v.epochTime);
		}

	}

	public void save() throws JAXBException {
		JaxbUtil.marshal(new Adapted(this), file);
	}

	public long getEpochTime() {
		return epochTime;
	}

	public long toVirtualTime(final long realTime) {
		return realTime - epochTime;
	}

	public long toRealTime(final long virtualTime) {
		return epochTime + virtualTime;
	}

	public long getVirtualCurrentTime() {
		return toVirtualTime(System.currentTimeMillis());
	}

}
