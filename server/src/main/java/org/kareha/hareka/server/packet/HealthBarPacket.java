package org.kareha.hareka.server.packet;

import org.kareha.hareka.protocol.WorldClientPacketType;
import org.kareha.hareka.server.entity.FieldEntity;
import org.kareha.hareka.server.protocol.WorldClientPacket;
import org.kareha.hareka.server.stat.StatPoints;
import org.kareha.hareka.server.user.IdCipher;
import org.kareha.hareka.protocol.PacketType;

public final class HealthBarPacket extends WorldClientPacket {

	public HealthBarPacket(final FieldEntity fieldEntity, final IdCipher fieldEntityIdCipher, final StatPoints points) {
		out.write(fieldEntityIdCipher.encrypt(fieldEntity.getId()));
		out.writeBoolean(points.isAlive());
		out.writeByte(points.getBar());
	}

	@Override
	protected PacketType getType() {
		return WorldClientPacketType.HEALTH_BAR;
	}

}
