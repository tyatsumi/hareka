package org.kareha.hareka.server.handler;

import java.io.IOException;
import java.util.ResourceBundle;
import java.util.logging.Logger;

import org.kareha.hareka.field.Vector;
import org.kareha.hareka.protocol.Handler;
import org.kareha.hareka.protocol.HandlerException;
import org.kareha.hareka.protocol.ProtocolException;
import org.kareha.hareka.protocol.ProtocolInput;
import org.kareha.hareka.server.entity.CharacterEntity;
import org.kareha.hareka.server.external.Session;
import org.kareha.hareka.server.field.FieldPosition;
import org.kareha.hareka.server.field.ServerField;
import org.kareha.hareka.server.field.ServerFieldObject;
import org.kareha.hareka.server.game.Player;
import org.kareha.hareka.server.stat.StatResult;
import org.kareha.hareka.server.user.User;

public final class TeleportToCenterHandler implements Handler<Session> {

	private static final Logger logger = Logger.getLogger(TeleportToPositionMemoryHandler.class.getName());

	private enum BundleKey {
		YouCannotUseThisFunction,
	}

	private static void inform(final Session session, final String key) {
		final ResourceBundle bundle = session.getBundle(TeleportToPositionMemoryHandler.class.getName());
		session.writeInformPacket(bundle.getString(key));
	}

	@Override
	public void handle(final ProtocolInput in, final Session session)
			throws IOException, ProtocolException, HandlerException {
		if (!session.getContext().settings().isRescueMethodsEnabled()) {
			inform(session, BundleKey.YouCannotUseThisFunction.name());
			logger.fine(session.getStamp() + "User cannot teleport");
			return;
		}

		final User user = session.getUser();
		if (user == null) {
			return;
		}

		final Player player = session.getPlayer();
		if (player == null) {
			return;
		}
		final CharacterEntity entity = player.getEntity();
		final ServerFieldObject fo = entity.getFieldObject();
		if (fo == null) {
			return;
		}
		final ServerField field = fo.getField();

		final StatResult result = entity.getStat().addMagicPoints(-32);
		if (result == null) {
			return;
		}
		entity.sendMagicBar(result.getPoints());
		entity.startMagicRegeneration();
		if (result.isTooSmall()) {
			return;
		}

		entity.teleport(FieldPosition.valueOf(field.getId(), Vector.ZERO));
	}

}
