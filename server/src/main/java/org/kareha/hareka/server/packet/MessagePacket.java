package org.kareha.hareka.server.packet;

import org.kareha.hareka.protocol.WorldClientPacketType;
import org.kareha.hareka.server.protocol.WorldClientPacket;
import org.kareha.hareka.protocol.PacketType;

public final class MessagePacket extends WorldClientPacket {

	public MessagePacket(final String message) {
		out.writeString(message);
	}

	@Override
	protected PacketType getType() {
		return WorldClientPacketType.MESSAGE;
	}

}
