package org.kareha.hareka.server.entity;

import org.kareha.hareka.field.Placement;
import org.kareha.hareka.field.TilePiece;
import org.kareha.hareka.field.Vector;
import org.kareha.hareka.server.field.ServerFieldObject;
import org.kareha.hareka.server.stat.StatPoints;

public interface FieldDriver {

	void handleAddInViewObject(ServerFieldObject v);

	void handleRemoveInViewObject(ServerFieldObject v);

	void handleInViewObjectMoved(ServerFieldObject v, Placement placement);

	void handleMove(Placement prevPlacement, Placement newPlacement);

	void handleAddEffect(String effectId, Vector origin, Vector target);

	void handleHealthPointsChanged(ServerFieldObject v, StatPoints points);

	void handleMagicPointsChanged(ServerFieldObject v, StatPoints points);

	void handleSetTile(TilePiece tilePiece);

}
