package org.kareha.hareka.server.field;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.kareha.hareka.annotation.Immutable;
import org.kareha.hareka.annotation.Private;
import org.kareha.hareka.field.Vector;

@Immutable
@XmlJavaTypeAdapter(FieldPosition.Adapter.class)
public class FieldPosition {

	@Private
	final FieldId fieldId;
	@Private
	final Vector position;

	private FieldPosition(final FieldId fieldId, final Vector position) {
		if (fieldId == null || position == null) {
			throw new IllegalArgumentException("null");
		}
		this.fieldId = fieldId;
		this.position = position;
	}

	public static FieldPosition valueOf(final FieldId fieldId, final Vector position) {
		return new FieldPosition(fieldId, position);
	}

	@XmlType(name = "fieldPosition")
	@XmlAccessorType(XmlAccessType.NONE)
	private static class Adapted {

		@XmlElement
		private FieldId fieldId;
		@XmlElement
		private Vector position;

		@SuppressWarnings("unused")
		private Adapted() {
			// used by JAXB
		}

		@Private
		Adapted(final FieldPosition v) {
			fieldId = v.fieldId;
			position = v.position;
		}

		@Private
		FieldPosition unmarshal() {
			return valueOf(fieldId, position);
		}

	}

	@Private
	Adapted marshal() {
		return new Adapted(this);
	}

	static class Adapter extends XmlAdapter<Adapted, FieldPosition> {

		@Override
		public Adapted marshal(final FieldPosition v) throws Exception {
			if (v == null) {
				return null;
			}
			return v.marshal();
		}

		@Override
		public FieldPosition unmarshal(final Adapted v) throws Exception {
			if (v == null) {
				return null;
			}
			return v.unmarshal();
		}

	}

	public FieldId getFieldId() {
		return fieldId;
	}

	public Vector getPosition() {
		return position;
	}

}
