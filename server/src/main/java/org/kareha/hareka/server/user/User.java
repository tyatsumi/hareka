package org.kareha.hareka.server.user;

import java.io.File;
import java.io.IOException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.kareha.hareka.annotation.ConfinedTo;
import org.kareha.hareka.annotation.GuardedBy;
import org.kareha.hareka.annotation.Private;
import org.kareha.hareka.annotation.ThreadSafe;
import org.kareha.hareka.key.KeyXml;
import org.kareha.hareka.server.entity.EntityId;
import org.kareha.hareka.server.external.Session;
import org.kareha.hareka.user.UserId;
import org.kareha.hareka.util.JaxbUtil;

@ThreadSafe
@XmlJavaTypeAdapter(User.Adapter.class)
public class User {

	@Private
	static final Logger logger = Logger.getLogger(User.class.getName());
	private static final String FILENAME = "user.xml";
	private static final JAXBContext jaxbUser;

	static {
		try {
			jaxbUser = JAXBContext.newInstance(Adapted.class);
		} catch (final JAXBException e) {
			throw new RuntimeException(e.getMessage(), e);
		}
	}

	@Private
	final UserId id;
	@GuardedBy("this")
	@Private
	PublicKey publicKey;
	@Private
	final UserId parentId;
	@GuardedBy("this")
	@Private
	List<UserId> childIds;
	@GuardedBy("this")
	@Private
	List<CharacterEntry> characterEntries;
	@Private
	final IdCipher chatEntityIdCipher;
	@Private
	final IdCipher fieldEntityIdCipher;
	@Private
	final IdCipher itemEntityIdCipher;
	@GuardedBy("this")
	@Private
	StorageCipher storageCipher;

	@GuardedBy("this")
	private Session session;

	public User(final UserId id, final PublicKey publicKey, final UserId parentId) {
		this(id, publicKey, parentId, null, null, new IdCipher(), new IdCipher(), new IdCipher(), null);
	}

	public User(final UserId id, final PublicKey publicKey, final UserId parentId, final Collection<UserId> childIds,
			final Collection<CharacterEntry> characterEntries, final IdCipher chatEntityIdCipher,
			final IdCipher fieldEntityIdCipher, final IdCipher itemEntityIdCipher, final StorageCipher storageCipher) {
		this.id = id;
		this.publicKey = publicKey;
		this.parentId = parentId;
		if (childIds != null) {
			this.childIds = new ArrayList<>(childIds);
		}
		if (characterEntries != null) {
			this.characterEntries = new ArrayList<>(characterEntries);
		}
		this.chatEntityIdCipher = chatEntityIdCipher;
		this.fieldEntityIdCipher = fieldEntityIdCipher;
		this.itemEntityIdCipher = itemEntityIdCipher;
		this.storageCipher = storageCipher;
	}

	@XmlJavaTypeAdapter(User.CharacterEntry.Adapter.class)
	public static class CharacterEntry {

		@Private
		final EntityId id;

		public CharacterEntry(final EntityId id) {
			this.id = id;
		}

		@XmlType(name = "characterEntry")
		@XmlAccessorType(XmlAccessType.NONE)
		private static class Adapted {

			@XmlAttribute
			private EntityId id;

			@SuppressWarnings("unused")
			private Adapted() {
				// used by JAXB
			}

			@Private
			Adapted(final CharacterEntry v) {
				id = v.id;
			}

			@Private
			CharacterEntry unmarshal() {
				return new CharacterEntry(id);
			}

		}

		@Private
		Adapted marshal() {
			return new Adapted(this);
		}

		private static class Adapter extends XmlAdapter<Adapted, CharacterEntry> {

			@Override
			public Adapted marshal(final CharacterEntry v) throws Exception {
				if (v == null) {
					return null;
				}
				return v.marshal();
			}

			@Override
			public CharacterEntry unmarshal(final Adapted v) throws Exception {
				if (v == null) {
					return null;
				}
				return v.unmarshal();
			}

		}

		public EntityId getId() {
			return id;
		}

	}

	@XmlRootElement(name = "user")
	@XmlAccessorType(XmlAccessType.NONE)
	private static class Adapted {

		@XmlAttribute
		private UserId id;
		@XmlElement
		private KeyXml publicKey;
		@XmlElement
		private UserId parentId;
		@XmlElement(name = "childId")
		private List<UserId> childIds;
		@XmlElement(name = "characterEntry")
		private List<CharacterEntry> characterEntries;
		@XmlElement
		private IdCipher chatEntityIdCipher;
		@XmlElement
		private IdCipher fieldEntityIdCipher;
		@XmlElement
		private IdCipher itemEntityIdCipher;
		@XmlElement
		private StorageCipher storageCipher;

		@SuppressWarnings("unused")
		private Adapted() {
			// used by JAXB
		}

		@Private
		Adapted(final User v) {
			id = v.id;
			parentId = v.parentId;
			synchronized (v) {
				publicKey = new KeyXml(v.publicKey);
				if (v.childIds != null) {
					childIds = new ArrayList<>(v.childIds);
				}
				if (v.characterEntries != null) {
					characterEntries = new ArrayList<>(v.characterEntries);
				}
			}
			chatEntityIdCipher = v.chatEntityIdCipher;
			fieldEntityIdCipher = v.fieldEntityIdCipher;
			itemEntityIdCipher = v.itemEntityIdCipher;
			synchronized (v) {
				storageCipher = v.storageCipher;
			}
		}

		@Private
		User unmarshal() {
			final PublicKey pk;
			if (publicKey != null) {
				Key key = null;
				try {
					key = publicKey.toObject();
				} catch (final NoSuchAlgorithmException | InvalidKeySpecException e) {
					logger.log(Level.WARNING, "", e);
				}
				if (key instanceof PublicKey) {
					pk = (PublicKey) key;
				} else {
					pk = null;
					logger.severe("Stored public key for userId=" + id + " is corrupted");
				}
			} else {
				pk = null;
			}
			final User user = new User(id, pk, parentId, childIds, characterEntries, chatEntityIdCipher,
					fieldEntityIdCipher, itemEntityIdCipher, storageCipher);
			return user;
		}

	}

	@Private
	Adapted marshal() {
		return new Adapted(this);
	}

	static class Adapter extends XmlAdapter<Adapted, User> {

		@Override
		public Adapted marshal(final User v) throws Exception {
			if (v == null) {
				return null;
			}
			return v.marshal();
		}

		@Override
		public User unmarshal(final Adapted v) throws Exception {
			if (v == null) {
				return null;
			}
			return v.unmarshal();
		}

	}

	public static User load(final File directory) throws JAXBException {
		final Adapted adapted = JaxbUtil.unmarshal(new File(directory, FILENAME), jaxbUser);
		return adapted.unmarshal();
	}

	@ConfinedTo("Users")
	public void save(final File directory) throws IOException, JAXBException {
		if (!directory.isDirectory()) {
			if (!directory.mkdir()) {
				throw new IOException("Cannot create directory: " + directory);
			}
		}
		JaxbUtil.marshal(new Adapted(this), new File(directory, FILENAME), jaxbUser);
	}

	public UserId getId() {
		return id;
	}

	public synchronized PublicKey getPublicKey() {
		return publicKey;
	}

	public synchronized void setPublicKey(final PublicKey publicKey) {
		this.publicKey = publicKey;
	}

	public UserId getParentId() {
		return parentId;
	}

	public synchronized Collection<UserId> getChildIds() {
		if (childIds == null) {
			return Collections.emptyList();
		}
		return new ArrayList<>(childIds);
	}

	public synchronized void addChildId(final UserId childId) {
		if (childIds == null) {
			childIds = new ArrayList<>();
		}
		childIds.add(childId);
	}

	public synchronized int getCharacterEntriesSize() {
		if (characterEntries == null) {
			return 0;
		}
		return characterEntries.size();
	}

	public synchronized CharacterEntry getCharacterEntry(final EntityId id) {
		if (characterEntries == null) {
			return null;
		}
		for (final CharacterEntry entry : characterEntries) {
			if (entry.getId().equals(id)) {
				return entry;
			}
		}
		return null;
	}

	public synchronized boolean addCharacterEntry(final EntityId id) {
		if (characterEntries == null) {
			characterEntries = new ArrayList<>();
		} else {
			for (final CharacterEntry entry : characterEntries) {
				if (entry.getId().equals(id)) {
					return false;
				}
			}
		}
		characterEntries.add(new CharacterEntry(id));
		return true;
	}

	public synchronized boolean removeCharacterEntry(final EntityId id) {
		if (characterEntries == null) {
			return false;
		}
		for (final Iterator<CharacterEntry> i = characterEntries.iterator(); i.hasNext();) {
			final CharacterEntry entry = i.next();
			if (entry.getId().equals(id)) {
				i.remove();
				if (characterEntries.size() < 1) {
					characterEntries = null;
				}
				return true;
			}
		}
		return false;
	}

	public synchronized Collection<CharacterEntry> getCharacterEntries() {
		if (characterEntries == null) {
			return Collections.emptyList();
		}
		return new ArrayList<>(characterEntries);
	}

	public IdCipher getChatEntityIdCipher() {
		return chatEntityIdCipher;
	}

	public IdCipher getFieldEntityIdCipher() {
		return fieldEntityIdCipher;
	}

	public IdCipher getItemEntityIdCipher() {
		return itemEntityIdCipher;
	}

	public synchronized StorageCipher getStorageCipher() {
		if (storageCipher == null) {
			storageCipher = new StorageCipher();
		}
		return storageCipher;
	}

	public synchronized Session getSession() {
		return session;
	}

	public synchronized void setSession(final Session v) {
		session = v;
	}

}
