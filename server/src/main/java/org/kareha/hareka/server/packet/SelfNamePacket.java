package org.kareha.hareka.server.packet;

import org.kareha.hareka.protocol.WorldClientPacketType;
import org.kareha.hareka.server.entity.ChatEntity;
import org.kareha.hareka.server.protocol.WorldClientPacket;
import org.kareha.hareka.server.user.IdCipher;
import org.kareha.hareka.protocol.PacketType;

public final class SelfNamePacket extends WorldClientPacket {

	public SelfNamePacket(final ChatEntity chatEntity, final IdCipher chatEntityIdCipher) {
		out.write(chatEntityIdCipher.encrypt(chatEntity.getId()));
		out.write(chatEntity.getName());
	}

	@Override
	protected PacketType getType() {
		return WorldClientPacketType.SELF_NAME;
	}

}
