package org.kareha.hareka.server.control;

import java.util.Collection;

import org.kareha.hareka.field.Direction;
import org.kareha.hareka.field.FieldObject;
import org.kareha.hareka.field.Placement;
import org.kareha.hareka.field.Tile;
import org.kareha.hareka.game.ActiveSkillType;
import org.kareha.hareka.server.Global;
import org.kareha.hareka.server.entity.CharacterEntity;
import org.kareha.hareka.server.field.ServerFieldObject;
import org.kareha.hareka.server.motion.MotionType;
import org.kareha.hareka.server.stat.ExperienceType;
import org.kareha.hareka.server.stat.ExperienceVariation;
import org.kareha.hareka.wait.WaitResult;
import org.kareha.hareka.wait.WaitType;

public class Control {

	private final CharacterEntity characterEntity;

	public Control(final CharacterEntity characterEntity) {
		this.characterEntity = characterEntity;
	}

	public Tile getTile() throws NotMountedException {
		final FieldObject fo = characterEntity.getFieldObject();
		if (fo == null) {
			throw new NotMountedException();
		}
		return fo.getField().getTile(fo.getPlacement().getPosition());
	}

	public int getMotionWait() throws NotMountedException {
		return characterEntity.getStat().getMotionWait(getTile().type());
	}

	public int getDistance(final CharacterEntity target) throws NotMountedException, TargetNotMountedException {
		final ServerFieldObject fo = characterEntity.getFieldObject();
		if (fo == null) {
			throw new NotMountedException();
		}
		final FieldObject targetFo = target.getFieldObject();
		if (targetFo == null) {
			throw new TargetNotMountedException();
		}
		return fo.getField().getBoundary().distance(fo.getPlacement().getPosition(),
				targetFo.getPlacement().getPosition());
	}

	public boolean isSameField(final CharacterEntity target) throws NotMountedException, TargetNotMountedException {
		final ServerFieldObject fo = characterEntity.getFieldObject();
		if (fo == null) {
			throw new NotMountedException();
		}
		final FieldObject targetFo = target.getFieldObject();
		if (targetFo == null) {
			throw new TargetNotMountedException();
		}
		return fo.getField() == targetFo.getField();
	}

	public boolean hasActiveSkill(final ActiveSkillType type) {
		return characterEntity.getStat().hasActiveSkill(type);
	}

	public int getHealthBar() {
		return characterEntity.getStat().getHealthPoints().getBar();
	}

	public boolean isStuck() {
		return characterEntity.isStuck();
	}

	public MotionType getMotion() {
		return characterEntity.getStat().getSpecies().getMotion();
	}

	public Placement getPlacement() throws NotMountedException {
		final ServerFieldObject fo = characterEntity.getFieldObject();
		if (fo == null) {
			throw new NotMountedException();
		}
		return fo.getPlacement();
	}

	public Tile getTile(final Placement placement) throws NotMountedException {
		final ServerFieldObject fo = characterEntity.getFieldObject();
		if (fo == null) {
			throw new NotMountedException();
		}
		return fo.getField().getTile(placement.getPosition());
	}

	public Placement move(final Placement placement) throws NotMountedException {
		final ServerFieldObject fo = characterEntity.getFieldObject();
		if (fo == null) {
			throw new NotMountedException();
		}
		return fo.move(placement);
	}

	public Collection<FieldObject> getInViewObjects() throws NotMountedException {
		final ServerFieldObject fo = characterEntity.getFieldObject();
		if (fo == null) {
			throw new NotMountedException();
		}
		return fo.getInViewObjects();
	}

	public boolean isSameFieldObject(final FieldObject o) throws NotMountedException {
		final ServerFieldObject fo = characterEntity.getFieldObject();
		if (fo == null) {
			throw new NotMountedException();
		}
		return o == fo;
	}

	public boolean isVisible(final FieldObject o) throws NotMountedException {
		final ServerFieldObject fo = characterEntity.getFieldObject();
		if (fo == null) {
			throw new NotMountedException();
		}
		return fo.isVisible((ServerFieldObject) o);
	}

	public int getDistance(final Placement placement) throws NotMountedException {
		final ServerFieldObject fo = characterEntity.getFieldObject();
		if (fo == null) {
			throw new NotMountedException();
		}
		return fo.getField().getBoundary().distance(fo.getPlacement().getPosition(), placement.getPosition());
	}

	public Direction getDirection(final CharacterEntity target) throws NotMountedException, TargetNotMountedException {
		final ServerFieldObject fo = characterEntity.getFieldObject();
		if (fo == null) {
			throw new NotMountedException();
		}
		final FieldObject targetFo = target.getFieldObject();
		if (targetFo == null) {
			throw new TargetNotMountedException();
		}
		return fo.getField().getBoundary().direction(fo.getPlacement().getPosition(),
				targetFo.getPlacement().getPosition());
	}

	public Tile getTile(final CharacterEntity target) throws TargetNotMountedException {
		final FieldObject targetFo = target.getFieldObject();
		if (targetFo == null) {
			throw new TargetNotMountedException();
		}
		return targetFo.getField().getTile(targetFo.getPlacement().getPosition());
	}

	public int getJumpUp() {
		return characterEntity.getJumpUp();
	}

	public int getJumpDown() {
		return characterEntity.getJumpDown();
	}

	public void attack(final CharacterEntity target) {
		characterEntity.attack(target);
	}

	public Direction getInverseDirection(final CharacterEntity target)
			throws NotMountedException, TargetNotMountedException {
		final ServerFieldObject fo = characterEntity.getFieldObject();
		if (fo == null) {
			throw new NotMountedException();
		}
		final FieldObject targetFo = target.getFieldObject();
		if (targetFo == null) {
			throw new TargetNotMountedException();
		}
		return fo.getField().getBoundary().direction(targetFo.getPlacement().getPosition(),
				fo.getPlacement().getPosition());
	}

	public void revive(final CharacterEntity loveTarget) {
		Global.INSTANCE.context().activeSkills().get(ActiveSkillType.REVIVE).use(characterEntity, loveTarget);
	}

	public int getAttackWait() throws NotMountedException {
		return characterEntity.getStat().getAttackWait(getTile().type());
	}

	public Tile getTargetTile(final CharacterEntity target) throws TargetNotMountedException {
		final FieldObject targetFo = target.getFieldObject();
		if (targetFo == null) {
			throw new TargetNotMountedException();
		}
		return targetFo.getField().getTile(targetFo.getPlacement().getPosition());
	}

	public WaitResult getNextMotionWait() throws NotMountedException {
		return characterEntity.getWaitGenerator().next(WaitType.MOTION,
				characterEntity.getStat().getMotionWait(getTile().type()));
	}

	public void sendWait(final WaitResult result) {
		characterEntity.sendWait(result.getWait());
	}

	public WaitResult getNextAttackWait() throws NotMountedException {
		return characterEntity.getWaitGenerator().next(WaitType.ATTACK,
				characterEntity.getStat().getAttackWait(getTile().type()));
	}

	public void incrementWalkExperience() {
		characterEntity.getStat().getSkills().get(ExperienceType.WALK).addValue(ExperienceVariation.WALK_INCREMENT);
	}

	public boolean isFieldDriversEmpty() {
		return characterEntity.isFieldDriversEmpty();
	}

	public void trySpawn(final Direction direction, final Placement newPlacement) throws NotMountedException {
		final ServerFieldObject fo = characterEntity.getFieldObject();
		if (fo == null) {
			throw new NotMountedException();
		}
		Global.INSTANCE.context().characterSpawner().trySpawnCharacter(fo, direction, newPlacement.getPosition());
		Global.INSTANCE.context().itemSpawner().trySpawnItem(fo, direction, newPlacement.getPosition());
	}

}