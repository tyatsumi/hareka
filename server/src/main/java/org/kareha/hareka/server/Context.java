package org.kareha.hareka.server;

import java.io.File;
import java.io.IOException;
import java.security.Key;

import javax.xml.bind.JAXBException;

import org.kareha.hareka.key.KeyPersistentFormat;
import org.kareha.hareka.key.KeyStack;
import org.kareha.hareka.logging.xml.SlowXmlLogger;
import org.kareha.hareka.logging.xml.XmlLogger;
import org.kareha.hareka.persistent.FastPersistentHashTable;
import org.kareha.hareka.persistent.PersistentHashTable;
import org.kareha.hareka.server.entity.Entities;
import org.kareha.hareka.server.entity.EntityStatic;
import org.kareha.hareka.server.external.SessionStatic;
import org.kareha.hareka.server.external.Sessions;
import org.kareha.hareka.server.external.WorldServer;
import org.kareha.hareka.server.field.FieldStatic;
import org.kareha.hareka.server.field.Fields;
import org.kareha.hareka.server.game.CharacterSpawner;
import org.kareha.hareka.server.game.Clock;
import org.kareha.hareka.server.game.ItemSpawner;
import org.kareha.hareka.server.item.Items;
import org.kareha.hareka.server.net.Server;
import org.kareha.hareka.server.skill.ActiveSkills;
import org.kareha.hareka.server.stat.SpeciesTable;
import org.kareha.hareka.server.user.AccessController;
import org.kareha.hareka.server.user.InvitationTokens;
import org.kareha.hareka.server.user.InvitationTokensLogRecord;
import org.kareha.hareka.server.user.RoleTokens;
import org.kareha.hareka.server.user.Roles;
import org.kareha.hareka.server.user.RolesLogRecord;
import org.kareha.hareka.server.user.Users;
import org.kareha.hareka.server.user.Workspace;
import org.kareha.hareka.user.UserId;
import org.kareha.hareka.user.UserIdPersistentFormat;
import org.kareha.hareka.user.UserStatic;

public class Context {

	private final ServerParameters parameters;
	private final ServerSettings serverSettings;
	private final SessionStatic sessionStatic;
	private final KeyStack keyStack;
	private final UserStatic userStatic;
	private final PersistentHashTable<Key, UserId> userIndex;
	private final Roles roles;
	private final XmlLogger<RolesLogRecord> rolesLogger;
	private final AccessController accessController;
	private final Workspace workspace;
	private final Users users;
	private final InvitationTokens invitationTokens;
	private final XmlLogger<InvitationTokensLogRecord> invitationTokensLogger;
	private final RoleTokens roleTokens;
	private final Sessions sessions;
	private final Clock clock;
	private final FieldStatic fieldStatic;
	private final Fields fields;
	private final EntityStatic entityStatic;
	private final Entities entities;
	private final SpeciesTable speciesTable;
	private final ActiveSkills activeSkills;
	private final Items items;
	private final CharacterSpawner characterSpawner;
	private final ItemSpawner itemSpawner;
	private final Server server;

	public Context(final ServerParameters parameters) throws IOException, JAXBException {
		this.parameters = parameters;
		final File dataDirectory = parameters.liveDirectory();
		serverSettings = new ServerSettings(new File(dataDirectory, "GameServerSettings.xml"));
		sessionStatic = new SessionStatic(new File(dataDirectory, "SessionStatic.xml"));
		keyStack = new KeyStack(new File(dataDirectory, "KeyStack.xml"));
		userStatic = new UserStatic(new File(dataDirectory, "UserStatic.xml"));
		userIndex = new FastPersistentHashTable<>(new File(dataDirectory, "UserIndex.dat"), new KeyPersistentFormat(),
				new UserIdPersistentFormat());
		roles = new Roles(new File(dataDirectory, "Roles.xml"));
		rolesLogger = new SlowXmlLogger<>(dataDirectory, "RolesLog", RolesLogRecord.class, true);
		accessController = new AccessController(new File(dataDirectory, "AccessController.xml"), roles);
		workspace = new Workspace(new File(dataDirectory, "Workspace.xml"));
		users = new Users(new File(dataDirectory, "user"), userStatic, userIndex);
		invitationTokens = new InvitationTokens(new File(dataDirectory, "InvitationTokens.xml"));
		invitationTokensLogger = new SlowXmlLogger<>(dataDirectory, "InvitationTokensLog",
				InvitationTokensLogRecord.class, true);
		roleTokens = new RoleTokens(new File(dataDirectory, "RoleTokens.xml"), accessController);
		sessions = new Sessions();
		clock = new Clock(new File(dataDirectory, "Clock.xml"));
		fieldStatic = new FieldStatic(new File(dataDirectory, "FieldStatic.xml"));
		fields = new Fields(new File(dataDirectory, "field"), fieldStatic);
		entityStatic = new EntityStatic(new File(dataDirectory, "EntityStatic.xml"));
		entities = new Entities(new File(dataDirectory, "entity"), entityStatic, fields);
		speciesTable = new SpeciesTable(new File(dataDirectory, "species"));
		activeSkills = new ActiveSkills();
		items = new Items();
		characterSpawner = new CharacterSpawner(new File(dataDirectory, "CharacterSpawner.xml"));
		itemSpawner = new ItemSpawner(new File(dataDirectory, "ItemSpawner.xml"));
		if (parameters.noTls()) {
			server = new WorldServer(this, parameters.port(), null);
		} else {
			final File keyStoreFile = new File(dataDirectory.getParent(), ServerConstants.KEY_STORE_FILENAME);
			server = new WorldServer(this, parameters.port(), keyStoreFile);
		}
	}

	public ServerParameters parameters() {
		return parameters;
	}

	public ServerSettings settings() {
		return serverSettings;
	}

	public SessionStatic sessionStatic() {
		return sessionStatic;
	}

	public KeyStack keyStack() {
		return keyStack;
	}

	public PersistentHashTable<Key, UserId> userIndex() {
		return userIndex;
	}

	public Roles roles() {
		return roles;
	}

	public XmlLogger<RolesLogRecord> rolesLogger() {
		return rolesLogger;
	}

	public AccessController accessController() {
		return accessController;
	}

	public Workspace workspace() {
		return workspace;
	}

	public Users users() {
		return users;
	}

	public InvitationTokens invitationTokens() {
		return invitationTokens;
	}

	public XmlLogger<InvitationTokensLogRecord> invitationTokensLogger() {
		return invitationTokensLogger;
	}

	public RoleTokens roleTokens() {
		return roleTokens;
	}

	public Sessions sessions() {
		return sessions;
	}

	public Clock clock() {
		return clock;
	}

	public Fields fields() {
		return fields;
	}

	public Entities entities() {
		return entities;
	}

	public SpeciesTable speciesTable() {
		return speciesTable;
	}

	public ActiveSkills activeSkills() {
		return activeSkills;
	}

	public Items items() {
		return items;
	}

	public CharacterSpawner characterSpawner() {
		return characterSpawner;
	}

	public ItemSpawner itemSpawner() {
		return itemSpawner;
	}

	public Server server() {
		return server;
	}

	public synchronized void load() throws IOException, JAXBException {
		// fields must be loaded before entities
		fields.load();
		// species must be loaded before entities
		speciesTable.load();
		entities.load();
	}

	public synchronized void save() throws JAXBException {
		serverSettings.save();
		sessionStatic.save();
		userStatic.save();
		// userIndex.close safely omitted
		roles.save();
		// rolesLogger.close do nothing
		accessController.save();
		invitationTokens.save();
		// invitationTokensLogger.close do nothing
		roleTokens.save();
		clock.save();
		fieldStatic.save();
		fields.save();
		speciesTable.save();
		entityStatic.save();
		entities.save();
		characterSpawner.save();
		itemSpawner.save();
	}

}
