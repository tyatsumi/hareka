package org.kareha.hareka.server;

import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.atomic.AtomicBoolean;

public enum Global {

	INSTANCE;

	private final ScheduledThreadPoolExecutor scheduledExecutor = new ScheduledThreadPoolExecutor(8);

	volatile Context context;
	private final AtomicBoolean motionEnabled = new AtomicBoolean(false);

	public ScheduledThreadPoolExecutor scheduledExecutor() {
		return scheduledExecutor;
	}

	public Context context() {
		return context;
	}

	public void setMotionEnabled(final boolean enabled) {
		motionEnabled.set(enabled);

	}

	public boolean isMotionEnabled() {
		return motionEnabled.get();
	}

}
