package org.kareha.hareka.server.handler;

import java.io.IOException;
import java.util.ResourceBundle;
import java.util.logging.Logger;

import org.kareha.hareka.protocol.Handler;
import org.kareha.hareka.protocol.HandlerException;
import org.kareha.hareka.protocol.ProtocolException;
import org.kareha.hareka.protocol.ProtocolInput;
import org.kareha.hareka.server.ServerConstants;
import org.kareha.hareka.server.external.Session;
import org.kareha.hareka.server.game.DiceRoll;
import org.kareha.hareka.server.packet.RequestDiceChoicePacket;

public final class DiceTokenHandler implements Handler<Session> {

	private static final Logger logger = Logger.getLogger(DiceTokenHandler.class.getName());

	private enum BundleKey {
		TheTokenIsTooLong, NoSuchDiceRoll,
	}

	private static void inform(final Session session, final String key) {
		final ResourceBundle bundle = session.getBundle(DiceTokenHandler.class.getName());
		session.writeInformPacket(bundle.getString(key));
	}

	@Override
	public void handle(final ProtocolInput in, final Session session)
			throws IOException, ProtocolException, HandlerException {
		final int id = in.readCompactUInt();
		final byte[] token = in.readByteArray();
		if (token.length > ServerConstants.MAX_DICE_ROLL_TOKEN_LENGTH) {
			inform(session, BundleKey.TheTokenIsTooLong.name());
			logger.fine(session.getStamp() + "Too long id=" + id + " length=" + token.length);
			return;
		}

		final DiceRoll diceRoll = session.getDiceRollTable().getDiceRoll(id);
		if (diceRoll == null) {
			inform(session, BundleKey.NoSuchDiceRoll.name());
			logger.fine(session.getStamp() + "Not found id=" + id);
			return;
		}
		diceRoll.setToken(token);
		session.write(new RequestDiceChoicePacket(diceRoll));
	}

}
