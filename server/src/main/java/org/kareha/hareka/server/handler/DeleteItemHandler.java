package org.kareha.hareka.server.handler;

import java.io.IOException;

import org.kareha.hareka.LocalEntityId;
import org.kareha.hareka.protocol.Handler;
import org.kareha.hareka.protocol.HandlerException;
import org.kareha.hareka.protocol.ProtocolException;
import org.kareha.hareka.protocol.ProtocolInput;
import org.kareha.hareka.server.entity.EntityId;
import org.kareha.hareka.server.external.Session;
import org.kareha.hareka.server.game.Inventory;
import org.kareha.hareka.server.game.Player;

public final class DeleteItemHandler implements Handler<Session> {

	@Override
	public void handle(final ProtocolInput in, final Session session)
			throws IOException, ProtocolException, HandlerException {
		// read arguments
		final LocalEntityId itemLocalId = LocalEntityId.readFrom(in);
		final long count = in.readCompactULong();

		final Player player = session.getPlayer();
		if (player == null) {
			return;
		}

		player.getEntity().stopMotion();

		final EntityId itemId = session.getUser().getItemEntityIdCipher().decrypt(itemLocalId);
		final Inventory inventory = player.getEntity().getInventory();
		inventory.remove(itemId, count);
	}

}
