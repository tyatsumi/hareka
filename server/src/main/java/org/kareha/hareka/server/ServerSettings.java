package org.kareha.hareka.server;

import java.io.File;

import javax.xml.bind.JAXBException;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.kareha.hareka.annotation.GuardedBy;
import org.kareha.hareka.annotation.Private;
import org.kareha.hareka.annotation.ThreadSafe;
import org.kareha.hareka.user.UserRegistrationMode;
import org.kareha.hareka.util.JaxbUtil;

@ThreadSafe
public class ServerSettings {

	private final File file;
	@GuardedBy("this")
	@Private
	UserRegistrationMode userRegistrationMode;
	@GuardedBy("this")
	@Private
	boolean rescueMethodsEnabled;

	public ServerSettings(final File file) throws JAXBException {
		this.file = file;
		if (!load()) {
			setupDefault();
			helperSave();
		}
	}

	private void setupDefault() {
		userRegistrationMode = UserRegistrationMode.INVITED_ONLY;
		rescueMethodsEnabled = false;
	}

	@XmlJavaTypeAdapter(ServerSettings.ConnectionEntry.Adapter.class)
	public static class ConnectionEntry {

		@Private
		final String host;
		@Private
		final int internalPort;
		@Private
		final int port;
		@Private
		final boolean connectSecurely;

		public ConnectionEntry(final String host, final int internalPort, final int port, final boolean connectSecurely,
				final String name) {
			this.host = host;
			this.internalPort = internalPort;
			this.port = port;
			this.connectSecurely = connectSecurely;
		}

		public String getHost() {
			return host;
		}

		public int getInternalPort() {
			return internalPort;
		}

		public int getPort() {
			return port;
		}

		// TODO connectSecurely option should be dropped.
		public boolean isConnectSecurely() {
			return connectSecurely;
		}

		@XmlType(name = "connectionEntry")
		@XmlAccessorType(XmlAccessType.NONE)
		private static class Adapted {

			@XmlElement
			private String host;
			@XmlElement
			private int internalPort;
			@XmlElement
			private int port;
			@XmlElement
			private boolean connectSecurely;
			@XmlElement
			private String name;

			@SuppressWarnings("unused")
			private Adapted() {
				// used by JAXB
			}

			@Private
			Adapted(final ConnectionEntry v) {
				host = v.host;
				internalPort = v.internalPort;
				port = v.port;
				connectSecurely = v.connectSecurely;
			}

			@Private
			ConnectionEntry unmarshal() {
				return new ConnectionEntry(host, internalPort, port, connectSecurely, name);
			}

		}

		@Private
		Adapted marshal() {
			return new Adapted(this);
		}

		private static class Adapter extends XmlAdapter<Adapted, ConnectionEntry> {

			@Override
			public Adapted marshal(final ConnectionEntry v) throws Exception {
				if (v == null) {
					return null;
				}
				return v.marshal();
			}

			@Override
			public ConnectionEntry unmarshal(final Adapted v) throws Exception {
				if (v == null) {
					return null;
				}
				return v.unmarshal();
			}

		}

	}

	@XmlRootElement(name = "gameServerSettings")
	@XmlAccessorType(XmlAccessType.NONE)
	private static class Adapted {

		@XmlElement
		@Private
		ConnectionEntry resourceServer;
		@XmlElement
		@Private
		UserRegistrationMode userRegistrationMode;
		@XmlElement
		@Private
		boolean rescueMethodsEnabled;

		@SuppressWarnings("unused")
		private Adapted() {
			// used by JAXB
		}

		@Private
		Adapted(final ServerSettings v) {
			synchronized (v) {
				userRegistrationMode = v.userRegistrationMode;
				rescueMethodsEnabled = v.rescueMethodsEnabled;
			}
		}

	}

	private boolean load() throws JAXBException {
		if (!file.isFile()) {
			return false;
		}
		final Adapted adapted = JaxbUtil.unmarshal(file, Adapted.class);
		userRegistrationMode = adapted.userRegistrationMode;
		rescueMethodsEnabled = adapted.rescueMethodsEnabled;
		return true;
	}

	private void helperSave() throws JAXBException {
		synchronized (this) {
			JaxbUtil.marshal(new Adapted(this), file);
		}
	}

	public void save() throws JAXBException {
		helperSave();
	}

	public synchronized UserRegistrationMode getUserRegistrationMode() {
		return userRegistrationMode;
	}

	public synchronized void setUserRegistrationMode(final UserRegistrationMode mode) {
		userRegistrationMode = mode;
	}

	public synchronized boolean isRescueMethodsEnabled() {
		return rescueMethodsEnabled;
	}

	public synchronized void setRescueMethodsEnabled(final boolean enabled) {
		rescueMethodsEnabled = enabled;
	}

}
