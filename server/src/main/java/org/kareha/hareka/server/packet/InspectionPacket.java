package org.kareha.hareka.server.packet;

import java.util.Collection;

import org.kareha.hareka.protocol.WorldClientPacketType;
import org.kareha.hareka.server.entity.Entity;
import org.kareha.hareka.server.protocol.WorldClientPacket;
import org.kareha.hareka.server.user.IdCipher;
import org.kareha.hareka.protocol.PacketType;
import org.kareha.hareka.user.UserId;

public final class InspectionPacket extends WorldClientPacket {

	public InspectionPacket(final Entity entity, final IdCipher chatEntityIdCipher, final IdCipher fieldEntityIdCipher,
			final Collection<UserId> userIds, final IdCipher userIdCipher) {
		out.write(chatEntityIdCipher.encrypt(entity.getId()));
		out.write(fieldEntityIdCipher.encrypt(entity.getId()));
		out.writeCompactUInt(userIds.size());
		for (final UserId i : userIds) {
			out.write(userIdCipher.encrypt(i));
		}
	}

	@Override
	protected PacketType getType() {
		return WorldClientPacketType.INSPECTION;
	}

}
