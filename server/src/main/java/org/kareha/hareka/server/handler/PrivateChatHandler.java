package org.kareha.hareka.server.handler;

import java.io.IOException;
import java.util.ResourceBundle;
import java.util.logging.Logger;

import org.kareha.hareka.LocalEntityId;
import org.kareha.hareka.protocol.Handler;
import org.kareha.hareka.protocol.HandlerException;
import org.kareha.hareka.protocol.ProtocolException;
import org.kareha.hareka.protocol.ProtocolInput;
import org.kareha.hareka.server.entity.ChatEntity;
import org.kareha.hareka.server.entity.Entity;
import org.kareha.hareka.server.entity.EntityId;
import org.kareha.hareka.server.external.Session;
import org.kareha.hareka.server.game.Player;

public final class PrivateChatHandler implements Handler<Session> {

	private static final Logger logger = Logger.getLogger(PrivateChatHandler.class.getName());

	private enum BundleKey {
		LoginCharacterFirst, PeerNotFound,
	}

	private static void inform(final Session session, final String key) {
		final ResourceBundle bundle = session.getBundle(PrivateChatHandler.class.getName());
		session.writeInformPacket(bundle.getString(key));
	}

	@Override
	public void handle(final ProtocolInput in, final Session session)
			throws IOException, ProtocolException, HandlerException {
		// read arguments
		final LocalEntityId localId = LocalEntityId.readFrom(in);
		final String content = in.readString();

		// check state
		final Player player = session.getPlayer();
		if (player == null) {
			inform(session, BundleKey.LoginCharacterFirst.name());
			logger.fine(session.getStamp() + "Character not logged in");
			return;
		}
		final EntityId id = session.getUser().getChatEntityIdCipher().decrypt(localId);
		if (id == null) {
			inform(session, BundleKey.PeerNotFound.name());
			logger.fine(session.getStamp() + "EntityId not found");
			return;
		}
		final Entity peerEntity = session.getContext().entities().get(id);
		if (!(peerEntity instanceof ChatEntity)) {
			inform(session, BundleKey.PeerNotFound.name());
			logger.fine(session.getStamp() + "Not a ChatEntity id=" + id);
			return;
		}
		final ChatEntity peer = (ChatEntity) peerEntity;
		final ChatEntity self = player.getEntity();
		self.sendPrivateChat(peer, content);
	}

}
