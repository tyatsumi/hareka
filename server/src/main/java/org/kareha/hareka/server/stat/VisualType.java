package org.kareha.hareka.server.stat;

import org.kareha.hareka.game.PassiveSkillType;

public enum VisualType {

	SUPER(PassiveSkillType.ABLE_TO_SEE_SUPER),

	SOUL(PassiveSkillType.ABLE_TO_SEE_SOUL),

	SPIRIT(PassiveSkillType.ABLE_TO_SEE_SPIRIT),

	FAIRY(PassiveSkillType.ABLE_TO_SEE_FAIRY),

	NORMAL(PassiveSkillType.ABLE_TO_SEE_NORMAL),

	;

	private final PassiveSkillType passiveSkill;

	private VisualType(final PassiveSkillType passiveSkill) {
		this.passiveSkill = passiveSkill;
	}

	public PassiveSkillType passiveSkill() {
		return passiveSkill;
	}

}
