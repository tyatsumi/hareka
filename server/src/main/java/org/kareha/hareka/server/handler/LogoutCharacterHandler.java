package org.kareha.hareka.server.handler;

import java.io.IOException;
import java.util.ResourceBundle;
import java.util.logging.Logger;

import org.kareha.hareka.protocol.Handler;
import org.kareha.hareka.protocol.HandlerException;
import org.kareha.hareka.protocol.ProtocolException;
import org.kareha.hareka.protocol.ProtocolInput;
import org.kareha.hareka.server.external.Session;
import org.kareha.hareka.server.game.Player;
import org.kareha.hareka.server.packet.CharactersPacket;
import org.kareha.hareka.server.user.User;

public final class LogoutCharacterHandler implements Handler<Session> {

	private static final Logger logger = Logger.getLogger(LogoutCharacterHandler.class.getName());

	private enum BundleKey {
		LoginUserFirst, LoginCharacterFirst, LogoutCharacterSuccess,
	}

	@Override
	public void handle(final ProtocolInput in, final Session session)
			throws IOException, ProtocolException, HandlerException {
		// read arguments
		final int requestId = in.readCompactUInt();

		final ResourceBundle bundle = session.getBundle(LogoutCharacterHandler.class.getName());

		// check state
		final User user = session.getUser();
		if (user == null) {
			session.writeRejectPacket(requestId, bundle.getString(BundleKey.LoginUserFirst.name()));
			logger.fine(session.getStamp() + "Not logged in");
			return;
		}
		final Player player = session.getPlayer();
		if (player == null) {
			session.writeRejectPacket(requestId, bundle.getString(BundleKey.LoginCharacterFirst.name()));
			logger.fine(session.getStamp() + "Character not logged in");
			return;
		}

		// process
		session.logoutCharacter();
		session.writeAcceptPacket(requestId, bundle.getString(BundleKey.LogoutCharacterSuccess.name()));

		session.write(new CharactersPacket(user.getCharacterEntries(), session.getContext().entities(),
				user.getChatEntityIdCipher(), session.getLocale().getLanguage()));
	}

}
