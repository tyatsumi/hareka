package org.kareha.hareka.server.stat;

public class StatPoints {

	private final int max;
	private final int current;

	public StatPoints(final int max, final int current) {
		this.max = max;
		this.current = current;
	}

	public int getMax() {
		return max;
	}

	public int getCurrent() {
		return current;
	}

	public boolean isAlive() {
		return current > 0;
	}

	public boolean isDamaged() {
		return current < max;
	}

	public int getBar() {
		if (max <= 0) {
			return Byte.MAX_VALUE;
		}
		if (current > max) {
			return Byte.MAX_VALUE;
		}
		return Byte.MAX_VALUE * current / max;
	}

}
