package org.kareha.hareka.server.packet;

import org.kareha.hareka.field.Placement;
import org.kareha.hareka.field.TileType;
import org.kareha.hareka.field.Transformation;
import org.kareha.hareka.field.Vector;
import org.kareha.hareka.protocol.WorldClientPacketType;
import org.kareha.hareka.server.entity.FieldEntity;
import org.kareha.hareka.server.protocol.WorldClientPacket;
import org.kareha.hareka.server.user.IdCipher;
import org.kareha.hareka.protocol.PacketType;

public final class PlaceFieldEntityPacket extends WorldClientPacket {

	public PlaceFieldEntityPacket(final FieldEntity entity, final Placement placement, final TileType tileType,
			final IdCipher fieldEntityIdCipher, final Transformation transformation, final Vector position) {
		out.write(fieldEntityIdCipher.encrypt(entity.getId()));
		out.write(entity.getFieldObject().getField().getBoundary().confine(placement, position)
				.transform(transformation));
		out.writeCompactUInt(entity.getMotionWait(tileType));
	}

	@Override
	protected PacketType getType() {
		return WorldClientPacketType.PLACE_FIELD_ENTITY;
	}

}
