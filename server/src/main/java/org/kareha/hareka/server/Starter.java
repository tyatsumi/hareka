package org.kareha.hareka.server;

import java.io.File;
import java.io.IOException;
import java.util.logging.Logger;

import javax.xml.bind.DatatypeConverter;
import javax.xml.bind.JAXBException;

import org.kareha.hareka.Constants;
import org.kareha.hareka.annotation.GuardedBy;
import org.kareha.hareka.server.net.ServerException;
import org.kareha.hareka.server.user.InvitationToken;
import org.kareha.hareka.server.user.RoleToken;
import org.kareha.hareka.util.FileUtil;

public enum Starter {

	INSTANCE;

	private static final Logger logger = Logger.getLogger(Starter.class.getName());
	private static final int EXIT_FAILURE = 1;

	@GuardedBy("this")
	private boolean shutdownHookAdded = false;

	public synchronized void start(final ServerParameters parameters)
			throws IOException, JAXBException, ServerException {
		FileUtil.ensureDirectoryExists(parameters.liveDirectory());

		final File crashedFile = new File(parameters.liveDirectory(), ServerConstants.CRASHED_FILENAME);
		while (true) {
			if (crashedFile.exists()) {
				if (parameters.backupDirectory().exists()) {
					logger.severe("Data corrupted and trying to recover from backup..");
					FileUtil.deleteRecursively(parameters.liveDirectory());
					if (!parameters.backupDirectory().renameTo(parameters.liveDirectory())) {
						logger.severe("Failed to recover from backup");
						System.exit(EXIT_FAILURE);
						return;
					}
					logger.severe("Data recovered from backup");
				} else {
					logger.severe("Data corrupted and no backup exists");
					System.exit(EXIT_FAILURE);
					return;
				}
			} else {
				break;
			}
		}

		// Create context
		final Context context = new Context(parameters);
		Global.INSTANCE.context = context;

		// Show version
		logger.info("Version=" + Constants.VERSION);

		// Booted or rebooted
		if (Rebooter.INSTANCE.isRebooted()) {
			logger.info("Rebooting");
		} else {
			logger.info("Booting");
		}

		// Load data
		context.load();

		// Backup data
		if (parameters.backupDirectory().exists()) {
			FileUtil.deleteRecursively(parameters.backupDirectory());
		}
		FileUtil.ensureDirectoryExists(parameters.backupDirectory());
		final File backupCrashedFile = new File(parameters.backupDirectory(), ServerConstants.CRASHED_FILENAME);
		backupCrashedFile.createNewFile();
		FileUtil.copyRecursively(parameters.liveDirectory(), parameters.backupDirectory());
		backupCrashedFile.delete();

		// Open transaction
		crashedFile.createNewFile();

		// Issue initial invitation token
		final InvitationToken invitationToken = context.invitationTokens().issueInitial();
		if (invitationToken != null) {
			final String hex = DatatypeConverter.printHexBinary(invitationToken.getValue()).toLowerCase();
			logger.info("Initial invitation token: " + hex);
		}

		// Issue initial role token
		final RoleToken roleToken = context.roleTokens().issueInitial();
		if (roleToken != null) {
			final String hex = DatatypeConverter.printHexBinary(roleToken.getValue()).toLowerCase();
			logger.info("Initial role token (" + roleToken.getRoleId() + "): " + hex);
		}

		// Add shutdown hook
		if (!shutdownHookAdded) {
			Runtime.getRuntime().addShutdownHook(new ShutdownHook(parameters.parameters().raw()));
			shutdownHookAdded = true;
		}

		// Start motion
		context.entities().startMotion();

		// Start listening
		context.server().start();
	}

}
