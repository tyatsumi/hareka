package org.kareha.hareka.server.packet;

import org.kareha.hareka.field.TileType;
import org.kareha.hareka.protocol.WorldClientPacketType;
import org.kareha.hareka.server.entity.CharacterEntity;
import org.kareha.hareka.server.protocol.WorldClientPacket;
import org.kareha.hareka.server.skill.ActiveSkill;
import org.kareha.hareka.protocol.PacketType;

public final class AddActiveSkillPacket extends WorldClientPacket {

	public AddActiveSkillPacket(final ActiveSkill ability, final CharacterEntity entity) {
		out.write(ability.getType());
		out.writeCompactUInt(ability.getReach());
		out.write(ability.getWaitType());
		out.writeCompactUInt(ability.getWait(entity, TileType.NULL));
	}

	@Override
	protected PacketType getType() {
		return WorldClientPacketType.ADD_ACTIVE_SKILL;
	}

}
