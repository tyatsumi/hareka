package org.kareha.hareka.server.packet;

import java.util.ArrayList;
import java.util.List;

import org.kareha.hareka.protocol.WorldClientPacketType;
import org.kareha.hareka.server.protocol.WorldClientPacket;
import org.kareha.hareka.server.user.AccessController;
import org.kareha.hareka.server.user.IdCipher;
import org.kareha.hareka.server.user.RoleToken;
import org.kareha.hareka.server.user.RoleTokens;
import org.kareha.hareka.server.user.Roles;
import org.kareha.hareka.server.user.User;
import org.kareha.hareka.protocol.PacketType;
import org.kareha.hareka.user.Permission;
import org.kareha.hareka.user.Role;

public final class RoleTokenListPacket extends WorldClientPacket {

	public RoleTokenListPacket(final RoleTokens roleTokens, final User user, final AccessController accessController,
			final Roles roles, final IdCipher userIdCipher) {
		final int rank;
		final Role mrole = accessController.getRoleSet(user).getHighestRole(Permission.MANAGE_ROLE_TOKENS);
		if (mrole == null) {
			rank = 0;
		} else {
			rank = mrole.getRank();
		}
		final List<RoleToken> list = new ArrayList<>();
		for (final RoleToken token : roleTokens.getRoleTokens()) {
			if (token.getIssuer().equals(user.getId())) {
				list.add(token);
				continue;
			}
			final Role role = roles.get(token.getRoleId());
			if (role != null && role.getRank() <= rank) {
				list.add(token);
				continue;

			}
		}
		out.writeCompactUInt(list.size());
		for (final RoleToken token : list) {
			out.writeCompactULong(token.getId());
			out.writeCompactLong(token.getIssuedDate());
			out.write(userIdCipher.encrypt(token.getIssuer()));
			out.writeString(token.getRoleId());
		}
	}

	@Override
	protected PacketType getType() {
		return WorldClientPacketType.ROLE_TOKEN_LIST;
	}

}
