package org.kareha.hareka.server.packet;

import org.kareha.hareka.protocol.WorldClientPacketType;
import org.kareha.hareka.server.protocol.WorldClientPacket;
import org.kareha.hareka.protocol.PacketType;

public final class TextPacket extends WorldClientPacket {

	public TextPacket(final String text) {
		out.writeString(text);
	}

	@Override
	protected PacketType getType() {
		return WorldClientPacketType.TEXT;
	}

}
