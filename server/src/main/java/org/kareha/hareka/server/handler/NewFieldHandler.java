package org.kareha.hareka.server.handler;

import java.io.IOException;
import java.security.SecureRandom;
import java.util.ResourceBundle;

import org.kareha.hareka.field.Placement;
import org.kareha.hareka.field.SolidTilePattern;
import org.kareha.hareka.field.Tile;
import org.kareha.hareka.field.TileType;
import org.kareha.hareka.field.Vector;
import org.kareha.hareka.protocol.Handler;
import org.kareha.hareka.protocol.HandlerException;
import org.kareha.hareka.protocol.ProtocolException;
import org.kareha.hareka.protocol.ProtocolInput;
import org.kareha.hareka.server.entity.CharacterEntity;
import org.kareha.hareka.server.external.Session;
import org.kareha.hareka.server.field.FieldPosition;
import org.kareha.hareka.server.field.Gate;
import org.kareha.hareka.server.field.PeriodicBoundary;
import org.kareha.hareka.server.field.ServerField;
import org.kareha.hareka.server.field.ServerFieldObject;
import org.kareha.hareka.server.game.Player;
import org.kareha.hareka.server.user.User;
import org.kareha.hareka.user.Permission;
import org.kareha.hareka.user.RoleSet;

public final class NewFieldHandler implements Handler<Session> {

	private static final int MAX_SIZE = 65536;

	private enum BundleKey {
		YouCannotAddFields, TheFieldBoundarySizeMustBeGreaterThanOrEqualTo, TheFieldBoundarySizeMustBeSmallerThan,
		ThisIsSpecialTile, YouCannotEditThisTile, ANewFieldHasBeenCreated,
	}

	private static void inform(final Session session, final String key) {
		final ResourceBundle bundle = session.getBundle(NewFieldHandler.class.getName());
		session.writeInformPacket(bundle.getString(key));
	}

	@Override
	public void handle(final ProtocolInput in, final Session session)
			throws IOException, ProtocolException, HandlerException {
		final int size = in.readCompactUInt();

		final User user = session.getUser();
		if (user == null) {
			return;
		}
		final RoleSet roles = session.getContext().accessController().getRoleSet(user);
		if (!roles.isAbleTo(Permission.EDIT_FIELDS)) {
			inform(session, BundleKey.YouCannotAddFields.name());
			return;
		}
		final boolean force = roles.isAbleTo(Permission.FORCE_EDIT_TILE_FIELDS);

		final Player player = session.getPlayer();
		if (player == null) {
			return;
		}
		final CharacterEntity entity = player.getEntity();
		final ServerFieldObject fo = entity.getFieldObject();
		if (fo == null) {
			return;
		}
		final ServerField field = fo.getField();
		if (size < field.getViewSize()) {
			final ResourceBundle bundle = session.getBundle(NewFieldHandler.class.getName());
			session.writeInformPacket(bundle.getString(BundleKey.TheFieldBoundarySizeMustBeGreaterThanOrEqualTo.name()),
					field.getViewSize());
			return;
		}
		if (size >= MAX_SIZE) {
			final ResourceBundle bundle = session.getBundle(NewFieldHandler.class.getName());
			session.writeInformPacket(bundle.getString(BundleKey.TheFieldBoundarySizeMustBeSmallerThan.name()),
					MAX_SIZE);
			return;
		}
		final Placement placement = fo.getPlacement();
		final Tile originalTile = field.getTile(placement.getPosition());
		if (originalTile.type().isSpecial()) {
			inform(session, BundleKey.ThisIsSpecialTile.name());
			return;
		}
		if (!force && !field.isMutable(placement.getPosition())) {
			inform(session, BundleKey.YouCannotEditThisTile.name());
			return;
		}
		field.setAndSyncTile(placement.getPosition(), Tile.valueOf(TileType.GATE, originalTile.elevation()), force);

		final ServerField.Builder builder = new ServerField.Builder();
		builder.defaultTilePattern(new SolidTilePattern(Tile.valueOf(TileType.WATER, 0)));
		builder.boundary(new PeriodicBoundary(size, new SecureRandom().nextBoolean()));
		builder.viewSize(field.getViewSize());
		final ServerField newField = session.getContext().fields().createField(builder);
		newField.setAndSyncTile(Vector.ZERO, Tile.valueOf(TileType.GATE, 0), force);
		final Gate gate = Gate.valueOf(FieldPosition.valueOf(field.getId(), placement.getPosition()),
				FieldPosition.valueOf(newField.getId(), Vector.ZERO));
		field.addGate(gate);
		final Gate gate2 = Gate.valueOf(FieldPosition.valueOf(newField.getId(), Vector.ZERO),
				FieldPosition.valueOf(field.getId(), placement.getPosition()));
		newField.addGate(gate2);
		inform(session, BundleKey.ANewFieldHasBeenCreated.name());
	}

}
