package org.kareha.hareka.server;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.SwingUtilities;
import javax.xml.bind.JAXBException;

import org.kareha.hareka.logging.FileLogger;
import org.kareha.hareka.logging.SimpleLogger;
import org.kareha.hareka.server.net.ServerException;
import org.kareha.hareka.util.FileUtil;
import org.kareha.hareka.util.KeyStoreUtil;
import org.kareha.hareka.util.Locker;
import org.kareha.hareka.util.LogUtil;
import org.kareha.hareka.util.Wiper;

final class Main {

	private static final Logger logger = Logger.getLogger(Main.class.getName());
	private static final Logger appRootLogger = Logger.getLogger("org.kareha.hareka");

	static {
		LogUtil.loadLogLevel(appRootLogger);
	}

	private Main() {
		throw new AssertionError();
	}

	public static void main(final String[] args) {
		final ServerParameters parameters = new ServerParameters(args);

		if (parameters.wipe()) {
			Wiper.confirmAndWipe(parameters.dataDirectory(), parameters.uiType());
		}

		FileUtil.ensureDirectoryExistsOrExit(parameters.dataDirectory(), parameters.uiType());
		Rebooter.INSTANCE.initialize(new File(parameters.dataDirectory(), "rebooting"));
		Locker.INSTANCE.lock(parameters.dataDirectory(), Rebooter.INSTANCE.isRebooted(), parameters.uiType());

		SimpleLogger.INSTANCE.addStream(System.err);
		FileLogger.connect(appRootLogger, new File(parameters.dataDirectory(), "log"), parameters.uiType());

		Thread.setDefaultUncaughtExceptionHandler((t, e) -> logger.log(Level.SEVERE, t.getName(), e));

		final File gameKeyStoreFile = new File(parameters.dataDirectory(), ServerConstants.KEY_STORE_FILENAME);
		if (!gameKeyStoreFile.exists()) {
			KeyStoreUtil.generate(parameters.dataDirectory(), ServerConstants.KEY_STORE_PASSWORD,
					ServerConstants.KEY_PASSWORD, "localhost");
		}

		switch (parameters.uiType()) {
		default:
			throw new AssertionError();
		case SWING:
			SwingUtilities.invokeLater(() -> new MainFrame(parameters).setVisible(true));
			break;
		case CONSOLE:
			try {
				Starter.INSTANCE.start(parameters);
			} catch (final IOException | JAXBException | ServerException e) {
				logger.log(Level.SEVERE, "", e);
				return;
			}
			break;
		}
	}

}
