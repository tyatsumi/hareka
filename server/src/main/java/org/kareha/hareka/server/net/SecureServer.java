package org.kareha.hareka.server.net;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.ServerSocket;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.KeyManager;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLServerSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

public abstract class SecureServer extends AbstractServer {

	private final File keyStoreFile;
	private final char[] keyStorePassword;
	private final char[] keyPassword;

	public SecureServer(final int port, final File keyStoreFile, final char[] keyStorePassword,
			final char[] keyPassword) {
		super(port);
		this.keyStoreFile = keyStoreFile;
		this.keyStorePassword = keyStorePassword.clone();
		this.keyPassword = keyPassword.clone();
	}

	@Override
	protected ServerSocket createServerSocket() throws IOException, ServerException {
		final SSLContext sslContext;
		try {
			sslContext = SSLContext.getInstance("TLS");
		} catch (final NoSuchAlgorithmException e) {
			throw new AssertionError(e);
		}

		final KeyStore keyStore;
		try {
			keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
		} catch (final KeyStoreException e) {
			throw new AssertionError(e);
		}
		try (final InputStream in = new BufferedInputStream(new FileInputStream(keyStoreFile))) {
			try {
				keyStore.load(in, keyStorePassword);
			} catch (final NoSuchAlgorithmException e) {
				throw new AssertionError(e);
			} catch (final CertificateException e) {
				throw new RuntimeException(e);
			} catch (final IOException e) {
				if (e.getCause() instanceof UnrecoverableKeyException) {
					throw new ServerException("Store password may be incorrect", e);
				}
				throw e;
			}
		} catch (final FileNotFoundException e) {
			throw new ServerException("KeyStore not found", e);
		}

		final KeyManagerFactory keyManagerFactory;
		try {
			keyManagerFactory = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
		} catch (final NoSuchAlgorithmException e) {
			throw new AssertionError(e);
		}
		try {
			keyManagerFactory.init(keyStore, keyPassword);
		} catch (final UnrecoverableKeyException e) {
			throw new ServerException("Key password may be incorrect", e);
		} catch (final KeyStoreException e) {
			throw new RuntimeException(e);
		} catch (final NoSuchAlgorithmException e) {
			throw new AssertionError(e);
		}
		final KeyManager[] km = keyManagerFactory.getKeyManagers();

		final TrustManager[] tm = new TrustManager[] { new X509TrustManager() {
			@Override
			public void checkClientTrusted(final X509Certificate[] chain, final String authType)
					throws CertificateException {
				// TODO
			}

			@Override
			public void checkServerTrusted(final X509Certificate[] chain, final String authType)
					throws CertificateException {
				// TODO
			}

			@Override
			public X509Certificate[] getAcceptedIssuers() {
				return new X509Certificate[0];
			}
		} };
		try {
			sslContext.init(km, tm, new SecureRandom());
		} catch (final KeyManagementException e) {
			throw new RuntimeException(e);
		}
		final SSLServerSocketFactory socketFactory = sslContext.getServerSocketFactory();
		return socketFactory.createServerSocket(port());
	}

}
