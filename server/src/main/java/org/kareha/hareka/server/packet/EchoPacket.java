package org.kareha.hareka.server.packet;

import org.kareha.hareka.protocol.WorldClientPacketType;
import org.kareha.hareka.server.protocol.WorldClientPacket;
import org.kareha.hareka.protocol.PacketType;

public final class EchoPacket extends WorldClientPacket {

	public EchoPacket() {
		// do nothing
	}

	@Override
	protected PacketType getType() {
		return WorldClientPacketType.ECHO;
	}

}
