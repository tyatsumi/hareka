package org.kareha.hareka.server.packet;

import org.kareha.hareka.protocol.WorldClientPacketType;
import org.kareha.hareka.server.protocol.WorldClientPacket;
import org.kareha.hareka.server.stat.LongStatPoints;
import org.kareha.hareka.protocol.PacketType;

public final class WeightBarPacket extends WorldClientPacket {

	public WeightBarPacket(final LongStatPoints points) {
		out.writeByte(points.getBar());
	}

	@Override
	protected PacketType getType() {
		return WorldClientPacketType.WEIGHT_BAR;
	}

}
