package org.kareha.hareka.server.item;

import org.kareha.hareka.field.TileType;
import org.kareha.hareka.game.ItemType;
import org.kareha.hareka.game.Name;
import org.kareha.hareka.server.entity.CharacterEntity;
import org.kareha.hareka.server.entity.FieldEntity;
import org.kareha.hareka.server.entity.ItemEntity;
import org.kareha.hareka.wait.WaitType;

public class PeachItem implements Item {

	private final Name name;

	public PeachItem() {
		name = new Name();
		name.set("en", "Peach");
		name.set("ja", "もも");
	}

	@Override
	public ItemType getType() {
		return ItemType.PEACH;
	}

	@Override
	public Name getName() {
		return name;
	}

	@Override
	public String getShape() {
		return "Peach";
	}

	@Override
	public boolean isExclusive() {
		return false;
	}

	@Override
	public boolean isStackable() {
		return true;
	}

	@Override
	public boolean isConsumable() {
		return true;
	}

	@Override
	public int getWeight() {
		return 32;
	}

	@Override
	public int getReach() {
		return 1;
	}

	@Override
	public WaitType getWaitType() {
		return WaitType.ATTACK;
	}

	@Override
	public int getWait(final CharacterEntity entity, final TileType tileType) {
		return entity.getStat().getAttackWait(tileType);
	}

	@Override
	public void use(final CharacterEntity characterEntity, final ItemEntity itemEntity, final FieldEntity target) {
		// TODO item function
		characterEntity.sendMessage("not implemented");
	}

}
