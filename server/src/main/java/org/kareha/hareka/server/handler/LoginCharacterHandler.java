package org.kareha.hareka.server.handler;

import java.io.IOException;
import java.util.ResourceBundle;
import java.util.logging.Logger;

import org.kareha.hareka.LocalEntityId;
import org.kareha.hareka.field.Placement;
import org.kareha.hareka.protocol.Handler;
import org.kareha.hareka.protocol.HandlerException;
import org.kareha.hareka.protocol.ProtocolException;
import org.kareha.hareka.protocol.ProtocolInput;
import org.kareha.hareka.server.entity.CharacterEntity;
import org.kareha.hareka.server.entity.Entity;
import org.kareha.hareka.server.entity.EntityId;
import org.kareha.hareka.server.external.Session;
import org.kareha.hareka.server.field.FieldId;
import org.kareha.hareka.server.field.ServerFieldObject;
import org.kareha.hareka.server.game.Player;
import org.kareha.hareka.server.user.User;
import org.kareha.hareka.server.user.User.CharacterEntry;

public final class LoginCharacterHandler implements Handler<Session> {

	private static final Logger logger = Logger.getLogger(LoginCharacterHandler.class.getName());

	private enum BundleKey {
		LoginUserFirst, NoSuchCharacterExists, InternalError, FailedToLogin, LoginSuccess,
	}

	@Override
	public void handle(final ProtocolInput in, final Session session)
			throws IOException, ProtocolException, HandlerException {
		// read arguments
		final int requestId = in.readCompactUInt();
		final LocalEntityId localId = LocalEntityId.readFrom(in);

		final ResourceBundle bundle = session.getBundle(LoginCharacterHandler.class.getName());

		// check state
		final User user = session.getUser();
		if (user == null) {
			session.writeRejectPacket(requestId, bundle.getString(BundleKey.LoginUserFirst.name()));
			logger.fine(session.getStamp() + "Not logged in");
			return;
		}
		final EntityId entityId = session.getUser().getChatEntityIdCipher().decrypt(localId);
		final CharacterEntry entry = user.getCharacterEntry(entityId);
		if (entry == null) {
			session.writeRejectPacket(requestId, bundle.getString(BundleKey.NoSuchCharacterExists.name()));
			logger.fine(session.getStamp() + "Not found id=" + entityId);
			return;
		}
		final Entity entity = session.getContext().entities().get(entry.getId());
		if (entity == null) {
			session.writeRejectPacket(requestId, bundle.getString(BundleKey.NoSuchCharacterExists.name()));
			logger.warning(session.getStamp() + "Entity not found id=" + entry.getId());
			return;
		}
		if (!(entity instanceof CharacterEntity)) {
			session.writeRejectPacket(requestId, bundle.getString(BundleKey.InternalError.name()));
			logger.warning(session.getStamp() + "Not character id=" + entry.getId());
			return;
		}
		final CharacterEntity characterEntity = (CharacterEntity) entity;
		final Player player;
		if (!characterEntity.getStat().getHealthPoints().isAlive()) {
			final FieldId fieldId;
			final Placement placement;
			if (characterEntity.isMounted()) {
				final ServerFieldObject fo = characterEntity.getFieldObject();
				fieldId = fo.getField().getId();
				placement = fo.getPlacement();
			} else {
				fieldId = characterEntity.getFieldId();
				placement = characterEntity.getPlacement();
			}
			if (fieldId == null) {
				session.writeRejectPacket(requestId, bundle.getString(BundleKey.FailedToLogin.name()));
				logger.warning(session.getStamp() + "Failed to login character id=" + entry.getId() + "; fieldId=null");
				return;
			}
			if (placement == null) {
				session.writeRejectPacket(requestId, bundle.getString(BundleKey.FailedToLogin.name()));
				logger.warning(
						session.getStamp() + "Failed to login character id=" + entry.getId() + "; placement=null");
				return;
			}
			final CharacterEntity newEntity = session.getContext().entities().createSoulEntity(fieldId, placement);
			if (user.removeCharacterEntry(characterEntity.getId())) {
				characterEntity.removeOwner(user.getId());
				if (user.addCharacterEntry(newEntity.getId())) {
					newEntity.addOwner(user.getId());
				}
			}
			newEntity.mount(session.getContext().fields());
			player = new Player(newEntity, session, user);
		} else {
			characterEntity.mount(session.getContext().fields());
			player = new Player(characterEntity, session, user);
		}
		if (!session.setPlayer(player)) {
			session.writeRejectPacket(requestId, bundle.getString(BundleKey.FailedToLogin.name()));
			logger.warning(session.getStamp() + "Failed to login character id=" + entry.getId());
			return;
		}
		session.writeAcceptPacket(requestId, bundle.getString(BundleKey.LoginSuccess.name()));

		// setup
		player.synchronize();
	}

}
