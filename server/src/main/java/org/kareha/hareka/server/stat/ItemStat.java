package org.kareha.hareka.server.stat;

import org.kareha.hareka.server.item.Item;

public interface ItemStat extends Stat {

	Item getBrand();

	long drainCount();

	long addCount(long v);

	long getWeight();

}
