package org.kareha.hareka.server.control;

@SuppressWarnings("serial")
public class NotMountedException extends Exception {

	public NotMountedException() {
		// do nothing
	}

	public NotMountedException(final String message) {
		super(message);
	}

	public NotMountedException(final String message, final Throwable cause) {
		super(message, cause);
	}

}
