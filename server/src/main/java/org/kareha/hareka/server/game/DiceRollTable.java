package org.kareha.hareka.server.game;

import java.security.SecureRandom;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

public class DiceRollTable {

	private final String hashAlgorithm;
	private final AtomicInteger count = new AtomicInteger();
	private final Map<Integer, DiceRoll> map = new ConcurrentHashMap<>();
	private final SecureRandom random = new SecureRandom();

	public DiceRollTable(final String hashAlgorithm) {
		this.hashAlgorithm = hashAlgorithm;
	}

	public DiceRoll initiate(final int faces, final int rate) {
		final int id = count.getAndIncrement();
		final DiceRoll diceRoll = new DiceRoll(id, faces, rate, random, hashAlgorithm);
		map.put(id, diceRoll);
		return diceRoll;
	}

	public DiceRoll getDiceRoll(final int id) {
		return map.get(id);
	}

}
