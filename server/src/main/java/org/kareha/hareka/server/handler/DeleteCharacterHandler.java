package org.kareha.hareka.server.handler;

import java.io.IOException;
import java.util.ResourceBundle;
import java.util.logging.Logger;

import org.kareha.hareka.LocalEntityId;
import org.kareha.hareka.protocol.Handler;
import org.kareha.hareka.protocol.HandlerException;
import org.kareha.hareka.protocol.ProtocolException;
import org.kareha.hareka.protocol.ProtocolInput;
import org.kareha.hareka.server.entity.CharacterEntity;
import org.kareha.hareka.server.entity.Entity;
import org.kareha.hareka.server.entity.EntityId;
import org.kareha.hareka.server.external.Session;
import org.kareha.hareka.server.packet.CharactersPacket;
import org.kareha.hareka.server.user.User;

public final class DeleteCharacterHandler implements Handler<Session> {

	private static final Logger logger = Logger.getLogger(DeleteCharacterHandler.class.getName());

	private enum BundleKey {
		LoginUserFirst, TheCharacterDeleted, CannotDeleteTheCharacter,
	}

	@Override
	public void handle(final ProtocolInput in, final Session session)
			throws IOException, ProtocolException, HandlerException {
		// read arguments
		final int requestId = in.readCompactUInt();
		final LocalEntityId localId = LocalEntityId.readFrom(in);

		final ResourceBundle bundle = session.getBundle(DeleteCharacterHandler.class.getName());

		// check state
		final User user = session.getUser();
		if (user == null) {
			session.writeRejectPacket(requestId, bundle.getString(BundleKey.LoginUserFirst.name()));
			logger.fine(session.getStamp() + "No user");
			return;
		}

		// delete character
		final EntityId entityId = user.getChatEntityIdCipher().decrypt(localId);
		if (user.removeCharacterEntry(entityId)) {
			final Entity entity = session.getContext().entities().get(entityId);
			if (entity instanceof CharacterEntity) {
				final CharacterEntity characterEntity = (CharacterEntity) entity;
				characterEntity.removeOwner(user.getId());
			}
		} else {
			session.writeRejectPacket(requestId, bundle.getString(BundleKey.CannotDeleteTheCharacter.name()));
			logger.fine(session.getStamp() + "Failed to delete id=" + entityId);
			return;
		}
		session.writeAcceptPacket(requestId, bundle.getString(BundleKey.TheCharacterDeleted.name()));
		logger.info(session.getStamp() + "Deleted id=" + entityId);

		session.write(new CharactersPacket(user.getCharacterEntries(), session.getContext().entities(),
				user.getChatEntityIdCipher(), session.getLocale().getLanguage()));
	}

}
