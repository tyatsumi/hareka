package org.kareha.hareka.server.packet;

import org.kareha.hareka.protocol.WorldClientPacketType;
import org.kareha.hareka.server.protocol.WorldClientPacket;
import org.kareha.hareka.protocol.PacketType;

public final class RequestPublicKeyPacket extends WorldClientPacket {

	public RequestPublicKeyPacket(final int handlerId, final int version, final byte[] nonce) {
		out.writeCompactUInt(handlerId);
		out.writeCompactUInt(version);
		out.writeByteArray(nonce);
	}

	@Override
	protected PacketType getType() {
		return WorldClientPacketType.REQUEST_PUBLIC_KEY;
	}

}
