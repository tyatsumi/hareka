package org.kareha.hareka.server.packet;

import org.kareha.hareka.protocol.WorldClientPacketType;
import org.kareha.hareka.server.ServerSettings;
import org.kareha.hareka.server.protocol.WorldClientPacket;
import org.kareha.hareka.protocol.PacketType;

public final class SettingsPacket extends WorldClientPacket {

	public SettingsPacket(final ServerSettings settings) {
		out.write(settings.getUserRegistrationMode());
		out.writeBoolean(settings.isRescueMethodsEnabled());
	}

	@Override
	protected PacketType getType() {
		return WorldClientPacketType.SETTINGS;
	}

}
