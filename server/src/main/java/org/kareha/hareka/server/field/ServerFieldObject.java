package org.kareha.hareka.server.field;

import java.util.HashMap;
import java.util.Map;

import org.kareha.hareka.annotation.GuardedBy;
import org.kareha.hareka.field.FieldObject;
import org.kareha.hareka.field.FieldObjectWithView;
import org.kareha.hareka.field.Placement;
import org.kareha.hareka.field.TileType;
import org.kareha.hareka.server.Global;
import org.kareha.hareka.server.entity.CharacterEntity;
import org.kareha.hareka.server.entity.FieldDriver;
import org.kareha.hareka.server.entity.FieldEntity;

public class ServerFieldObject extends FieldObjectWithView {

	protected final FieldEntity fieldEntity;
	protected final Object visibilityLock = new Object();
	@GuardedBy("visibilityLock")
	protected Map<ServerFieldObject, Long> visibleObjects;

	public ServerFieldObject(final ServerField field, final Placement placement, final FieldEntity fieldEntity) {
		super(field, field.getBoundary().confine(placement));
		this.fieldEntity = fieldEntity;
	}

	@Override
	public ServerField getField() {
		return (ServerField) field;
	}

	public FieldEntity getFieldEntity() {
		return fieldEntity;
	}

	@Override
	public boolean addInViewObject(final FieldObject v) {
		final boolean result = super.addInViewObject(v);
		final ServerFieldObject sv = (ServerFieldObject) v;
		if (result && isVisible(sv)) {
			for (final FieldDriver driver : fieldEntity.getFieldDrivers()) {
				driver.handleAddInViewObject(sv);
			}
		}
		return result;
	}

	@Override
	public boolean removeInViewObject(final FieldObject v) {
		final boolean result = super.removeInViewObject(v);
		final ServerFieldObject sv = (ServerFieldObject) v;
		if (result && isVisible(sv)) {
			for (final FieldDriver driver : fieldEntity.getFieldDrivers()) {
				driver.handleRemoveInViewObject(sv);
			}

			synchronized (visibilityLock) {
				if (visibleObjects != null && visibleObjects.remove(sv) != null && visibleObjects.isEmpty()) {
					visibleObjects = null;
				}
			}
		}
		return result;
	}

	@Override
	public void inViewObjectMoved(final FieldObject v, final Placement placement) {
		super.inViewObjectMoved(v, placement);
		final ServerFieldObject sv = (ServerFieldObject) v;
		if (isVisible(sv)) {
			for (final FieldDriver driver : fieldEntity.getFieldDrivers()) {
				driver.handleInViewObjectMoved(sv, placement);
			}
		}
	}

	@Override
	public Placement move(final Placement p) {
		final Placement placement = getField().getBoundary().confine(p);
		final Placement prevPlacement = super.move(placement);
		if (prevPlacement == null) {
			return null;
		}
		for (final FieldDriver driver : fieldEntity.getFieldDrivers()) {
			driver.handleMove(prevPlacement, placement);
		}
		switch (field.getTile(placement.getPosition()).type()) {
		default:
			break;
		case AIR: {
			ServerField currentField = getField();
			while (true) {
				final FieldId nextId = currentField.getDownstairs();
				final ServerField nextField = nextId == null ? null
						: Global.INSTANCE.context().fields().getField(nextId);
				if (nextField == null) {
					break;
				}
				final TileType type = nextField.getTile(placement.getPosition()).type();
				if (type == TileType.AIR) {
					currentField = nextField;
					continue;
				} else if (type.isWalkable()) {
					currentField = nextField;
					break;
				} else {
					break;
				}
			}
			if (currentField != field) {
				fieldEntity.teleport(FieldPosition.valueOf(currentField.getId(), placement.getPosition()));
			}
		}
			break;
		case DOWNSTAIRS:
			final FieldId downstairs = getField().getDownstairs();
			if (downstairs != null) {
				fieldEntity.teleport(FieldPosition.valueOf(downstairs, placement.getPosition()));
			}
			break;
		case GATE:
			final Gate gate = getField().getGate(placement.getPosition());
			if (gate != null) {
				fieldEntity.teleport(gate.getDestination());
			}
			break;
		case UPSTAIRS:
			final FieldId upstairs = getField().getUpstairs();
			if (upstairs != null) {
				fieldEntity.teleport(FieldPosition.valueOf(upstairs, placement.getPosition()));
			}
			break;
		}
		if (fieldEntity instanceof CharacterEntity) {
			final CharacterEntity characterEntity = (CharacterEntity) fieldEntity;
			characterEntity.updateLastActionTime();
		}
		return prevPlacement;
	}

	@Override
	public boolean isExclusive() {
		return fieldEntity.getStat().isExclusive();
	}

	@Override
	public int getJumpDown() {
		return fieldEntity.getJumpDown();
	}

	@Override
	public int getJumpUp() {
		return fieldEntity.getJumpUp();
	}

	public boolean isVisible(final ServerFieldObject fieldObject) {
		if (fieldEntity.isVisible(fieldObject.getFieldEntity().getVisualType())) {
			return true;
		}
		return containsVisibleObject(fieldObject);
	}

	public void addVisibleObject(final ServerFieldObject fieldObject, final int duration) {
		if (isVisible(fieldObject)) {
			return;
		}
		synchronized (visibilityLock) {
			if (visibleObjects == null) {
				visibleObjects = new HashMap<>();
			}
			visibleObjects.put(fieldObject, System.currentTimeMillis() + duration);
		}
		for (final FieldDriver driver : fieldEntity.getFieldDrivers()) {
			driver.handleAddInViewObject(fieldObject);
		}
	}

	private boolean containsVisibleObject(final ServerFieldObject fieldObject) {
		synchronized (visibilityLock) {
			if (visibleObjects == null) {
				return false;
			}
			final Long date = visibleObjects.get(fieldObject);
			if (date == null) {
				return false;
			}
			if (System.currentTimeMillis() < date) {
				return true;
			}
			if (visibleObjects.size() <= 1) {
				visibleObjects = null;
			} else {
				visibleObjects.remove(fieldObject);
			}
		}
		for (final FieldDriver driver : fieldEntity.getFieldDrivers()) {
			driver.handleRemoveInViewObject(fieldObject);
		}
		return false;
	}

}
