package org.kareha.hareka.server.packet;

import org.kareha.hareka.field.Vector;
import org.kareha.hareka.protocol.WorldClientPacketType;
import org.kareha.hareka.server.entity.CharacterEntity;
import org.kareha.hareka.server.field.PositionMemory;
import org.kareha.hareka.server.field.ServerField;
import org.kareha.hareka.server.protocol.WorldClientPacket;
import org.kareha.hareka.server.user.StorageCipher;
import org.kareha.hareka.protocol.PacketType;

public final class PositionMemoryPacket extends WorldClientPacket {

	public PositionMemoryPacket(final CharacterEntity entity, final ServerField field, final Vector position,
			final StorageCipher cipher) {
		out.writeByteArray(new PositionMemory(field, position, entity).encrypt(cipher));
	}

	@Override
	protected PacketType getType() {
		return WorldClientPacketType.POSITION_MEMORY;
	}

}
