package org.kareha.hareka.server.entity;

import java.util.Collection;

import org.kareha.hareka.game.Name;

public interface ChatEntity extends Entity {

	String getName(String language);

	void setName(String language, String name);

	Name getName();

	void setName(Name name);

	void sendLocalChat(String content);

	void receiveLocalChat(ChatEntity speaker, String content);

	void sendPrivateChat(ChatEntity peer, String content);

	void receivePrivateChat(ChatEntity speaker, String content);

	void addChatDriver(ChatDriver driver);

	boolean removeChatDriver(ChatDriver driver);

	Collection<ChatDriver> getChatDrivers();

}
