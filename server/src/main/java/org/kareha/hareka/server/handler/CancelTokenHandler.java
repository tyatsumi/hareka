package org.kareha.hareka.server.handler;

import java.io.IOException;
import java.util.logging.Logger;

import org.kareha.hareka.protocol.Handler;
import org.kareha.hareka.protocol.HandlerException;
import org.kareha.hareka.protocol.ProtocolException;
import org.kareha.hareka.protocol.ProtocolInput;
import org.kareha.hareka.server.external.Session;
import org.kareha.hareka.server.game.TokenGrabber;

public final class CancelTokenHandler implements Handler<Session> {

	private static final Logger logger = Logger.getLogger(CancelTokenHandler.class.getName());

	@Override
	public void handle(final ProtocolInput in, final Session session)
			throws IOException, ProtocolException, HandlerException {
		final int handlerId = in.readCompactUInt();

		final TokenGrabber.Entry entry = session.getTokenGrabber().get(handlerId);
		if (entry == null) {
			logger.fine(session.getStamp() + "Not found handerId=" + handlerId);
			return;
		}
		entry.cancel();
	}

}
