package org.kareha.hareka.server.handler;

import java.io.IOException;

import org.kareha.hareka.protocol.Handler;
import org.kareha.hareka.protocol.HandlerException;
import org.kareha.hareka.protocol.ProtocolException;
import org.kareha.hareka.protocol.ProtocolInput;
import org.kareha.hareka.server.external.Session;
import org.kareha.hareka.server.packet.RoleTokenListPacket;
import org.kareha.hareka.server.user.RoleToken;
import org.kareha.hareka.server.user.User;
import org.kareha.hareka.user.Permission;
import org.kareha.hareka.user.Role;
import org.kareha.hareka.user.RoleSet;

public final class DeleteRoleTokenHandler implements Handler<Session> {

	@Override
	public void handle(final ProtocolInput in, final Session session)
			throws IOException, ProtocolException, HandlerException {
		final long id = in.readCompactULong();

		final User user = session.getUser();
		if (user == null) {
			return;
		}
		final RoleToken token = session.getContext().roleTokens().get(id);
		if (token == null) {
			return;
		}
		final RoleSet roleSet = session.getContext().accessController().getRoleSet(user);
		final boolean accepted;
		if (token.getIssuer().equals(user.getId())) {
			accepted = true;
		} else {
			final int rank;
			final Role mrole = roleSet.getHighestRole(Permission.MANAGE_ROLE_TOKENS);
			if (mrole == null) {
				rank = 0;
			} else {
				rank = mrole.getRank();
			}
			final Role role = session.getContext().roles().get(token.getRoleId());
			if (role != null && role.getRank() <= rank) {
				accepted = true;
			} else {
				accepted = false;
			}
		}
		if (!accepted) {
			return;
		}

		if (session.getContext().roleTokens().remove(token.getId())) {
			session.write(new RoleTokenListPacket(session.getContext().roleTokens(), user,
					session.getContext().accessController(), session.getContext().roles(),
					session.getContext().workspace().getUserIdCipher()));
		}
	}

}
