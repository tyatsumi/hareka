package org.kareha.hareka.server.stat;

public class StatResult {

	private final StatPoints points;
	private final int value;

	public StatResult(final StatPoints points, final int value) {
		this.points = points;
		this.value = value;
	}

	public StatPoints getPoints() {
		return points;
	}

	public int getValue() {
		return value;
	}

	public boolean isTooSmall() {
		return value < 0;
	}

	public boolean isTooLarge() {
		return value > points.getMax();
	}

	public int getOverflow() {
		if (value < 0) {
			return -value;
		} else if (value > points.getMax()) {
			return value - points.getMax();
		} else {
			return 0;
		}
	}

}
