package org.kareha.hareka.server.relationship;

import org.kareha.hareka.annotation.GuardedBy;

public class Relationship {

	public static final long MAX_VALUE = (1L << 48) - 1;

	@GuardedBy("this")
	private long love;
	@GuardedBy("this")
	private long hate;

	public Relationship() {

	}

	public Relationship(final long love, final long hate) {
		this.love = love;
		this.hate = hate;
	}

	public synchronized long getLove() {
		return love;
	}

	public synchronized void addLove(final long v) {
		final long newValue = love + v;
		if (newValue < 0) {
			love = 0;
		} else if (newValue > MAX_VALUE) {
			love = MAX_VALUE;
		} else {
			love = newValue;
		}
	}

	public synchronized long getHate() {
		return hate;
	}

	public synchronized void addHate(final long v) {
		final long newValue = hate + v;
		if (newValue < 0) {
			hate = 0;
		} else if (newValue > MAX_VALUE) {
			hate = MAX_VALUE;
		} else {
			hate = newValue;
		}
	}

}
