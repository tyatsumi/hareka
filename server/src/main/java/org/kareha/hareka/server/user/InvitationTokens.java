package org.kareha.hareka.server.user;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import javax.xml.bind.JAXBException;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.kareha.hareka.annotation.GuardedBy;
import org.kareha.hareka.annotation.Private;
import org.kareha.hareka.annotation.ThreadSafe;
import org.kareha.hareka.util.JaxbUtil;

@ThreadSafe
public class InvitationTokens {

	private final File file;
	@GuardedBy("this")
	@Private
	long count;
	@GuardedBy("this")
	@Private
	long initialId = -1;
	@GuardedBy("this")
	@Private
	final List<InvitationToken> list = new ArrayList<>();

	public InvitationTokens(final File file) throws JAXBException {
		this.file = file;
		final Adapted adapted = load();
		if (adapted != null) {
			count = Long.parseLong(adapted.count, 16);
			initialId = Long.parseLong(adapted.initialId, 16);
			if (adapted.list != null) {
				list.addAll(adapted.list);
			}
		}
	}

	@XmlRootElement(name = "invitationTokens")
	@XmlAccessorType(XmlAccessType.NONE)
	private static class Adapted {

		@XmlElement
		@Private
		String count;
		@XmlElement
		@Private
		String initialId;
		@XmlElement(name = "invitationToken")
		@Private
		List<InvitationToken> list;

		@SuppressWarnings("unused")
		private Adapted() {
			// used by JAXB
		}

		@Private
		Adapted(final InvitationTokens v) {
			synchronized (v) {
				count = Long.toHexString(v.count);
				initialId = Long.toHexString(v.initialId);
				list = new ArrayList<>(v.list);
			}
		}

	}

	private Adapted load() throws JAXBException {
		if (!file.isFile()) {
			return null;
		}
		return JaxbUtil.unmarshal(file, Adapted.class);
	}

	public void save() throws JAXBException {
		JaxbUtil.marshal(new Adapted(this), file);
	}

	public synchronized InvitationToken get(final long id) {
		for (final InvitationToken token : list) {
			if (token.getId() == id) {
				return token;
			}
		}
		return null;
	}

	public synchronized InvitationToken remove(final long id) {
		for (final Iterator<InvitationToken> i = list.iterator(); i.hasNext();) {
			final InvitationToken token = i.next();
			if (token.getId() == id) {
				i.remove();
				return token;
			}
		}
		return null;
	}

	public synchronized Collection<InvitationToken> getInvitationTokens() {
		return new ArrayList<>(list);
	}

	public synchronized InvitationToken issueInitial() {
		if (initialId == -1) {
			final InvitationToken token = new InvitationToken(count, null);
			initialId = count;
			count++;
			list.add(token);
			return token;
		}
		for (final InvitationToken token : list) {
			if (token.getId() == initialId) {
				return token;
			}
		}
		return null;
	}

	public synchronized InvitationToken issue(final User user) {
		final InvitationToken token = new InvitationToken(count, user.getId());
		count++;
		list.add(token);
		return token;
	}

	public synchronized InvitationToken consume(final byte[] token) {
		for (final Iterator<InvitationToken> i = list.iterator(); i.hasNext();) {
			final InvitationToken t = i.next();
			if (Arrays.equals(token, t.getValue())) {
				i.remove();
				return t;
			}
		}
		return null;
	}

}
