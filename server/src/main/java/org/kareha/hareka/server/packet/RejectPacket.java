package org.kareha.hareka.server.packet;

import org.kareha.hareka.protocol.WorldClientPacketType;
import org.kareha.hareka.server.protocol.WorldClientPacket;
import org.kareha.hareka.protocol.PacketType;

public final class RejectPacket extends WorldClientPacket {

	public RejectPacket(final int requestId, final String message) {
		out.writeCompactUInt(requestId);
		out.writeString(message);
	}

	@Override
	protected PacketType getType() {
		return WorldClientPacketType.REJECT;
	}

}
