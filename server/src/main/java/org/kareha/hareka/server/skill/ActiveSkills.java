package org.kareha.hareka.server.skill;

import java.lang.reflect.InvocationTargetException;
import java.util.EnumMap;
import java.util.Map;

import org.kareha.hareka.game.ActiveSkillType;
import org.kareha.hareka.util.CamelCase;

public class ActiveSkills {

	private final Map<ActiveSkillType, ActiveSkill> map;

	public ActiveSkills() {
		final String base = "org.kareha.hareka.server.skill";
		final String postfix = "ActiveSkill";
		final Map<ActiveSkillType, ActiveSkill> m = new EnumMap<>(ActiveSkillType.class);
		for (final ActiveSkillType type : ActiveSkillType.values()) {
			final String camelCase = CamelCase.snakeToCamel(type.name());
			if (camelCase == null) {
				throw new RuntimeException("The name must be underscore: " + type.name());
			}
			final StringBuilder sb = new StringBuilder(base).append(".").append(camelCase).append(postfix);
			Class<?> clazz;
			try {
				clazz = Class.forName(sb.toString());
			} catch (final ClassNotFoundException e) {
				throw new IllegalArgumentException(e.getMessage(), e);
			}
			final Object o;
			try {
				o = clazz.getDeclaredConstructor().newInstance();
			} catch (final InstantiationException e) {
				throw new IllegalArgumentException(e.getMessage(), e);
			} catch (final IllegalAccessException e) {
				throw new IllegalArgumentException(e.getMessage(), e);
			} catch (final InvocationTargetException e) {
				throw new IllegalArgumentException(e.getMessage(), e);
			} catch (final NoSuchMethodException e) {
				throw new IllegalArgumentException(e.getMessage(), e);
			} catch (final SecurityException e) {
				throw new IllegalArgumentException(e.getMessage(), e);
			}
			if (!(o instanceof ActiveSkill)) {
				throw new IllegalArgumentException(o.getClass().getName() + " is not instance of ActiveSkill");
			}
			final ActiveSkill t = (ActiveSkill) o;
			m.put(type, t);
		}
		map = m;
	}

	public ActiveSkill get(final ActiveSkillType type) {
		return map.get(type);
	}

}
