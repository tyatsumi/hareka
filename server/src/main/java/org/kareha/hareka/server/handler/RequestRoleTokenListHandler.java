package org.kareha.hareka.server.handler;

import java.io.IOException;

import org.kareha.hareka.protocol.Handler;
import org.kareha.hareka.protocol.HandlerException;
import org.kareha.hareka.protocol.ProtocolException;
import org.kareha.hareka.protocol.ProtocolInput;
import org.kareha.hareka.server.external.Session;
import org.kareha.hareka.server.packet.RoleTokenListPacket;
import org.kareha.hareka.server.user.User;

public final class RequestRoleTokenListHandler implements Handler<Session> {

	@Override
	public void handle(final ProtocolInput in, final Session session)
			throws IOException, ProtocolException, HandlerException {
		final User user = session.getUser();
		if (user == null) {
			return;
		}

		session.write(new RoleTokenListPacket(session.getContext().roleTokens(), user,
				session.getContext().accessController(), session.getContext().roles(),
				session.getContext().workspace().getUserIdCipher()));
	}

}
