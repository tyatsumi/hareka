package org.kareha.hareka.server.packet;

import org.kareha.hareka.field.Transformation;
import org.kareha.hareka.field.Vector;
import org.kareha.hareka.protocol.WorldClientPacketType;
import org.kareha.hareka.server.entity.FieldEntity;
import org.kareha.hareka.server.field.ServerFieldObject;
import org.kareha.hareka.server.protocol.WorldClientPacket;
import org.kareha.hareka.server.stat.Stat;
import org.kareha.hareka.server.stat.StatPoints;
import org.kareha.hareka.server.user.IdCipher;
import org.kareha.hareka.protocol.PacketType;

public final class AddFieldEntityPacket extends WorldClientPacket {

	public AddFieldEntityPacket(final FieldEntity entity, final IdCipher fieldEntityIdCipher,
			final Transformation transformation, final Vector position) {
		out.write(fieldEntityIdCipher.encrypt(entity.getId()));
		final ServerFieldObject fo = entity.getFieldObject();
		if (fo == null) {
			return;
		}
		out.write(fo.getField().getBoundary().confine(fo.getPlacement(), position).transform(transformation));
		out.writeString(entity.getShape());
		final Stat stat = entity.getStat();
		final StatPoints hp;
		final StatPoints mp;
		synchronized (stat) {
			hp = stat.getHealthPoints();
			mp = stat.getMagicPoints();
		}
		out.writeBoolean(hp.isAlive());
		out.writeByte(hp.getBar());
		out.writeBoolean(mp.isAlive());
		out.writeByte(mp.getBar());
		out.writeCompactULong(stat.getCount());
	}

	@Override
	protected PacketType getType() {
		return WorldClientPacketType.ADD_FIELD_ENTITY;
	}

}
