package org.kareha.hareka.server.stat;

public final class ExperienceVariation {

	public static final int HEALTH_INCREMENT = 1;
	public static final int STRENGTH_INCREMENT = 2;
	public static final int FANG_INCREMENT = 1;
	public static final int SKIN_INCREMENT = 1;
	public static final int AGILITY_INCREMENT = 2;
	public static final int DEXTERITY_INCREMENT = 4;
	public static final int DEXTERITY_DECREMENT = -1;
	public static final int DODGE_INCREMENT = 4;
	public static final int DODGE_DECREMENT = -1;
	public static final int RECOVERY_INCREMENT = 1;
	public static final int WALK_INCREMENT = 1;
	public static final int MIND_DECREMENT = -1;
	public static final int MAGIC_RECOVERY_INCREMENT = 1;

}
