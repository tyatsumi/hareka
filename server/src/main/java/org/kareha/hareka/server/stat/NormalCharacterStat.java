package org.kareha.hareka.server.stat;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import org.kareha.hareka.annotation.GuardedBy;
import org.kareha.hareka.annotation.Private;
import org.kareha.hareka.annotation.ThreadSafe;
import org.kareha.hareka.field.TileType;
import org.kareha.hareka.game.PassiveSkillType;
import org.kareha.hareka.math.IntRange;
import org.kareha.hareka.server.Global;
import org.kareha.hareka.server.relationship.IndividualRelationships;

@ThreadSafe
public class NormalCharacterStat extends AbstractCharacterStat {

	@Private
	static int getMaxHealthPoints(final Species species, final Experiences experiences) {
		return 4 * getEffectiveSkill(species, experiences, ExperienceType.HEALTH);
	}

	@Private
	static int getMaxMagicPoints(final Species race, final Experiences experiences) {
		return 4 * getEffectiveSkill(race, experiences, ExperienceType.MAGIC);
	}

	public static class Builder extends AbstractCharacterStat.Builder<Builder> {

		protected Integer healthPoints;
		protected Integer magicPoints;
		protected Experiences skills;
		protected IndividualRelationships relationships;

		public Builder healthPoints(final int healthPoints) {
			this.healthPoints = healthPoints;
			return this;
		}

		public Builder magicPoints(final int magicPoints) {
			this.magicPoints = magicPoints;
			return this;
		}

		public Builder skills(final Experiences v) {
			skills = v;
			return this;
		}

		public Builder relationships(final IndividualRelationships v) {
			relationships = v;
			return this;
		}

		@Override
		protected Builder self() {
			return this;
		}

		@Override
		public NormalCharacterStat build() {
			if (skills == null) {
				skills = new Experiences();
			}
			if (healthPoints == null) {
				healthPoints = getMaxHealthPoints(species, skills);
			}
			if (magicPoints == null) {
				magicPoints = getMaxMagicPoints(species, skills);
			}
			return new NormalCharacterStat(this);
		}

	}

	@GuardedBy("this")
	protected int healthPoints;
	@GuardedBy("this")
	protected int magicPoints;
	protected final Experiences skills;
	@GuardedBy("this")
	protected IndividualRelationships relationships;

	protected NormalCharacterStat(final Builder builder) {
		super(builder);
		healthPoints = builder.healthPoints;
		magicPoints = builder.magicPoints;
		skills = builder.skills;
		relationships = builder.relationships;
	}

	@XmlType(name = "normalCharacterStat")
	@XmlAccessorType(XmlAccessType.NONE)
	protected static class Adapted extends AbstractCharacterStat.Adapted {

		@XmlElement
		protected int healthPoints;
		@XmlElement
		protected int magicPoints;
		@XmlElement
		protected Experiences skills;
		@XmlElement
		protected IndividualRelationships relationships;

		protected Adapted() {
			// used by JAXB
		}

		protected Adapted(final NormalCharacterStat v) {
			super(v);
			synchronized (v) {
				healthPoints = v.healthPoints;
				magicPoints = v.magicPoints;
				relationships = v.relationships;
			}
			skills = v.skills;
		}

		@Override
		protected NormalCharacterStat unmarshal() {
			final Builder builder = new Builder();
			final Species species = Global.INSTANCE.context().speciesTable().getSpecies(speciesId);
			builder.species(species).shape(shape);
			builder.healthPoints(healthPoints).magicPoints(magicPoints);
			builder.skills(skills);
			builder.relationships(relationships);
			return builder.build();
		}

	}

	@Override
	protected Adapted marshal() {
		return new Adapted(this);
	}

	private int getMaxHealthPoints() {
		return getMaxHealthPoints(species, skills);
	}

	@Override
	public synchronized StatPoints getHealthPoints() {
		return new StatPoints(getMaxHealthPoints(), healthPoints);
	}

	private int getMaxMagicPoints() {
		return getMaxMagicPoints(species, skills);
	}

	@Override
	public synchronized StatPoints getMagicPoints() {
		return new StatPoints(getMaxMagicPoints(), magicPoints);
	}

	@Override
	public synchronized boolean isExclusive() {
		return species.hasPassiveSkill(PassiveSkillType.EXCLUSIVE) && healthPoints > 0;
	}

	@Override
	public int getJumpDown() {
		return getEffectiveSkill(ExperienceType.JUMP_DOWN);
	}

	@Override
	public int getJumpUp() {
		return getEffectiveSkill(ExperienceType.JUMP_UP);
	}

	@Override
	public synchronized int getMotionWait(final TileType tileType) {
		switch (tileType) {
		default: {
			// 1 - 0.5 * (max - current) / max = 0.5 * (max + current) / max
			final int maxHp = getMaxHealthPoints();
			return 1000 * 127 * (maxHp + healthPoints) / 2 / maxHp / getEffectiveSkill(ExperienceType.WALK) / 4;
		}
		case WATER: {
			// 1 - 0.5 * (max - current) / max = 0.5 * (max + current) / max
			final int maxHp = getMaxHealthPoints();
			return 1000 * 127 * (maxHp + healthPoints) / 2 / maxHp / getEffectiveSkill(ExperienceType.WALK) / 4 * 127
					/ getEffectiveSkill(ExperienceType.SWIM);
		}
		}
	}

	@Override
	public synchronized int getAttackWait(final TileType tileType) {
		switch (tileType) {
		default:
			return 1000 * 127 / getEffectiveSkill(ExperienceType.AGILITY) / 4;
		case WATER:
			return 1000 * 127 / getEffectiveSkill(ExperienceType.AGILITY) / 4 * 127
					/ getEffectiveSkill(ExperienceType.SWIM);
		}
	}

	@GuardedBy("this")
	private int helperAddHealthPoints(final int v) {
		final int value = healthPoints + v;
		if (value < 0) {
			healthPoints = 0;
		} else if (value > getMaxHealthPoints()) {
			healthPoints = getMaxHealthPoints();
		} else {
			healthPoints = value;
		}
		return value;
	}

	@Override
	public synchronized StatResult addHealthPoints(final int v) {
		if (v < 0) {
			if (healthPoints <= 0) {
				return null;
			}
		} else if (v > 0) {
			if (healthPoints >= getMaxHealthPoints()) {
				return null;
			}
		} else {
			return null;
		}
		final int value = helperAddHealthPoints(v);
		return new StatResult(new StatPoints(getMaxHealthPoints(), healthPoints), value);
	}

	@GuardedBy("this")
	private int helperAddMagicPoints(final int v) {
		final int value = magicPoints + v;
		if (value < 0) {
			magicPoints = 0;
		} else if (value > getMaxMagicPoints()) {
			magicPoints = getMaxMagicPoints();
		} else {
			magicPoints = value;
		}
		return value;
	}

	@Override
	public synchronized StatResult addMagicPoints(final int v) {
		if (v < 0) {
			if (magicPoints <= 0) {
				return null;
			}
		} else if (v > 0) {
			if (magicPoints >= getMaxMagicPoints()) {
				return null;
			}
		} else {
			return null;
		}
		final int value = helperAddMagicPoints(v);
		return new StatResult(new StatPoints(getMaxMagicPoints(), magicPoints), value);
	}

	@Override
	public synchronized boolean revive() {
		if (healthPoints > 0) {
			return false;
		}
		healthPoints = 1;
		return true;
	}

	private static int getEffective(final int min, final int max, final long skillValue) {
		return (int) (min + (max - min) * skillValue / Experience.MAX_VALUE);
	}

	private static int getEffectiveSkill(final Species race, final Experiences skills, final ExperienceType type) {
		final IntRange range = race.getExperienceRange(type);
		final Experience skill = skills.get(type);
		return getEffective(range.getMin(), range.getMax(), skill.getValue());
	}

	@Override
	public int getEffectiveSkill(final ExperienceType type) {
		return getEffectiveSkill(species, skills, type);
	}

	private int calib(final int v) {
		final int unsoundness = getEffectiveSkill(ExperienceType.UNSOUNDNESS);
		return v * 127 * (127 - unsoundness);
	}

	@Override
	public int getWeaponClass() {
		final int fang = getEffectiveSkill(ExperienceType.FANG);
		// TODO weapon class calculation
		return calib(fang);
	}

	@Override
	public int getArmorClass() {
		final int skin = getEffectiveSkill(ExperienceType.SKIN);
		// TODO armor class calculation
		return calib(skin);
	}

	@Override
	public int getAttackPoints(final int targetArmorClass) {
		return getEffectiveSkill(ExperienceType.STRENGTH) * getWeaponClass() / targetArmorClass;
	}

	@Override
	public int getAccuracy(final int targetDodge) {
		final int dexterity = getEffectiveSkill(ExperienceType.DEXTERITY);
		final int d = dexterity + targetDodge;
		if (d < 1) {
			return 0;
		}
		final int a = 128 * dexterity / d;
		return calib(a);
	}

	@Override
	public long getWeightCapacity() {
		return 4096L * getEffectiveSkill(ExperienceType.STRENGTH);
	}

	@Override
	public Experiences getSkills() {
		return skills;
	}

	@Override
	public synchronized IndividualRelationships getRelationships() {
		if (relationships == null) {
			relationships = new IndividualRelationships();
		}
		return relationships;
	}

}
