package org.kareha.hareka.server.net;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.kareha.hareka.annotation.GuardedBy;
import org.kareha.hareka.annotation.Private;

public abstract class AbstractServer implements Server {

	@Private
	static final Logger logger = Logger.getLogger(AbstractServer.class.getName());

	private final int port;
	@GuardedBy("this")
	private ServerThread thread;

	public AbstractServer(final int port) {
		this.port = port;
	}

	@Override
	public int port() {
		return port;
	}

	protected abstract ServerSocket createServerSocket() throws IOException, ServerException;

	@SuppressWarnings("resource")
	@Override
	public synchronized boolean start() throws IOException, ServerException {
		if (thread != null) {
			return false;
		}
		thread = new ServerThread(createServerSocket());
		thread.start();
		return true;
	}

	@Override
	public synchronized boolean stop() throws IOException, InterruptedException {
		if (thread == null) {
			return false;
		}
		thread.interrupt();
		thread.join();
		thread = null;
		return true;
	}

	protected abstract void accepted(Socket clientSocket);

	private class ServerThread extends Thread {

		private final ServerSocket serverSocket;

		ServerThread(final ServerSocket serverSocket) {
			this.serverSocket = serverSocket;
		}

		@SuppressWarnings("resource")
		@Override
		public void run() {
			try {
				while (!Thread.currentThread().isInterrupted()) {
					final Socket clientSocket;
					try {
						clientSocket = serverSocket.accept();
					} catch (final SocketException e) {
						break;
					} catch (final IOException e) {
						logger.log(Level.SEVERE, "", e);
						break;
					}
					accepted(clientSocket);
				}
			} finally {
				try {
					serverSocket.close();
				} catch (final IOException e) {
					logger.log(Level.SEVERE, "", e);
				}
			}
		}

		@Override
		public void interrupt() {
			try {
				serverSocket.close();
			} catch (final IOException e) {
				logger.log(Level.SEVERE, "", e);
			}
			super.interrupt();
		}

	}

}
