package org.kareha.hareka.server.handler;

import java.io.IOException;
import java.util.logging.Logger;

import org.kareha.hareka.key.KeyGrabber;
import org.kareha.hareka.protocol.Handler;
import org.kareha.hareka.protocol.HandlerException;
import org.kareha.hareka.protocol.ProtocolException;
import org.kareha.hareka.protocol.ProtocolInput;
import org.kareha.hareka.server.external.Session;

public final class NoPublicKeyHandler implements Handler<Session> {

	private static final Logger logger = Logger.getLogger(NoPublicKeyHandler.class.getName());

	@Override
	public void handle(final ProtocolInput in, final Session session)
			throws IOException, ProtocolException, HandlerException {
		final int handlerId = in.readCompactUInt();
		final int version = in.readCompactUInt();

		final KeyGrabber.Entry entry = session.getKeyGrabber().get(handlerId);
		if (entry == null) {
			logger.fine(session.getStamp() + "Not found handlerId=" + handlerId);
			return;
		}
		entry.handle(version, null);
	}

}
