package org.kareha.hareka.server.user;

import java.nio.ByteBuffer;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.kareha.hareka.Constants;
import org.kareha.hareka.LocalEntityId;
import org.kareha.hareka.LocalSessionId;
import org.kareha.hareka.LocalUserId;
import org.kareha.hareka.annotation.GuardedBy;
import org.kareha.hareka.annotation.Private;
import org.kareha.hareka.annotation.ThreadSafe;
import org.kareha.hareka.server.entity.EntityId;
import org.kareha.hareka.server.external.SessionId;
import org.kareha.hareka.user.UserId;

@ThreadSafe
@XmlJavaTypeAdapter(IdCipher.Adapter.class)
public class IdCipher {

	private static final String ALGORITHM = "AES/ECB/NoPadding";
	private static final byte[] ZEROS = new byte[Constants.ID_CIPHER_KEY_LENGTH - Long.BYTES];

	@Private
	final byte[] key;
	// Cipher is not thread-safe
	@GuardedBy("encrypter")
	private final Cipher encrypter;
	// Cipher is not thread-safe
	@GuardedBy("decrypter")
	private final Cipher decrypter;

	private static byte[] generate(final int size) {
		final byte[] b = new byte[size];
		new SecureRandom().nextBytes(b);
		return b;
	}

	public IdCipher() {
		this(generate(Constants.ID_CIPHER_KEY_LENGTH));
	}

	public IdCipher(final byte[] key) {
		if (key == null) {
			throw new IllegalArgumentException("null");
		}
		if (key.length != Constants.ID_CIPHER_KEY_LENGTH) {
			throw new IllegalArgumentException("key.length must be " + Constants.ID_CIPHER_KEY_LENGTH);
		}
		this.key = key.clone();

		try {
			encrypter = Cipher.getInstance(ALGORITHM);
		} catch (final NoSuchAlgorithmException e) {
			throw new AssertionError();
		} catch (final NoSuchPaddingException e) {
			throw new AssertionError();
		}
		final SecretKeySpec encryptSpec = new SecretKeySpec(key, "AES");
		try {
			encrypter.init(Cipher.ENCRYPT_MODE, encryptSpec);
		} catch (final InvalidKeyException e) {
			throw new AssertionError();
		}

		try {
			decrypter = Cipher.getInstance(ALGORITHM);
		} catch (final NoSuchAlgorithmException e) {
			throw new AssertionError();
		} catch (final NoSuchPaddingException e) {
			throw new AssertionError();
		}
		final SecretKeySpec decryptSpec = new SecretKeySpec(key, "AES");
		try {
			decrypter.init(Cipher.DECRYPT_MODE, decryptSpec);
		} catch (final InvalidKeyException e) {
			throw new AssertionError();
		}
	}

	@XmlType(name = "idCipher")
	@XmlAccessorType(XmlAccessType.NONE)
	private static class Adapted {

		@XmlElement
		private byte[] key;

		@SuppressWarnings("unused")
		private Adapted() {
			// used by JAXB
		}

		@Private
		Adapted(final IdCipher v) {
			key = v.key.clone();
		}

		@Private
		IdCipher unmarshal() {
			return new IdCipher(key);
		}

	}

	@Private
	Adapted marshal() {
		return new Adapted(this);
	}

	static class Adapter extends XmlAdapter<Adapted, IdCipher> {

		@Override
		public Adapted marshal(final IdCipher v) throws Exception {
			if (v == null) {
				return null;
			}
			return v.marshal();
		}

		@Override
		public IdCipher unmarshal(final Adapted v) throws Exception {
			if (v == null) {
				return null;
			}
			return v.unmarshal();
		}

	}

	public byte[] getKey() {
		return key.clone();
	}

	private static byte[] toByteArray(final long value) {
		final ByteBuffer buffer = ByteBuffer.allocate(Long.BYTES);
		buffer.putLong(value);
		return buffer.array();
	}

	private byte[] encrypt(final long id) {
		synchronized (encrypter) {
			try {
				encrypter.update(ZEROS);
				return encrypter.doFinal(toByteArray(id));
			} catch (final IllegalBlockSizeException e) {
				throw new AssertionError();
			} catch (final BadPaddingException e) {
				throw new AssertionError();
			}
		}
	}

	private static long longValueOf(final byte[] v) {
		// TODO check zeros
		final ByteBuffer buffer = ByteBuffer.allocate(Long.BYTES);
		buffer.put(v, ZEROS.length, Long.BYTES);
		buffer.flip();
		return buffer.getLong();
	}

	private long decrypt(final byte[] encryptedId) {
		synchronized (decrypter) {
			try {
				return longValueOf(decrypter.doFinal(encryptedId));
			} catch (final IllegalBlockSizeException e) {
				throw new AssertionError();
			} catch (final BadPaddingException e) {
				throw new AssertionError();
			}
		}
	}

	public LocalEntityId encrypt(final EntityId id) {
		return LocalEntityId.valueOf(encrypt(id.value()));
	}

	public EntityId decrypt(final LocalEntityId localId) {
		return EntityId.valueOf(decrypt(localId.value()));
	}

	public LocalUserId encrypt(final UserId id) {
		return LocalUserId.valueOf(encrypt(id.value()));
	}

	public UserId decrypt(final LocalUserId localId) {
		return UserId.valueOf(decrypt(localId.value()));
	}

	public LocalSessionId encrypt(final SessionId id) {
		return LocalSessionId.valueOf(encrypt(id.value()));
	}

	public SessionId decrypt(final LocalSessionId localId) {
		return SessionId.valueOf(decrypt(localId.value()));
	}

}
