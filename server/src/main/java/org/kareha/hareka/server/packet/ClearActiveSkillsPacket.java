package org.kareha.hareka.server.packet;

import org.kareha.hareka.protocol.WorldClientPacketType;
import org.kareha.hareka.server.protocol.WorldClientPacket;
import org.kareha.hareka.protocol.PacketType;

public final class ClearActiveSkillsPacket extends WorldClientPacket {

	public ClearActiveSkillsPacket() {
		// do nothing
	}

	@Override
	protected PacketType getType() {
		return WorldClientPacketType.CLEAR_ACTIVE_SKILLS;
	}

}
