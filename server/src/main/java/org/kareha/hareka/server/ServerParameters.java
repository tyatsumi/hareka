package org.kareha.hareka.server;

import java.io.File;

import org.kareha.hareka.annotation.Immutable;
import org.kareha.hareka.ui.UiType;
import org.kareha.hareka.util.Parameters;

@Immutable
public class ServerParameters {

	private final Parameters parameters;

	private final UiType uiType;
	private final boolean noTls;
	private final int port;
	private final File dataDirectory;
	private final boolean wipe;
	private final File liveDirectory;
	private final File backupDirectory;

	public ServerParameters(final String[] args) {
		parameters = new Parameters(args);

		uiType = UiType.parse(parameters);

		noTls = parameters.unnamed().contains("--notls");

		final String portOption = parameters.named().get("port");
		if (portOption != null) {
			// may throw NumberFormatException
			port = Integer.parseInt(portOption);
		} else {
			port = ServerDefaults.PORT;
		}

		final String rootOption = parameters.named().get("datadir");
		if (rootOption != null) {
			dataDirectory = new File(rootOption);
		} else {
			dataDirectory = new File(ServerDefaults.DATA_DIRECTORY);
		}

		wipe = parameters.unnamed().contains("--wipe");

		liveDirectory = new File(dataDirectory, "live");
		backupDirectory = new File(dataDirectory, "backup");
	}

	public Parameters parameters() {
		return parameters;
	}

	public UiType uiType() {
		return uiType;
	}

	public boolean noTls() {
		return noTls;
	}

	public int port() {
		return port;
	}

	public File dataDirectory() {
		return dataDirectory;
	}

	public boolean wipe() {
		return wipe;
	}

	public File liveDirectory() {
		return liveDirectory;
	}

	public File backupDirectory() {
		return backupDirectory;
	}

}
