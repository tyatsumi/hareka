package org.kareha.hareka.server.handler;

import java.io.IOException;

import org.kareha.hareka.protocol.Handler;
import org.kareha.hareka.protocol.HandlerException;
import org.kareha.hareka.protocol.ProtocolException;
import org.kareha.hareka.protocol.ProtocolInput;
import org.kareha.hareka.server.external.Session;
import org.kareha.hareka.server.packet.SettingsPacket;
import org.kareha.hareka.server.user.User;
import org.kareha.hareka.user.Permission;
import org.kareha.hareka.user.RoleSet;

public final class RequestSettingsHandler implements Handler<Session> {

	@Override
	public void handle(final ProtocolInput in, final Session session)
			throws IOException, ProtocolException, HandlerException {
		final User user = session.getUser();
		if (user == null) {
			return;
		}
		final RoleSet roles = session.getContext().accessController().getRoleSet(user);
		if (!roles.isAbleTo(Permission.MANAGE_SETTINGS)) {
			return;
		}
		session.write(new SettingsPacket(session.getContext().settings()));
	}

}
