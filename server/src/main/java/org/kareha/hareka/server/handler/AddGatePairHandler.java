package org.kareha.hareka.server.handler;

import java.io.IOException;
import java.util.ResourceBundle;

import org.kareha.hareka.field.Placement;
import org.kareha.hareka.field.Tile;
import org.kareha.hareka.field.TileType;
import org.kareha.hareka.protocol.Handler;
import org.kareha.hareka.protocol.HandlerException;
import org.kareha.hareka.protocol.ProtocolException;
import org.kareha.hareka.protocol.ProtocolInput;
import org.kareha.hareka.server.entity.CharacterEntity;
import org.kareha.hareka.server.external.Session;
import org.kareha.hareka.server.field.FieldPosition;
import org.kareha.hareka.server.field.Gate;
import org.kareha.hareka.server.field.ServerField;
import org.kareha.hareka.server.field.ServerFieldObject;
import org.kareha.hareka.server.game.Player;
import org.kareha.hareka.server.user.User;
import org.kareha.hareka.user.Permission;
import org.kareha.hareka.user.RoleSet;

public final class AddGatePairHandler implements Handler<Session> {

	private enum BundleKey {
		ThisCharacterCannotEditTileFields, YouCannotEditGates, NoMarkHasBeenSet, YouCannotEditThisTile,
		ThisIsSpecialTile, YouCannotEditTheTileAtTheMark, TheTileAtTheMarkIsSpecial, AGatePairHasBeenPlaced,
	}

	private static void inform(final Session session, final String key) {
		final ResourceBundle bundle = session.getBundle(AddGatePairHandler.class.getName());
		session.writeInformPacket(bundle.getString(key));
	}

	@Override
	public void handle(final ProtocolInput in, final Session session)
			throws IOException, ProtocolException, HandlerException {
		final User user = session.getUser();
		if (user == null) {
			return;
		}
		final RoleSet roles = session.getContext().accessController().getRoleSet(user);
		if (!roles.isAbleTo(Permission.EDIT_GATES)) {
			inform(session, BundleKey.YouCannotEditGates.name());
			return;
		}
		final boolean force = roles.isAbleTo(Permission.FORCE_EDIT_TILE_FIELDS);

		final Player player = session.getPlayer();
		if (player == null) {
			return;
		}
		final FieldPosition mark = player.getMark();
		if (mark == null) {
			inform(session, BundleKey.NoMarkHasBeenSet.name());
			return;
		}
		final CharacterEntity entity = player.getEntity();
		if (!force && !entity.isAbleToEditTileFields()) {
			inform(session, BundleKey.ThisCharacterCannotEditTileFields.name());
			return;
		}
		final ServerFieldObject fo = entity.getFieldObject();
		if (fo == null) {
			return;
		}
		final ServerField field = fo.getField();
		final Placement placement = fo.getPlacement();
		if (!force && !field.isMutable(placement.getPosition())) {
			inform(session, BundleKey.YouCannotEditThisTile.name());
			return;
		}
		final Tile originalTile = field.getTile(placement.getPosition());
		if (originalTile.type().isSpecial()) {
			inform(session, BundleKey.ThisIsSpecialTile.name());
			return;
		}
		final ServerField targetField = session.getContext().fields().getField(mark.getFieldId());
		if (targetField == null) {
			return;
		}
		if (!force && !targetField.isMutable(mark.getPosition())) {
			inform(session, BundleKey.YouCannotEditTheTileAtTheMark.name());
			return;
		}
		final Tile targetTile = targetField.getTile(mark.getPosition());
		if (targetTile.type().isSpecial()) {
			inform(session, BundleKey.TheTileAtTheMarkIsSpecial.name());
			return;
		}

		field.setAndSyncTile(placement.getPosition(), Tile.valueOf(TileType.GATE, originalTile.elevation()), force);
		targetField.setAndSyncTile(mark.getPosition(), Tile.valueOf(TileType.GATE, targetTile.elevation()), force);

		final Gate gate = Gate.valueOf(FieldPosition.valueOf(field.getId(), placement.getPosition()),
				FieldPosition.valueOf(mark.getFieldId(), mark.getPosition()));
		field.addGate(gate);
		final Gate gate2 = Gate.valueOf(FieldPosition.valueOf(mark.getFieldId(), mark.getPosition()),
				FieldPosition.valueOf(field.getId(), placement.getPosition()));
		targetField.addGate(gate2);
		inform(session, BundleKey.AGatePairHasBeenPlaced.name());
	}

}
