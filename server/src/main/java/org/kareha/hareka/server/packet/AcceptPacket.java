package org.kareha.hareka.server.packet;

import org.kareha.hareka.protocol.WorldClientPacketType;
import org.kareha.hareka.server.protocol.WorldClientPacket;
import org.kareha.hareka.protocol.PacketType;

public final class AcceptPacket extends WorldClientPacket {

	public AcceptPacket(final int requestId, final String message) {
		out.writeCompactUInt(requestId);
		out.writeString(message);
	}

	@Override
	protected PacketType getType() {
		return WorldClientPacketType.ACCEPT;
	}

}
