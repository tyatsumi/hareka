package org.kareha.hareka.server.packet;

import java.util.Collection;

import org.kareha.hareka.field.TilePiece;
import org.kareha.hareka.field.Transformation;
import org.kareha.hareka.field.Vector;
import org.kareha.hareka.protocol.WorldClientPacketType;
import org.kareha.hareka.server.field.Boundary;
import org.kareha.hareka.server.protocol.WorldClientPacket;
import org.kareha.hareka.protocol.PacketType;

public final class TilesPacket extends WorldClientPacket {

	public TilesPacket(final Collection<TilePiece> tilePieces, final Transformation transformation,
			final Vector position, final Boundary boundary) {
		out.writeCompactUInt(tilePieces.size());
		for (final TilePiece tilePiece : tilePieces) {
			out.write(boundary.confine(tilePiece, position).transform(transformation));
		}
	}

	public TilesPacket(final TilePiece tilePiece, final Transformation transformation, final Vector position,
			final Boundary boundary) {
		out.writeCompactUInt(1);
		out.write(boundary.confine(tilePiece, position).transform(transformation));
	}

	@Override
	protected PacketType getType() {
		return WorldClientPacketType.TILES;
	}

}
