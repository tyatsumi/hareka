package org.kareha.hareka.server.stat;

import java.util.Collection;

import org.kareha.hareka.game.ActiveSkillType;
import org.kareha.hareka.game.Name;
import org.kareha.hareka.game.PassiveSkillType;
import org.kareha.hareka.math.IntRange;
import org.kareha.hareka.server.motion.MotionType;
import org.kareha.hareka.server.relationship.SpeciesRelationships;

public interface Species {

	String getId();

	Name getName();

	Collection<ActiveSkillType> getActiveSkills();

	boolean hasActiveSkill(ActiveSkillType v);

	Collection<PassiveSkillType> getPassiveSkills();

	boolean hasPassiveSkill(PassiveSkillType v);

	IntRange getExperienceRange(ExperienceType type);

	SpeciesRelationships getRelationships();

	MotionType getMotion();

	VisualType getVisualType();

	boolean isVisible(VisualType visualType);

}
