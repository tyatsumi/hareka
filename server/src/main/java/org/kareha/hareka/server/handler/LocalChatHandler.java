package org.kareha.hareka.server.handler;

import java.io.IOException;
import java.util.ResourceBundle;
import java.util.logging.Logger;

import org.kareha.hareka.game.PassiveSkillType;
import org.kareha.hareka.protocol.Handler;
import org.kareha.hareka.protocol.HandlerException;
import org.kareha.hareka.protocol.ProtocolException;
import org.kareha.hareka.protocol.ProtocolInput;
import org.kareha.hareka.server.entity.CharacterEntity;
import org.kareha.hareka.server.external.Session;
import org.kareha.hareka.server.game.Player;

public final class LocalChatHandler implements Handler<Session> {

	private static final Logger logger = Logger.getLogger(LocalChatHandler.class.getName());

	private enum BundleKey {
		LoginCharacterFirst, ThisCharacterCannotSpeak,
	}

	private static void inform(final Session session, final String key) {
		final ResourceBundle bundle = session.getBundle(LocalChatHandler.class.getName());
		session.writeInformPacket(bundle.getString(key));
	}

	@Override
	public void handle(final ProtocolInput in, final Session session)
			throws IOException, ProtocolException, HandlerException {
		// read arguments
		final String content = in.readString();

		// check state
		final Player player = session.getPlayer();
		if (player == null) {
			inform(session, BundleKey.LoginCharacterFirst.name());
			logger.fine(session.getStamp() + "No player");
			return;
		}
		final CharacterEntity entity = player.getEntity();
		if (!entity.getStat().hasPassiveSkill(PassiveSkillType.ABLE_TO_SPEAK)) {
			inform(session, BundleKey.ThisCharacterCannotSpeak.name());
			return;
		}
		entity.sendLocalChat(content);
	}

}
