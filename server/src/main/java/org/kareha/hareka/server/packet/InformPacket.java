package org.kareha.hareka.server.packet;

import org.kareha.hareka.protocol.WorldClientPacketType;
import org.kareha.hareka.server.protocol.WorldClientPacket;
import org.kareha.hareka.protocol.PacketType;

public final class InformPacket extends WorldClientPacket {

	public InformPacket(final String message) {
		out.writeString(message);
	}

	@Override
	protected PacketType getType() {
		return WorldClientPacketType.INFORM;
	}

}
