package org.kareha.hareka.server.net;

import java.io.IOException;

public interface Server {

	int port();

	boolean start() throws IOException, ServerException;

	boolean stop() throws IOException, InterruptedException;

}
