package org.kareha.hareka.server.packet;

import org.kareha.hareka.protocol.WorldClientPacketType;
import org.kareha.hareka.server.entity.ItemEntity;
import org.kareha.hareka.server.protocol.WorldClientPacket;
import org.kareha.hareka.server.user.IdCipher;
import org.kareha.hareka.protocol.PacketType;

public final class RemoveItemPacket extends WorldClientPacket {

	public RemoveItemPacket(final ItemEntity itemEntity, final IdCipher itemEntityIdCipher) {
		out.write(itemEntityIdCipher.encrypt(itemEntity.getId()));
	}

	@Override
	protected PacketType getType() {
		return WorldClientPacketType.REMOVE_ITEM;
	}

}
