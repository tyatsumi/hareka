package org.kareha.hareka.server.packet;

import java.math.BigInteger;

import org.kareha.hareka.protocol.WorldClientPacketType;
import org.kareha.hareka.server.protocol.WorldClientPacket;
import org.kareha.hareka.protocol.PacketType;

public final class RequestPowPacket extends WorldClientPacket {

	public RequestPowPacket(final int handlerId, final BigInteger target, final byte[] data, final String message) {
		out.writeCompactUInt(handlerId);
		out.writeBigInteger(target);
		out.writeByteArray(data);
		out.writeString(message);
	}

	@Override
	protected PacketType getType() {
		return WorldClientPacketType.REQUEST_POW;
	}

}
