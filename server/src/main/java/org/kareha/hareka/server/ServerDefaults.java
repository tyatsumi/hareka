package org.kareha.hareka.server;

import java.io.File;

public final class ServerDefaults {

	private ServerDefaults() {
		throw new AssertionError();
	}

	public static final int PORT = 2468;
	public static final String DATA_DIRECTORY = System.getProperty("user.dir") + File.separator + "harekaserv_data";

}
