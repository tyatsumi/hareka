package org.kareha.hareka.server.skill;

import org.kareha.hareka.field.TileType;
import org.kareha.hareka.game.ActiveSkillType;
import org.kareha.hareka.server.entity.CharacterEntity;
import org.kareha.hareka.server.entity.FieldEntity;
import org.kareha.hareka.server.entity.ItemEntity;
import org.kareha.hareka.wait.WaitType;

public class PickUpActiveSkill implements ActiveSkill {

	@Override
	public ActiveSkillType getType() {
		return ActiveSkillType.PICK_UP;
	}

	@Override
	public int getReach() {
		return 1;
	}

	@Override
	public WaitType getWaitType() {
		return WaitType.ATTACK;
	}

	@Override
	public int getWait(final CharacterEntity entity, final TileType tileType) {
		return entity.getStat().getAttackWait(tileType);
	}

	@Override
	public void use(final CharacterEntity entity, final FieldEntity target) {
		if (target == entity) {
			return;
		}
		if (!(target instanceof ItemEntity)) {
			return;
		}
		if (entity.isStuck()) {
			return;
		}
		final ItemEntity itemEntity = (ItemEntity) target;
		if (!itemEntity.unmount()) {
			return;
		}
		entity.getInventory().put(itemEntity);
	}

}
