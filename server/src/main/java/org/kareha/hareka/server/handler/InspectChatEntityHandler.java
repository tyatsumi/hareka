package org.kareha.hareka.server.handler;

import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.ResourceBundle;
import java.util.logging.Logger;

import org.kareha.hareka.LocalEntityId;
import org.kareha.hareka.protocol.Handler;
import org.kareha.hareka.protocol.HandlerException;
import org.kareha.hareka.protocol.ProtocolException;
import org.kareha.hareka.protocol.ProtocolInput;
import org.kareha.hareka.server.entity.CharacterEntity;
import org.kareha.hareka.server.entity.Entity;
import org.kareha.hareka.server.entity.EntityId;
import org.kareha.hareka.server.external.Session;
import org.kareha.hareka.server.packet.InspectionPacket;
import org.kareha.hareka.server.user.User;
import org.kareha.hareka.user.Permission;
import org.kareha.hareka.user.UserId;

public final class InspectChatEntityHandler implements Handler<Session> {

	private static final Logger logger = Logger.getLogger(InspectChatEntityHandler.class.getName());

	private enum BundleKey {
		LoginUserFirst, YouCannotUseThisFunction,
	}

	private static void inform(final Session session, final String key) {
		final ResourceBundle bundle = session.getBundle(InspectChatEntityHandler.class.getName());
		session.writeInformPacket(bundle.getString(key));
	}

	@Override
	public void handle(final ProtocolInput in, final Session session)
			throws IOException, ProtocolException, HandlerException {
		// read arguments
		final LocalEntityId localId = LocalEntityId.readFrom(in);

		// check state
		final User user = session.getUser();
		if (user == null) {
			inform(session, BundleKey.LoginUserFirst.name());
			logger.fine(session.getStamp() + "User not logged in");
			return;
		}
		if (!session.getContext().accessController().getRoleSet(user).isAbleTo(Permission.INSPECT_ENTITIES)) {
			inform(session, BundleKey.YouCannotUseThisFunction.name());
			logger.fine(session.getStamp() + "User cannot use inspect entities function");
			return;
		}

		final EntityId targetId = user.getChatEntityIdCipher().decrypt(localId);
		final Entity targetEntity = session.getContext().entities().get(targetId);
		if (targetEntity == null) {
			return;
		}

		final Collection<UserId> userIds;
		if (targetEntity instanceof CharacterEntity) {
			final CharacterEntity characterEntity = (CharacterEntity) targetEntity;
			userIds = characterEntity.getOwners();
		} else {
			userIds = Collections.emptyList();
		}
		session.write(new InspectionPacket(targetEntity, user.getChatEntityIdCipher(), user.getFieldEntityIdCipher(),
				userIds, session.getContext().workspace().getUserIdCipher()));
	}

}
