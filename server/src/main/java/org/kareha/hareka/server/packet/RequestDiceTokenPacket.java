package org.kareha.hareka.server.packet;

import org.kareha.hareka.protocol.WorldClientPacketType;
import org.kareha.hareka.server.game.DiceRoll;
import org.kareha.hareka.server.protocol.WorldClientPacket;
import org.kareha.hareka.protocol.PacketType;

public final class RequestDiceTokenPacket extends WorldClientPacket {

	public RequestDiceTokenPacket(final DiceRoll diceRoll) {
		out.writeCompactUInt(diceRoll.getId());
	}

	@Override
	protected PacketType getType() {
		return WorldClientPacketType.REQUEST_DICE_TOKEN;
	}

}
