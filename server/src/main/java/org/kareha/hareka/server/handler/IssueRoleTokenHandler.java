package org.kareha.hareka.server.handler;

import java.io.IOException;
import java.util.ResourceBundle;
import java.util.logging.Logger;

import javax.xml.bind.DatatypeConverter;

import org.kareha.hareka.protocol.Handler;
import org.kareha.hareka.protocol.HandlerException;
import org.kareha.hareka.protocol.ProtocolException;
import org.kareha.hareka.protocol.ProtocolInput;
import org.kareha.hareka.server.external.Session;
import org.kareha.hareka.server.packet.TextPacket;
import org.kareha.hareka.server.user.RoleToken;
import org.kareha.hareka.server.user.User;
import org.kareha.hareka.user.Permission;
import org.kareha.hareka.user.Role;
import org.kareha.hareka.user.RoleSet;

public final class IssueRoleTokenHandler implements Handler<Session> {

	private static final Logger logger = Logger.getLogger(IssueRoleTokenHandler.class.getName());

	private enum BundleKey {
		LoginUserFirst, YouCannotUseThisFunction, InvalidRoleId, InsufficientRank,
	}

	private static void inform(final Session session, final String key) {
		final ResourceBundle bundle = session.getBundle(IssueRoleTokenHandler.class.getName());
		session.writeInformPacket(bundle.getString(key));
	}

	@Override
	public void handle(final ProtocolInput in, final Session session)
			throws IOException, ProtocolException, HandlerException {
		// read arguments
		final String roleId = in.readString();

		// check state
		final User user = session.getUser();
		if (user == null) {
			inform(session, BundleKey.LoginUserFirst.name());
			logger.fine(session.getStamp() + "User not logged in");
			return;
		}
		final RoleSet roles = session.getContext().accessController().getRoleSet(user);
		final Role role = roles.getHighestRole(Permission.ISSUE_ROLE_TOKENS);
		if (role == null) {
			inform(session, BundleKey.YouCannotUseThisFunction.name());
			logger.fine(session.getStamp() + "User cannot use issue role token function");
			return;
		}
		final Role targetRole = session.getContext().roles().get(roleId);
		if (targetRole == null) {
			inform(session, BundleKey.InvalidRoleId.name());
			logger.fine(session.getStamp() + "Invalid role ID");
			return;
		}
		if (targetRole.getRank() >= role.getRank()) {
			inform(session, BundleKey.InsufficientRank.name());
			logger.fine(session.getStamp() + "Insufficient rank");
			return;
		}

		// process
		final RoleToken token = session.getContext().roleTokens().issue(user, roleId);
		final String hex = DatatypeConverter.printHexBinary(token.getValue()).toLowerCase();
		session.write(new TextPacket("Role token (" + token.getRoleId() + "): " + hex + "\n"));
	}

}
