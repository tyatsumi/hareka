package org.kareha.hareka.server.handler;

import java.io.IOException;
import java.util.ResourceBundle;
import java.util.logging.Logger;

import org.kareha.hareka.protocol.Handler;
import org.kareha.hareka.protocol.HandlerException;
import org.kareha.hareka.protocol.ProtocolException;
import org.kareha.hareka.protocol.ProtocolInput;
import org.kareha.hareka.server.Rebooter;
import org.kareha.hareka.server.external.Session;
import org.kareha.hareka.server.user.User;
import org.kareha.hareka.user.Permission;

public final class ShutdownHandler implements Handler<Session> {

	private static final Logger logger = Logger.getLogger(ShutdownHandler.class.getName());

	private enum BundleKey {
		LoginUserFirst, YouCannotUseThisFunction,
	}

	private static void inform(final Session session, final String key) {
		final ResourceBundle bundle = session.getBundle(ShutdownHandler.class.getName());
		session.writeInformPacket(bundle.getString(key));
	}

	@Override
	public void handle(final ProtocolInput in, final Session session)
			throws IOException, ProtocolException, HandlerException {
		// read arguments

		// check state
		final User user = session.getUser();
		if (user == null) {
			inform(session, BundleKey.LoginUserFirst.name());
			logger.fine(session.getStamp() + "User not logged in");
			return;
		}
		if (!session.getContext().accessController().getRoleSet(user).isAbleTo(Permission.SHUTDOWN)) {
			inform(session, BundleKey.YouCannotUseThisFunction.name());
			logger.fine(session.getStamp() + "User cannot use shutdown function");
			return;
		}

		// process
		Rebooter.INSTANCE.shutdown();
	}

}
