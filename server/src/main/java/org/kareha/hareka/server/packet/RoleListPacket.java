package org.kareha.hareka.server.packet;

import java.util.ArrayList;
import java.util.List;

import org.kareha.hareka.protocol.WorldClientPacketType;
import org.kareha.hareka.server.protocol.WorldClientPacket;
import org.kareha.hareka.server.user.AccessController;
import org.kareha.hareka.server.user.Roles;
import org.kareha.hareka.server.user.User;
import org.kareha.hareka.protocol.PacketType;
import org.kareha.hareka.user.Role;

public final class RoleListPacket extends WorldClientPacket {

	public RoleListPacket(final Roles roles, final User user, final AccessController accessController) {
		final int rank = accessController.getRoleSet(user).getHighestRank();
		final List<Role> list = new ArrayList<>();
		for (final Role role : roles.getRoles()) {
			if (role.getRank() <= rank) {
				list.add(role);
			}
		}
		out.writeCompactUInt(list.size());
		for (final Role role : list) {
			out.write(role);
		}
	}

	@Override
	protected PacketType getType() {
		return WorldClientPacketType.ROLE_LIST;
	}

}
