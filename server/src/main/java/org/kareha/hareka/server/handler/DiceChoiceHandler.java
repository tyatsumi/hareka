package org.kareha.hareka.server.handler;

import java.io.IOException;
import java.util.ResourceBundle;
import java.util.logging.Logger;

import org.kareha.hareka.protocol.Handler;
import org.kareha.hareka.protocol.HandlerException;
import org.kareha.hareka.protocol.ProtocolException;
import org.kareha.hareka.protocol.ProtocolInput;
import org.kareha.hareka.server.ServerConstants;
import org.kareha.hareka.server.external.Session;
import org.kareha.hareka.server.game.DiceRoll;
import org.kareha.hareka.server.packet.DiceResultPacket;

public final class DiceChoiceHandler implements Handler<Session> {

	private static final Logger logger = Logger.getLogger(DiceChoiceHandler.class.getName());

	private enum BundleKey {
		TheChoiceIsTooLong, NoSuchDiceRoll, IllegalChoice,
	}

	private static void inform(final Session session, final String key) {
		final ResourceBundle bundle = session.getBundle(DiceChoiceHandler.class.getName());
		session.writeInformPacket(bundle.getString(key));
	}

	@Override
	public void handle(final ProtocolInput in, final Session session)
			throws IOException, ProtocolException, HandlerException {
		final int id = in.readCompactUInt();
		final int length = in.readCompactUInt();
		if (length > ServerConstants.MAX_DICE_ROLL_CHOICE_LENGTH) {
			inform(session, BundleKey.TheChoiceIsTooLong.name());
			logger.fine(session.getStamp() + "Too long id=" + id + " length=" + length);
			return;
		}
		final int[] choice = new int[length];
		for (int i = 0; i < length; i++) {
			choice[i] = in.readCompactUInt();
		}

		final DiceRoll diceRoll = session.getDiceRollTable().getDiceRoll(id);
		if (diceRoll == null) {
			inform(session, BundleKey.NoSuchDiceRoll.name());
			logger.fine(session.getStamp() + "Not found id=" + id);
			return;
		}
		if (!diceRoll.setChoice(choice)) {
			inform(session, BundleKey.IllegalChoice.name());
			logger.fine(session.getStamp() + "Illegal choice id=" + id);
			return;
		}
		session.write(new DiceResultPacket(diceRoll));
	}

}
