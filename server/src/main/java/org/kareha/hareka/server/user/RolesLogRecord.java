package org.kareha.hareka.server.user;

import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.kareha.hareka.annotation.GuardedBy;
import org.kareha.hareka.logging.xml.XmlLogRecord;
import org.kareha.hareka.user.UserId;

@XmlRootElement(name = "record")
@XmlAccessorType(XmlAccessType.NONE)
public class RolesLogRecord implements XmlLogRecord {

	public enum Type {
		DEFINE, UNDEFINE,
	}

	@XmlAttribute
	@GuardedBy("this")
	private long id;
	@XmlAttribute
	@GuardedBy("this")
	private Date date;
	@XmlElement
	@GuardedBy("this")
	private Type type;
	@XmlElement
	@GuardedBy("this")
	private UserId user;
	@XmlElement
	@GuardedBy("this")
	private CustomRole role;
	@XmlElement
	@GuardedBy("this")
	private String roleId;

	@SuppressWarnings("unused")
	private RolesLogRecord() {
		// used by JAXB
	}

	public RolesLogRecord(final UserId user, final CustomRole role) {
		type = Type.DEFINE;
		this.user = user;
		this.role = role;
	}

	public RolesLogRecord(final UserId user, final String roleId) {
		type = Type.UNDEFINE;
		this.user = user;
		this.roleId = roleId;
	}

	@Override
	public synchronized long getId() {
		return id;
	}

	@Override
	public synchronized void setId(final long id) {
		this.id = id;
	}

	@Override
	public synchronized long getDate() {
		return date.getTime();
	}

	@Override
	public synchronized void setDate(final long date) {
		this.date = new Date(date);
	}

	public synchronized Type getType() {
		return type;
	}

	public synchronized CustomRole getRole() {
		return role;
	}

	public synchronized String getRoleId() {
		return roleId;
	}

}
