package org.kareha.hareka.server.handler;

import java.io.IOException;
import java.security.KeyPair;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.bind.JAXBException;

import org.kareha.hareka.Constants;
import org.kareha.hareka.protocol.Handler;
import org.kareha.hareka.protocol.HandlerException;
import org.kareha.hareka.protocol.ProtocolException;
import org.kareha.hareka.protocol.ProtocolInput;
import org.kareha.hareka.server.external.Session;
import org.kareha.hareka.server.packet.NoPublicKeyPacket;
import org.kareha.hareka.server.packet.PublicKeyPacket;

public final class RequestPublicKeyHandler implements Handler<Session> {

	private static final Logger logger = Logger.getLogger(RequestPublicKeyHandler.class.getName());

	@Override
	public void handle(final ProtocolInput in, final Session session)
			throws IOException, ProtocolException, HandlerException {
		final int handlerId = in.readCompactUInt();
		final int version = in.readCompactUInt();
		final byte[] nonce = in.readByteArray();

		final KeyPair keyPair;
		try {
			keyPair = session.getContext().keyStack().get(version, (char[]) null);
		} catch (final JAXBException e) {
			logger.log(Level.SEVERE, "", e);
			session.write(new NoPublicKeyPacket(handlerId, version));
			return;
		}
		if (keyPair == null) {
			session.write(new NoPublicKeyPacket(handlerId, version));
		} else {
			session.write(new PublicKeyPacket(handlerId, version, nonce, keyPair, Constants.SIGNATURE_ALGORITHM));
		}
	}

}
