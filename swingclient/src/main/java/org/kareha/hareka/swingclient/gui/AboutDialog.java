package org.kareha.hareka.swingclient.gui;

import java.awt.BorderLayout;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ResourceBundle;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import org.kareha.hareka.Constants;
import org.kareha.hareka.swingclient.Strap;

public class AboutDialog {

	private enum BundleKey {
		Title, ClientVersion, Ok,
	}

	public AboutDialog(final Strap strap) {
		final JDialog dialog = new JDialog(strap.getGui().getMainFrame());

		dialog.setModal(true);
		final ResourceBundle bundle = ResourceBundle.getBundle(AboutDialog.class.getName());
		dialog.setTitle(bundle.getString(BundleKey.Title.name()));

		final JLabel label = new JLabel(bundle.getString(BundleKey.ClientVersion.name()) + " " + Constants.VERSION);
		final JPanel northPanel = new JPanel();
		northPanel.add(label);

		final JPanel centerPanel = new JPanel();
		try {
			final BufferedImage image = strap.getLoader().getImageLoader().load("connect.png");
			final JButton imageButton = new JButton(new ImageIcon(image));
			imageButton.setBorderPainted(false);
			imageButton.setFocusPainted(false);
			imageButton.setContentAreaFilled(false);
			imageButton.addActionListener(e -> {
				dialog.dispose();
			});
			centerPanel.add(imageButton);
		} catch (final IOException e) {
			JOptionPane.showMessageDialog(dialog, e.getMessage());
		}

		final JButton button = new JButton(bundle.getString(BundleKey.Ok.name()));
		button.addActionListener(e -> {
			dialog.dispose();
		});
		final JPanel southPanel = new JPanel();
		southPanel.add(button);

		dialog.add(northPanel, BorderLayout.NORTH);
		dialog.add(centerPanel, BorderLayout.CENTER);
		dialog.add(southPanel, BorderLayout.SOUTH);
		dialog.pack();

		dialog.setLocationRelativeTo(strap.getGui().getMainFrame());
		dialog.setVisible(true);
	}

}
