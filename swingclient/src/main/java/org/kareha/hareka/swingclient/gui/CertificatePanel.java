package org.kareha.hareka.swingclient.gui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.security.cert.Certificate;
import java.util.ResourceBundle;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

@SuppressWarnings("serial")
public class CertificatePanel extends JComponent {

	private enum BundleKey {
		AcceptCertificate, ShowCertificate,
	}

	public CertificatePanel(final Component parentComponent, final Certificate cert) {
		final ResourceBundle bundle = ResourceBundle.getBundle(CertificatePanel.class.getName());
		final JLabel label = new JLabel(bundle.getString(BundleKey.AcceptCertificate.name()));
		final JButton button = new JButton(bundle.getString(BundleKey.ShowCertificate.name()));
		button.addActionListener(e -> {
			JOptionPane.showMessageDialog(parentComponent, new CertificateView(cert));
		});
		setLayout(new BorderLayout());
		add(label, BorderLayout.CENTER);
		add(button, BorderLayout.SOUTH);
	}

	private static class CertificateView extends JComponent {

		CertificateView(final Certificate cert) {
			final JTextArea textArea = new JTextArea(cert.toString());
			textArea.setEditable(false);
			final JScrollPane scrollPane = new JScrollPane(textArea);
			scrollPane.setPreferredSize(new Dimension(400, 400));
			setLayout(new BorderLayout());
			add(scrollPane, BorderLayout.CENTER);
		}

	}

}
