package org.kareha.hareka.swingclient;

import java.io.File;
import java.util.List;

import org.kareha.hareka.client.ClientDefaults;
import org.kareha.hareka.util.Parameters;

public class SwingClientParameters {

	private final File dataDirectory;
	private final boolean wipe;
	private final boolean noTls;
	private final List<String> raw;

	public SwingClientParameters(final String[] args) {
		final Parameters parameters = new Parameters(args);

		final String datadirOption = parameters.named().get("datadir");
		if (datadirOption != null) {
			dataDirectory = new File(datadirOption);
		} else {
			dataDirectory = ClientDefaults.DATA_DIRECTORY;
		}

		wipe = parameters.unnamed().contains("--wipe");

		noTls = parameters.unnamed().contains("--notls");

		raw = parameters.raw();
	}

	public File dataDirectory() {
		return dataDirectory;
	}

	public boolean wipe() {
		return wipe;
	}

	public boolean noTls() {
		return noTls;
	}

	public List<String> raw() {
		return raw;
	}

}
