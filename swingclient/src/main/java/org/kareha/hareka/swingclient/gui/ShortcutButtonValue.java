package org.kareha.hareka.swingclient.gui;

import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;

public abstract class ShortcutButtonValue implements Transferable {

	public static final DataFlavor shortcutFlavor = new DataFlavor(ShortcutButtonValue.class, "Shortcut");

	protected final boolean shortcut;

	public abstract String getIconName();

	public abstract String getName();

	public ShortcutButtonValue(final boolean shortcut) {
		this.shortcut = shortcut;
	}

	public boolean isShortcut() {
		return shortcut;
	}

}
