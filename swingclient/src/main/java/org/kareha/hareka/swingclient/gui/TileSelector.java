package org.kareha.hareka.swingclient.gui;

import java.awt.Component;
import java.awt.Graphics;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsEnvironment;
import java.awt.Image;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListCellRenderer;

import org.kareha.hareka.field.Tile;
import org.kareha.hareka.field.TileType;
import org.kareha.hareka.field.Vector;
import org.kareha.hareka.graphics.sprite.Screen;
import org.kareha.hareka.graphics.tile.TileSprite;
import org.kareha.hareka.graphics.tile.ViewDirection;
import org.kareha.hareka.graphics.tile.ViewTile;
import org.kareha.hareka.swingclient.Strap;

@SuppressWarnings("serial")
public final class TileSelector extends JPanel {

	private static final int ICON_WIDTH = 48;
	private static final int ICON_HEIGHT = 48;

	private final JComboBox<TileItem> typeSelector;

	private static final class TileItem {

		private static final TileItem[] tileItems;

		static {
			final TileType[] tiles = TileType.values();
			tileItems = new TileItem[tiles.length];
			final ResourceBundle bundle = ResourceBundle.getBundle(TileType.class.getName());
			for (final TileType tile : tiles) {
				final String name = bundle.getString(tile.name());
				tileItems[tile.ordinal()] = new TileItem(tile, name);
			}
		}

		static TileItem[] values() {
			return tileItems;
		}

		private final TileType tile;
		private final String name;

		private TileItem(final TileType tile, final String name) {
			this.tile = tile;
			this.name = name;
		}

		TileType getTile() {
			return tile;
		}

		String getName() {
			return name;
		}

	}

	public TileSelector(final Strap strap) {
		final List<TileItem> list = new ArrayList<>();
		for (final TileItem i : TileItem.values()) {
			if (i.getTile().isSpecial()) {
				continue;
			}
			list.add(i);
		}
		typeSelector = new JComboBox<>(list.toArray(new TileItem[0]));
		typeSelector.setRenderer(new TileRenderer(strap));

		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		add(typeSelector);
	}

	public TileType getTile() {
		return ((TileItem) typeSelector.getSelectedItem()).getTile();
	}

	public void setTile(final TileType v) {
		for (int i = 0; i < typeSelector.getItemCount(); i++) {
			final TileItem tte = typeSelector.getItemAt(i);
			if (tte.getTile().equals(v)) {
				typeSelector.setSelectedIndex(i);
				return;
			}
		}
	}

	private static class TileRenderer extends JLabel implements ListCellRenderer<TileItem> {

		private final Strap strap;

		TileRenderer(final Strap strap) {
			this.strap = strap;
		}

		@Override
		public Component getListCellRendererComponent(JList<? extends TileItem> list, TileItem value, int index,
				boolean isSelected, boolean cellHasFocus) {
			final GraphicsConfiguration gc = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice()
					.getDefaultConfiguration();
			final Image image = gc.createCompatibleImage(ICON_WIDTH, ICON_HEIGHT);
			final Graphics g = image.getGraphics();
			final Screen screen = new Screen();
			final ViewTile tile = ViewTile.valueOf(Tile.valueOf(value.getTile(), 0));
			final TileSprite tileSprite = strap.getLoader().tileLoader().createTile(tile, Vector.ZERO, 0,
					TileSprite.Part.TOP, ViewDirection.NULL);
			tileSprite.addTo(screen);
			screen.setViewport(-ICON_WIDTH / 2, -ICON_HEIGHT / 2);
			screen.paint(g);
			final ImageIcon icon = new ImageIcon(image);
			setIcon(icon);
			setText(value.getName());
			return this;
		}

	}

}
