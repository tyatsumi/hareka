package org.kareha.hareka.swingclient.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.util.ResourceBundle;

import javax.swing.JInternalFrame;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.WindowConstants;

import org.kareha.hareka.swingclient.Strap;
import org.kareha.hareka.swingclient.SwingClientConstants;

@SuppressWarnings("serial")
public class InventoryFrame extends JInternalFrame {

	private enum BundleKey {
		Title,
	}

	private final InventoryPanel panel;
	private final Bar bar;

	public InventoryFrame(final Strap strap) {
		// non-resizable, closable, non-maximizable, non-iconifiable
		super("", false, true, false, false);
		setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
		final ResourceBundle bundle = ResourceBundle.getBundle(InventoryFrame.class.getName());
		setTitle(bundle.getString(BundleKey.Title.name()));

		panel = new InventoryPanel(strap);
		final JScrollPane scrollPane = new JScrollPane(panel, ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS,
				ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.getViewport().setPreferredSize(panel.getViewportSize());
		scrollPane.getVerticalScrollBar().setUnitIncrement(SwingClientConstants.ICON_HEIGHT);

		bar = new Bar(Color.WHITE, Color.BLUE, Color.RED);
		bar.setPreferredSize(new Dimension(128, 16));

		add(scrollPane, BorderLayout.CENTER);
		add(bar, BorderLayout.SOUTH);
		setBorder(null);
		pack();
	}

	public InventoryPanel getPanel() {
		return panel;
	}

	public void setWeightBar(final int v) {
		bar.setValue(Byte.MAX_VALUE, v);
	}

}