package org.kareha.hareka.swingclient;

import java.awt.BorderLayout;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.security.KeyPair;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.xml.bind.JAXBException;

import org.kareha.hareka.annotation.NotPure;
import org.kareha.hareka.key.KeyStack;
import org.kareha.hareka.swingclient.gui.Gui;
import org.kareha.hareka.ui.swing.PasswordReader;
import org.kareha.hareka.util.JarRunner;

public final class SwingClientUtil {

	private enum BundleKey {
		PasswordReaderTitle, PasswordReaderMessage,

		Paste,
	}

	private SwingClientUtil() {
		throw new AssertionError();
	}

	public static KeyPair getKeyPair(final Strap strap, final int version) throws JAXBException {
		final Gui gui = strap.getGui();
		final KeyStack stack = strap.getContext().getKeyStack();
		if (version > 0) {
			final ResourceBundle bundle = ResourceBundle.getBundle(SwingClientUtil.class.getName());
			final String title = bundle.getString(BundleKey.PasswordReaderTitle.name());
			final String message = MessageFormat.format(bundle.getString(BundleKey.PasswordReaderMessage.name()),
					version);
			return stack.get(version, new PasswordReader(gui.getMainFrame(), title, message));
		}
		return stack.get(version, new PasswordReader(gui.getMainFrame()));
	}

	@NotPure
	public static void runUpdater(final File dataDirectory, final String url, final List<String> clientArgs)
			throws IOException {
		final List<String> argList = new ArrayList<>();

		argList.add("--update");

		if (dataDirectory != null) {
			argList.add("--datadir=" + dataDirectory.getAbsolutePath());
		}

		final String path;
		try {
			path = Main.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath();
		} catch (final URISyntaxException e) {
			throw new RuntimeException(e);
		}
		if (new File(path).isFile()) {
			argList.add("--path=" + path);
		}

		if (url != null) {
			argList.add("--url=" + url);
		}

		if (clientArgs != null) {
			argList.add("--");
			argList.addAll(clientArgs);
		}

		JarRunner.runJar(SwingClientConstants.UPDATER_FILE, argList);
	}

	@SuppressWarnings("serial")
	private static class InputWithPasteButton extends JComponent {

		final JTextField field;

		InputWithPasteButton(final JFrame frame, final String message) {
			setLayout(new BorderLayout());
			add(BorderLayout.NORTH, new JLabel(message));
			field = new JTextField(32);
			add(BorderLayout.CENTER, field);
			final ResourceBundle bundle = ResourceBundle.getBundle(SwingClientUtil.class.getName());
			final String paste = bundle.getString(BundleKey.Paste.name());
			final JButton button = new JButton(paste);
			button.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(final ActionEvent e) {
					final Clipboard clipboard = frame.getToolkit().getSystemClipboard();
					final Transferable data = clipboard.getContents(clipboard);
					if (data.isDataFlavorSupported(DataFlavor.stringFlavor)) {
						String input;
						try {
							input = (String) data.getTransferData(DataFlavor.stringFlavor);
						} catch (final UnsupportedFlavorException | IOException ex) {
							throw new RuntimeException(ex);
						}
						field.setText(input);
					}

				}
			});
			add(BorderLayout.EAST, button);
		}

		String getText() {
			return field.getText();
		}

	}

	public static String showInputWithPasteButtonDialog(final JFrame frame, final String message) {
		final InputWithPasteButton input = new InputWithPasteButton(frame, message);
		final int result = JOptionPane.showConfirmDialog(frame, input);
		if (result != JOptionPane.OK_OPTION) {
			return null;
		}
		return input.getText();
	}

}
