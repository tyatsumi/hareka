package org.kareha.hareka.swingclient.gui;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridLayout;

import javax.swing.JComponent;

import org.kareha.hareka.swingclient.Strap;
import org.kareha.hareka.swingclient.SwingClientConstants;

@SuppressWarnings("serial")
public class ShortcutsPanel extends JComponent {

	private static final int COLUMNS = 5;
	private static final int ROWS = 2;

	private final Strap strap;
	private final GridLayout layout = new GridLayout(0, COLUMNS);
	private int shortcutsSize;
	private int cursor;
	private ShortcutButton dragButton;
	private ShortcutButton swapButton;

	public ShortcutsPanel(final Strap strap) {
		this.strap = strap;
		setLayout(layout);
		helperClear();
		getButton(cursor).setSelected(true);
	}

	int getShortcutsSize() {
		return shortcutsSize;
	}

	void setShortcutsSize(final int v) {
		shortcutsSize = v;
		for (final Component c : getComponents()) {
			c.repaint();
		}
	}

	private int getShortcutsRows() {
		return Math.max((shortcutsSize + COLUMNS - 1) / COLUMNS, ROWS);
	}

	private void extend(final int maxIndex) {
		final int requiredRows = maxIndex / COLUMNS + 1;
		final int currentRows = layout.getRows();
		if (requiredRows > currentRows) {
			layout.setRows(requiredRows);
			for (int j = currentRows; j < requiredRows; j++) {
				for (int i = 0; i < COLUMNS; i++) {
					final int index = COLUMNS * j + i;
					final ShortcutButton button = new ShortcutButton(strap, this, index);
					add(button);
				}
			}
			validate();
		}
	}

	private void helperClear() {
		removeAll();
		layout.setRows(0);
		extend(COLUMNS * getShortcutsRows() - 1);
	}

	public void clear() {
		helperClear();
	}

	public ShortcutButton getButton(final int index) {
		extend(index);
		return (ShortcutButton) getComponent(index);
	}

	public ShortcutButton getButton(final ShortcutButtonValue v) {
		for (final Component c : getComponents()) {
			final ShortcutButton b = (ShortcutButton) c;
			if (b.getValue() == v) {
				return b;
			}
		}
		return null;
	}

	public void setValue(final int index, final ShortcutButtonValue value) {
		final ShortcutButton button = getButton(index);
		button.setValue(value);
	}

	@SuppressWarnings("unused")
	private void repaintButtons() {
		for (int j = 0, r = layout.getRows(); j < r; j++) {
			for (int i = 0; i < COLUMNS; i++) {
				final int index = COLUMNS * j + i;
				final ShortcutButton button = (ShortcutButton) getComponent(index);
				button.repaint();
			}
		}
	}

	void setDragButton(final ShortcutButton v) {
		dragButton = v;
	}

	void setSwapButton(final ShortcutButton v) {
		swapButton = v;
	}

	void swap() {
		if (swapButton == null) {
			return;
		}
		strap.getShortcuts().swapShortcuts(dragButton.getIndex(), swapButton.getIndex());
		swapButton = null;
	}

	public void swapShortcuts(final int indexA, final int indexB) {
		final ShortcutButton buttonA = getButton(indexA);
		final ShortcutButton buttonB = getButton(indexB);
		final ShortcutButtonValue value = buttonA.getValue();
		buttonA.setValue(buttonB.getValue());
		buttonB.setValue(value);
	}

	public Dimension getViewportSize() {
		return new Dimension(SwingClientConstants.ICON_WIDTH * COLUMNS, SwingClientConstants.ICON_HEIGHT * ROWS);
	}

	public void repaintShortcutButton(final ShortcutButtonValue v) {
		for (final Component c : getComponents()) {
			final ShortcutButton b = (ShortcutButton) c;
			if (b.getValue() == v) {
				b.repaint();
			}
		}
	}

	public void moveCursor(final int amount) {
		final ShortcutButton b1 = getButton(cursor);
		b1.setSelected(false);
		b1.repaint();

		int c = (cursor + amount) % shortcutsSize;
		if (c < 0) {
			c += shortcutsSize;
		}
		cursor = c;

		final ShortcutButton b2 = getButton(cursor);
		b2.setSelected(true);
		b2.repaint();
	}

	public void use() {
		getButton(cursor).useShortcut();
	}

	public boolean checkKey(final int keyCode) {
		boolean found = false;
		for (final Component c : getComponents()) {
			final ShortcutButton b = (ShortcutButton) c;
			found |= b.checkKey(keyCode);
		}
		return found;
	}

}
