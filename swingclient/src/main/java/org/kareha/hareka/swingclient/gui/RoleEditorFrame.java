package org.kareha.hareka.swingclient.gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Set;

import javax.swing.BoxLayout;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.WindowConstants;

import org.kareha.hareka.client.user.CustomRole;
import org.kareha.hareka.swingclient.Strap;
import org.kareha.hareka.user.Permission;
import org.kareha.hareka.user.Role;

@SuppressWarnings("serial")
public class RoleEditorFrame extends JInternalFrame {

	private static final int FRAME_WIDTH = 512;
	private static final int FRAME_HEIGHT = 384;

	private enum BundleKey {

		Title, NotConnected,

		New, Template, Load, RoleId, Rank, Allowed, Denied, Deny, Allow,

		Apply, Delete, ConfirmDeleteRole,

	}

	private final Strap strap;
	private final JComboBox<Role> templateComboBox;
	private final JTextField roleIdField;
	private final JTextField rankField;
	private final DefaultListModel<Permission> includedListModel;
	private final DefaultListModel<Permission> excludedListModel;

	public RoleEditorFrame(final Strap strap) {
		super("", true, true, true, true);
		this.strap = strap;
		setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
		final ResourceBundle bundle = ResourceBundle.getBundle(RoleEditorFrame.class.getName());
		setTitle(bundle.getString(BundleKey.Title.name()));

		final JPanel northPanel = new JPanel();
		northPanel.setLayout(new BoxLayout(northPanel, BoxLayout.Y_AXIS));
		final JPanel newButtonPanel = new JPanel();
		final JButton newButton = new JButton(bundle.getString(BundleKey.New.name()));
		newButtonPanel.add(newButton);
		northPanel.add(newButtonPanel);
		final JPanel templatePanel = new JPanel();
		templatePanel.add(new JLabel(bundle.getString(BundleKey.Template.name())));
		templateComboBox = new JComboBox<>();
		templatePanel.add(templateComboBox);
		final JButton loadButton = new JButton(bundle.getString(BundleKey.Load.name()));
		loadButton.addActionListener(e -> applyTemplate());
		templatePanel.add(loadButton);
		northPanel.add(templatePanel);

		final JPanel centerNorthPanel = new JPanel();
		centerNorthPanel.add(new JLabel(bundle.getString(BundleKey.RoleId.name())));
		roleIdField = new JTextField(12);
		centerNorthPanel.add(roleIdField);
		centerNorthPanel.add(new JLabel(bundle.getString(BundleKey.Rank.name())));
		rankField = new JTextField(12);
		centerNorthPanel.add(rankField);
		northPanel.add(centerNorthPanel, BorderLayout.NORTH);

		includedListModel = new DefaultListModel<>();
		final JList<Permission> includedList = new JList<>(includedListModel);
		final JScrollPane includedListScrollPane = new JScrollPane(includedList);
		final JPanel includedListPanel = new JPanel();
		includedListPanel.setLayout(new BoxLayout(includedListPanel, BoxLayout.Y_AXIS));
		includedListPanel.add(new JLabel(bundle.getString(BundleKey.Allowed.name())));
		includedListPanel.add(includedListScrollPane);
		final JButton denyButton = new JButton(bundle.getString(BundleKey.Deny.name()));
		includedListPanel.add(denyButton);

		excludedListModel = new DefaultListModel<>();
		final JList<Permission> excludedList = new JList<>(excludedListModel);
		final JScrollPane excludedListScrollPane = new JScrollPane(excludedList);
		final JPanel excludedListPanel = new JPanel();
		excludedListPanel.setLayout(new BoxLayout(excludedListPanel, BoxLayout.Y_AXIS));
		excludedListPanel.add(new JLabel(bundle.getString(BundleKey.Denied.name())));
		excludedListPanel.add(excludedListScrollPane);
		final JButton allowButton = new JButton(bundle.getString(BundleKey.Allow.name()));
		excludedListPanel.add(allowButton);

		allowButton.addActionListener(e -> {
			final Permission permission = excludedList.getSelectedValue();
			if (permission == null) {
				return;
			}
			includedListModel.addElement(permission);
			excludedListModel.removeElement(permission);
		});
		denyButton.addActionListener(e -> {
			final Permission permission = includedList.getSelectedValue();
			if (permission == null) {
				return;
			}
			excludedListModel.addElement(permission);
			includedListModel.removeElement(permission);
		});

		final JPanel southPanel = new JPanel();
		final JButton applyButton = new JButton(bundle.getString(BundleKey.Apply.name()));
		applyButton.addActionListener(e -> {
			final String roleId = roleIdField.getText();
			if (roleId == null || roleId.isEmpty()) {
				return;
			}
			final int rank;
			try {
				rank = Integer.parseInt(rankField.getText());
			} catch (final NumberFormatException ex) {
				JOptionPane.showInternalMessageDialog(this, ex.getMessage());
				return;
			}
			final int size = includedListModel.getSize();
			final Set<Permission> permissions = EnumSet.noneOf(Permission.class);
			for (int i = 0; i < size; i++) {
				permissions.add(includedListModel.getElementAt(i));
			}
			final Role role = new CustomRole(roleId, rank, permissions);
			if (!strap.isConnected()) {
				JOptionPane.showInternalMessageDialog(this, bundle.getString(BundleKey.NotConnected.name()));
				return;
			}
			strap.adminControl().createRole(role);
		});
		southPanel.add(applyButton);
		final JButton deleteButton = new JButton(bundle.getString(BundleKey.Delete.name()));
		deleteButton.addActionListener(e -> {
			final String roleId = roleIdField.getText();
			if (roleId == null || roleId.isEmpty()) {
				return;
			}
			final int result = JOptionPane.showInternalConfirmDialog(this,
					bundle.getString(BundleKey.ConfirmDeleteRole.name()));
			if (result != JOptionPane.YES_OPTION) {
				return;
			}
			if (!strap.isConnected()) {
				JOptionPane.showInternalMessageDialog(this, bundle.getString(BundleKey.NotConnected.name()));
				return;
			}
			strap.adminControl().deleteRole(roleId);
		});
		southPanel.add(deleteButton);

		newButton.addActionListener(e -> {
			roleIdField.setText("NewRank");
			rankField.setText(Integer.toString(1));
			includedListModel.clear();
			excludedListModel.clear();
			for (final Permission permission : Permission.values()) {
				excludedListModel.addElement(permission);
			}
		});

		add(northPanel, BorderLayout.NORTH);
		add(includedListPanel, BorderLayout.WEST);
		add(excludedListPanel, BorderLayout.EAST);
		add(southPanel, BorderLayout.SOUTH);
		setPreferredSize(new Dimension(FRAME_WIDTH, FRAME_HEIGHT));
		pack();
	}

	public void updateRoles() {
		if (!strap.isConnected()) {
			return;
		}
		final List<Role> roles = new ArrayList<>(strap.adminControl().getRoleList());
		roles.sort((a, b) -> {
			if (a.getRank() > b.getRank()) {
				return -1;
			} else if (a.getRank() < b.getRank()) {
				return 1;
			} else {
				return a.getId().compareTo(b.getId());
			}
		});

		final int selectedIndex = templateComboBox.getSelectedIndex();
		final Role selectedRole;
		if (selectedIndex == -1) {
			selectedRole = null;
		} else {
			selectedRole = templateComboBox.getItemAt(selectedIndex);
		}
		templateComboBox.setModel(new DefaultComboBoxModel<>(roles.toArray(new Role[0])));
		if (selectedRole != null) {
			for (int i = 0; i < templateComboBox.getItemCount(); i++) {
				final Role r = templateComboBox.getItemAt(i);
				if (r.getId().equals(selectedRole.getId())) {
					templateComboBox.setSelectedIndex(i);
					break;
				}
			}
		}
		applyTemplate();
	}

	private void applyTemplate() {
		final int index = templateComboBox.getSelectedIndex();
		if (index == -1) {
			return;
		}
		final Role role = templateComboBox.getItemAt(index);
		roleIdField.setText(role.getId());
		rankField.setText(Integer.toString(role.getRank()));

		includedListModel.clear();
		final List<Permission> permissions = new ArrayList<>(role.getPermissions());
		permissions.sort((a, b) -> {
			if (a.ordinal() < b.ordinal()) {
				return -1;
			} else if (a.ordinal() > b.ordinal()) {
				return 1;
			} else {
				return 0;
			}
		});
		for (final Permission permission : permissions) {
			includedListModel.addElement(permission);
		}
		excludedListModel.clear();
		for (final Permission permission : Permission.values()) {
			if (!permissions.contains(permission)) {
				excludedListModel.addElement(permission);
			}
		}
	}

}
