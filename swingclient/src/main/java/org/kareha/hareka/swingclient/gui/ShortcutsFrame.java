package org.kareha.hareka.swingclient.gui;

import java.awt.BorderLayout;
import java.util.ResourceBundle;

import javax.swing.JInternalFrame;
import javax.swing.WindowConstants;

import org.kareha.hareka.swingclient.Strap;

@SuppressWarnings("serial")
public class ShortcutsFrame extends JInternalFrame {

	private static final int SHORTCUTS_SIZE = 10;

	private enum BundleKey {
		Title,
	}

	private final ShortcutsPanel panel;

	public ShortcutsFrame(final Strap strap) {
		// non-resizable, non-closable, non-maximizable, non-iconifiable
		setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
		final ResourceBundle bundle = ResourceBundle.getBundle(ShortcutsFrame.class.getName());
		setTitle(bundle.getString(BundleKey.Title.name()));

		panel = new ShortcutsPanel(strap);
		panel.setShortcutsSize(SHORTCUTS_SIZE);

		add(panel, BorderLayout.CENTER);
		setBorder(null);
		pack();
	}

	public ShortcutsPanel getPanel() {
		return panel;
	}

}
