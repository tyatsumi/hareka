package org.kareha.hareka.swingclient.gui;

import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.util.ResourceBundle;

import org.kareha.hareka.client.mirror.ActiveSkillMirror;
import org.kareha.hareka.game.ActiveSkillType;

public class SkillButtonValue extends ShortcutButtonValue {

	public static final DataFlavor skillFlavor = new DataFlavor(SkillButtonValue.class, "skill");
	private static final ResourceBundle skillTypeBundle = ResourceBundle.getBundle(ActiveSkillType.class.getName());

	protected final ActiveSkillMirror.Entry entry;

	public SkillButtonValue(final ActiveSkillMirror.Entry entry) {
		this(false, entry);
	}

	public SkillButtonValue(final boolean shortcut, final ActiveSkillMirror.Entry entry) {
		super(shortcut);
		this.entry = entry;
	}

	public ActiveSkillMirror.Entry getEntry() {
		return entry;
	}

	@Override
	public String getIconName() {
		return entry.getType().name();
	}

	@Override
	public String getName() {
		return skillTypeBundle.getString(entry.getType().name());
	}

	@Override
	public Object getTransferData(final DataFlavor flavor) throws UnsupportedFlavorException {
		if (flavor.equals(shortcutFlavor)) {
			return this;
		} else if (flavor.equals(skillFlavor)) {
			return this;
		} else {
			throw new UnsupportedFlavorException(flavor);
		}
	}

	@Override
	public DataFlavor[] getTransferDataFlavors() {
		final DataFlavor[] array = { shortcutFlavor, skillFlavor, };
		return array;
	}

	@Override
	public boolean isDataFlavorSupported(final DataFlavor flavor) {
		if (flavor.equals(shortcutFlavor)) {
			return true;
		} else if (flavor.equals(skillFlavor)) {
			return true;
		} else {
			return false;
		}
	}

}