package org.kareha.hareka.swingclient.gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ResourceBundle;

import javax.swing.JComponent;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.ToolTipManager;
import javax.swing.TransferHandler;

import org.kareha.hareka.annotation.Private;
import org.kareha.hareka.swingclient.Strap;
import org.kareha.hareka.swingclient.SwingClientConstants;
import org.kareha.hareka.ui.swing.SwingUtil;

@SuppressWarnings("serial")
public class ItemButton extends JComponent {

	private static final String BACKGROUND_IMAGE_NAME = "item.png";
	private static final String CURSOR_IMAGE_NAME = "icon_cursor.png";

	private enum BundleKey {
		HowMany, Use, Equip, Drop, Delete, ConfirmDelete, SetShortcut, Compact,
	}

	private final Strap strap;
	private final InventoryPanel panel;
	private final int index;
	private ItemButtonValue value;
	private final BufferedImage backgroundImage;
	private final BufferedImage cursorImage;

	public ItemButton(final Strap strap, final InventoryPanel panel, final int index) {
		this.strap = strap;
		this.panel = panel;
		this.index = index;

		addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(final MouseEvent e) {
				if (e.getButton() == MouseEvent.BUTTON1) {
					panel.setSelectedButton(ItemButton.this);
					useItem();
				}
			}

			@Override
			public void mousePressed(final MouseEvent e) {
				if (e.isPopupTrigger()) {
					createMenu().show(ItemButton.this, e.getX(), e.getY());
				}
			}

			@Override
			public void mouseReleased(final MouseEvent e) {
				if (e.isPopupTrigger()) {
					createMenu().show(ItemButton.this, e.getX(), e.getY());
				}
			}
		});
		addMouseMotionListener(new MouseMotionAdapter() {
			@Override
			public void mouseDragged(final MouseEvent e) {
				panel.setDragButton(ItemButton.this);
				final JComponent c = (JComponent) e.getSource();
				final TransferHandler th = c.getTransferHandler();
				th.exportAsDrag(c, e, TransferHandler.MOVE);
			}
		});

		setTransferHandler(new ItemButtonTransferHandler());

		setPreferredSize(new Dimension(SwingClientConstants.ICON_WIDTH, SwingClientConstants.ICON_HEIGHT));

		ToolTipManager.sharedInstance().registerComponent(this);

		try {
			backgroundImage = strap.getLoader().getImageLoader().load(BACKGROUND_IMAGE_NAME);
			cursorImage = strap.getLoader().getImageLoader().load(CURSOR_IMAGE_NAME);
		} catch (final IOException e) {
			throw new RuntimeException(e.getMessage(), e);
		}
	}

	private boolean isActive() {
		// return index < panel.getInventorySize();
		return true;
	}

	InventoryPanel getPanel() {
		return panel;
	}

	public int getIndex() {
		return index;
	}

	public ItemButtonValue getValue() {
		return value;
	}

	public void setValue(final ItemButtonValue v) {
		value = v;
		repaint();
	}

	public void useItem() {
		if (value == null) {
			return;
		}
		strap.getGui().getViewPane().startAction(value.getEntry(), false, true);
	}

	public void equipItem() {
		if (value == null) {
			return;
		}
		// value.getEntry().equip(!value.isEquipped());
	}

	public void dropItem() {
		if (value == null) {
			return;
		}
		final long count;
		if (value.getCount() > 1) {
			final ResourceBundle bundle = ResourceBundle.getBundle(ItemButton.class.getName());
			String countString = JOptionPane.showInputDialog(this, bundle.getString(BundleKey.HowMany.name()),
					Long.toString(value.getCount()));
			try {
				count = Integer.parseInt(countString);
			} catch (final NumberFormatException e) {
				return;
			}
		} else {
			count = 1;
		}

		strap.getGui().getViewPane().stopAutopilot();

		strap.control().dropItem(value.getEntry().getId(), count);
	}

	public void deleteItem() {
		if (value == null) {
			return;
		}
		final ResourceBundle bundle = ResourceBundle.getBundle(ItemButton.class.getName());
		final int count;
		if (value.getCount() > 1) {
			final String countString = JOptionPane.showInputDialog(this, bundle.getString(BundleKey.HowMany.name()),
					Long.toString(value.getCount()));
			try {
				count = Integer.parseInt(countString);
			} catch (final NumberFormatException e) {
				return;
			}
		} else {
			count = 1;
		}
		final int result = JOptionPane.showConfirmDialog(this, bundle.getString(BundleKey.ConfirmDelete.name()));
		if (result != JOptionPane.YES_OPTION) {
			return;
		}
		strap.control().deleteItem(value.getEntry().getId(), count);
	}

	@Private
	JPopupMenu createMenu() {
		JPopupMenu menu = new JPopupMenu();

		final ResourceBundle bundle = ResourceBundle.getBundle(ItemButton.class.getName());
		JMenuItem item;
		item = new JMenuItem(bundle.getString(BundleKey.Use.name()));
		item.addActionListener(e -> useItem());
		menu.add(item);
		item = new JMenuItem(bundle.getString(BundleKey.Equip.name()));
		item.addActionListener(e -> equipItem());
		menu.add(item);
		item = new JMenuItem(bundle.getString(BundleKey.Drop.name()));
		item.addActionListener(e -> dropItem());
		menu.add(item);
		item = new JMenuItem(bundle.getString(BundleKey.Delete.name()));
		item.addActionListener(e -> deleteItem());
		menu.add(item);

		JMenu m = new JMenu(bundle.getString(BundleKey.SetShortcut.name()));
		for (int i = 0, n = strap.getGui().getShortcutsFrame().getPanel().getShortcutsSize(); i < n; i++) {
			final int index = i;
			JMenuItem mi = new JMenuItem(Integer.toString(1 + i));
			mi.addActionListener(e -> {
				final ShortcutButton button = strap.getGui().getShortcutsFrame().getPanel().getButton(index);
				button.setValue(value);
				button.setRepeating(false);
				button.setSelfTargetting(true);
				button.send();
				button.repaint();
			});
			m.add(mi);
		}
		menu.add(m);

		item = new JMenuItem(bundle.getString(BundleKey.Compact.name()));
		item.addActionListener(e -> panel.compactInventory());
		menu.add(item);

		return menu;
	}

	@Override
	protected void paintComponent(final Graphics g) {
		if (isActive()) {
			g.drawImage(backgroundImage, getSize().width / 2 - backgroundImage.getWidth() / 2,
					getSize().height / 2 - backgroundImage.getHeight() / 2, null);
		}

		final int cx = getSize().width / 2;
		final int cy = getSize().height / 2;

		if (panel.getSelectedButton() == this) {
			g.drawImage(cursorImage, getSize().width / 2 - cursorImage.getWidth() / 2,
					getSize().height / 2 - cursorImage.getHeight() / 2, null);
		}

		if (value != null) {
			if (value.getCount() > 0) {
				strap.getLoader().getIconPainterTable().get(value.getIconName()).paint(g, cx, cy);
			}

			final FontMetrics fm = g.getFontMetrics();

			// if (value.isEquipped()) {
			// GuiUtil.drawBorderedString(g, "E", cx -
			// gui.settings().getIconWidth() / 2,
			// cy - gui.settings().getIconHeight() / 2 + fm.getHeight() -
			// fm.getDescent(), Color.black,
			// Color.lightGray);
			// }

			if (value.getCount() > 1) {
				final String s = Long.toString(value.getCount());
				SwingUtil.drawString(g, s, cx + SwingClientConstants.ICON_WIDTH / 2 - fm.stringWidth(s),
						cy + SwingClientConstants.ICON_HEIGHT / 2, Color.black, Color.white);
			}
		}
	}

	@Override
	public String getToolTipText(final MouseEvent e) {
		if (value == null) {
			return null;
		}
		return value.getName();
	}

}
