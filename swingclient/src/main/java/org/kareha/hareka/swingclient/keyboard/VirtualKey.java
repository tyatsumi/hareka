package org.kareha.hareka.swingclient.keyboard;

import java.awt.event.KeyEvent;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.HashMap;
import java.util.Map;

public final class VirtualKey {

	private static final Map<Integer, String> nameMap = new HashMap<>();
	private static final Map<String, Integer> codeMap = new HashMap<>();

	static {
		final Field[] fields = KeyEvent.class.getDeclaredFields();
		for (final Field f : fields) {
			if (f.getName().matches("^VK_.+$")) {
				if (f.getType() != int.class) {
					continue;
				}
				if ((f.getModifiers() & Modifier.STATIC) == 0) {
					continue;
				}
				final int code;
				try {
					code = f.getInt(null);
				} catch (final IllegalArgumentException | IllegalAccessException e) {
					throw new AssertionError(e);
				}
				final String name = f.getName().substring(3);
				nameMap.put(code, name);
				codeMap.put(name, code);
			}
		}
	}

	private VirtualKey() {
		throw new AssertionError();
	}

	public static String toName(final int code) {
		return nameMap.get(code);
	}

	public static int toCode(final String name) {
		return codeMap.get(name);
	}

	public static int[] codes() {
		return nameMap.keySet().stream().mapToInt(i -> i).sorted().toArray();
	}

}
