package org.kareha.hareka.swingclient.gui;

import java.awt.datatransfer.Transferable;

import javax.swing.JComponent;
import javax.swing.TransferHandler;

@SuppressWarnings("serial")
public class SkillButtonTransferHandler extends TransferHandler {

	@Override
	public int getSourceActions(final JComponent c) {
		return MOVE;
	}

	@Override
	protected Transferable createTransferable(final JComponent c) {
		final SkillButton button = (SkillButton) c;
		return new SkillButtonValue(false, button.getValue().getEntry());
	}

	@Override
	protected void exportDone(final JComponent source, final Transferable data, final int action) {
		if (action == MOVE) {
			final SkillButton button = (SkillButton) source;
			button.getPanel().swap();
		}
	}

	@Override
	public boolean canImport(TransferHandler.TransferSupport support) {
		if (!support.isDrop()) {
			return false;
		}
		return support.isDataFlavorSupported(SkillButtonValue.skillFlavor);
	}

	@Override
	public boolean importData(TransferHandler.TransferSupport support) {
		if (!canImport(support)) {
			return false;
		}
		final SkillButton button = (SkillButton) support.getComponent();
		button.getPanel().setSwapButton(button);
		return true;
	}

}
