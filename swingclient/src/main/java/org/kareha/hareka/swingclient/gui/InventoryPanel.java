package org.kareha.hareka.swingclient.gui;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.util.logging.Logger;

import javax.swing.JComponent;

import org.kareha.hareka.LocalEntityId;
import org.kareha.hareka.game.ItemType;
import org.kareha.hareka.swingclient.Strap;
import org.kareha.hareka.swingclient.SwingClientConstants;

@SuppressWarnings("serial")
public class InventoryPanel extends JComponent {

	private static final Logger logger = Logger.getLogger(InventoryPanel.class.getName());

	private static final int COLUMNS = 4;
	private static final int ROWS = 4;

	private final Strap strap;
	private final GridLayout layout = new GridLayout(0, COLUMNS);
	private int inventorySize;
	private ItemButton selectedButton;
	@SuppressWarnings("unused")
	private ItemButton dragButton;
	private ItemButton swapButton;

	public InventoryPanel(final Strap strap) {
		this.strap = strap;
		setLayout(layout);
		helperClear();
	}

	int getInventorySize() {
		return inventorySize;
	}

	@SuppressWarnings("unused")
	private void setInventorySize(final int v) {
		inventorySize = v;
		for (final Component c : getComponents()) {
			c.repaint();
		}
	}

	private int getInventoryRows() {
		return Math.max((inventorySize + COLUMNS - 1) / COLUMNS, ROWS);
	}

	private void extend(final int maxIndex) {
		final int requiredRows = maxIndex / COLUMNS + 1;
		final int currentRows = layout.getRows();
		if (requiredRows > currentRows) {
			layout.setRows(requiredRows);
			for (int j = currentRows; j < requiredRows; j++) {
				for (int i = 0; i < COLUMNS; i++) {
					final int index = COLUMNS * j + i;
					final ItemButton button = new ItemButton(strap, this, index);
					add(button);
				}
			}
			validate();
		}
	}

	private void helperClear() {
		removeAll();
		layout.setRows(0);
		extend(COLUMNS * getInventoryRows() - 1);
	}

	public void clear() {
		helperClear();
	}

	public ItemButton getButton(final int index) {
		extend(index);
		return (ItemButton) getComponent(index);
	}

	public ItemButton getButton(final ItemType type) {
		for (int j = 0, r = layout.getRows(); j < r; j++) {
			for (int i = 0; i < COLUMNS; i++) {
				final int index = COLUMNS * j + i;
				final ItemButton button = (ItemButton) getComponent(index);
				final ItemButtonValue value = button.getValue();
				if (value == null) {
					continue;
				}
				if (value.getEntry().getType().equals(type)) {
					return button;
				}
			}
		}
		return null;
	}

	public ItemButton getButton(final LocalEntityId id) {
		for (int j = 0, r = layout.getRows(); j < r; j++) {
			for (int i = 0; i < COLUMNS; i++) {
				final int index = COLUMNS * j + i;
				final ItemButton button = (ItemButton) getComponent(index);
				final ItemButtonValue value = button.getValue();
				if (value == null) {
					continue;
				}
				if (value.getEntry().getId().equals(id)) {
					return button;
				}
			}
		}
		return null;
	}

	public void setValue(final int index, final ItemButtonValue value) {
		final ItemButton button = getButton(index);
		button.setValue(value);
	}

	public void addValue(final ItemButtonValue value) {
		for (int i = 0;; i++) {
			final ItemButton button = getButton(i);
			if (button.getValue() == null) {
				setValue(i, value);
				return;
			}
		}
	}

	public void removeButton(final LocalEntityId id) {
		for (int j = 0, r = layout.getRows(); j < r; j++) {
			for (int i = 0; i < COLUMNS; i++) {
				final int index = COLUMNS * j + i;
				final ItemButton button = (ItemButton) getComponent(index);
				final ItemButtonValue value = button.getValue();
				if (value == null) {
					continue;
				}
				if (value.getEntry().getId().equals(id)) {
					button.setValue(null);
					return;
				}
			}
		}
	}

	@SuppressWarnings("unused")
	private void repaintButtons() {
		for (int j = 0, r = layout.getRows(); j < r; j++) {
			for (int i = 0; i < COLUMNS; i++) {
				final int index = COLUMNS * j + i;
				final ItemButton button = (ItemButton) getComponent(index);
				button.repaint();
			}
		}
	}

	public ItemButton getSelectedButton() {
		return selectedButton;
	}

	void setSelectedButton(final ItemButton v) {
		final ItemButton b = selectedButton;
		selectedButton = v;
		if (b != null) {
			b.repaint();
		}
		if (v != null) {
			v.repaint();
		}
	}

	void setDragButton(final ItemButton v) {
		dragButton = v;
	}

	void setSwapButton(final ItemButton v) {
		swapButton = v;
	}

	void swap() {
		if (swapButton == null) {
			return;
		}
		// gui.handle().mirror().items().swap(dragButton.getIndex(),
		// swapButton.getIndex());
		// TODO implement item swapping
		logger.info("Swapping item buttons is not implemented");
		swapButton = null;
	}

	public void swapItems(final int indexA, final int indexB) {
		final ItemButton buttonA = getButton(indexA);
		final ItemButton buttonB = getButton(indexB);
		final ItemButtonValue value = buttonA.getValue();
		buttonA.setValue(buttonB.getValue());
		buttonB.setValue(value);

		final ShortcutButton sa = strap.getGui().getShortcutsFrame().getPanel().getButton(buttonA.getValue());
		if (sa != null) {
			if (sa.getValue() != null) {
				// sa.getValue().setTableIndex(indexA);
				sa.send();
			}
		}
		final ShortcutButton sb = strap.getGui().getShortcutsFrame().getPanel().getButton(buttonB.getValue());
		if (sb != null) {
			if (sb.getValue() != null) {
				// sb.getValue().setTableIndex(indexB);
				sb.send();
			}
		}
	}

	Dimension getViewportSize() {
		return new Dimension(SwingClientConstants.ICON_WIDTH * COLUMNS, SwingClientConstants.ICON_HEIGHT * ROWS);
	}

	public void repaintItemButton(final LocalEntityId id) {
		final ItemButton button = getButton(id);
		button.repaint();
	}

	public void compactInventory() {
		// TODO implement inventory compacting method
	}

}
