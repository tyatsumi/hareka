package org.kareha.hareka.swingclient.gui;

import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;

import org.kareha.hareka.client.mirror.InventoryMirror;

public class ItemButtonValue extends ShortcutButtonValue {

	public static final DataFlavor itemFlavor = new DataFlavor(ItemButtonValue.class, "Item");

	protected final InventoryMirror.Entry entry;

	public ItemButtonValue(final InventoryMirror.Entry entry) {
		this(false, entry);
	}

	public ItemButtonValue(final boolean shortcut, final InventoryMirror.Entry entry) {
		super(shortcut);
		this.entry = entry;
	}

	public InventoryMirror.Entry getEntry() {
		return entry;
	}

	public long getCount() {
		return entry.getCount();
	}

	@Override
	public String getIconName() {
		return entry.getType().name();
	}

	@Override
	public String getName() {
		final StringBuilder sb = new StringBuilder();
		sb.append(entry.getName());
		if (getCount() > 1) {
			sb.append(" (");
			sb.append(entry.getCount());
			sb.append(")");
		}
		sb.append(" wt. ");
		sb.append(entry.getTotalWeight());
		return sb.toString();
	}

	@Override
	public Object getTransferData(final DataFlavor flavor) throws UnsupportedFlavorException, IOException {
		if (flavor.equals(shortcutFlavor)) {
			return this;
		} else if (flavor.equals(itemFlavor)) {
			return this;
		} else {
			throw new UnsupportedFlavorException(flavor);
		}
	}

	@Override
	public DataFlavor[] getTransferDataFlavors() {
		final DataFlavor[] array = { shortcutFlavor, itemFlavor, };
		return array;
	}

	@Override
	public boolean isDataFlavorSupported(final DataFlavor flavor) {
		if (flavor.equals(shortcutFlavor)) {
			return true;
		} else if (flavor.equals(itemFlavor)) {
			return true;
		} else {
			return false;
		}
	}

}