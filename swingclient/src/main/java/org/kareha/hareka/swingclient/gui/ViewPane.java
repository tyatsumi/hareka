package org.kareha.hareka.swingclient.gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsEnvironment;
import java.awt.Shape;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.LinkedHashSet;
import java.util.ResourceBundle;
import java.util.Set;

import javax.swing.JCheckBoxMenuItem;
import javax.swing.JDesktopPane;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.Timer;

import org.kareha.hareka.ManuallyClosable;
import org.kareha.hareka.annotation.Private;
import org.kareha.hareka.client.field.ClientField;
import org.kareha.hareka.client.field.FieldEntity;
import org.kareha.hareka.client.mirror.ActiveSkillMirror;
import org.kareha.hareka.client.mirror.InventoryMirror;
import org.kareha.hareka.field.Direction;
import org.kareha.hareka.field.Tile;
import org.kareha.hareka.field.TilePiece;
import org.kareha.hareka.field.TileType;
import org.kareha.hareka.field.Vector;
import org.kareha.hareka.swingclient.Strap;
import org.kareha.hareka.swingclient.SwingClientConstants;
import org.kareha.hareka.swingclient.keyboard.KeyCommand;
import org.kareha.hareka.swingclient.keyboard.KeyHandler;
import org.kareha.hareka.swingclient.keyboard.Keymap;
import org.kareha.hareka.ui.swing.SwingUtil;

@SuppressWarnings("serial")
public class ViewPane extends JDesktopPane implements ManuallyClosable {

	private enum BundleKey {
		SetNickname, Inspect, EditMode, Autopilot,
	}

	@Private
	final Strap strap;
	@Private
	View view;
	private boolean viewVisible;
	private final ActionListener repaintHandler;
	private Timer repaintTimer;
	@Private
	final MotionHandler motionHandler;
	private Timer motionTimer;
	@Private
	final Set<Integer> pressedKeys = new LinkedHashSet<>();
	@Private
	final KeyHandler handler = new KeyHandler(new Keymap());
	@Private
	final Menu menu;
	@Private
	Vector previousTilePosition;
	@Private
	FieldEntity cursorEntity;
	private BufferedImage nowLoadingImage;

	private final PaintStat paintStat = new PaintStat();

	private ViewPane(final Strap strap) {
		this.strap = strap;
		final Gui gui = strap.getGui();

		enableInputMethods(false);
		addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(final KeyEvent e) {
				final int code = e.getKeyCode();
				if (pressedKeys.contains(code)) {
					return;
				}
				pressedKeys.add(code);

				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					gui.getChatFrame().getChatPanel().requestFocusToInput();
					return;
				}

				if (gui.getShortcutsFrame().getPanel().checkKey(code)) {
					return;
				}

				final KeyCommand command = handler.keyPressed(e);
				if (view == null) {
					return;
				}
				switch (command) {
				default:
					break;
				case STOP:
					motionHandler.stop();
					break;
				case RIGHT:
				case LOWER_RIGHT:
				case LOWER_LEFT:
				case LEFT:
				case UPPER_LEFT:
				case UPPER_RIGHT:
					final Direction direction = command.toDirection();
					motionHandler.move(direction);
					break;
				case ROTATE_RIGHT:
					view.rotate(1);
					break;
				case ROTATE_LEFT:
					view.rotate(-1);
					break;
				case MIRROR:
					view.reflect();
					break;
				case ATTACK:
					// if (motionHandler.getType() == MotionMode.ACTION) {
					// motionHandler.stop();
					// } else if (cursorEntity != null) {
					// motionHandler.attack(cursorEntity);
					// }
					break;
				}
			}

			@Override
			public void keyReleased(final KeyEvent e) {
				final int code = e.getKeyCode();
				if (!pressedKeys.remove(code)) {
					return;
				}

				final KeyCommand command = handler.keyReleased(e);
				if (view == null) {
					return;
				}
				switch (command) {
				default:
					break;
				case STOP:
					motionHandler.stop();
					break;
				case RIGHT:
				case LOWER_RIGHT:
				case LOWER_LEFT:
				case LEFT:
				case UPPER_LEFT:
				case UPPER_RIGHT:
					final Direction direction = command.toDirection();
					motionHandler.move(direction);
					break;
				}
			}
		});

		addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(final MouseEvent e) {
				if (e.isPopupTrigger()) {
					menu.getMenu().show(ViewPane.this, e.getX(), e.getY());
				} else if (e.getButton() == MouseEvent.BUTTON1) {
					requestFocus();
					if (view == null) {
						return;
					}
					if (isEditMode()) {
						final Vector position = view.detectTilePosition(e.getX(), e.getY());
						if (position != null) {
							view.setSelectedTilePosition(position);
							putTile(position);
							previousTilePosition = position;
						}
					} else {
						// Set cursor on clicked entity
						final FieldEntity entity = view.detectEntity(e.getX(), e.getY());
						if (entity != null && entity != cursorEntity) {
							cursorEntity = entity;
							view.setSelectedEntity(entity);
							return;
						}

						final Vector position = view.detectTilePosition(e.getX(), e.getY());
						if (position != null) {
							if (e.isShiftDown()) {
								view.setSelectedTilePosition(position);
							} else {
								motionHandler.move(position);
							}
						}
					}
				} else if (e.getButton() == MouseEvent.BUTTON2) {
					gui.getShortcutsFrame().getPanel().use();
				}
			}

			@Override
			public void mouseReleased(final MouseEvent e) {
				if (e.isPopupTrigger()) {
					menu.getMenu().show(ViewPane.this, e.getX(), e.getY());
				} else if (e.getButton() == MouseEvent.BUTTON1) {
					motionHandler.stop();
				}
			}
		});
		addMouseMotionListener(new MouseMotionAdapter() {
			@Override
			public void mouseMoved(final MouseEvent e) {
				if (isEditMode()) {
					final Vector position = view.detectTilePosition(e.getX(), e.getY());
					if (position != null) {
						if (!position.equals(previousTilePosition)) {
							view.setSelectedTilePosition(position);
							previousTilePosition = position;
						}
					}
				}
			}

			@Override
			public void mouseDragged(final MouseEvent e) {
				if (motionHandler.getType() == MotionMode.WALK) {
					if (view == null) {
						return;
					}
					if (isEditMode()) {
						final Vector position = view.detectTilePosition(e.getX(), e.getY());
						if (position != null) {
							if (!position.equals(previousTilePosition)) {
								view.setSelectedTilePosition(position);
								putTile(position);
								previousTilePosition = position;
							}
						}
					} else {
						if (motionHandler.getType() == MotionMode.WALK) {
							final Vector position = view.detectTilePosition(e.getX(), e.getY());
							if (position != null) {
								motionHandler.move(position);
							}
						}
					}
				}
			}
		});
		addMouseWheelListener(new MouseWheelListener() {
			@Override
			public void mouseWheelMoved(final MouseWheelEvent e) {
				if (isEditMode()) {
					if (previousTilePosition != null) {
						changeTileElevation(previousTilePosition, -e.getWheelRotation());
					}
				} else {
					gui.getShortcutsFrame().getPanel().moveCursor(e.getWheelRotation());
				}
			}
		});

		repaintHandler = new RepaintHandler();
		motionHandler = new MotionHandler();

		menu = new Menu();
	}

	public void startAction(final ActiveSkillMirror.Entry entry, final boolean repeating, final boolean targetSelf) {
		if (targetSelf) {
			motionHandler.action(entry, strap.control().getSelfFieldEntity(), repeating);
		} else {
			motionHandler.action(entry, cursorEntity, repeating);
		}
	}

	public void doNullSkill(final ActiveSkillMirror.Entry entry) {
		stopAutopilot();

		entry.use();
	}

	public void doTileSkill(final ActiveSkillMirror.Entry entry) {
		final Vector target = view.getSelectedTilePosition();
		if (target == null) {
			return;
		}
		if (target.distance(strap.control().getSelfPlacement().getPosition()) <= entry.getReach()) {
			stopAutopilot();

			entry.use(target);
		}
	}

	public void startAction(final InventoryMirror.Entry entry, final boolean repeating, final boolean targetSelf) {
		if (targetSelf) {
			motionHandler.action(entry, strap.control().getSelfFieldEntity(), repeating);
		} else {
			motionHandler.action(entry, cursorEntity, repeating);
		}
	}

	@Override
	public void close() {
		repaintTimer.stop();
		motionTimer.stop();
	}

	public void start() {
		cursorEntity = null;
		motionHandler.stop();
		if (!repaintTimer.isRunning()) {
			repaintTimer.start();
		}
		if (!motionTimer.isRunning()) {
			motionTimer.start();
		}
	}

	public void stop() {
		repaintTimer.stop();
		motionTimer.stop();
	}

	private class RepaintHandler implements ActionListener {

		private long past = Long.MIN_VALUE;

		@Private
		RepaintHandler() {

		}

		@Override
		public void actionPerformed(final ActionEvent e) {
			final long now = System.currentTimeMillis();
			if (view == null) {
				return;
			}
			if (past == Long.MIN_VALUE) {
				past = now;
			}
			view.animate((int) (now - past));
			repaint();
			past = now;
		}

	}

	public void setTargetFrameRate(final int targetFrameRate) {
		repaintTimer.setDelay(1000 / targetFrameRate);
		motionTimer.setDelay(1000 / targetFrameRate);
	}

	public FieldEntity getTagetEntity() {
		return cursorEntity;
	}

	@Private
	void putTile(final Vector position) {
		final Gui gui = strap.getGui();
		final TileType type = gui.getTileFrame().getTile();
		final int elevation = gui.getTileFrame().getElevation();
		final Tile tile = Tile.valueOf(type, elevation);
		strap.editControl().setTile(TilePiece.valueOf(position, tile));
	}

	@Private
	void changeTileElevation(final Vector position, final int delta) {
		final Tile orig = view.getField().getTile(position);
		final Tile tile = Tile.valueOf(orig.type(), orig.elevation() + delta);
		strap.editControl().setTile(TilePiece.valueOf(position, tile));
	}

	private class Menu {
		private final JPopupMenu menu;
		final JMenuItem setNicknameItem;
		final JMenuItem inspectItem;
		final JCheckBoxMenuItem editModeCheckBox;
		final JCheckBoxMenuItem autopilotCheckBox;

		Menu() {
			final ResourceBundle bundle = ResourceBundle.getBundle(ViewPane.class.getName());
			menu = new JPopupMenu();

			setNicknameItem = new JMenuItem(bundle.getString(BundleKey.SetNickname.name()));
			setNicknameItem.addActionListener(e -> {
				if (!strap.isConnected()) {
					return;
				}
				final String defaultName = strap.userControl().getNickname(cursorEntity.getLocalId());
				final String input = (String) JOptionPane.showInputDialog(ViewPane.this,
						bundle.getString(BundleKey.SetNickname.name()), bundle.getString(BundleKey.SetNickname.name()),
						JOptionPane.QUESTION_MESSAGE, null, null, defaultName != null ? defaultName : "");
				if (input == null) {
					// canceled
					return;
				}
				if (input.isEmpty()) {
					strap.userControl().removeNickname(cursorEntity.getLocalId());
				} else {
					strap.userControl().addNickname(cursorEntity.getLocalId(), input);
				}
				view.updateEntityName(cursorEntity);
			});
			menu.add(setNicknameItem);

			inspectItem = new JMenuItem(bundle.getString(BundleKey.Inspect.name()));
			inspectItem.addActionListener(e -> {
				strap.adminControl().inspect(cursorEntity);
			});
			menu.add(inspectItem);

			menu.addSeparator();

			editModeCheckBox = new JCheckBoxMenuItem(bundle.getString(BundleKey.EditMode.name()));
			menu.add(editModeCheckBox);

			autopilotCheckBox = new JCheckBoxMenuItem(bundle.getString(BundleKey.Autopilot.name()));
			autopilotCheckBox.addActionListener(e -> {
				strap.control().setAutopilot(autopilotCheckBox.isSelected());
			});
			menu.add(autopilotCheckBox);
		}

		JPopupMenu getMenu() {
			setNicknameItem.setEnabled(cursorEntity != null);
			inspectItem.setEnabled(cursorEntity != null);
			return menu;
		}

	}

	boolean isEditMode() {
		return menu.editModeCheckBox.isSelected();
	}

	public void setEditMode(final boolean v) {
		menu.editModeCheckBox.setSelected(v);
	}

	public boolean isAutopilot() {
		return menu.autopilotCheckBox.isSelected();
	}

	public void setAutopilot(final boolean v) {
		menu.autopilotCheckBox.setSelected(v);
	}

	public static ViewPane newInstance(final Strap strap) {
		final ViewPane viewPane = new ViewPane(strap);

		viewPane.repaintTimer = new Timer(1000 / strap.getSwingClientSettings().getTargetFrameRate(),
				viewPane.repaintHandler);
		viewPane.repaintTimer.setInitialDelay(0);
		// viewPane.repaintTimer.start();

		viewPane.motionTimer = new Timer(1000 / strap.getSwingClientSettings().getTargetFrameRate(),
				viewPane.motionHandler);
		viewPane.motionTimer.setInitialDelay(0);
		// viewPane.motionTimer.start();

		return viewPane;
	}

	public View newView(final ClientField field) {
		final GraphicsConfiguration gc = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice()
				.getDefaultConfiguration();
		view = new View(strap, gc, field);
		nowLoadingImage = null;
		return view;
	}

	public View getView() {
		return view;
	}

	public void setViewVisible(final boolean v) {
		viewVisible = v;
	}

	@Override
	protected void paintComponent(final Graphics g) {
		if (view == null) {
			if (nowLoadingImage == null) {
				try {
					nowLoadingImage = strap.getLoader().getImageLoader().load("login.png");
				} catch (final IOException e) {
					return;
				}
			}
			final Dimension size = getSize();
			final int x = (size.width - nowLoadingImage.getWidth()) / 2;
			final int y = (size.height - nowLoadingImage.getHeight()) / 2;
			g.drawImage(nowLoadingImage, x, y, null);
			return;
		}

		if (!viewVisible) {
			final Dimension size = getSize();
			g.clearRect(0, 0, size.width, size.height);
			return;
		}

		if (strap.getSwingClientSettings().isShowFrameRate()) {
			paintStat.startPaint();
		}

		switch (view.getAtmosphere()) {
		default:
			break;
		case GROUND:
			g.setColor(Color.cyan);
			g.fillRect(0, 0, SwingClientConstants.VIEW_WIDTH, SwingClientConstants.VIEW_HEIGHT);
			break;
		case INDOOR:
			g.setColor(Color.gray);
			g.fillRect(0, 0, SwingClientConstants.VIEW_WIDTH, SwingClientConstants.VIEW_HEIGHT);
			break;
		case UNDERGROUND:
			g.setColor(Color.black);
			g.fillRect(0, 0, SwingClientConstants.VIEW_WIDTH, SwingClientConstants.VIEW_HEIGHT);
			break;
		case SKY:
			g.setColor(Color.white);
			g.fillRect(0, 0, SwingClientConstants.VIEW_WIDTH, SwingClientConstants.VIEW_HEIGHT);
			break;

		}

		if (strap.getSwingClientSettings().isEnableClip()) {
			final Shape clip = g.getClip();
			g.setClip(0, 0, SwingClientConstants.VIEW_WIDTH, SwingClientConstants.VIEW_HEIGHT);
			view.paint(g);
			g.setClip(clip);
		} else {
			view.paint(g);
		}

		if (strap.getSwingClientSettings().isShowFrameRate()) {
			paintStat.endPaint(strap.getSwingClientSettings().getTargetFrameRate());
			final String frameRateString = String.format("frame rate: %6.2f", paintStat.getFrameRate());
			final String paintLoadString = String.format("paint load: %6.2f%%", paintStat.getPaintLoad() * 100);
			SwingUtil.drawString(g, frameRateString, 0, g.getFontMetrics().getAscent());
			SwingUtil.drawString(g, paintLoadString, 0, g.getFontMetrics().getAscent() * 2);
		}
	}

	private enum MotionMode {

		STOP, WALK, ACTION,

	}

	private enum ActionMode {
		NONE, SKILL, ITEM,
	}

	private class MotionHandler implements ActionListener {

		private MotionMode mode = MotionMode.STOP;
		private Direction direction = Direction.NULL;
		private FieldEntity actionTarget;
		private ActionMode actionMode = ActionMode.NONE;
		private ActiveSkillMirror.Entry skill;
		private InventoryMirror.Entry item;
		private boolean repeating;

		@Private
		MotionHandler() {

		}

		MotionMode getType() {
			return mode;
		}

		void stop() {
			mode = MotionMode.STOP;
		}

		void move(final Direction direction) {
			this.direction = direction;
			mode = MotionMode.WALK;
		}

		void move(final Vector position) {
			final FieldEntity entity = strap.control().getSelfFieldEntity();
			if (entity == null) {
				return;
			}
			direction = position.subtract(entity.getPlacement().getPosition()).direction()
					.transform(view.getTransformation());
			mode = MotionMode.WALK;
		}

		void action(final ActiveSkillMirror.Entry skill, final FieldEntity entity, final boolean repeating) {
			if (mode == MotionMode.ACTION) {
				if (this.skill == skill && actionTarget == entity && this.repeating && repeating) {
					this.skill = null;
					mode = MotionMode.STOP;
					return;
				}
			}
			this.skill = skill;
			actionMode = ActionMode.SKILL;
			actionTarget = entity;
			mode = MotionMode.ACTION;
			this.repeating = repeating;
		}

		void action(final InventoryMirror.Entry item, final FieldEntity entity, final boolean repeating) {
			if (mode == MotionMode.ACTION) {
				if (this.item == item && actionTarget == entity && this.repeating && repeating) {
					this.item = null;
					mode = MotionMode.STOP;
					return;
				}
			}
			this.item = item;
			actionMode = ActionMode.ITEM;
			actionTarget = entity;
			mode = MotionMode.ACTION;
			this.repeating = repeating;
		}

		@Override
		public void actionPerformed(final ActionEvent e) {
			outer: switch (mode) {
			default:
				break;
			case WALK: {
				final FieldEntity selfEntity = strap.control().getSelfFieldEntity();
				if (selfEntity == null) {
					mode = MotionMode.STOP;
					break;
				}

				stopAutopilot();

				final Vector position = selfEntity.getPlacement().getPosition()
						.neighbor(direction.invert(view.getTransformation()));
				strap.control().move(position);
			}
				break;
			case ACTION: {
				if (actionTarget == null) {
					mode = MotionMode.STOP;
					break;
				}
				final FieldEntity selfEntity = strap.control().getSelfFieldEntity();
				if (selfEntity == null) {
					mode = MotionMode.STOP;
					break;
				}
				if (skill == null) {
					mode = MotionMode.STOP;
					break;
				}
				if (!actionTarget.getHealthBar().isAlive()) {
					switch (skill.getType()) {
					default:
						mode = MotionMode.STOP;
						break outer;
					case PULL:
					case PUSH:
					case REVIVE:
						break;
					}
				}

				stopAutopilot();

				final Vector selfPosition = selfEntity.getPlacement().getPosition();
				final Vector targetPosition = actionTarget.getPlacement().getPosition();
				boolean performed = false;
				switch (actionMode) {
				default:
					break;
				case SKILL:
					if (targetPosition.distance(selfPosition) <= skill.getReach()) {
						skill.use(actionTarget);
						performed = true;
						if (!repeating) {
							mode = MotionMode.STOP;
						}
					}
					break;
				case ITEM:
					if (targetPosition.distance(selfPosition) <= item.getReach()) {
						item.use(actionTarget);
						performed = true;
						if (!repeating) {
							mode = MotionMode.STOP;
						}
					}
					break;
				}
				if (!performed) {
					final Direction targetDirection = targetPosition.subtract(selfPosition).direction();
					final Vector position = selfPosition.neighbor(targetDirection);
					strap.control().move(position);
				}
			}
				break;
			}
		}

	}

	public void stopAutopilot() {
		if (menu.autopilotCheckBox.isSelected()) {
			menu.autopilotCheckBox.setSelected(false);
			strap.control().setAutopilot(false);
		}
	}
}
