package org.kareha.hareka.swingclient.gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javax.swing.BoxLayout;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.WindowConstants;

import org.kareha.hareka.swingclient.Strap;
import org.kareha.hareka.user.Permission;
import org.kareha.hareka.user.Role;

@SuppressWarnings("serial")
public class RoleTokenIssueFrame extends JInternalFrame {

	private static final int FRAME_WIDTH = 384;
	private static final int FRAME_HEIGHT = 256;

	private enum BundleKey {

		Title, Message, Ok, Cancel,

		Permissions, NotConnected,

	}

	private final Strap strap;
	private final JComboBox<Role> roleComboBox;
	private final DefaultListModel<Permission> listModel;

	public RoleTokenIssueFrame(final Strap strap) {
		super("", true, true, true, true);
		this.strap = strap;
		setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
		final ResourceBundle bundle = ResourceBundle.getBundle(RoleTokenIssueFrame.class.getName());
		setTitle(bundle.getString(BundleKey.Title.name()));

		final JPanel northPanel = new JPanel();
		northPanel.setLayout(new BoxLayout(northPanel, BoxLayout.Y_AXIS));
		northPanel.add(new JLabel(bundle.getString(BundleKey.Message.name())));
		roleComboBox = new JComboBox<>();
		northPanel.add(roleComboBox);

		final JPanel centerPanel = new JPanel();
		centerPanel.setLayout(new BoxLayout(centerPanel, BoxLayout.Y_AXIS));
		centerPanel.add(new JLabel(bundle.getString(BundleKey.Permissions.name())));
		listModel = new DefaultListModel<>();
		final JList<Permission> permissionList = new JList<>(listModel);
		final JScrollPane permissionListScrollPane = new JScrollPane(permissionList,
				ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		centerPanel.add(permissionListScrollPane);

		roleComboBox.addActionListener(e -> updatePermissionList());

		final JPanel southPanel = new JPanel();
		final JButton okButton = new JButton(bundle.getString(BundleKey.Ok.name()));
		okButton.addActionListener(e -> {
			final int index = roleComboBox.getSelectedIndex();
			if (index == -1) {
				return;
			}
			final Role role = roleComboBox.getItemAt(index);
			if (!strap.isConnected()) {
				JOptionPane.showInternalMessageDialog(this, bundle.getString(BundleKey.NotConnected.name()));
				return;
			}
			strap.adminControl().issueRoleToken(role.getId());
			setVisible(false);
		});
		southPanel.add(okButton);
		final JButton cancelButton = new JButton(bundle.getString(BundleKey.Cancel.name()));
		cancelButton.addActionListener(e -> setVisible(false));
		southPanel.add(cancelButton);

		add(northPanel, BorderLayout.NORTH);
		add(centerPanel, BorderLayout.CENTER);
		add(southPanel, BorderLayout.SOUTH);
		setPreferredSize(new Dimension(FRAME_WIDTH, FRAME_HEIGHT));
		pack();
	}

	public void updateRoles() {
		final Role pr = strap.adminControl().getHighestRole(Permission.ISSUE_ROLE_TOKENS);
		final int rank;
		if (pr == null) {
			rank = 0;
		} else {
			rank = pr.getRank();
		}
		final List<Role> roles = new ArrayList<>();
		for (final Role role : strap.adminControl().getRoleList()) {
			if (role.getRank() < rank) {
				roles.add(role);
			}
		}
		roles.sort((a, b) -> {
			if (a.getRank() > b.getRank()) {
				return -1;
			} else if (a.getRank() < b.getRank()) {
				return 1;
			} else {
				return a.getId().compareTo(b.getId());
			}
		});

		roleComboBox.setModel(new DefaultComboBoxModel<>(roles.toArray(new Role[0])));
		updatePermissionList();
	}

	private void updatePermissionList() {
		final int index = roleComboBox.getSelectedIndex();
		if (index == -1) {
			listModel.clear();
			return;
		}
		final Role role = roleComboBox.getItemAt(index);
		final List<Permission> permissions = new ArrayList<>(role.getPermissions());
		permissions.sort((a, b) -> {
			if (a.ordinal() < b.ordinal()) {
				return -1;
			} else if (a.ordinal() > b.ordinal()) {
				return 1;
			} else {
				return 0;
			}
		});
		listModel.clear();
		for (final Permission permission : permissions) {
			listModel.addElement(permission);
		}
	}

}
