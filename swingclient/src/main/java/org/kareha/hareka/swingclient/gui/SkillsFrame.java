package org.kareha.hareka.swingclient.gui;

import java.awt.BorderLayout;
import java.util.ResourceBundle;

import javax.swing.JInternalFrame;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.WindowConstants;

import org.kareha.hareka.swingclient.Strap;
import org.kareha.hareka.swingclient.SwingClientConstants;

@SuppressWarnings("serial")
public class SkillsFrame extends JInternalFrame {

	private enum BundleKey {
		Title,
	}

	private final SkillsPanel panel;

	public SkillsFrame(final Strap strap) {
		// non-resizable, closable, non-maximizable, non-iconifiable
		super("", false, true, false, false);
		setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
		final ResourceBundle bundle = ResourceBundle.getBundle(SkillsFrame.class.getName());
		setTitle(bundle.getString(BundleKey.Title.name()));

		panel = new SkillsPanel(strap);
		final JScrollPane scrollPane = new JScrollPane(panel, ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS,
				ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.getViewport().setPreferredSize(panel.getViewportSize());
		scrollPane.getVerticalScrollBar().setUnitIncrement(SwingClientConstants.ICON_HEIGHT);

		add(scrollPane, BorderLayout.CENTER);
		setBorder(null);
		pack();
	}

	public SkillsPanel getPanel() {
		return panel;
	}

}
