package org.kareha.hareka.swingclient.gui;

import java.awt.Graphics;
import java.awt.GraphicsConfiguration;
import java.awt.Point;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.kareha.hareka.client.chat.ChatEntity;
import org.kareha.hareka.client.field.ClientField;
import org.kareha.hareka.client.field.FieldEntity;
import org.kareha.hareka.field.Placement;
import org.kareha.hareka.field.Tile;
import org.kareha.hareka.field.TilePiece;
import org.kareha.hareka.field.TileType;
import org.kareha.hareka.field.Transformation;
import org.kareha.hareka.field.Vector;
import org.kareha.hareka.graphics.GraphicsConstants;
import org.kareha.hareka.graphics.GraphicsLoader;
import org.kareha.hareka.graphics.model.Effect;
import org.kareha.hareka.graphics.model.Model;
import org.kareha.hareka.graphics.sprite.Screen;
import org.kareha.hareka.graphics.sprite.Sprite;
import org.kareha.hareka.graphics.tile.Atmosphere;
import org.kareha.hareka.graphics.tile.ViewTileField;
import org.kareha.hareka.graphics.tile.ViewTilePiece;
import org.kareha.hareka.graphics.tile.ViewUtil;
import org.kareha.hareka.swingclient.Strap;
import org.kareha.hareka.swingclient.SwingClientConstants;

public class View {

	private final Strap strap;
	private final ClientField field;
	private final Screen screen;
	private final ViewTileField viewTileField;
	private Transformation transformation = Transformation.IDENTITY;
	private Vector position = Vector.ZERO;
	private Vector prevPosition = Vector.ZERO;
	private float walkRatio;
	private Map<FieldEntity, Model> modelMap = new HashMap<>();
	private List<Effect> effects = new ArrayList<>();
	private final ChatView chatView;
	private FieldEntity selectedEntity;

	public View(final Strap strap, final GraphicsConfiguration gc, final ClientField field) {
		this.strap = strap;
		this.field = field;

		screen = new Screen();
		viewTileField = new ViewTileField(field, screen, strap.getLoader().tileLoader());

		chatView = new ChatView(this, strap, gc);
		viewTileField.render(transformation, position, SwingClientConstants.VIEW_SIZE);
		updateViewport();
	}

	public ClientField getField() {
		return field;
	}

	public void paint(final Graphics g) {
		screen.paint(g);
		chatView.paint(g);
	}

	public void animate(final int v) {
		for (final Model model : modelMap.values()) {
			model.animate(v);
		}

		for (final Iterator<Effect> i = effects.iterator(); i.hasNext();) {
			final Effect effect = i.next();
			effect.animate(v);
			if (effect.isFinished()) {
				effect.removeFrom(screen);
				i.remove();
			}
		}

		viewTileField.animate(v);

		final int motionWait;
		final FieldEntity entity = strap.control().getSelfFieldEntity();
		if (entity == null) {
			motionWait = SwingClientConstants.DEFAULT_MOTION_WAIT;
		} else {
			motionWait = entity.getMotionWait();
		}
		if (walkRatio < 1) {
			if (motionWait != 0) {
				walkRatio += (float) v / motionWait;
			}
			if (walkRatio > 1) {
				walkRatio = 1;
			}
			updateViewport();
		}

		chatView.update();
	}

	public Atmosphere getAtmosphere() {
		return viewTileField.getAtmosphere();
	}

	public void setPosition(final Vector v) {
		prevPosition = v;
		position = v;
		walkRatio = 1;
		viewTileField.render(transformation, position, SwingClientConstants.VIEW_SIZE);
		strap.getSoundContext().getMusicPlayer().play(viewTileField.getAtmosphere().name(), true);
		updateViewport();
	}

	public void rotate(final int v) {
		final int v2;
		if (transformation.reflection()) {
			v2 = -v;
		} else {
			v2 = v;
		}
		final Transformation t = Transformation.valueOf(position, 0, false, 0)
				.transform(Transformation.valueOf(Vector.ZERO, v2, false, 0))
				.transform(Transformation.valueOf(position.opposite(), 0, false, 0));
		transformation = transformation.transform(t);
		viewTileField.render(transformation, position, SwingClientConstants.VIEW_SIZE);
		for (final Model model : modelMap.values()) {
			model.transform(t);
		}
		updateViewport();
	}

	public void reflect() {
		final Transformation t = Transformation.valueOf(position, 0, false, 0)
				.transform(Transformation.valueOf(Vector.ZERO, 0, true, 0))
				.transform(Transformation.valueOf(position.opposite(), 0, false, 0));
		transformation = transformation.transform(t);
		viewTileField.render(transformation, position, SwingClientConstants.VIEW_SIZE);
		for (final Model model : modelMap.values()) {
			model.transform(t);
		}
		updateViewport();
	}

	public void walk(final Vector position) {
		viewTileField.walk(position);

		prevPosition = this.position;
		this.position = position;
		walkRatio = 0;
		updateViewport();
	}

	public void addTiles(final Collection<TilePiece> tiles) {
		viewTileField.addTiles(tiles);
		for (final TilePiece tilePiece : tiles) {
			updateEntityElevation(tilePiece.position());
		}
		strap.getSoundContext().getMusicPlayer().play(viewTileField.getAtmosphere().name(), true);
	}

	private int getSink(final TileType type, final boolean alive) {
		switch (type) {
		default:
			return 0;
		case AIR:
			return 2;
		case DOWNSTAIRS:
			return 1;
		case UPSTAIRS:
			return -1;
		case WATER:
			if (!alive && strap.getSwingClientSettings().isFloatDeadBodies()) {
				return 0;
			}
			return 2;
		}
	}

	public void addEntity(final FieldEntity v) {
		final GraphicsLoader loader = strap.getLoader();
		final Model model = loader.getModelLoader().createModel(v);
		setModelName(v, model);
		model.addTo(screen);
		setModelElevation(v, model);
		modelMap.put(v, model);
	}

	public void updateEntityName(final FieldEntity v) {
		final Model model = modelMap.get(v);
		if (model == null) {
			return;
		}
		setModelName(v, model);
	}

	private void updateEntityElevation(final FieldEntity v) {
		final Model model = modelMap.get(v);
		if (model == null) {
			return;
		}
		setModelElevation(v, model);
	}

	public void updateEntityElevation(final Vector position) {
		for (final FieldEntity entity : field.getEntities(position)) {
			updateEntityElevation(entity);
		}
	}

	private void setModelName(final FieldEntity entity, final Model model) {
		final ChatEntity chatEntity = strap.userControl().getChatEntity(entity);
		final String alias;
		if (chatEntity != null) {
			alias = chatEntity.getAlias();
		} else {
			alias = null;
		}
		final String nickname = strap.userControl().getNickname(entity.getLocalId());
		final String name;
		if (alias != null && nickname != null) {
			if (alias.equals(nickname)) {
				name = alias;
			} else {
				name = nickname + " (" + alias + ")";
			}
		} else if (alias != null) {
			name = alias;
		} else {
			name = nickname;
		}
		model.setName(name);
	}

	private void setModelElevation(final FieldEntity entity, final Model model) {
		final Tile tile = field.getTile(entity.getPlacement().getPosition());
		final int h = tile.elevation() - getSink(tile.type(), entity.isAlive());
		model.setPlacement(entity.getPlacement().transform(transformation), h);
	}

	public void removeEntity(final FieldEntity v) {
		final Model model = modelMap.remove(v);
		if (model != null) {
			model.removeFrom(screen);
		}
	}

	public void placeEntity(final FieldEntity v, final Placement p, final int motionWait) {
		final Tile tile = field.getTile(v.getPlacement().getPosition());
		final int h = tile.elevation() - getSink(tile.type(), v.isAlive());
		modelMap.get(v).place(p.transform(transformation), h, motionWait);
	}

	public Effect addEffect(final String id, final FieldEntity target) {
		final Effect effect = strap.getLoader().getEffectLoader().createEffect(id);
		effect.addTo(screen);
		final Tile tile = field.getTile(target.getPlacement().getPosition());
		final int h = tile.elevation() - getSink(tile.type(), target.isAlive());
		effect.setPlacement(target.getPlacement().transform(transformation), h);
		effects.add(effect);
		return effect;
	}

	public void removeEffect(final Effect v) {
		if (!effects.remove(v)) {
			return;
		}
		v.removeFrom(screen);
	}

	public Effect addShotEffect(final String id, final Vector origin, final Vector target) {
		final Effect effect = strap.getLoader().getEffectLoader().createEffect(id);
		effect.addTo(screen);
		final Placement p = Placement.valueOf(origin, target.subtract(origin).direction());
		final int oh = field.getTile(origin).elevation();
		effect.setPlacement(p.transform(transformation), oh);
		final Placement tp = Placement.valueOf(target, target.subtract(origin).direction());
		final Tile targetTile = field.getTile(target);
		final int th = targetTile.elevation() - getSink(targetTile.type(), true);
		effect.place(tp.transform(transformation), th, 375);
		effects.add(effect);
		return effect;
	}

	public Vector detectTilePosition(final int x, final int y) {
		final Collection<Sprite> sprites = screen.detect(x, y);
		final List<ViewTilePiece> list = new ArrayList<>();
		for (final Sprite sprite : sprites) {
			final Object entity = sprite.getObject();
			if (entity instanceof ViewTilePiece) {
				list.add((ViewTilePiece) entity);
			}
		}
		if (list.size() < 1) {
			return null;
		}
		return ViewUtil.toVector(list.get(list.size() - 1).position()).invert(transformation);
	}

	public FieldEntity detectEntity(final int x, final int y) {
		final Collection<Sprite> sprites = screen.detect(x, y);
		final List<FieldEntity> list = new ArrayList<>();
		for (final Sprite sprite : sprites) {
			final Object entity = sprite.getObject();
			if (entity instanceof FieldEntity) {
				list.add((FieldEntity) entity);
			}
		}
		if (list.size() < 1) {
			return null;
		}
		return list.get(list.size() - 1);
	}

	public Vector getSelectedTilePosition() {
		return viewTileField.getCursor();
	}

	public void setSelectedTilePosition(final Vector position) {
		viewTileField.setCursor(position);
	}

	public void setSelectedEntity(final FieldEntity entity) {
		if (selectedEntity == entity) {
			return;
		}
		if (selectedEntity != null) {
			final Model model = modelMap.get(selectedEntity);
			if (model != null) {
				model.setCursor(screen, false);
			}
		}
		if (entity != null) {
			final Model model = modelMap.get(entity);
			if (model != null) {
				model.setCursor(screen, true);
			}
		}
		selectedEntity = entity;
	}

	public void reloadTiles() {
		viewTileField.setLoader(strap.getLoader().tileLoader());
		viewTileField.render(transformation, position, SwingClientConstants.VIEW_SIZE);
	}

	public void reloadEntity(final FieldEntity v) {
		final Model model = modelMap.remove(v);
		if (model != null) {
			model.removeFrom(screen);
		}
		addEntity(v);
	}

	public void reloadEntities() {
		final Set<FieldEntity> objects = new HashSet<>(modelMap.keySet());
		clearEntities();
		for (final FieldEntity object : objects) {
			addEntity(object);
		}
	}

	public Transformation getTransformation() {
		return transformation;
	}

	public void addChat(final ChatEntity entity, final String content) {
		chatView.add(entity, content);
	}

	public void setChatDisplayDuration(final int duration) {
		chatView.duration = duration;
	}

	private Point toView(final Vector position) {
		final Vector p = position.transform(transformation);
		final int x = GraphicsConstants.TILE_WIDTH * p.x() + GraphicsConstants.TILE_WIDTH / 2 * p.y();
		final int y = GraphicsConstants.TILE_HEIGHT * p.y();
		return new Point(x, y);
	}

	private void updateViewport() {
		final Point prevP = toView(prevPosition);
		final Point p = toView(position);
		final int x = (int) (prevP.x + (p.x - prevP.x) * walkRatio);
		final int y = (int) (prevP.y + (p.y - prevP.y) * walkRatio);

		final int ph = field.getTile(prevPosition).elevation();
		final int h = field.getTile(position).elevation();
		final float rh = ph + (h - ph) * walkRatio;
		final int el = (int) (GraphicsConstants.TILE_DEPTH * rh);
		final int viewportX = -SwingClientConstants.VIEW_WIDTH / 2 + x;
		final int viewportY = -SwingClientConstants.VIEW_HEIGHT / 2 + y - el;
		screen.setViewport(viewportX, viewportY);
		chatView.setViewport(viewportX, viewportY);
	}

	private void clearEntities() {
		for (final Model model : modelMap.values()) {
			model.removeFrom(screen);
		}
		modelMap.clear();
	}

	public Model getModel(final FieldEntity entity) {
		return modelMap.get(entity);
	}

}
