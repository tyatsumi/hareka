package org.kareha.hareka.swingclient.gui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import javax.swing.WindowConstants;

import org.kareha.hareka.annotation.Private;
import org.kareha.hareka.client.ConnectionSettings;
import org.kareha.hareka.swingclient.Strap;
import org.kareha.hareka.swingclient.SwingClientConstants;
import org.kareha.hareka.swingclient.SwingClientUtil;

@SuppressWarnings("serial")
public class UpdaterDownloader extends JFrame {

	@Private
	static final Logger logger = Logger.getLogger(UpdaterDownloader.class.getName());
	private static final int BUFFER_SIZE = 8192;
	private static final int BAR_UPDATE_INTERVAL = 32;

	private enum BundleKey {
		Title, Message, Cancel,
	}

	@Private
	final Strap strap;
	@Private
	final ConnectionSettings.Entry connection;
	@Private
	final Worker worker;
	@Private
	final JProgressBar bar;

	public UpdaterDownloader(final Strap strap, final ConnectionSettings.Entry connection) {
		this.strap = strap;
		this.connection = connection;
		final ResourceBundle bundle = ResourceBundle.getBundle(UpdaterDownloader.class.getName());
		setTitle(bundle.getString(BundleKey.Title.name()));

		worker = new Worker();

		setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(final WindowEvent e) {
				worker.cancel(true);
				dispose();
				// strap.getGui().newConnectionFrame();
			}
		});

		final JLabel label = new JLabel(bundle.getString(BundleKey.Message.name()));
		final JPanel labelPanel = new JPanel(new FlowLayout(FlowLayout.CENTER, 16, 16));
		labelPanel.add(label);
		add(labelPanel, BorderLayout.NORTH);

		bar = new JProgressBar();
		bar.setIndeterminate(true);
		final JPanel barPanel = new JPanel(new FlowLayout(FlowLayout.CENTER, 16, 16));
		barPanel.add(bar);
		add(barPanel, BorderLayout.CENTER);

		final JButton cancelButton = new JButton(bundle.getString(BundleKey.Cancel.name()));
		cancelButton.addActionListener(e -> worker.cancel(true));
		final JPanel buttonPanel = new JPanel(new FlowLayout(FlowLayout.CENTER, 16, 16));
		buttonPanel.add(cancelButton);
		add(buttonPanel, BorderLayout.SOUTH);

		pack();
		setResizable(false);
		setLocationRelativeTo(null);
	}

	public void downloadAndRun() {
		worker.execute();
	}

	private class Worker extends SwingWorker<Void, Void> {

		@Private
		Worker() {

		}

		@Override
		protected Void doInBackground() {
			boolean result = false;
			try {
				final URL updaterUrl = new URL(connection.getUpdater());
				final URLConnection connection = updaterUrl.openConnection();
				final int contentLength = connection.getContentLength();
				if (contentLength != -1) {
					SwingUtilities.invokeLater(() -> {
						bar.setIndeterminate(false);
						bar.setMaximum(contentLength);
					});
				}
				try (final OutputStream out = new BufferedOutputStream(
						new FileOutputStream(SwingClientConstants.UPDATER_FILE))) {
					try (final InputStream in = new BufferedInputStream(connection.getInputStream())) {
						final byte[] buffer = new byte[BUFFER_SIZE];
						int total = 0;
						int count = 0;
						while (true) {
							if (Thread.currentThread().isInterrupted()) {
								return null;
							}
							final int length = in.read(buffer);
							if (length == -1) {
								break;
							}
							out.write(buffer, 0, length);
							total += length;
							count++;
							if (count % BAR_UPDATE_INTERVAL == 0) {
								if (contentLength != -1) {
									final int current = total;
									SwingUtilities.invokeLater(() -> bar.setValue(current));
								}
							}
						}
					}
					out.flush();
					result = true;
				}
			} catch (final IOException e) {
				logger.log(Level.SEVERE, "", e);
				JOptionPane.showMessageDialog(UpdaterDownloader.this, e.getMessage());
				cancel(true);
				return null;
			} finally {
				if (!result) {
					SwingClientConstants.UPDATER_FILE.delete();
				}
			}
			return null;
		}

		@Override
		protected void done() {
			try {
				if (isCancelled()) {
					return;
				}
				try {
					SwingClientUtil.runUpdater(strap.getContext().getDataDirectory(), connection.getClient(),
							strap.getParameters().raw());
					strap.exit();
				} catch (final IOException e) {
					logger.log(Level.SEVERE, "", e);
					JOptionPane.showMessageDialog(UpdaterDownloader.this, e.getMessage());
					return;
				}
			} finally {
				dispose();
				strap.getGui().newConnectionFrame();
			}
		}

	}

}
