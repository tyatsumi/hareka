package org.kareha.hareka.swingclient.gui;

import java.awt.datatransfer.Transferable;

import javax.swing.JComponent;
import javax.swing.TransferHandler;

@SuppressWarnings("serial")
public class ItemButtonTransferHandler extends TransferHandler {

	@Override
	public int getSourceActions(final JComponent c) {
		return MOVE;
	}

	@Override
	protected Transferable createTransferable(final JComponent c) {
		final ItemButton button = (ItemButton) c;
		return new ItemButtonValue(false, button.getValue().getEntry());
	}

	@Override
	protected void exportDone(final JComponent source, final Transferable data, final int action) {
		if (action == MOVE) {
			final ItemButton button = (ItemButton) source;
			button.getPanel().swap();
		}
	}

	@Override
	public boolean canImport(final TransferHandler.TransferSupport support) {
		if (!support.isDrop()) {
			return false;
		}
		return support.isDataFlavorSupported(ItemButtonValue.itemFlavor);
	}

	@Override
	public boolean importData(final TransferHandler.TransferSupport support) {
		if (!canImport(support)) {
			return false;
		}
		final ItemButton button = (ItemButton) support.getComponent();
		button.getPanel().setSwapButton(button);
		return true;
	}

}
