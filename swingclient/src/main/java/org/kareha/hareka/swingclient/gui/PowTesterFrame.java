package org.kareha.hareka.swingclient.gui;

import java.awt.BorderLayout;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.ResourceBundle;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.WindowConstants;

import org.kareha.hareka.swingclient.Strap;

@SuppressWarnings("serial")
public class PowTesterFrame extends JInternalFrame {

	private enum BundleKey {
		Title, Difficulty, DataLength, Randomness, Random, AllZero, Threads, ElapsedTime, Go, PowTest,
	}

	public PowTesterFrame(final Strap strap) {
		super("", false, true, false, false);
		setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		final ResourceBundle bundle = ResourceBundle.getBundle(PowTesterFrame.class.getName());
		setTitle(bundle.getString(BundleKey.Title.name()));

		final Gui gui = strap.getGui();

		final JPanel centerPanel = new JPanel();
		centerPanel.setLayout(new BoxLayout(centerPanel, BoxLayout.Y_AXIS));

		final JPanel difficultyPanel = new JPanel();
		difficultyPanel.add(new JLabel(bundle.getString(BundleKey.Difficulty.name())));
		final JTextField difficultyField = new JTextField(8);
		difficultyField.setText("22");
		difficultyPanel.add(difficultyField);
		centerPanel.add(difficultyPanel);

		final JPanel dataLengthPanel = new JPanel();
		dataLengthPanel.add(new JLabel(bundle.getString(BundleKey.DataLength.name())));
		final JTextField dataLengthField = new JTextField(8);
		dataLengthField.setText("32");
		dataLengthPanel.add(dataLengthField);
		centerPanel.add(dataLengthPanel);

		final JPanel randomnessPanel = new JPanel();
		randomnessPanel.add(new JLabel(bundle.getString(BundleKey.Randomness.name())));
		final String[] randomnessArray = new String[2];
		randomnessArray[0] = bundle.getString(BundleKey.Random.name());
		randomnessArray[1] = bundle.getString(BundleKey.AllZero.name());
		final JComboBox<String> randomnessComboBox = new JComboBox<>(randomnessArray);
		randomnessPanel.add(randomnessComboBox);
		centerPanel.add(randomnessPanel);

		final JPanel threadsPanel = new JPanel();
		threadsPanel.add(new JLabel(bundle.getString(BundleKey.Threads.name())));
		final JTextField threadsField = new JTextField(8);
		threadsField.setText(Integer.toString(Runtime.getRuntime().availableProcessors()));
		threadsPanel.add(threadsField);
		centerPanel.add(threadsPanel);

		final JPanel elapsedTimePanel = new JPanel();
		elapsedTimePanel.add(new JLabel(bundle.getString(BundleKey.ElapsedTime.name())));
		final JTextField elapsedTimeField = new JTextField(12);
		elapsedTimeField.setEditable(false);
		elapsedTimePanel.add(elapsedTimeField);
		centerPanel.add(elapsedTimePanel);

		final JButton goButton = new JButton(bundle.getString(BundleKey.Go.name()));
		goButton.addActionListener(e -> {
			final int difficulty = Integer.parseInt(difficultyField.getText());
			final int dataLength = Integer.parseInt(dataLengthField.getText());
			final byte[] data = new byte[dataLength];
			if (randomnessComboBox.getSelectedIndex() == 0) {
				new SecureRandom().nextBytes(data);
			}
			final int threads = Integer.parseInt(threadsField.getText());

			final long startTime = System.currentTimeMillis();

			final PowFrame.Handler handler = new PowFrame.Handler() {
				@Override
				public void performed(byte[] nonce) {
					final long elapsedTime = System.currentTimeMillis() - startTime;
					elapsedTimeField.setText(Float.toString(elapsedTime / 1000f));
				}

				@Override
				public void canceled() {
					elapsedTimeField.setText("");
				}
			};

			new PowFrame(strap, BigInteger.ONE.shiftLeft(256 - difficulty), data, threads, BundleKey.PowTest.name(),
					handler);
		});

		add(centerPanel, BorderLayout.CENTER);
		final JPanel goPanel = new JPanel();
		goPanel.add(goButton);
		add(goPanel, BorderLayout.SOUTH);
		pack();

		gui.getViewPane().add(this);
		gui.center(this);
		setVisible(true);
	}

}
