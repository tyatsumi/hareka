package org.kareha.hareka.swingclient.gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ResourceBundle;

import javax.swing.JCheckBoxMenuItem;
import javax.swing.JFileChooser;
import javax.swing.JInternalFrame;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;

import org.kareha.hareka.annotation.Private;
import org.kareha.hareka.swingclient.Strap;

@SuppressWarnings("serial")
public class TextFrame extends JInternalFrame {

	private static final int FRAME_WIDTH = 384;
	private static final int FRAME_HEIGHT = 256;
	private static final String DEFAULT_FILENAME = "text.txt";

	private enum BundleKey {
		Title, Edit, Save,
	}

	@Private
	final JTextArea textArea;
	private JPopupMenu menu;
	private JFileChooser fileChooser;

	public TextFrame(final Strap strap) {
		super("", true, true, true, true);
		final ResourceBundle bundle = ResourceBundle.getBundle(TextFrame.class.getName());
		setTitle(bundle.getString(BundleKey.Title.name()));

		textArea = new JTextArea();
		textArea.setEditable(false);
		textArea.setLineWrap(true);

		textArea.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(final MouseEvent e) {
				if (e.isPopupTrigger()) {
					getMenu().show(textArea, e.getX(), e.getY());
				}
			}

			@Override
			public void mouseReleased(final MouseEvent e) {
				if (e.isPopupTrigger()) {
					getMenu().show(textArea, e.getX(), e.getY());
				}
			}
		});

		final JScrollPane scrollPane = new JScrollPane(textArea, ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS,
				ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

		add(scrollPane, BorderLayout.CENTER);
		setPreferredSize(new Dimension(FRAME_WIDTH, FRAME_HEIGHT));
		pack();
	}

	@Private
	JPopupMenu getMenu() {
		if (menu == null) {
			menu = new JPopupMenu();
			final ResourceBundle bundle = ResourceBundle.getBundle(TextFrame.class.getName());

			final JCheckBoxMenuItem editItem = new JCheckBoxMenuItem(bundle.getString(BundleKey.Edit.name()));
			editItem.addActionListener(e -> {
				textArea.setEditable(editItem.isSelected());
			});
			menu.add(editItem);

			final JMenuItem saveItem = new JMenuItem(bundle.getString(BundleKey.Save.name()));
			saveItem.addActionListener(e -> {
				if (fileChooser == null) {
					fileChooser = new JFileChooser();
					fileChooser.setSelectedFile(new File(DEFAULT_FILENAME));
				}
				if (fileChooser.showSaveDialog(TextFrame.this) != JFileChooser.APPROVE_OPTION) {
					return;
				}
				try (final OutputStream out = new FileOutputStream(fileChooser.getSelectedFile())) {
					out.write(textArea.getText().getBytes("UTF-8"));
				} catch (final IOException ex) {
					JOptionPane.showMessageDialog(TextFrame.this, ex.getMessage());
					return;
				}
			});
			menu.add(saveItem);
		}
		return menu;
	}

	public void setText(final String text) {
		textArea.setText(text);
	}

}
