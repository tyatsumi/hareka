package org.kareha.hareka.swingclient.gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.util.ResourceBundle;

import javax.swing.BoxLayout;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.WindowConstants;

import org.kareha.hareka.swingclient.Strap;
import org.kareha.hareka.user.UserRegistrationMode;

@SuppressWarnings("serial")
public class ServerSettingsFrame extends JInternalFrame {

	private static final int FRAME_WIDTH = 512;
	private static final int FRAME_HEIGHT = 384;

	private enum BundleKey {
		Title, NotConnected,

		UserRegistrationMode, RescueMethodsEnabled,
	}

	private final JComboBox<UserRegistrationMode> userRegistrationModeComboBox;
	private final JCheckBox rescueMethodsEnabledCheckBox;
	private boolean tempDisabled;

	public ServerSettingsFrame(final Strap strap) {
		super("", true, true, true, true);
		setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
		final ResourceBundle bundle = ResourceBundle.getBundle(ServerSettingsFrame.class.getName());
		setTitle(bundle.getString(BundleKey.Title.name()));

		final JPanel userRegistrationModePanel = new JPanel();
		userRegistrationModePanel.add(new JLabel(bundle.getString(BundleKey.UserRegistrationMode.name())));
		userRegistrationModeComboBox = new JComboBox<>(UserRegistrationMode.values());
		userRegistrationModeComboBox.addActionListener(e -> {
			if (tempDisabled) {
				return;
			}
			final int index = userRegistrationModeComboBox.getSelectedIndex();
			if (index == -1) {
				return;
			}
			final UserRegistrationMode mode = userRegistrationModeComboBox.getItemAt(index);
			if (!strap.isConnected()) {
				JOptionPane.showInternalMessageDialog(this, bundle.getString(BundleKey.NotConnected.name()));
				return;
			}
			strap.adminControl().setUserRegistrationMode(mode);
		});
		userRegistrationModePanel.add(userRegistrationModeComboBox);

		final JPanel rescueMethodsEnabledPanel = new JPanel();
		rescueMethodsEnabledPanel.add(new JLabel(bundle.getString(BundleKey.RescueMethodsEnabled.name())));
		rescueMethodsEnabledCheckBox = new JCheckBox();
		rescueMethodsEnabledCheckBox.addActionListener(e -> {
			if (tempDisabled) {
				return;
			}
			final boolean enabled = rescueMethodsEnabledCheckBox.isSelected();
			if (!strap.isConnected()) {
				JOptionPane.showInternalMessageDialog(this, bundle.getString(BundleKey.NotConnected.name()));
				return;
			}
			strap.adminControl().setRescueMethodsEnabled(enabled);
		});
		rescueMethodsEnabledPanel.add(rescueMethodsEnabledCheckBox);

		final JPanel centerPanel = new JPanel();
		centerPanel.setLayout(new BoxLayout(centerPanel, BoxLayout.Y_AXIS));
		centerPanel.add(userRegistrationModePanel);
		centerPanel.add(rescueMethodsEnabledPanel);

		add(centerPanel, BorderLayout.CENTER);
		setPreferredSize(new Dimension(FRAME_WIDTH, FRAME_HEIGHT));
		pack();
	}

	public void setSettings(final UserRegistrationMode userRegistrationMode, final boolean resqueMethodsEnabled) {
		tempDisabled = true;
		userRegistrationModeComboBox.setSelectedItem(userRegistrationMode);
		rescueMethodsEnabledCheckBox.setSelected(resqueMethodsEnabled);
		tempDisabled = false;
	}

}
