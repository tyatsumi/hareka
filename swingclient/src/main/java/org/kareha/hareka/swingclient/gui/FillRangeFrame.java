package org.kareha.hareka.swingclient.gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.util.ResourceBundle;
import java.util.function.Supplier;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.WindowConstants;

import org.kareha.hareka.client.field.FieldEntity;
import org.kareha.hareka.field.OneUniformTilePattern;
import org.kareha.hareka.field.Placement;
import org.kareha.hareka.field.SolidTilePattern;
import org.kareha.hareka.field.Tile;
import org.kareha.hareka.field.TilePattern;
import org.kareha.hareka.field.TilePatternType;
import org.kareha.hareka.field.Vector;
import org.kareha.hareka.swingclient.Strap;

@SuppressWarnings("serial")
public class FillRangeFrame extends JInternalFrame {

	private static final int FRAME_WIDTH = 384;
	private static final int FRAME_HEIGHT = 384;

	private enum BundleKey {
		Title, NotConnected,

		Center, CurrentPosition, Size, Type,

		Draw, ForceDraw, Fill, ForceFill,
	}

	public FillRangeFrame(final Strap strap) {
		super("", true, true, true, true);
		setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
		final ResourceBundle bundle = ResourceBundle.getBundle(FillRangeFrame.class.getName());
		setTitle(bundle.getString(BundleKey.Title.name()));

		final JPanel northPanel = new JPanel();
		northPanel.setLayout(new BoxLayout(northPanel, BoxLayout.Y_AXIS));
		final JPanel positionPanel = new JPanel();
		positionPanel.add(new JLabel(bundle.getString(BundleKey.Center.name())));
		positionPanel.add(new JLabel("X"));
		final JTextField xField = new JTextField("0", 6);
		positionPanel.add(xField);
		positionPanel.add(new JLabel("Y"));
		final JTextField yField = new JTextField("0", 6);
		positionPanel.add(yField);
		final Runnable currentPositionRunnable = () -> {
			if (!strap.isConnected()) {
				return;
			}
			final FieldEntity entity = strap.control().getSelfFieldEntity();
			if (entity == null) {
				return;
			}
			final Placement placement = entity.getPlacement();
			xField.setText(Integer.toString(placement.getPosition().x()));
			yField.setText(Integer.toString(placement.getPosition().y()));
		};
		currentPositionRunnable.run();
		final JButton currentPositionButton = new JButton(bundle.getString(BundleKey.CurrentPosition.name()));
		currentPositionButton.addActionListener(e -> currentPositionRunnable.run());
		positionPanel.add(currentPositionButton);
		northPanel.add(positionPanel);
		final JPanel sizePanel = new JPanel();
		sizePanel.add(new JLabel(bundle.getString(BundleKey.Size.name())));
		final JTextField sizeField = new JTextField("4", 6);
		sizePanel.add(sizeField);
		northPanel.add(sizePanel);

		final JPanel typePanel = new JPanel();
		typePanel.add(new JLabel(bundle.getString(BundleKey.Type.name())));
		final JComboBox<TilePatternType> typeComboBox = new JComboBox<>(TilePatternType.values());
		typePanel.add(typeComboBox);

		final JPanel tilePanel = new JPanel();
		tilePanel.setLayout(new BoxLayout(tilePanel, BoxLayout.Y_AXIS));
		final JPanel tilePanelA = new JPanel();
		final TileSelector tileSelectorA = new TileSelector(strap);
		tilePanelA.add(tileSelectorA);
		final JTextField elevationFieldA = new JTextField("0", 6);
		tilePanelA.add(elevationFieldA);
		tilePanel.add(tilePanelA);
		final JPanel tilePanelB = new JPanel();
		final TileSelector tileSelectorB = new TileSelector(strap);
		tilePanelB.add(tileSelectorB);
		final JTextField elevationFieldB = new JTextField("0", 6);
		tilePanelB.add(elevationFieldB);
		tilePanel.add(tilePanelB);
		final JPanel tilePanelC = new JPanel();
		final TileSelector tileSelectorC = new TileSelector(strap);
		tilePanelC.add(tileSelectorC);
		final JTextField elevationFieldC = new JTextField("0", 6);
		tilePanelC.add(elevationFieldC);
		tilePanel.add(tilePanelC);

		tilePanelB.setVisible(false);
		tilePanelC.setVisible(false);

		typeComboBox.addActionListener(e -> {
			switch (typeComboBox.getItemAt(typeComboBox.getSelectedIndex())) {
			case SOLID:
				tilePanelA.setVisible(true);
				tilePanelB.setVisible(false);
				tilePanelC.setVisible(false);
				break;
			case ONE_UNIFORM:
				tilePanelA.setVisible(true);
				tilePanelB.setVisible(true);
				tilePanelC.setVisible(true);
				break;
			}
		});

		final JPanel centerPanel = new JPanel();
		centerPanel.setLayout(new BoxLayout(centerPanel, BoxLayout.Y_AXIS));
		centerPanel.add(typePanel);
		centerPanel.add(tilePanel);

		final Supplier<TilePattern> tilePatternSupplier = () -> {
			final TilePatternType type = typeComboBox.getItemAt(typeComboBox.getSelectedIndex());
			final int elevationA;
			final int elevationB;
			final int elevationC;
			try {
				elevationA = Integer.parseInt(elevationFieldA.getText());
				elevationB = Integer.parseInt(elevationFieldB.getText());
				elevationC = Integer.parseInt(elevationFieldC.getText());
			} catch (final NumberFormatException ex) {
				JOptionPane.showInternalMessageDialog(this, ex.getMessage());
				return null;
			}
			final Tile tileA = Tile.valueOf(tileSelectorA.getTile(), elevationA);
			final Tile tileB = Tile.valueOf(tileSelectorB.getTile(), elevationB);
			final Tile tileC = Tile.valueOf(tileSelectorC.getTile(), elevationC);
			switch (type) {
			default:
				return null;
			case SOLID:
				return new SolidTilePattern(tileA);
			case ONE_UNIFORM:
				return new OneUniformTilePattern(tileA, tileB, tileC);
			}
		};

		final JPanel fillButtonPanel = new JPanel();
		final JButton fillButton = new JButton(bundle.getString(BundleKey.Fill.name()));
		fillButton.addActionListener(e -> {
			if (!strap.isConnected()) {
				JOptionPane.showInternalMessageDialog(this, bundle.getString(BundleKey.NotConnected.name()));
				return;
			}
			final Vector center;
			final int size;
			try {
				center = Vector.valueOf(Integer.parseInt(xField.getText()), Integer.parseInt(yField.getText()));
				size = Integer.parseInt(sizeField.getText());
			} catch (final NumberFormatException ex) {
				JOptionPane.showInternalMessageDialog(this, ex.getMessage());
				return;
			}
			final TilePattern tilePattern = tilePatternSupplier.get();
			if (tilePattern == null) {
				return;
			}
			strap.editControl().fillRange(center, size, tilePattern, false);
		});
		fillButtonPanel.add(fillButton);
		final JButton forceFillButton = new JButton(bundle.getString(BundleKey.ForceFill.name()));
		forceFillButton.addActionListener(e -> {
			if (!strap.isConnected()) {
				JOptionPane.showInternalMessageDialog(this, bundle.getString(BundleKey.NotConnected.name()));
				return;
			}
			final Vector center;
			final int size;
			try {
				center = Vector.valueOf(Integer.parseInt(xField.getText()), Integer.parseInt(yField.getText()));
				size = Integer.parseInt(sizeField.getText());
			} catch (final NumberFormatException ex) {
				JOptionPane.showInternalMessageDialog(this, ex.getMessage());
				return;
			}
			final TilePattern tilePattern = tilePatternSupplier.get();
			if (tilePattern == null) {
				return;
			}
			strap.editControl().fillRange(center, size, tilePattern, true);
		});
		fillButtonPanel.add(forceFillButton);

		final JPanel drawButtonPanel = new JPanel();
		final JButton drawButton = new JButton(bundle.getString(BundleKey.Draw.name()));
		drawButton.addActionListener(e -> {
			if (!strap.isConnected()) {
				JOptionPane.showInternalMessageDialog(this, bundle.getString(BundleKey.NotConnected.name()));
				return;
			}
			final Vector center;
			final int size;
			try {
				center = Vector.valueOf(Integer.parseInt(xField.getText()), Integer.parseInt(yField.getText()));
				size = Integer.parseInt(sizeField.getText());
			} catch (final NumberFormatException ex) {
				JOptionPane.showInternalMessageDialog(this, ex.getMessage());
				return;
			}
			final TilePattern tilePattern = tilePatternSupplier.get();
			if (tilePattern == null) {
				return;
			}
			strap.editControl().drawRing(center, size, tilePattern, false);
		});
		drawButtonPanel.add(drawButton);
		final JButton forceDrawButton = new JButton(bundle.getString(BundleKey.ForceDraw.name()));
		forceDrawButton.addActionListener(e -> {
			if (!strap.isConnected()) {
				JOptionPane.showInternalMessageDialog(this, bundle.getString(BundleKey.NotConnected.name()));
				return;
			}
			final Vector center;
			final int size;
			try {
				center = Vector.valueOf(Integer.parseInt(xField.getText()), Integer.parseInt(yField.getText()));
				size = Integer.parseInt(sizeField.getText());
			} catch (final NumberFormatException ex) {
				JOptionPane.showInternalMessageDialog(this, ex.getMessage());
				return;
			}
			final TilePattern tilePattern = tilePatternSupplier.get();
			if (tilePattern == null) {
				return;
			}
			strap.editControl().drawRing(center, size, tilePattern, true);
		});
		drawButtonPanel.add(forceDrawButton);

		final JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.Y_AXIS));
		buttonPanel.add(fillButtonPanel);
		buttonPanel.add(drawButtonPanel);

		add(northPanel, BorderLayout.NORTH);
		add(centerPanel, BorderLayout.CENTER);
		add(buttonPanel, BorderLayout.SOUTH);
		setPreferredSize(new Dimension(FRAME_WIDTH, FRAME_HEIGHT));
		pack();
	}

}
