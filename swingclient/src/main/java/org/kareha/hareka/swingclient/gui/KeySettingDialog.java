package org.kareha.hareka.swingclient.gui;

import java.awt.BorderLayout;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ResourceBundle;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.kareha.hareka.annotation.Private;
import org.kareha.hareka.swingclient.keyboard.VirtualKey;

public final class KeySettingDialog {

	private enum BundleKey {
		Title, Message, Ok, Cancel,
	}

	@Private
	boolean canceled = true;
	@Private
	int keyCode = -1;

	public KeySettingDialog(final Frame owner) {
		final JDialog dialog = new JDialog(owner);

		dialog.setModal(true);
		final ResourceBundle bundle = ResourceBundle.getBundle(KeySettingDialog.class.getName());
		dialog.setTitle(bundle.getString(BundleKey.Title.name()));

		final JTextField field = new JTextField(bundle.getString(BundleKey.Message.name()));
		field.addKeyListener(new KeyListener() {
			@Override
			public void keyPressed(final KeyEvent e) {
				keyCode = e.getKeyCode();
				field.setText(VirtualKey.toName(keyCode));
			}

			@Override
			public void keyReleased(final KeyEvent e) {
				field.setText(VirtualKey.toName(keyCode));
			}

			@Override
			public void keyTyped(final KeyEvent e) {
				field.setText(VirtualKey.toName(keyCode));
			}
		});

		final JButton okButton = new JButton(bundle.getString(BundleKey.Ok.name()));
		okButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				if (keyCode != -1) {
					canceled = false;
				}
				dialog.setVisible(false);
			}
		});

		final JButton cancelButton = new JButton(bundle.getString(BundleKey.Cancel.name()));
		cancelButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				dialog.setVisible(false);
			}
		});

		dialog.add(field, BorderLayout.CENTER);
		JPanel buttonPanel = new JPanel();
		buttonPanel.add(okButton);
		buttonPanel.add(cancelButton);
		dialog.add(buttonPanel, BorderLayout.SOUTH);
		dialog.pack();

		dialog.setLocationRelativeTo(owner);
		dialog.setVisible(true);
	}

	public boolean isCanceled() {
		return canceled;
	}

	public int getKeyCode() {
		return keyCode;
	}

}
