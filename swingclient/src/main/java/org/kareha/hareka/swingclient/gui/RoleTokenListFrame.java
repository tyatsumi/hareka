package org.kareha.hareka.swingclient.gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.util.Date;
import java.util.ResourceBundle;

import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JInternalFrame;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.WindowConstants;

import org.kareha.hareka.client.user.RoleToken;
import org.kareha.hareka.swingclient.Strap;

@SuppressWarnings("serial")
public class RoleTokenListFrame extends JInternalFrame {

	private static final int FRAME_WIDTH = 384;
	private static final int FRAME_HEIGHT = 256;

	private enum BundleKey {

		Title, Delete, NotConnected,

	}

	private final Strap strap;
	private final DefaultListModel<RoleTokenEntry> listModel;

	public RoleTokenListFrame(final Strap strap) {
		super("", true, true, true, true);
		this.strap = strap;
		setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
		final ResourceBundle bundle = ResourceBundle.getBundle(RoleTokenListFrame.class.getName());
		setTitle(bundle.getString(BundleKey.Title.name()));

		final JPanel centerPanel = new JPanel();
		centerPanel.setLayout(new BoxLayout(centerPanel, BoxLayout.Y_AXIS));
		listModel = new DefaultListModel<>();
		final JList<RoleTokenEntry> tokenList = new JList<>(listModel);
		final JScrollPane tokenListScrollPane = new JScrollPane(tokenList,
				ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		centerPanel.add(tokenListScrollPane);

		final JPanel southPanel = new JPanel();
		final JButton deleteButton = new JButton(bundle.getString(BundleKey.Delete.name()));
		deleteButton.addActionListener(e -> {
			final RoleTokenEntry entry = tokenList.getSelectedValue();
			if (entry == null) {
				return;
			}
			if (!strap.isConnected()) {
				JOptionPane.showInternalMessageDialog(this, bundle.getString(BundleKey.NotConnected.name()));
				return;
			}
			strap.adminControl().deleteRoleToken(entry.getRoleToken().getId());
		});
		southPanel.add(deleteButton);

		add(centerPanel, BorderLayout.CENTER);
		add(southPanel, BorderLayout.SOUTH);
		setPreferredSize(new Dimension(FRAME_WIDTH, FRAME_HEIGHT));
		pack();
	}

	public void updateRoleTokenList() {
		if (!strap.isConnected()) {
			return;
		}
		listModel.clear();
		for (final RoleToken token : strap.adminControl().getRoleTokenList()) {
			listModel.addElement(new RoleTokenEntry(token));
		}
	}

	private static class RoleTokenEntry {

		private final RoleToken token;

		RoleTokenEntry(final RoleToken token) {
			this.token = token;
		}

		RoleToken getRoleToken() {
			return token;
		}

		@Override
		public String toString() {
			final Date issuedDate = new Date(token.getIssuedDate());
			return token.getRoleId() + " (" + issuedDate + ")";
		}

	}

}
