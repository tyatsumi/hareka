package org.kareha.hareka.swingclient.gui;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ResourceBundle;

import javax.swing.JComponent;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.ToolTipManager;
import javax.swing.TransferHandler;

import org.kareha.hareka.annotation.Private;
import org.kareha.hareka.swingclient.Strap;
import org.kareha.hareka.swingclient.SwingClientConstants;

@SuppressWarnings("serial")
public final class SkillButton extends JComponent {

	private enum BundleKey {
		Use, SetShortcut,
	}

	private final Strap strap;
	private final SkillsPanel panel;
	private final int index;
	private SkillButtonValue value;
	private final BufferedImage backgroundImage;
	private final BufferedImage cursorImage;

	public SkillButton(final Strap strap, final SkillsPanel panel, final int index) {
		this.strap = strap;
		this.panel = panel;
		this.index = index;

		addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(final MouseEvent e) {
				if (e.getButton() == MouseEvent.BUTTON1) {
					panel.setSelectedButton(SkillButton.this);
					useSkill();
				}
			}

			@Override
			public void mousePressed(final MouseEvent e) {
				if (e.isPopupTrigger()) {
					createMenu().show(SkillButton.this, e.getX(), e.getY());
				}
			}

			@Override
			public void mouseReleased(final MouseEvent e) {
				if (e.isPopupTrigger()) {
					createMenu().show(SkillButton.this, e.getX(), e.getY());
				}
			}
		});
		addMouseMotionListener(new MouseMotionAdapter() {
			@Override
			public void mouseDragged(final MouseEvent e) {
				panel.setDragButton(SkillButton.this);
				final JComponent c = (JComponent) e.getSource();
				final TransferHandler th = c.getTransferHandler();
				th.exportAsDrag(c, e, TransferHandler.MOVE);
			}
		});

		setTransferHandler(new SkillButtonTransferHandler());

		setPreferredSize(new Dimension(SwingClientConstants.ICON_WIDTH, SwingClientConstants.ICON_HEIGHT));

		ToolTipManager.sharedInstance().registerComponent(this);

		try {
			backgroundImage = strap.getLoader().getImageLoader().load("skill.png");
			cursorImage = strap.getLoader().getImageLoader().load("icon_cursor.png");
		} catch (final IOException e) {
			throw new RuntimeException(e.getMessage(), e);
		}
	}

	private boolean isActive() {
		// return index < panel.getSkillsSize();
		return true;
	}

	SkillsPanel getPanel() {
		return panel;
	}

	public int getIndex() {
		return index;
	}

	public SkillButtonValue getValue() {
		return value;
	}

	public void setValue(final SkillButtonValue v) {
		value = v;
		repaint();
	}

	public void useSkill() {
		if (value == null) {
			return;
		}
		switch (value.getEntry().getType().getTargetType()) {
		default:
			return;
		case NULL:
			strap.getGui().getViewPane().doNullSkill(value.getEntry());
			break;
		case FIELD_ENTITY:
			strap.getGui().getViewPane().startAction(value.getEntry(), true, false);
			break;
		case TILE:
			strap.getGui().getViewPane().doTileSkill(value.getEntry());
			break;
		}
	}

	@Private
	JPopupMenu createMenu() {
		JPopupMenu menu = new JPopupMenu();

		JMenuItem item;
		final ResourceBundle bundle = ResourceBundle.getBundle(SkillButton.class.getName());
		item = new JMenuItem(bundle.getString(BundleKey.Use.name()));
		item.addActionListener(e -> useSkill());
		menu.add(item);

		JMenu m = new JMenu(bundle.getString(BundleKey.SetShortcut.name()));
		for (int i = 0, n = strap.getGui().getShortcutsFrame().getPanel().getShortcutsSize(); i < n; i++) {
			JMenuItem mi = new JMenuItem(Integer.toString(1 + i));
			final int fi = i;
			mi.addActionListener(e -> {
				if (value == null) {
					return;
				}
				final ShortcutsPanel p = strap.getGui().getShortcutsFrame().getPanel();
				p.getButton(fi).setValue(value);
				p.getButton(fi).setRepeating(true);
				p.getButton(fi).setSelfTargetting(false);
				p.getButton(fi).send();
				p.getButton(fi).repaint();
			});
			m.add(mi);
		}
		menu.add(m);

		return menu;
	}

	@Override
	protected void paintComponent(final Graphics g) {
		if (isActive()) {
			g.drawImage(backgroundImage, getSize().width / 2 - backgroundImage.getWidth() / 2,
					getSize().height / 2 - backgroundImage.getHeight() / 2, null);
		}

		final int cx = getSize().width / 2;
		final int cy = getSize().height / 2;

		if (panel.getSelectedButton() == this) {
			g.drawImage(cursorImage, getSize().width / 2 - cursorImage.getWidth() / 2,
					getSize().height / 2 - cursorImage.getHeight() / 2, null);
		}

		if (value != null) {
			strap.getLoader().getIconPainterTable().get(value.getIconName()).paint(g, cx, cy);
		}
	}

	@Override
	public String getToolTipText(final MouseEvent e) {
		if (value == null) {
			return null;
		}
		return value.getName();
	}

}
