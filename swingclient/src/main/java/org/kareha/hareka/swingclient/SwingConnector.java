package org.kareha.hareka.swingclient;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.math.BigInteger;
import java.net.Socket;
import java.net.UnknownHostException;
import java.security.KeyManagementException;
import java.security.KeyPair;
import java.security.PublicKey;
import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.List;
import java.util.ResourceBundle;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.net.ssl.SSLHandshakeException;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.X509TrustManager;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.xml.bind.JAXBException;

import org.kareha.hareka.LocalEntityId;
import org.kareha.hareka.LocalSessionId;
import org.kareha.hareka.LocalUserId;
import org.kareha.hareka.annotation.Private;
import org.kareha.hareka.client.ConnectionSettings;
import org.kareha.hareka.client.Connector;
import org.kareha.hareka.client.ResponseHandler;
import org.kareha.hareka.client.WorldSession;
import org.kareha.hareka.client.chat.ChatSession;
import org.kareha.hareka.client.field.ClientField;
import org.kareha.hareka.client.field.FieldEntity;
import org.kareha.hareka.client.field.PositionMemory;
import org.kareha.hareka.client.mirror.ActiveSkillMirror;
import org.kareha.hareka.client.mirror.AdminMirror;
import org.kareha.hareka.client.mirror.ChatMirror;
import org.kareha.hareka.client.mirror.EntityMirror;
import org.kareha.hareka.client.mirror.FieldMirror;
import org.kareha.hareka.client.mirror.GroupMirror;
import org.kareha.hareka.client.mirror.InventoryMirror;
import org.kareha.hareka.client.mirror.SelfMirror;
import org.kareha.hareka.client.mirror.SystemMirror;
import org.kareha.hareka.client.mirror.UserMirror;
import org.kareha.hareka.client.mirror.UserMirror.CharacterEntry;
import org.kareha.hareka.client.user.RoleToken;
import org.kareha.hareka.field.Placement;
import org.kareha.hareka.field.Region;
import org.kareha.hareka.field.TilePiece;
import org.kareha.hareka.field.Vector;
import org.kareha.hareka.game.Name;
import org.kareha.hareka.graphics.model.Model;
import org.kareha.hareka.key.SimpleTrustManager;
import org.kareha.hareka.protocol.HandlerTable;
import org.kareha.hareka.protocol.WorldClientPacketType;
import org.kareha.hareka.swingclient.gui.CertificatePanel;
import org.kareha.hareka.swingclient.gui.ChatPanel.ChannelItem;
import org.kareha.hareka.swingclient.gui.Gui;
import org.kareha.hareka.swingclient.gui.ItemButtonValue;
import org.kareha.hareka.swingclient.gui.PowFrame;
import org.kareha.hareka.swingclient.gui.SkillButtonValue;
import org.kareha.hareka.swingclient.gui.UpdaterDownloader;
import org.kareha.hareka.swingclient.gui.View;
import org.kareha.hareka.user.Role;
import org.kareha.hareka.user.RoleSet;
import org.kareha.hareka.user.UserRegistrationMode;

public final class SwingConnector {

	@Private
	static final Logger logger = Logger.getLogger(SwingConnector.class.getName());

	private enum BundleKey {
		FailedToConnect, NoServerSelected, AcceptCertificate, FailedToHandshake, VersionMismatch, UpdateClientNow,
	}

	private SwingConnector() {
		throw new AssertionError();
	}

	public static void connect(final Strap strap) {
		if (!connectHelper(strap)) {
			strap.getGui().newConnectionFrame();
		}
	}

	@SuppressWarnings("resource")
	private static boolean connectHelper(final Strap strap) {
		final Gui gui = strap.getGui();
		final ResourceBundle bundle = ResourceBundle.getBundle(SwingConnector.class.getName());
		final ConnectionSettings.Entry entry = strap.getContext().getConnectionSettings().getSelectedEntry();
		final Socket socket;
		if (strap.getParameters().noTls()) {
			try {
				socket = Connector.connectPlainly(entry.getHost(), entry.getPort());
			} catch (final UnknownHostException e) {
				logger.log(Level.WARNING, "", e);
				JOptionPane.showMessageDialog(gui.getViewPane(), bundle.getString(BundleKey.FailedToConnect.name())
						+ System.getProperty("line.separator") + e.getMessage());
				return false;
			} catch (final IOException e) {
				logger.log(Level.WARNING, "", e);
				JOptionPane.showMessageDialog(gui.getViewPane(), bundle.getString(BundleKey.FailedToConnect.name())
						+ System.getProperty("line.separator") + e.getMessage());
				return false;
			}
		} else {
			final X509TrustManager trustManager = new SimpleTrustManager(strap.getContext().getSimpleKeyStore(),
					entry.getHost()) {
				// This method is invoked in current thread i.e. Swing event
				// thread
				@Override
				protected boolean acceptCertificate(final X509Certificate cert) {
					final CertificatePanel panel = new CertificatePanel(gui.getViewPane(), cert);
					final int result = JOptionPane.showConfirmDialog(gui.getViewPane(), panel);
					return result == JOptionPane.YES_OPTION;
				}
			};
			try {
				socket = Connector.connectSecurely(entry.getHost(), entry.getPort(), trustManager);
			} catch (final UnknownHostException e) {
				logger.log(Level.WARNING, "", e);
				JOptionPane.showMessageDialog(gui.getViewPane(), bundle.getString(BundleKey.FailedToConnect.name())
						+ System.getProperty("line.separator") + e.getMessage());
				return false;
			} catch (final SecurityException e) {
				logger.log(Level.WARNING, "", e);
				JOptionPane.showMessageDialog(gui.getViewPane(), bundle.getString(BundleKey.FailedToConnect.name())
						+ System.getProperty("line.separator") + e.getMessage());
				return false;
			} catch (final SSLHandshakeException e) {
				JOptionPane.showMessageDialog(gui.getViewPane(), bundle.getString(BundleKey.FailedToHandshake.name()));
				return false;
			} catch (final IOException e) {
				logger.log(Level.WARNING, "", e);
				JOptionPane.showMessageDialog(gui.getViewPane(), bundle.getString(BundleKey.FailedToConnect.name())
						+ System.getProperty("line.separator") + e.getMessage());
				return false;
			} catch (final KeyManagementException e) {
				logger.log(Level.WARNING, "", e);
				JOptionPane.showMessageDialog(gui.getViewPane(), bundle.getString(BundleKey.FailedToConnect.name())
						+ System.getProperty("line.separator") + e.getMessage());
				return false;
			}
		}

		// handshake here
		// If this handshake is not done and the certificate is not accepted, an
		// exception will be thrown later in PacketSocket.
		if (socket instanceof SSLSocket) {
			final SSLSocket sslSocket = (SSLSocket) socket;
			try {
				sslSocket.startHandshake();
			} catch (final SSLHandshakeException e) {
				JOptionPane.showMessageDialog(gui.getViewPane(), bundle.getString(BundleKey.FailedToHandshake.name()));
				try {
					sslSocket.close();
				} catch (final IOException e1) {
					logger.log(Level.WARNING, "", e1);
				}
				return false;
			} catch (final IOException e) {
				logger.log(Level.WARNING, "", e);
				JOptionPane.showMessageDialog(gui.getViewPane(), bundle.getString(BundleKey.FailedToConnect.name())
						+ System.getProperty("line.separator") + e.getMessage());
				try {
					sslSocket.close();
				} catch (final IOException e1) {
					logger.log(Level.WARNING, "", e1);
				}
				return false;
			}
		}

		final HandlerTable<WorldClientPacketType, WorldSession> parserTable = new HandlerTable<>(
				"org.kareha.hareka.client.handler", WorldClientPacketType.class, "Handler");
		final WorldSession session = WorldSession.newInstance(strap.getContext(), entry, socket, parserTable);
		session.getMirrors().getSystemMirror().addListener(new SystemMirror.Listener() {
			@Override
			public void versionChecked(final boolean matched) {
				if (matched) {
					SwingUtilities.invokeLater(() -> {
						strap.getSoundContext().getMusicPlayer().play("login", true);
					});
				} else {
					SwingUtilities.invokeLater(() -> {
						JOptionPane.showMessageDialog(gui.getViewPane(),
								bundle.getString(BundleKey.VersionMismatch.name()));
						strap.disconnect();
						final int result = JOptionPane.showConfirmDialog(gui.getViewPane(),
								bundle.getString(BundleKey.UpdateClientNow.name()));
						if (result == JOptionPane.YES_OPTION) {
							final UpdaterDownloader frame = new UpdaterDownloader(strap, entry);
							frame.setVisible(true);
							frame.downloadAndRun();
						} else {
							gui.newConnectionFrame();
						}
					});
				}
			}

			@Override
			public void exitting(final String message) {
				try {
					SwingUtilities.invokeAndWait(() -> JOptionPane.showMessageDialog(gui.getViewPane(), message));
				} catch (final InvocationTargetException e) {
					logger.log(Level.WARNING, "", e);
					return;
				} catch (final InterruptedException e) {
					Thread.currentThread().interrupt();
					return;
				}
				SwingUtilities.invokeLater(() -> {
					strap.disconnect();
					gui.newConnectionFrame();
				});
			}

			@Override
			public void informed(final String message) {
				SwingUtilities.invokeLater(() -> gui.showInformation(message));
			}

			@Override
			public void messageArrived(final String message) {
				SwingUtilities.invokeLater(() -> gui.addMessage(message));
			}

			@Override
			public void textArrived(final String text) {
				SwingUtilities.invokeLater(() -> gui.showText(text));
			}

			@Override
			public void peerKeyArrived(final PublicKey key) {
				SwingUtilities.invokeLater(() -> login(strap));
			}

			@Override
			public void tokenRequested(final int handlerId, final String message) {
				SwingUtilities.invokeLater(() -> gui.requestToken(handlerId, message));
			}

			@Override
			public void generatePow(final int handlerId, final BigInteger target, final byte[] data,
					final String message) {
				SwingUtilities.invokeLater(() -> {
					final PowFrame.Handler handler = new PowFrame.Handler() {
						@Override
						public void performed(final byte[] nonce) {
							strap.control().pow(handlerId, nonce);
						}

						@Override
						public void canceled() {
							strap.control().cancelPow(handlerId);
						}
					};
					new PowFrame(strap, target, data, message, handler);
				});
			}

			@Override
			public void resourceServer(final String host, final int port, final boolean connectSecurely,
					final LocalSessionId sessionKey) {
				// TODO connect to the resource server
			}

			@Override
			public void positionMemoryAdded(final PositionMemory positionMemory) {
				SwingUtilities.invokeLater(() -> gui.getPositionMemoryFrame().addPositionMemory(positionMemory));
			}
		});
		session.getMirrors().getSystemMirror().setKeyPairProvider(new SystemMirror.KeyPairProvider() {
			@Override
			public KeyPair getKeyPair(final int version) {
				final AtomicReference<KeyPair> result = new AtomicReference<>();
				try {
					SwingUtilities.invokeAndWait(() -> {
						try {
							result.set(SwingClientUtil.getKeyPair(strap, version));
						} catch (final JAXBException e) {
							result.set(null);
						}
					});
				} catch (final InvocationTargetException | InterruptedException e) {
					logger.log(Level.SEVERE, "", e);
				}
				return result.get();
			}
		});
		session.getMirrors().getGroupMirror().addListener(new GroupMirror.Listener() {
			@Override
			public void inspected(final LocalEntityId chatLocalId, final LocalEntityId fieldLocalId,
					final List<LocalUserId> userIds) {
				SwingUtilities.invokeLater(() -> {
					gui.showInspection(chatLocalId, fieldLocalId, userIds);
				});
			}
		});
		session.getMirrors().getUserMirror().addListener(new UserMirror.Listener() {
			@Override
			public void charactersLoaded(final WorldSession session,
					final Collection<CharacterEntry> characterEntries) {
				SwingUtilities.invokeLater(() -> {
					gui.getCharactersFrame().loadCharacters(characterEntries);
					if (characterEntries.isEmpty()) {
						createCharacter(strap);
					} else if (characterEntries.size() == 1 && !strap.isDefaultCharacterSelected()) {
						gui.getCharactersFrame().setVisible(false);
						for (final CharacterEntry entry : characterEntries) {
							loginCharacter(strap, entry);
						}
						strap.setDefaultCharacterSelected(true);
					} else {
						gui.getCharactersFrame().setVisible(true);
					}
				});
			}
		});
		session.getMirrors().getChatMirror().addListener(new ChatMirror.Listener() {
			@Override
			public void chatSessionAdded(final ChatSession session) {
				SwingUtilities.invokeLater(() ->
				// create channel entry
				gui.getChatFrame().getChatPanel().getChatPanelChannel(session));
				session.addListener(new ChatSession.Listener() {
					@Override
					public void chatReceived(final ChatSession.Message message) {
						SwingUtilities.invokeLater(() -> {
							final View view = gui.getViewPane().getView();
							if (view != null) {
								final FieldEntity fieldEntity = strap.userControl()
										.getFieldEntity(message.getSpeaker());
								if (fieldEntity != null) {
									view.updateEntityName(fieldEntity);
								}
							}
							final ChannelItem channelItem = gui.getChatFrame().getChatPanel()
									.getChatPanelChannel(session);
							channelItem.addChat(message.getSpeaker(), message.getContent());
						});
					}
				});
			}

			@Override
			public void chatSessionRemoved(final ChatSession session) {
				SwingUtilities.invokeLater(() -> gui.getChatFrame().getChatPanel().removeChatPanelChannel(session));
			}
		});
		session.getMirrors().getFieldMirror().addListener(new FieldMirror.Listener() {
			@Override
			public void fieldCreated(final ClientField field) {
				SwingUtilities.invokeLater(() -> {
					gui.showViewPane();
					gui.getViewPane().setViewVisible(false);
					final View view = gui.getViewPane().newView(field);
					field.addListener(new ClientField.Listener() {
						@Override
						public void tilesSet(final Collection<TilePiece> tiles) {
							SwingUtilities.invokeLater(() -> view.addTiles(tiles));
						}

						@Override
						public void regionsSet(final Collection<Region> regions) {
							SwingUtilities.invokeLater(() -> gui.getRegionsFrame().setRegions(regions));
						}
					});
					gui.getRegionsFrame().clearRegions();
					// strap.getSoundContext().getMusicPlayer().play("field", true);
				});
			}

			@Override
			public void effectAdded(final String effectId, final Vector origin, final Vector target) {
				SwingUtilities.invokeLater(() -> {
					final View view = gui.getViewPane().getView();
					if (view != null) {
						view.addShotEffect(effectId, origin, target);
					}
				});
			}
		});
		session.getMirrors().getEntityMirror().addListener(new EntityMirror.Listener() {
			@Override
			public void fieldEntityRemoved(final FieldEntity entity) {
				SwingUtilities.invokeLater(() -> {
					final Model model = gui.getViewPane().getView().getModel(entity);
					if (model != null) {
						final float ratio = model.getWalkRatio();
						if (ratio < 1) {
							final int wait = (int) (model.getMinMotionWait() * (1f - ratio));
							final Runnable task = new Runnable() {
								@Override
								public void run() {
									SwingUtilities.invokeLater(() -> {
										gui.getViewPane().getView().removeEntity(entity);
									});
								}
							};
							strap.getScheduledExecutor().schedule(task, wait, TimeUnit.MILLISECONDS);
						} else {
							gui.getViewPane().getView().removeEntity(entity);
						}
					} else {
						gui.getViewPane().getView().removeEntity(entity);
					}
				});
			}

			@Override
			public void fieldEntityAdded(final FieldEntity entity) {
				entity.addListener(new FieldEntity.Listener() {
					@Override
					public void fieldEntityPlaced(final Placement placement, final int motionWait) {
						SwingUtilities.invokeLater(() -> {
							gui.getViewPane().getView().placeEntity(entity, placement, motionWait);
							if (entity == strap.control().getSelfFieldEntity()) {
								gui.getViewPane().getView().walk(placement.getPosition());
							}
						});
					}

					@Override
					public void fieldEntityShapeChanged(final String shape) {
						SwingUtilities.invokeLater(() -> {
							final View view = gui.getViewPane().getView();
							if (view == null) {
								return;
							}
							view.reloadEntity(entity);
						});

					}
				});
				SwingUtilities.invokeLater(() -> gui.getViewPane().getView().addEntity(entity));
			}
		});
		session.getMirrors().getSelfMirror().addListener(new SelfMirror.Listener() {
			@Override
			public void selfFieldEntitySet(final FieldEntity fieldEntity) {
				if (fieldEntity == null) {
					return;
				}
				SwingUtilities.invokeLater(() -> {
					gui.getViewPane().getView().setPosition(fieldEntity.getPlacement().getPosition());
					gui.getViewPane().setViewVisible(true);
					gui.getPositionMemoryFrame().initPositionMemories(fieldEntity);

					strap.control().setAutopilot(gui.getViewPane().isAutopilot());
				});
			}

			@Override
			public void selfNameSet(final Name name) {
				// TODO update own name
			}

			@Override
			public void selfRefreshed() {
				SwingUtilities.invokeLater(() -> {
					final FieldEntity entity = strap.control().getSelfFieldEntity();
					if (entity != null) {
						strap.getShortcuts().setOwner(entity);
					}
				});
			}
		});
		session.getMirrors().getSelfMirror().setWaitTeleport(true);
		session.getMirrors().getSkillMirror().addListener(new ActiveSkillMirror.Listener() {
			@Override
			public void skillsCleared() {
				SwingUtilities.invokeLater(() -> {
					gui.getSkillsFrame().getPanel().clear();
				});
			}

			@Override
			public void skillAdded(final ActiveSkillMirror.Entry entry) {
				SwingUtilities.invokeLater(() -> gui.getSkillsFrame().getPanel().addValue(new SkillButtonValue(entry)));
			}
		});
		session.getMirrors().getInventoryMirror().addListener(new InventoryMirror.Listener() {
			@Override
			public void itemsCleared() {
				SwingUtilities.invokeLater(() -> {
					gui.getInventoryFrame().getPanel().clear();
				});
			}

			@Override
			public void itemCountSet(final InventoryMirror.Entry entry) {
				SwingUtilities.invokeLater(() -> {
					gui.getInventoryFrame().getPanel().repaintItemButton(entry.getId());
				});
			}

			@Override
			public void itemAdded(final InventoryMirror.Entry entry) {
				SwingUtilities
						.invokeLater(() -> gui.getInventoryFrame().getPanel().addValue(new ItemButtonValue(entry)));
			}

			@Override
			public void itemRemoved(final InventoryMirror.Entry entry) {
				SwingUtilities.invokeLater(() -> gui.getInventoryFrame().getPanel().removeButton(entry.getId()));
			}

			@Override
			public void weightBarChanged(final int bar) {
				SwingUtilities.invokeLater(() -> gui.getInventoryFrame().setWeightBar(bar));
			}
		});
		session.getMirrors().getAdminMirror().addListener(new AdminMirror.Listener() {
			@Override
			public void roleListLoaded(final Collection<Role> roles) {
				SwingUtilities.invokeLater(() -> {
					gui.getRoleTokenIssueFrame().updateRoles();
					gui.getRoleEditorFrame().updateRoles();
				});
			}

			@Override
			public void myRolesLoaded(final RoleSet roleSet) {
				SwingUtilities.invokeLater(() -> {
					gui.getRoleTokenIssueFrame().updateRoles();
					gui.getRoleEditorFrame().updateRoles();
				});
			}

			@Override
			public void roleTokenListLoaded(final Collection<RoleToken> roleTokens) {
				SwingUtilities.invokeLater(() -> {
					gui.getRoleTokenListFrame().updateRoleTokenList();
				});
			}

			@Override
			public void commandOutAdded(final String result) {
				SwingUtilities.invokeLater(() -> {
					gui.getCommandFrame().addOut(result);
				});
			}

			@Override
			public void settingsChanged(final UserRegistrationMode userRegistrationMode,
					final boolean rescueMethodsEnabled) {
				SwingUtilities.invokeLater(() -> {
					gui.getServerSettingsFrame().setSettings(userRegistrationMode, rescueMethodsEnabled);
				});
			}
		});

		strap.setSession(session);
		session.start();

		return true;
	}

	@Private
	static void login(final Strap strap) {
		final Gui gui = strap.getGui();
		final ResponseHandler responseHandler = new ResponseHandler() {
			@Override
			public void rejected(final String message) {
				SwingUtilities.invokeLater(() -> {
					JOptionPane.showMessageDialog(gui.getViewPane(), message);
					// TODO what to do after failed login?
				});
			}

			@Override
			public void accepted(final String message) {
				// nothing to do
			}
		};
		strap.userControl().loginUser(responseHandler);
	}

	@Private
	static void createCharacter(final Strap strap) {
		final Gui gui = strap.getGui();
		final ResponseHandler responseHandler = new ResponseHandler() {
			@Override
			public void rejected(final String message) {
				SwingUtilities.invokeLater(() -> JOptionPane.showMessageDialog(gui.getViewPane(), message));
			}

			@Override
			public void accepted(final String message) {
				SwingUtilities.invokeLater(() -> gui.addMessage(message));
			}
		};
		strap.userControl().newCharacter(responseHandler);
	}

	@Private
	static void loginCharacter(final Strap strap, final CharacterEntry entry) {
		final Gui gui = strap.getGui();
		final ResponseHandler responseHandler = new ResponseHandler() {
			@Override
			public void rejected(final String message) {
				SwingUtilities.invokeLater(() -> {
					JOptionPane.showMessageDialog(gui.getViewPane(), message);
					gui.getCharactersFrame().setVisible(true);
				});
			}

			@Override
			public void accepted(final String message) {
				SwingUtilities.invokeLater(() -> {
					strap.control().activateLocalChat();

					gui.getViewPane().setEditMode(false);
				});
			}
		};
		strap.userControl().loginCharacter(entry.getId(), responseHandler);
	}

}
