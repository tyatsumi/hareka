package org.kareha.hareka.swingclient.gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ResourceBundle;

import javax.swing.JCheckBoxMenuItem;
import javax.swing.JComponent;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.ToolTipManager;
import javax.swing.TransferHandler;

import org.kareha.hareka.annotation.Private;
import org.kareha.hareka.swingclient.Shortcuts;
import org.kareha.hareka.swingclient.Strap;
import org.kareha.hareka.swingclient.SwingClientConstants;
import org.kareha.hareka.swingclient.keyboard.VirtualKey;
import org.kareha.hareka.ui.swing.SwingUtil;

@SuppressWarnings("serial")
public final class ShortcutButton extends JComponent {

	private enum BundleKey {
		Use, Repeating, SelfTargetting, KeySetting, Delete,
	}

	private final Strap strap;
	private final ShortcutsPanel panel;
	private final int index;
	private boolean selected;
	private ShortcutButtonValue value;
	private boolean repeating;
	private boolean selfTargetting;
	private int keyCode;
	private final BufferedImage shortcutBackgroundImage;
	private final BufferedImage itemBackgroundImage;
	private final BufferedImage skillBackgroundImage;
	private final BufferedImage cursorImage;

	public ShortcutButton(final Strap strap, final ShortcutsPanel panel, final int index) {
		this.strap = strap;
		this.panel = panel;
		this.index = index;

		addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(final MouseEvent e) {
				if (e.getButton() == MouseEvent.BUTTON1) {
					useShortcut();
				}
			}

			@Override
			public void mousePressed(final MouseEvent e) {
				if (e.isPopupTrigger()) {
					createMenu().show(ShortcutButton.this, e.getX(), e.getY());
				}

				if (e.getButton() == MouseEvent.BUTTON2) {
					panel.use();
				}
			}

			@Override
			public void mouseReleased(final MouseEvent e) {
				if (e.isPopupTrigger()) {
					createMenu().show(ShortcutButton.this, e.getX(), e.getY());
				}
			}
		});
		addMouseMotionListener(new MouseMotionAdapter() {
			@Override
			public void mouseDragged(final MouseEvent e) {
				panel.setDragButton(ShortcutButton.this);
				final JComponent c = (JComponent) e.getSource();
				final TransferHandler th = c.getTransferHandler();
				th.exportAsDrag(c, e, TransferHandler.MOVE);
			}
		});
		addMouseWheelListener(e -> panel.moveCursor(e.getWheelRotation()));

		setTransferHandler(new ShortcutButtonTransferHandler());

		setPreferredSize(new Dimension(SwingClientConstants.ICON_WIDTH, SwingClientConstants.ICON_HEIGHT));

		ToolTipManager.sharedInstance().registerComponent(this);

		try {
			shortcutBackgroundImage = strap.getLoader().getImageLoader().load("shortcut.png");
			itemBackgroundImage = strap.getLoader().getImageLoader().load("item.png");
			skillBackgroundImage = strap.getLoader().getImageLoader().load("skill.png");
			cursorImage = strap.getLoader().getImageLoader().load("icon_cursor.png");
		} catch (final IOException e) {
			throw new RuntimeException(e.getMessage(), e);
		}
	}

	private boolean isActive() {
		// return index < panel.getShortcutsSize();
		return true;
	}

	ShortcutsPanel getPanel() {
		return panel;
	}

	public int getIndex() {
		return index;
	}

	public void setSelected(final boolean v) {
		selected = v;
	}

	public ShortcutButtonValue getValue() {
		return value;
	}

	public void setValue(final ShortcutButtonValue v) {
		value = v;
		repaint();
	}

	public void setRepeating(final boolean v) {
		repeating = v;
	}

	public void setSelfTargetting(final boolean v) {
		selfTargetting = v;
	}

	public void setKeyCode(final int v) {
		keyCode = v;
	}

	public void useShortcut() {
		if (value == null) {
			return;
		}
		if (value instanceof ItemButtonValue) {
			final ItemButtonValue entry = (ItemButtonValue) value;
			strap.getGui().getViewPane().startAction(entry.getEntry(), repeating, selfTargetting);
		} else if (value instanceof SkillButtonValue) {
			final SkillButtonValue entry = (SkillButtonValue) value;
			switch (entry.getEntry().getType().getTargetType()) {
			default:
				return;
			case NULL:
				strap.getGui().getViewPane().doNullSkill(entry.getEntry());
				break;
			case FIELD_ENTITY:
				strap.getGui().getViewPane().startAction(entry.getEntry(), repeating, selfTargetting);
				break;
			case TILE:
				strap.getGui().getViewPane().doTileSkill(entry.getEntry());
				break;
			}
		}
	}

	public boolean checkKey(final int keyCode) {
		if (keyCode != this.keyCode) {
			return false;
		}
		useShortcut();
		return true;
	}

	public void send() {
		final Shortcuts.Entry entry;
		if (value instanceof SkillButtonValue) {
			entry = new Shortcuts.SkillEntry(repeating, selfTargetting, keyCode,
					((SkillButtonValue) value).getEntry().getType());
		} else if (value instanceof ItemButtonValue) {
			entry = new Shortcuts.ItemEntry(repeating, selfTargetting, keyCode,
					((ItemButtonValue) value).getEntry().getId());
		} else {
			entry = new Shortcuts.BlankEntry(repeating, selfTargetting, keyCode);
		}
		strap.getShortcuts().addShortcut(index, entry);
	}

	public void send(final ShortcutButtonValue shortcut) {
		final Shortcuts.Entry entry;
		if (shortcut instanceof SkillButtonValue) {
			entry = new Shortcuts.SkillEntry(repeating, selfTargetting, keyCode,
					((SkillButtonValue) shortcut).getEntry().getType());
		} else if (shortcut instanceof ItemButtonValue) {
			entry = new Shortcuts.ItemEntry(repeating, selfTargetting, keyCode,
					((ItemButtonValue) shortcut).getEntry().getId());
		} else {
			entry = new Shortcuts.BlankEntry(repeating, selfTargetting, keyCode);
		}
		strap.getShortcuts().addShortcut(index, entry);
	}

	public void sendRemove() {
		final Shortcuts.Entry entry = new Shortcuts.BlankEntry(repeating, selfTargetting, keyCode);
		strap.getShortcuts().addShortcut(index, entry);
	}

	@Private
	JPopupMenu createMenu() {
		final JPopupMenu menu = new JPopupMenu();

		JMenuItem i;
		final ResourceBundle bundle = ResourceBundle.getBundle(ShortcutButton.class.getName());
		i = new JMenuItem(bundle.getString(BundleKey.Use.name()));
		i.addActionListener(e -> useShortcut());
		menu.add(i);
		final JCheckBoxMenuItem repeatingItem = new JCheckBoxMenuItem(bundle.getString(BundleKey.Repeating.name()));
		repeatingItem.setSelected(repeating);
		repeatingItem.addActionListener(e -> {
			repeating = repeatingItem.isSelected();
			send();
			repaint();
		});
		menu.add(repeatingItem);
		final JCheckBoxMenuItem selfTargettingItem = new JCheckBoxMenuItem(
				bundle.getString(BundleKey.SelfTargetting.name()));
		selfTargettingItem.setSelected(selfTargetting);
		selfTargettingItem.addActionListener(e -> {
			selfTargetting = selfTargettingItem.isSelected();
			send();
			repaint();
		});
		menu.add(selfTargettingItem);
		i = new JMenuItem(bundle.getString(BundleKey.KeySetting.name()));
		i.addActionListener(e -> {
			final KeySettingDialog dialog = new KeySettingDialog(strap.getGui().getMainFrame());
			if (dialog.isCanceled()) {
				return;
			}
			keyCode = dialog.getKeyCode();
			send();
			repaint();
		});
		menu.add(i);
		i = new JMenuItem(bundle.getString(BundleKey.Delete.name()));
		i.addActionListener(e -> {
			value = null;
			sendRemove();
			repaint();
		});
		menu.add(i);

		return menu;
	}

	@Override
	protected void paintComponent(final Graphics g) {
		if (isActive()) {
			if (value instanceof ItemButtonValue) {
				g.drawImage(itemBackgroundImage, getSize().width / 2 - itemBackgroundImage.getWidth() / 2,
						getSize().height / 2 - itemBackgroundImage.getHeight() / 2, null);
			} else if (value instanceof SkillButtonValue) {
				g.drawImage(skillBackgroundImage, getSize().width / 2 - skillBackgroundImage.getWidth() / 2,
						getSize().height / 2 - skillBackgroundImage.getHeight() / 2, null);
			} else {
				g.drawImage(shortcutBackgroundImage, getSize().width / 2 - shortcutBackgroundImage.getWidth() / 2,
						getSize().height / 2 - shortcutBackgroundImage.getHeight() / 2, null);
			}
		}

		if (value instanceof ItemButtonValue) {
			final ItemButtonValue entry = (ItemButtonValue) value;
			if (entry.getCount() > 0) {
				strap.getLoader().getIconPainterTable().get(entry.getIconName()).paint(g, getSize().width / 2,
						getSize().height / 2);
			}
		} else if (value instanceof SkillButtonValue) {
			final SkillButtonValue entry = (SkillButtonValue) value;
			strap.getLoader().getIconPainterTable().get(entry.getIconName()).paint(g, getSize().width / 2,
					getSize().height / 2);
		}

		if (selected) {
			g.drawImage(cursorImage, getSize().width / 2 - cursorImage.getWidth() / 2,
					getSize().height / 2 - cursorImage.getHeight() / 2, null);
		}

		// g.setColor(Color.darkGray);
		// g.drawRect(4, 4, getSize().width - 8, getSize().height - 8);

		final FontMetrics fm = g.getFontMetrics();

		if (keyCode != KeyEvent.VK_UNDEFINED) {
			SwingUtil.drawString(g, VirtualKey.toName(keyCode), 0, fm.getHeight() - fm.getDescent(), Color.black,
					Color.lightGray);
		}

		if (value instanceof ItemButtonValue) {
			final ItemButtonValue entry = (ItemButtonValue) value;
			if (entry.getCount() > 1) {
				final String s = Long.toString(entry.getCount());
				SwingUtil.drawString(g, s, getSize().width - fm.stringWidth(s), getSize().height, Color.black,
						Color.white);
			}
		}
	}

	@Override
	public String getToolTipText(final MouseEvent e) {
		if (value == null) {
			return null;
		}
		return value.getName();
	}

}
