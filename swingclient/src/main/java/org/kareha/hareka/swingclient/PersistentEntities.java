package org.kareha.hareka.swingclient;

import java.io.File;

import org.kareha.hareka.client.field.FieldEntity;
import org.kareha.hareka.client.server.ServerId;

public class PersistentEntities {

	private static final String DIRECTORY_NAME = "entity";

	private final Strap strap;

	public PersistentEntities(final Strap strap) {
		this.strap = strap;
	}

	public File getDirectory(final FieldEntity entity) {
		if (entity == null) {
			return null;
		}
		if (!strap.isConnected()) {
			return null;
		}
		final ServerId serverId = strap.userControl().getServerId();
		if (serverId == null) {
			return null;
		}
		final File serverDirectory = strap.getContext().getServers().getDirectory(serverId);
		return new File(serverDirectory, DIRECTORY_NAME + File.separator + entity.getLocalId());
	}

}
