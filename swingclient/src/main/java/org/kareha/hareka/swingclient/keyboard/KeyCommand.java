package org.kareha.hareka.swingclient.keyboard;

import org.kareha.hareka.field.Direction;

public enum KeyCommand {

	NONE, STOP,

	RIGHT, LOWER_RIGHT, LOWER_LEFT, LEFT, UPPER_LEFT, UPPER_RIGHT, ROTATE_RIGHT, ROTATE_LEFT, MIRROR,

	ATTACK,

	;

	public Direction toDirection() {
		switch (this) {
		default:
			return Direction.NULL;
		case RIGHT:
			return Direction.RIGHT;
		case LOWER_RIGHT:
			return Direction.DOWN_RIGHT;
		case LOWER_LEFT:
			return Direction.DOWN_LEFT;
		case LEFT:
			return Direction.LEFT;
		case UPPER_LEFT:
			return Direction.UP_LEFT;
		case UPPER_RIGHT:
			return Direction.UP_RIGHT;
		}
	}

}
