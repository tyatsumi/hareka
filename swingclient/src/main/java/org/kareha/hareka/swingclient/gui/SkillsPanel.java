package org.kareha.hareka.swingclient.gui;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.util.logging.Logger;

import javax.swing.JComponent;

import org.kareha.hareka.game.ActiveSkillType;
import org.kareha.hareka.swingclient.Strap;
import org.kareha.hareka.swingclient.SwingClientConstants;

@SuppressWarnings("serial")
public class SkillsPanel extends JComponent {

	private static final Logger logger = Logger.getLogger(SkillsPanel.class.getName());

	private static final int COLUMNS = 4;
	private static final int ROWS = 2;

	private final Strap strap;
	private final GridLayout layout = new GridLayout(0, COLUMNS);
	private int skillsSize;
	private SkillButton selectedButton;
	@SuppressWarnings("unused")
	private SkillButton dragButton;
	private SkillButton swapButton;
	private int count; // XXX temporal implementation

	public SkillsPanel(final Strap strap) {
		this.strap = strap;
		setLayout(layout);
		helperClear();
	}

	int getSkillsSize() {
		return skillsSize;
	}

	void setSkillsSize(final int v) {
		skillsSize = v;
		for (final Component c : getComponents()) {
			c.repaint();
		}
	}

	private int getSkillsRows() {
		return Math.max((skillsSize + COLUMNS - 1) / COLUMNS, ROWS);
	}

	private void extend(final int maxIndex) {
		final int requiredRows = maxIndex / COLUMNS + 1;
		final int currentRows = layout.getRows();
		if (requiredRows > currentRows) {
			layout.setRows(requiredRows);
			for (int j = currentRows; j < requiredRows; j++) {
				for (int i = 0; i < COLUMNS; i++) {
					final int index = COLUMNS * j + i;
					final SkillButton button = new SkillButton(strap, this, index);
					add(button);
				}
			}
			validate();
		}
	}

	private void helperClear() {
		removeAll();
		layout.setRows(0);
		extend(COLUMNS * getSkillsRows() - 1);
		count = 0;
	}

	public void clear() {
		helperClear();
	}

	public SkillButton getButton(final int index) {
		extend(index);
		return (SkillButton) getComponent(index);
	}

	public SkillButton getButton(final ActiveSkillType type) {
		for (int j = 0, r = layout.getRows(); j < r; j++) {
			for (int i = 0; i < COLUMNS; i++) {
				final int index = COLUMNS * j + i;
				final SkillButton button = (SkillButton) getComponent(index);
				final SkillButtonValue value = button.getValue();
				if (value == null) {
					continue;
				}
				if (value.getEntry().getType().equals(type)) {
					return button;
				}
			}
		}
		return null;
	}

	public void setValue(final int index, final SkillButtonValue value) {
		final SkillButton button = getButton(index);
		button.setValue(value);
	}

	public void addValue(final SkillButtonValue value) {
		// XXX temporal implementation
		setValue(count, value);
		count++;
	}

	@SuppressWarnings("unused")
	private void repaintButtons() {
		for (int j = 0, r = layout.getRows(); j < r; j++) {
			for (int i = 0; i < COLUMNS; i++) {
				final int index = COLUMNS * j + i;
				final SkillButton button = (SkillButton) getComponent(index);
				button.repaint();
			}
		}
	}

	public SkillButton getSelectedButton() {
		return selectedButton;
	}

	void setSelectedButton(final SkillButton v) {
		final SkillButton b = selectedButton;
		selectedButton = v;
		if (b != null) {
			b.repaint();
		}
		if (v != null) {
			v.repaint();
		}
	}

	void setDragButton(final SkillButton v) {
		dragButton = v;
	}

	void setSwapButton(final SkillButton v) {
		swapButton = v;
	}

	void swap() {
		if (swapButton == null) {
			return;
		}
		// gui.handle().mirror().skills().swapSkills(dragButton.getIndex(),
		// swapButton.getIndex());
		// TODO implement skill swapping
		logger.info("Swapping skill buttons is not implemented");
		swapButton = null;
	}

	public void swapSkills(final int indexA, final int indexB) {
		final SkillButton buttonA = getButton(indexA);
		final SkillButton buttonB = getButton(indexB);
		final SkillButtonValue value = buttonA.getValue();
		buttonA.setValue(buttonB.getValue());
		buttonB.setValue(value);

		final ShortcutButton sa = strap.getGui().getShortcutsFrame().getPanel().getButton(buttonA.getValue());
		if (sa != null) {
			if (sa.getValue() != null) {
				// sa.getValue().setTableIndex(indexA);
				// sa.send();
			}
		}
		final ShortcutButton sb = strap.getGui().getShortcutsFrame().getPanel().getButton(buttonB.getValue());
		if (sb != null) {
			if (sb.getValue() != null) {
				// sb.getValue().setTableIndex(indexB);
				// sb.send();
			}
		}
	}

	public Dimension getViewportSize() {
		return new Dimension(SwingClientConstants.ICON_WIDTH * COLUMNS, SwingClientConstants.ICON_HEIGHT * ROWS);
	}

}