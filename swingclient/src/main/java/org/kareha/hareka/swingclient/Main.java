package org.kareha.hareka.swingclient;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.SwingUtilities;
import javax.xml.bind.JAXBException;

import org.kareha.hareka.client.Context;
import org.kareha.hareka.logging.FileLogger;
import org.kareha.hareka.logging.SimpleLogger;
import org.kareha.hareka.sound.SoundContext;
import org.kareha.hareka.ui.UiType;
import org.kareha.hareka.util.FileUtil;
import org.kareha.hareka.util.Locker;
import org.kareha.hareka.util.LogUtil;
import org.kareha.hareka.util.Wiper;

final class Main {

	private static final Logger logger = Logger.getLogger(Main.class.getName());
	private static final Logger appRootLogger = Logger.getLogger("org.kareha.hareka");

	static {
		LogUtil.loadLogLevel(appRootLogger);
	}

	private Main() {
		throw new AssertionError();
	}

	public static void main(final String[] args) {
		final SwingClientParameters parameters = new SwingClientParameters(args);

		if (parameters.wipe()) {
			Wiper.confirmAndWipe(parameters.dataDirectory(), UiType.SWING);
		}

		FileUtil.ensureDirectoryExistsOrExit(parameters.dataDirectory(), UiType.SWING);

		Locker.INSTANCE.lock(parameters.dataDirectory(), false, UiType.SWING);

		SimpleLogger.INSTANCE.addStream(System.err);
		FileLogger.connect(appRootLogger, new File(parameters.dataDirectory(), "log"), UiType.SWING);

		Thread.setDefaultUncaughtExceptionHandler((t, e) -> logger.log(Level.SEVERE, t.getName(), e));

		SwingUtilities.invokeLater(() -> {
			try {
				// final Context context = new Context(new Version("test"),
				// parameters.getDataDirectory());
				final Context context = new Context(parameters.dataDirectory());
				final SoundContext soundContext = new SoundContext(parameters.dataDirectory());
				final Strap strap = new Strap(parameters, context, soundContext);

				strap.getGui().getMainFrame().setVisible(true);

				SwingConnector.connect(strap);
			} catch (final IOException | JAXBException e) {
				logger.log(Level.SEVERE, "", e);
			}
		});
	}

}
