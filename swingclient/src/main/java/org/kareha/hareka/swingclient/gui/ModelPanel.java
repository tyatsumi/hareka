package org.kareha.hareka.swingclient.gui;

import java.awt.Dimension;
import java.awt.Graphics;

import javax.swing.JComponent;
import javax.swing.Timer;

import org.kareha.hareka.LocalEntityId;
import org.kareha.hareka.client.field.FieldEntity;
import org.kareha.hareka.client.field.StatBar;
import org.kareha.hareka.field.Direction;
import org.kareha.hareka.field.Placement;
import org.kareha.hareka.field.Vector;
import org.kareha.hareka.graphics.GraphicsConstants;
import org.kareha.hareka.graphics.GraphicsLoader;
import org.kareha.hareka.graphics.model.Model;
import org.kareha.hareka.graphics.sprite.Screen;

@SuppressWarnings("serial")
public class ModelPanel extends JComponent {

	private static final int SCREEN_WIDTH = 80;
	private static final int SCREEN_HEIGHT = 80;
	private static final int REPAINT_FRAMES_PER_SECOND = 15;

	private final Screen screen;
	private final Timer repaintTimer;
	private float walkRatio;
	private boolean walking;

	public ModelPanel(final GraphicsLoader loader, final String shape) {
		setPreferredSize(new Dimension(SCREEN_WIDTH, SCREEN_HEIGHT));

		screen = new Screen();
		screen.setViewport(-SCREEN_WIDTH / 2, -SCREEN_HEIGHT / 2 - GraphicsConstants.TILE_HEIGHT);

		final int motionWait = 250; // hard-coded dummy value
		final Placement placement = Placement.valueOf(Vector.ZERO, Direction.RIGHT);
		final FieldEntity entity = new FieldEntity(LocalEntityId.valueOf(new byte[16]), null, placement, shape, 1);
		entity.setHealthBar(StatBar.FULL);
		entity.setMagicBar(StatBar.FULL);
		final Model model = loader.getModelLoader().createModel(entity);
		model.addTo(screen);
		model.setPlacement(placement, 0);

		model.animate(0); // initialization?

		repaintTimer = new Timer(1000 / REPAINT_FRAMES_PER_SECOND, e -> {
			final int v = 1000 / REPAINT_FRAMES_PER_SECOND;
			walkRatio += (float) v / motionWait;
			if (walking) {
				if (walkRatio > 1) {
					model.place(placement, 0, motionWait);
					walkRatio = 0;
				}
			}
			model.animate(1000 / REPAINT_FRAMES_PER_SECOND);
			repaint();
		});
		repaintTimer.setInitialDelay(0);
		repaintTimer.start();
	}

	public void stopMotion() {
		repaintTimer.stop();
	}

	public void startMotion() {
		repaintTimer.start();
	}

	@Override
	protected void paintComponent(final Graphics g) {
		screen.paint(g);
	}

	public void setWalking(final boolean v) {
		walking = v;
		if (walking) {
			walkRatio = 0;
		}
	}

}
