package org.kareha.hareka.swingclient.gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ResourceBundle;

import javax.swing.AbstractListModel;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.xml.bind.JAXBException;

import org.kareha.hareka.annotation.Private;
import org.kareha.hareka.client.ConnectionSettings;
import org.kareha.hareka.client.ConnectionSettings.Entry;
import org.kareha.hareka.swingclient.Strap;

@SuppressWarnings("serial")
public class ConnectionPanel extends JComponent {

	private enum BundleKey {
		Add, Up, Down, Remove, Name, Host, Port, Site,
	}

	private final Strap strap;
	private final ConnectionsModel model;
	@Private
	final JList<ConnectionSettings.Entry> list;
	@Private
	final ConnectionEditorPanel connectionEditorPanel;

	public ConnectionPanel(final Strap strap, final ConnectionFrame frame) {
		this.strap = strap;
		final ResourceBundle bundle = ResourceBundle.getBundle(ConnectionPanel.class.getName());

		connectionEditorPanel = new ConnectionEditorPanel();

		model = new ConnectionsModel();
		list = new JList<>(model);
		list.setFocusable(true);
		list.addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(final ListSelectionEvent e) {
				final ConnectionSettings.Entry entry = list.getSelectedValue();
				if (entry == null) {
					return;
				}
				connectionEditorPanel.loadEntry(entry);
			}
		});
		list.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(final MouseEvent e) {
				if (e.getClickCount() != 2) {
					return;
				}
				final ConnectionSettings.Entry entry = list.getSelectedValue();
				if (entry == null) {
					return;
				}
				connectionEditorPanel.loadEntry(entry);

				frame.doConnect();
			}

		});
		final JScrollPane scrollPane = new JScrollPane(list);
		scrollPane.setPreferredSize(new Dimension(256, 128));

		final JButton addButton = new JButton(bundle.getString(BundleKey.Add.name()));
		addButton.addActionListener(e -> doAdd());
		final JButton upButton = new JButton(bundle.getString(BundleKey.Up.name()));
		upButton.addActionListener(e -> doUp());
		final JButton downButton = new JButton(bundle.getString(BundleKey.Down.name()));
		downButton.addActionListener(e -> doDown());
		final JButton removeButton = new JButton(bundle.getString(BundleKey.Remove.name()));
		removeButton.addActionListener(e -> doDelete());

		final JPanel eastPanel = new JPanel();
		eastPanel.setLayout(new BoxLayout(eastPanel, BoxLayout.Y_AXIS));
		eastPanel.add(addButton);
		eastPanel.add(upButton);
		eastPanel.add(downButton);
		eastPanel.add(removeButton);

		setLayout(new BorderLayout());
		add(connectionEditorPanel, BorderLayout.NORTH);
		add(scrollPane, BorderLayout.CENTER);
		add(eastPanel, BorderLayout.EAST);

		final int index = getConnectionSettings().getSelectedIndex();
		if (index >= 0 && index < getConnectionSettings().size()) {
			list.setSelectedIndex(index);
		}
	}

	@Private
	ConnectionSettings getConnectionSettings() {
		return strap.getContext().getConnectionSettings();
	}

	public ConnectionSettings.Entry getValue() {
		return connectionEditorPanel.getValue();
	}

	private void doAdd() {
		final ConnectionSettings.Entry entry = connectionEditorPanel.getValue();
		final int index = list.getSelectedIndex();
		if (index == -1) {
			model.add(entry);
			list.setSelectedIndex(0);
		} else {
			model.add(index, entry);
			list.setSelectedIndex(index);
		}
	}

	private void doUp() {
		final int index = list.getSelectedIndex();
		if (index == -1) {
			return;
		}
		model.up(index);
		if (index > 0) {
			list.setSelectedIndex(index - 1);
		}
	}

	private void doDown() {
		final int index = list.getSelectedIndex();
		if (index == -1) {
			return;
		}
		model.down(index);
		if (index < getConnectionSettings().size() - 1) {
			list.setSelectedIndex(index + 1);
		}
	}

	private void doDelete() {
		final int index = list.getSelectedIndex();
		if (index == -1) {
			return;
		}
		model.remove(index);
	}

	public void save() {
		final Gui gui = strap.getGui();
		getConnectionSettings().setSelectedIndex(list.getSelectedIndex());
		try {
			getConnectionSettings().save();
		} catch (final JAXBException e) {
			JOptionPane.showMessageDialog(gui.getViewPane(), e.getMessage());
			return;
		}
	}

	private class ConnectionsModel extends AbstractListModel<ConnectionSettings.Entry> {

		@Private
		ConnectionsModel() {

		}

		@Override
		public Entry getElementAt(final int index) {
			return getConnectionSettings().getEntry(index);
		}

		@Override
		public int getSize() {
			return getConnectionSettings().size();
		}

		void add(final ConnectionSettings.Entry value) {
			getConnectionSettings().addEntry(value);
			final int index = getConnectionSettings().size() - 1;
			fireIntervalAdded(this, index, index);
		}

		void add(final int index, final ConnectionSettings.Entry value) {
			getConnectionSettings().addEntry(index, value);
			fireIntervalAdded(this, index, index);
		}

		void remove(final int index) {
			getConnectionSettings().removeEntry(index);
			fireIntervalRemoved(this, index, index);
		}

		void up(final int index) {
			if (index < 1) {
				return;
			}
			final ConnectionSettings.Entry temp = getConnectionSettings().removeEntry(index);
			getConnectionSettings().addEntry(index - 1, temp);
			fireContentsChanged(this, index - 1, index);
		}

		void down(final int index) {
			if (index >= getConnectionSettings().size() - 1) {
				return;
			}
			final ConnectionSettings.Entry temp = getConnectionSettings().removeEntry(index);
			getConnectionSettings().addEntry(index + 1, temp);
			fireContentsChanged(this, index, index + 1);
		}

	}

	private class ConnectionEditorPanel extends JComponent {

		@Private
		final JTextField nameField;
		@Private
		final JTextField hostField;
		@Private
		final JTextField portField;
		@Private
		final JTextField siteField;

		ConnectionEditorPanel() {
			final ResourceBundle bundle = ResourceBundle.getBundle(ConnectionPanel.class.getName());

			nameField = new JTextField(24);
			hostField = new JTextField(24);
			portField = new JTextField(24);
			siteField = new JTextField(24);

			nameField.addFocusListener(new FocusAdapter() {
				@Override
				public void focusGained(final FocusEvent e) {
					nameField.selectAll();
				}
			});
			nameField.addActionListener(e -> this.hostField.requestFocus());

			hostField.addFocusListener(new FocusAdapter() {
				@Override
				public void focusGained(final FocusEvent e) {
					hostField.selectAll();
				}
			});
			hostField.addActionListener(e -> this.portField.requestFocus());

			portField.addFocusListener(new FocusAdapter() {
				@Override
				public void focusGained(final FocusEvent e) {
					portField.selectAll();
				}
			});
			portField.addActionListener(e -> this.siteField.requestFocus());

			siteField.addFocusListener(new FocusAdapter() {
				@Override
				public void focusGained(final FocusEvent e) {
					siteField.selectAll();
				}
			});
			siteField.addActionListener(e -> {
				// doAdd();
			});

			final JPanel centerPanel = new JPanel();
			centerPanel.setLayout(new BoxLayout(centerPanel, BoxLayout.Y_AXIS));
			final JPanel namePanel = new JPanel();
			namePanel.add(new JLabel(bundle.getString(BundleKey.Name.name())));
			namePanel.add(nameField);
			centerPanel.add(namePanel);
			final JPanel hostPanel = new JPanel();
			hostPanel.add(new JLabel(bundle.getString(BundleKey.Host.name())));
			hostPanel.add(hostField);
			centerPanel.add(hostPanel);
			final JPanel portPanel = new JPanel();
			portPanel.add(new JLabel(bundle.getString(BundleKey.Port.name())));
			portPanel.add(portField);
			centerPanel.add(portPanel);
			final JPanel sitePanel = new JPanel();
			sitePanel.add(new JLabel(bundle.getString(BundleKey.Site.name())));
			sitePanel.add(siteField);
			centerPanel.add(sitePanel);

			setLayout(new BorderLayout());
			add(centerPanel, BorderLayout.CENTER);
		}

		void loadEntry(final ConnectionSettings.Entry entry) {
			nameField.setText(entry.getName());
			hostField.setText(entry.getHost());
			portField.setText(Integer.toString(entry.getPort()));
			siteField.setText(entry.getSite());
		}

		ConnectionSettings.Entry getValue() {
			return new ConnectionSettings.Entry(nameField.getText(), hostField.getText(),
					Integer.parseInt(portField.getText()), siteField.getText());
		}

	}

}
