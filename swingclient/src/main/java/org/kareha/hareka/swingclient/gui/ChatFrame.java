package org.kareha.hareka.swingclient.gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.util.ResourceBundle;

import javax.swing.JInternalFrame;
import javax.swing.WindowConstants;

import org.kareha.hareka.swingclient.Strap;

@SuppressWarnings("serial")
public class ChatFrame extends JInternalFrame {

	private static final int FRAME_WIDTH = 384;
	private static final int FRAME_HEIGHT = 192;

	private enum BundleKey {
		Chat,
	}

	private final ChatPanel chatPanel;

	public ChatFrame(final Strap strap, final ChatPanel chatPanel) {
		super("", false, false, true, false);
		this.chatPanel = chatPanel;
		final ResourceBundle bundle = ResourceBundle.getBundle(ChatFrame.class.getName());
		setTitle(bundle.getString(BundleKey.Chat.name()));
		setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
		add(chatPanel, BorderLayout.CENTER);
		setPreferredSize(new Dimension(FRAME_WIDTH, FRAME_HEIGHT));
		setBorder(null);
		pack();
	}

	public ChatPanel getChatPanel() {
		return chatPanel;
	}

}
