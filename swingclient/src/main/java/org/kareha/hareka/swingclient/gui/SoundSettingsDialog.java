package org.kareha.hareka.swingclient.gui;

import java.awt.BorderLayout;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ResourceBundle;

import javax.sound.midi.MidiDevice;
import javax.sound.midi.MidiSystem;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.WindowConstants;

import org.kareha.hareka.annotation.Private;
import org.kareha.hareka.sound.SoundContext;

@SuppressWarnings("serial")
public class SoundSettingsDialog extends JDialog {

	private enum BundleKey {
		SoundSettings, MusicEnabled, MidiDevice, Close,
	}

	private final SoundContext context;
	private final JCheckBox musicEnabledCheckBox;
	private final JComboBox<MidiDevice.Info> midiDeviceComboBox;
	private final JButton closeButton;
	private boolean modified;

	public SoundSettingsDialog(final JFrame owner, final SoundContext context) {
		super(owner);
		this.context = context;
		final ResourceBundle bundle = ResourceBundle.getBundle(SoundSettingsDialog.class.getName());
		setTitle(bundle.getString(BundleKey.SoundSettings.name()));
		setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(final WindowEvent e) {
				doClose();
			}
		});
		setModal(true);

		musicEnabledCheckBox = new JCheckBox(bundle.getString(BundleKey.MusicEnabled.name()));
		musicEnabledCheckBox.setSelected(context.getSettings().isMusicEnabled());
		musicEnabledCheckBox.addActionListener(e -> {
			context.getSettings().setMusicEnabled(musicEnabledCheckBox.isSelected());
			context.update();
			modified = true;
		});
		final JPanel musicEnabledPanel = new JPanel();
		musicEnabledPanel.add(musicEnabledCheckBox);

		final MidiDevice.Info[] midiDeviceInfos = MidiSystem.getMidiDeviceInfo();
		midiDeviceComboBox = new JComboBox<>(midiDeviceInfos);
		final String midiDevice = context.getSettings().getMidiDevice();
		for (final MidiDevice.Info info : midiDeviceInfos) {
			if (info.getName().equals(midiDevice)) {
				midiDeviceComboBox.setSelectedItem(info);
				break;
			}
		}
		midiDeviceComboBox.addActionListener(e -> {
			final MidiDevice.Info info = midiDeviceComboBox.getItemAt(midiDeviceComboBox.getSelectedIndex());
			context.getSettings().setMidiDevice(info.getName());
			context.update();
			modified = true;
		});
		final JPanel midiDevicePanel = new JPanel(new BorderLayout());
		midiDevicePanel.add(new JLabel(bundle.getString(BundleKey.MidiDevice.name())), BorderLayout.NORTH);
		midiDevicePanel.add(midiDeviceComboBox, BorderLayout.CENTER);

		closeButton = new JButton(bundle.getString(BundleKey.Close.name()));
		closeButton.addActionListener(e -> doClose());

		final JPanel centerPanel = new JPanel();
		centerPanel.setLayout(new BoxLayout(centerPanel, BoxLayout.Y_AXIS));
		centerPanel.add(musicEnabledPanel);
		centerPanel.add(midiDevicePanel);

		final JPanel southPanel = new JPanel();
		southPanel.add(closeButton);

		add(centerPanel, BorderLayout.CENTER);
		add(southPanel, BorderLayout.SOUTH);
		pack();
		setResizable(false);
		setLocationRelativeTo(owner);
		setVisible(true);
	}

	@Private
	void doClose() {
		if (modified) {
			context.getSettings().save();
		}
		setVisible(false);
		dispose();
	}

}
