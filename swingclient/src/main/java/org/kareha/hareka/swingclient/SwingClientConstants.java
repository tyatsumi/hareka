package org.kareha.hareka.swingclient;

import java.io.File;

public final class SwingClientConstants {

	private SwingClientConstants() {
		throw new AssertionError();
	}

	public static final int VIEW_SIZE = 12;
	public static final int VIEW_WIDTH = 720;
	public static final int VIEW_HEIGHT = 360;
	public static final int ICON_WIDTH = 32;
	public static final int ICON_HEIGHT = 32;
	public static final int DEFAULT_MOTION_WAIT = 375;

	public static final File UPDATER_FILE = new File(System.getProperty("user.dir"), "harekaupd.jar");

}
