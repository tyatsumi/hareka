package org.kareha.hareka.swingclient.keyboard;

import java.awt.event.KeyEvent;
import java.util.EnumSet;
import java.util.Set;

public class KeyHandler {

	private final Keymap keymap;
	private Set<GameKey> directionKeys = EnumSet.noneOf(GameKey.class);
	private KeyState state = KeyState.STOP;

	public KeyHandler(final Keymap keymap) {
		this.keymap = keymap;
	}

	private KeyState createState() {
		if (directionKeys.contains(GameKey.UP)) {
			if (directionKeys.contains(GameKey.LEFT)) {
				return KeyState.UPPER_LEFT;
			} else if (directionKeys.contains(GameKey.RIGHT)) {
				return KeyState.UPPER_RIGHT;
			} else {
				return KeyState.UPPER_LEFT;
			}
		} else if (directionKeys.contains(GameKey.DOWN)) {
			if (directionKeys.contains(GameKey.LEFT)) {
				return KeyState.LOWER_LEFT;
			} else if (directionKeys.contains(GameKey.RIGHT)) {
				return KeyState.LOWER_RIGHT;
			} else {
				return KeyState.LOWER_RIGHT;
			}
		} else if (directionKeys.contains(GameKey.LEFT)) {
			return KeyState.LEFT;
		} else if (directionKeys.contains(GameKey.RIGHT)) {
			return KeyState.RIGHT;
		} else if (directionKeys.contains(GameKey.UPPER_RIGHT)) {
			return KeyState.UPPER_RIGHT;
		} else if (directionKeys.contains(GameKey.LOWER_LEFT)) {
			return KeyState.LOWER_LEFT;
		} else {
			return KeyState.STOP;
		}
	}

	public KeyCommand keyPressed(final KeyEvent e) {
		final GameKey key = keymap.get(e.getKeyCode());
		switch (key) {
		default:
			break;
		case RIGHT:
		case DOWN:
		case LOWER_LEFT:
		case LEFT:
		case UP:
		case UPPER_RIGHT:
			directionKeys.add(key);
			break;
		case ROTATE_LEFT:
			return KeyCommand.ROTATE_LEFT;
		case ROTATE_RIGHT:
			return KeyCommand.ROTATE_RIGHT;
		case MIRROR:
			return KeyCommand.MIRROR;
		case ATTACK:
			return KeyCommand.ATTACK;
		}
		final KeyState s = createState();
		final KeyCommand command;
		if (s != state) {
			command = s.toKeyCommand();
		} else {
			command = KeyCommand.NONE;
		}
		state = s;
		return command;
	}

	public KeyCommand keyReleased(final KeyEvent e) {
		final GameKey key = keymap.get(e.getKeyCode());
		switch (key) {
		default:
			break;
		case RIGHT:
		case DOWN:
		case LOWER_LEFT:
		case LEFT:
		case UP:
		case UPPER_RIGHT:
			directionKeys.remove(key);
			break;
		}
		final KeyState s = createState();
		final KeyCommand command;
		if (s != state) {
			command = s.toKeyCommand();
		} else {
			command = KeyCommand.NONE;
		}
		state = s;
		return command;
	}

	public KeyState getState() {
		return state;
	}

}
