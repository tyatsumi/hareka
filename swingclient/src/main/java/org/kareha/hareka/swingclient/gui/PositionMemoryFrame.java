package org.kareha.hareka.swingclient.gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.util.ResourceBundle;

import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JInternalFrame;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.WindowConstants;

import org.kareha.hareka.client.field.FieldEntity;
import org.kareha.hareka.client.field.PositionMemory;
import org.kareha.hareka.swingclient.Strap;

@SuppressWarnings("serial")
public class PositionMemoryFrame extends JInternalFrame {

	private static final int FRAME_WIDTH = 256;
	private static final int FRAME_HEIGHT = 192;

	private enum BundleKey {
		Title, NotConnected,

		WhatIsTheNameOfThisPosition, Somewhere,

		Memorize, Teleport,

		Up, Down, Delete, ConfirmDelete,
	}

	private final Strap strap;
	private final JList<PositionMemory> list;

	public PositionMemoryFrame(final Strap strap) {
		super("", true, true, true, true);
		this.strap = strap;
		setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
		final ResourceBundle bundle = ResourceBundle.getBundle(PositionMemoryFrame.class.getName());
		setTitle(bundle.getString(BundleKey.Title.name()));

		list = new JList<>(new DefaultListModel<>());
		final JScrollPane scrollPane = new JScrollPane(list, ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED,
				ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

		final JPanel southPanel = new JPanel();
		final JButton memorizeButton = new JButton(bundle.getString(BundleKey.Memorize.name()));
		memorizeButton.addActionListener(e -> {
			if (!strap.isConnected()) {
				return;
			}
			strap.control().getPositionMemory();
		});
		southPanel.add(memorizeButton);
		final JButton teleportButton = new JButton(bundle.getString(BundleKey.Teleport.name()));
		teleportButton.addActionListener(e -> {
			final int index = list.getSelectedIndex();
			if (index == -1) {
				return;
			}
			final PositionMemory memory = list.getModel().getElementAt(index);
			if (!strap.isConnected()) {
				return;
			}
			strap.control().teleportToPositionMemory(memory.getData());
		});
		southPanel.add(teleportButton);

		final JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.Y_AXIS));
		final JButton upButton = new JButton(bundle.getString(BundleKey.Up.name()));
		upButton.addActionListener(e -> {
			final int index = list.getSelectedIndex();
			if (index == -1) {
				return;
			}
			if (index < 1) {
				return;
			}
			if (!strap.isConnected()) {
				return;
			}
			final PositionMemory memory = strap.userControl().removePositionMemory(index);
			strap.userControl().addPositionMemory(index - 1, memory);
			final PositionMemory memory2 = getModel().remove(index);
			getModel().add(index - 1, memory2);
			list.setSelectedIndex(index - 1);
		});
		buttonPanel.add(upButton);
		final JButton downButton = new JButton(bundle.getString(BundleKey.Down.name()));
		downButton.addActionListener(e -> {
			final int index = list.getSelectedIndex();
			if (index == -1) {
				return;
			}
			if (index >= list.getModel().getSize() - 1) {
				return;
			}
			if (!strap.isConnected()) {
				return;
			}
			final PositionMemory memory = strap.userControl().removePositionMemory(index);
			strap.userControl().addPositionMemory(index + 1, memory);
			final PositionMemory memory2 = getModel().remove(index);
			getModel().add(index + 1, memory2);
			list.setSelectedIndex(index + 1);
		});
		buttonPanel.add(downButton);
		final JButton deleteButton = new JButton(bundle.getString(BundleKey.Delete.name()));
		deleteButton.addActionListener(e -> {
			final int index = list.getSelectedIndex();
			if (index == -1) {
				return;
			}
			if (strap.isConnected()) {
				return;
			}
			final int result = JOptionPane.showInternalConfirmDialog(this,
					bundle.getString(BundleKey.ConfirmDelete.name()));
			if (result != JOptionPane.YES_OPTION) {
				return;
			}
			strap.userControl().removePositionMemory(index);
			getModel().remove(index);
		});
		buttonPanel.add(deleteButton);

		add(scrollPane, BorderLayout.CENTER);
		add(southPanel, BorderLayout.SOUTH);
		add(buttonPanel, BorderLayout.EAST);
		setPreferredSize(new Dimension(FRAME_WIDTH, FRAME_HEIGHT));
		pack();
	}

	private DefaultListModel<PositionMemory> getModel() {
		return (DefaultListModel<PositionMemory>) list.getModel();
	}

	public void initPositionMemories(final FieldEntity entity) {
		getModel().clear();
		if (!strap.isConnected()) {
			return;
		}
		for (final PositionMemory memory : strap.userControl().getPositionMemoryList()) {
			if (!memory.getEntityId().equals(entity.getLocalId())) {
				continue;
			}
			getModel().addElement(memory);
		}
	}

	public void addPositionMemory(final PositionMemory memory) {
		final ResourceBundle bundle = ResourceBundle.getBundle(PositionMemoryFrame.class.getName());
		final String defaultName = bundle.getString(BundleKey.Somewhere.name()) + " " + list.getModel().getSize();
		final String name = JOptionPane.showInputDialog(this,
				bundle.getString(BundleKey.WhatIsTheNameOfThisPosition.name()), defaultName);
		if (name == null) {
			memory.setName(defaultName);
		} else {
			memory.setName(name);
		}
		getModel().addElement(memory);
	}

}
