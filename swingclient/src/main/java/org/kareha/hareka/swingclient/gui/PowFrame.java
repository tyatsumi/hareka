package org.kareha.hareka.swingclient.gui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.math.BigInteger;
import java.util.ResourceBundle;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicInteger;

import javax.swing.FocusManager;
import javax.swing.JButton;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import javax.swing.Timer;
import javax.swing.event.InternalFrameAdapter;
import javax.swing.event.InternalFrameEvent;

import org.kareha.hareka.annotation.Private;
import org.kareha.hareka.pow.Pow;
import org.kareha.hareka.pow.PowUtil;
import org.kareha.hareka.swingclient.Strap;

@SuppressWarnings("serial")
public class PowFrame extends JInternalFrame {

	public static interface Handler {

		void performed(byte[] nonce);

		void canceled();

	}

	private enum BundleKey {
		ProofOfWork, Cancel,
	}

	private static final int PREF_WIDTH = 256;
	private static final int PREF_HEIGHT = 128;

	@Private
	final Strap strap;
	@Private
	final JProgressBar bar;
	Timer timer;

	private final Worker worker;

	public PowFrame(final Strap strap, final BigInteger target, final byte[] data, final String message,
			final Handler handler) {
		this(strap, target, data, Runtime.getRuntime().availableProcessors(), message, handler);
	}

	public PowFrame(final Strap strap, final BigInteger target, final byte[] data, final int threads,
			final String message, final Handler handler) {
		super("", false, false, false, false);
		this.strap = strap;
		final Gui gui = strap.getGui();
		final ResourceBundle bundle = ResourceBundle.getBundle(PowFrame.class.getName());

		final Component focusedComponent = FocusManager.getCurrentManager().getFocusOwner();
		addInternalFrameListener(new InternalFrameAdapter() {
			@Override
			public void internalFrameClosed(final InternalFrameEvent e) {
				if (focusedComponent != null) {
					focusedComponent.requestFocus();
				}
			}

			@Override
			public void internalFrameOpened(final InternalFrameEvent e) {
				SwingUtilities.invokeLater(() -> {
					if (focusedComponent != null) {
						focusedComponent.requestFocus();
					}
				});
			}
		});

		worker = new Worker(target, data, threads, handler);

		final JLabel messageLabel = new JLabel(message);
		bar = new JProgressBar();
		final Pow.Result prevPowResult = strap.getPrevPowResult();
		if (prevPowResult == null) {
			bar.setIndeterminate(true);
		} else {
			final int RESOLUTION = 16;
			final AtomicInteger progress = new AtomicInteger();
			timer = new Timer((int) (PowUtil.estimatedTime(prevPowResult.nonce(), prevPowResult.elapsedTime(), target)
					/ RESOLUTION), new ActionListener() {
						@Override
						public void actionPerformed(final ActionEvent e) {
							bar.setValue(100 * progress.get() / RESOLUTION);
							progress.set(progress.get() + 1);
						}
					});
		}

		final JButton cancelButton = new JButton(bundle.getString(BundleKey.Cancel.name()));
		cancelButton.addActionListener(e -> worker.cancel(true));

		add(messageLabel, BorderLayout.NORTH);
		final JPanel barPanel = new JPanel();
		barPanel.add(bar);
		add(barPanel, BorderLayout.CENTER);
		final JPanel buttonPanel = new JPanel();
		buttonPanel.add(cancelButton);
		add(buttonPanel, BorderLayout.SOUTH);

		setPreferredSize(new Dimension(PREF_WIDTH, PREF_HEIGHT));
		pack();

		gui.getViewPane().add(this);
		gui.center(this);
		setVisible(true);

		if (timer != null) {
			timer.start();
		}
		worker.execute();
	}

	private class Worker extends SwingWorker<Pow.Result, Integer> {

		private final BigInteger target;
		private final byte[] data;
		private final int threads;
		private final Handler handler;

		Worker(final BigInteger target, final byte[] data, final int threads, final Handler handler) {
			this.target = target;
			this.data = data;
			this.threads = threads;
			this.handler = handler;
		}

		@Override
		protected Pow.Result doInBackground() throws InterruptedException {
			return new Pow(target, data, threads).perform();
		}

		@Override
		protected void done() {
			final Gui gui = strap.getGui();
			try {
				final Pow.Result result = get();
				handler.performed(result.nonce());
				strap.setPrevPowResult(result);
			} catch (final CancellationException e) {
				handler.canceled();
			} catch (final InterruptedException e) {
				Thread.currentThread().interrupt();
				return;
			} catch (final ExecutionException e) {
				JOptionPane.showMessageDialog(gui.getViewPane(), e.getMessage());
				return;
			} finally {
				if (timer != null) {
					timer.stop();
					bar.setValue(100);
				}
				gui.getViewPane().remove(PowFrame.this);
				dispose();
				gui.getViewPane().repaint();
			}
		}
	}

}
