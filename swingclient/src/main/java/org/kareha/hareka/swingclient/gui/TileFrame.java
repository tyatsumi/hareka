package org.kareha.hareka.swingclient.gui;

import java.awt.BorderLayout;
import java.util.ResourceBundle;

import javax.swing.JInternalFrame;
import javax.swing.JTextField;
import javax.swing.WindowConstants;

import org.kareha.hareka.field.TileType;
import org.kareha.hareka.swingclient.Strap;

@SuppressWarnings("serial")
public class TileFrame extends JInternalFrame {

	private enum BundleKey {
		Tile,
	}

	private final TileSelector tileSelector;
	private final JTextField elevationField;

	public TileFrame(final Strap strap) {
		super("", false, false, true, false);
		final ResourceBundle bundle = ResourceBundle.getBundle(TileFrame.class.getName());
		setTitle(bundle.getString(BundleKey.Tile.name()));
		setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
		tileSelector = new TileSelector(strap);
		elevationField = new JTextField("0", 6);
		add(tileSelector, BorderLayout.CENTER);
		add(elevationField, BorderLayout.EAST);
		setBorder(null);
		pack();
	}

	public TileType getTile() {
		return tileSelector.getTile();
	}

	public int getElevation() {
		try {
			return Integer.parseInt(elevationField.getText());
		} catch (final NumberFormatException e) {
			elevationField.setText("0");
			return 0;
		}
	}

}
