package org.kareha.hareka.swingclient.keyboard;

import java.awt.event.KeyEvent;
import java.util.HashMap;
import java.util.Map;

public class Keymap {

	private final Map<Integer, GameKey> map = new HashMap<>();

	public Keymap() {
		map.put(KeyEvent.VK_A, GameKey.LEFT);
		map.put(KeyEvent.VK_D, GameKey.RIGHT);
		map.put(KeyEvent.VK_W, GameKey.UP);
		map.put(KeyEvent.VK_S, GameKey.DOWN);

		map.put(KeyEvent.VK_LEFT, GameKey.LEFT);
		map.put(KeyEvent.VK_RIGHT, GameKey.RIGHT);
		map.put(KeyEvent.VK_UP, GameKey.UP);
		map.put(KeyEvent.VK_DOWN, GameKey.DOWN);

		map.put(KeyEvent.VK_X, GameKey.DOWN);
		map.put(KeyEvent.VK_E, GameKey.UPPER_RIGHT);
		map.put(KeyEvent.VK_Z, GameKey.LOWER_LEFT);

		map.put(KeyEvent.VK_Q, GameKey.ROTATE_LEFT);
		map.put(KeyEvent.VK_R, GameKey.ROTATE_RIGHT);
		map.put(KeyEvent.VK_F, GameKey.MIRROR);

		map.put(KeyEvent.VK_SPACE, GameKey.ATTACK);
	}

	public GameKey get(final int keyCode) {
		final GameKey gameKey = map.get(keyCode);
		if (gameKey == null) {
			return GameKey.NONE;
		}
		return gameKey;
	}

}
