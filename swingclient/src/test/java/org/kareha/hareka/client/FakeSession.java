package org.kareha.hareka.client;

import java.net.Socket;

import org.kareha.hareka.client.protocol.WorldServerPacket;
import org.kareha.hareka.protocol.HandlerTable;
import org.kareha.hareka.protocol.ProtocolSocket;
import org.kareha.hareka.protocol.WorldClientPacketType;
import org.kareha.hareka.swingclient.Strap;

public class FakeSession extends WorldSession {

	@SuppressWarnings("unused")
	private final Strap strap;

	FakeSession(final Strap strap) {
		super(strap.getContext());
		this.strap = strap;
	}

	@Override
	ProtocolSocket<WorldClientPacketType, WorldSession> newPacketSocket(final Socket socket,
			final HandlerTable<WorldClientPacketType, WorldSession> parserTable) {
		return null;
	}

	public static WorldSession newInstance(final Strap strap) {
		final WorldSession session = new FakeSession(strap);
		session.initialize(null, null);
		return session;
	}

	@Override
	public void write(final WorldServerPacket packet) {
		// nothing to do
	}

}
