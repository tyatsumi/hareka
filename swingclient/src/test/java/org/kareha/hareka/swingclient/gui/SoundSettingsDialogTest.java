package org.kareha.hareka.swingclient.gui;

import org.kareha.hareka.sound.SoundContext;

final class SoundSettingsDialogTest {

	private SoundSettingsDialogTest() {
		throw new AssertionError();
	}

	public static void main(final String[] args) {
		final SoundContext context = new SoundContext(null);
		new SoundSettingsDialog(null, context);
	}

}
