package org.kareha.hareka.swingclient.gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;
import javax.xml.bind.JAXBException;

import org.kareha.hareka.LocalEntityId;
import org.kareha.hareka.client.Context;
import org.kareha.hareka.client.FakeSession;
import org.kareha.hareka.client.WorldSession;
import org.kareha.hareka.client.chat.ChatEntity;
import org.kareha.hareka.client.chat.ChatSession;
import org.kareha.hareka.client.mirror.ChatMirror;
import org.kareha.hareka.swingclient.Strap;
import org.kareha.hareka.swingclient.gui.ChatPanel.ChannelItem;

final class ChatPanelTest {

	private static final Logger logger = Logger.getLogger(ChatPanelTest.class.getName());

	private ChatPanelTest() {
		throw new AssertionError();
	}

	public static void main(final String[] args) throws IOException, JAXBException {
		final Context context = new Context(null);

		final ChatEntity selfEntity = new ChatEntity(LocalEntityId.valueOf(new byte[16]), "Self");
		// selfEntity.setNickname("Selfy");

		try {
			SwingUtilities.invokeAndWait(() -> {
				final Strap strap = new Strap(null, context, null);
				final Gui gui = strap.getGui();
				final WorldSession session = FakeSession.newInstance(strap);
				strap.setSession(session);
				session.getMirrors().getSelfMirror().setChatEntity(selfEntity);

				final ChatPanel chatPanel = gui.getChatFrame().getChatPanel();

				session.getMirrors().getChatMirror().addListener(new ChatMirror.Listener() {
					@Override
					public void chatSessionAdded(final ChatSession chatChannel) {
						final ChannelItem channelItem = chatPanel.getChatPanelChannel(chatChannel);
						chatChannel.addListener(new ChatSession.Listener() {
							@Override
							public void chatReceived(final ChatSession.Message message) {
								channelItem.addChat(message.getSpeaker(), message.getContent());
							}
						});
					}

					@Override
					public void chatSessionRemoved(final ChatSession chatChannel) {
						chatPanel.removeChatPanelChannel(chatChannel);
					}
				});

				chatPanel.setPreferredSize(new Dimension(384, 192));

				final TestPanel testPanel = new TestPanel(session, chatPanel);
				final JPanel borderPanel = new JPanel(new BorderLayout());
				borderPanel.add(new JLabel("---- TEST ----"), BorderLayout.NORTH);
				borderPanel.add(testPanel, BorderLayout.CENTER);

				final JFrame frame = new JFrame(ChatPanelTest.class.getSimpleName());
				frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
				frame.add(chatPanel, BorderLayout.CENTER);
				frame.add(borderPanel, BorderLayout.SOUTH);
				frame.pack();
				frame.setResizable(false);
				frame.setVisible(true);

				chatPanel.requestFocusToInput();

				session.getMirrors().getChatMirror().getLocalChatSession();
			});
		} catch (final InvocationTargetException e) {
			logger.log(Level.SEVERE, "", e);
			return;
		} catch (final InterruptedException e) {
			Thread.currentThread().interrupt();
			logger.log(Level.SEVERE, "", e);
			return;
		}
	}

	@SuppressWarnings("serial")
	private static class TestPanel extends JPanel {

		private final AtomicInteger counter = new AtomicInteger(256);

		TestPanel(final WorldSession session, final ChatPanel chatPanel) {
			final JTextField alternateField = new JTextField("Alternate Field");
			alternateField.addActionListener(e -> chatPanel.requestFocusToInput());
			chatPanel.setAlternateComponent(alternateField);

			final JComboBox<String> nameComboBox = new JComboBox<>();
			nameComboBox.setEditable(true);
			nameComboBox.addItem("Alice");
			nameComboBox.addItem("Bob");
			nameComboBox.addItem("Carol");

			final JTextField contentField = new JTextField("hello");

			final JButton sendButton = new JButton("Send");
			sendButton.addActionListener(e -> {
				final int i = counter.getAndIncrement();
				final byte[] b = new byte[16];
				b[12] = (byte) (i >>> 24);
				b[13] = (byte) (i >>> 16);
				b[14] = (byte) (i >>> 8);
				b[15] = (byte) i;
				final ChatEntity entity = new ChatEntity(LocalEntityId.valueOf(b),
						(String) nameComboBox.getSelectedItem());
				session.getMirrors().getChatMirror().getPrivateChatSession(entity).receiveChat(entity,
						contentField.getText());
			});

			setLayout(new BorderLayout());
			add(alternateField, BorderLayout.NORTH);
			add(nameComboBox, BorderLayout.WEST);
			add(contentField, BorderLayout.CENTER);
			add(sendButton, BorderLayout.EAST);
		}

	}

}
