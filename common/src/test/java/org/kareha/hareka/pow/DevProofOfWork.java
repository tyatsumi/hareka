package org.kareha.hareka.pow;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.kareha.hareka.annotation.GuardedBy;
import org.kareha.hareka.annotation.Private;

// SHA_d-256: m -> SHA-256(SHA-256(0^512 || m))
public class DevProofOfWork {

	private static final String HASH_ALGORITHM = "SHA-256";
	@Private
	static final byte[] bound;

	static {
		bound = new byte[256 / 8];
		Arrays.fill(bound, (byte) 0xff);
	}

	public interface Listener {

		void progress(int goal, int current);

	}

	@Private
	final byte[] target;
	@Private
	final byte[] data;
	private final int threads;

	public DevProofOfWork(final byte[] target, final byte[] data, final int threads) {
		this.target = target.clone();
		this.data = data.clone();
		this.threads = threads;
	}

	public DevProofOfWork(final byte[] target, final byte[] data) {
		this(target, data, Runtime.getRuntime().availableProcessors());
	}

	@Private
	static int bitLength(final byte[] a) {
		for (int i = 0; i < a.length; i++) {
			int b = a[i];
			for (int k = 0; k < 8; k++) {
				if ((b & 0x80) != 0) {
					return 7 - k + 8 * (a.length - 1 - i);
				}
				b <<= 1;
			}
		}
		return 0;
	}

	private class Total {

		private final Listener listener;
		@GuardedBy("this")
		private byte[] min = bound.clone();
		private final int targetBits = bitLength(target);

		Total(final Listener listener) {
			this.listener = listener;
		}

		void init() {
			listener.progress(256 - targetBits, 0);
		}

		synchronized byte[] update(final byte[] candidate) {
			if (compareByteArray(candidate, min) < 0) {
				min = candidate.clone();
				listener.progress(256 - targetBits, 256 - bitLength(min));
			}
			return min.clone();
		}

	}

	@Private
	static byte[] longToByteArray(final long v) {
		long bound = 0x100;
		for (int n = 1; n < 8; n++) {
			if (v < bound || n >= 7) {
				final byte[] a = new byte[n];
				for (int i = 0; i < n; i++) {
					a[i] = (byte) (v >> (8 * (n - 1 - i)));
				}
				return a;
			}
			bound *= 0x100;
		}
		throw new AssertionError();
	}

	@Private
	static int compareByteArray(final byte[] a, final byte[] b) {
		for (int i = 0; i < a.length; i++) {
			int c = (a[i] & 0xff) - (b[i] & 0xff);
			if (c != 0) {
				return c;
			}
		}
		return 0;
	}

	private class Task implements Callable<byte[]> {

		private final int offset;
		private final int step;
		private final Total total;
		private final MessageDigest md0;

		Task(final int offset, final int step, final Total total) {
			this.offset = offset;
			this.step = step;
			this.total = total;

			final MessageDigest md;
			try {
				md = MessageDigest.getInstance(HASH_ALGORITHM);
			} catch (final NoSuchAlgorithmException e) {
				throw new AssertionError(e);
			}
			if (md instanceof Cloneable) {
				md.update(new byte[64]);
				md.update(data);
				md0 = md;
			} else {
				md0 = null;
			}
		}

		@Override
		public byte[] call() throws InterruptedException {
			long nonce = offset;
			byte[] min = bound.clone();
			while (!Thread.currentThread().isInterrupted()) {
				final byte[] b = longToByteArray(nonce);
				final byte[] current = hash(b);
				if (compareByteArray(current, min) < 0) {
					min = total.update(current);
				}
				if (compareByteArray(current, target) <= 0) {
					return b;
				}
				nonce += step;
			}
			throw new InterruptedException();
		}

		private byte[] hash(final byte[] nonce) {
			final MessageDigest md;
			if (md0 == null) {
				try {
					md = MessageDigest.getInstance(HASH_ALGORITHM);
				} catch (final NoSuchAlgorithmException e) {
					throw new AssertionError(e);
				}
				md.update(new byte[64]);
				md.update(data);
			} else {
				try {
					md = (MessageDigest) md0.clone();
				} catch (final CloneNotSupportedException e) {
					throw new AssertionError();
				}
			}
			return md.digest(md.digest(nonce));
		}

	}

	public byte[] generate(final Listener listener) throws InterruptedException {
		final ExecutorService executorService = Executors.newFixedThreadPool(threads);
		final CompletionService<byte[]> completionService = new ExecutorCompletionService<>(executorService);
		final Total total = new Total(listener);
		total.init();
		try {
			for (int i = 0; i < threads; i++) {
				completionService.submit(new Task(i, threads, total));
			}
			final Future<byte[]> future = completionService.take();
			try {
				return future.get();
			} catch (final ExecutionException e) {
				throw new RuntimeException(e);
			}
		} finally {
			executorService.shutdownNow();
		}
	}

	byte[] naiveHash(final byte[] nonce) {
		final MessageDigest md;
		try {
			md = MessageDigest.getInstance(HASH_ALGORITHM);
		} catch (final NoSuchAlgorithmException e) {
			throw new AssertionError(e);
		}
		md.update(new byte[64]);
		md.update(data);
		return md.digest(md.digest(nonce));
	}

	public boolean verify(final byte[] nonce) {
		return compareByteArray(naiveHash(nonce), target) <= 0;
	}

}
