package org.kareha.hareka.pow;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class NaiveProofOfWork {

	private static final String HASH_ALGORITHM = "SHA-256";
	private static final BigInteger bound = BigInteger.ONE.shiftLeft(256);

	public interface Listener {

		void progress(int goal, int current);

	}

	private final BigInteger target;
	private final byte[] data;
	private final MessageDigest md0;

	public NaiveProofOfWork(final BigInteger target, final byte[] data) {
		this.target = target;

		final MessageDigest md;
		try {
			md = MessageDigest.getInstance(HASH_ALGORITHM);
		} catch (final NoSuchAlgorithmException e) {
			throw new AssertionError(e);
		}
		boolean clonable = true;
		try {
			md.clone();
		} catch (final CloneNotSupportedException e) {
			clonable = false;
		}
		if (clonable) {
			md.update(new byte[64]);
			md.update(data);
			md0 = md;
			this.data = null;
		} else {
			md0 = null;
			this.data = data.clone();
		}
	}

	public byte[] generate() {
		BigInteger nonce = BigInteger.ZERO;
		while (true) {
			final byte[] b = nonce.toByteArray();
			if (verify(b)) {
				return b;
			}
			nonce = nonce.add(BigInteger.ONE);
		}
	}

	public byte[] generate(final Listener listener) throws InterruptedException {
		BigInteger nonce = BigInteger.ZERO;
		BigInteger min = bound;
		final int targetBits = target.bitLength();
		listener.progress(256 - targetBits, 0);
		while (!Thread.currentThread().isInterrupted()) {
			final byte[] b = nonce.toByteArray();
			final BigInteger current = new BigInteger(1, hash(b));
			if (current.compareTo(min) == -1) {
				min = current;
				listener.progress(256 - targetBits, 256 - min.bitLength());
			}
			if (current.compareTo(target) == -1) {
				return b;
			}
			nonce = nonce.add(BigInteger.ONE);
		}
		throw new InterruptedException();
	}

	byte[] hash(final byte[] nonce) {
		final MessageDigest md;
		if (md0 == null) {
			try {
				md = MessageDigest.getInstance(HASH_ALGORITHM);
			} catch (final NoSuchAlgorithmException e) {
				throw new AssertionError(e);
			}
			md.update(new byte[64]);
			md.update(data);
		} else {
			try {
				md = (MessageDigest) md0.clone();
			} catch (final CloneNotSupportedException e) {
				throw new AssertionError();
			}
		}
		return md.digest(md.digest(nonce));
	}

	public boolean verify(final byte[] nonce) {
		return new BigInteger(1, hash(nonce)).compareTo(target) == -1;
	}

}
