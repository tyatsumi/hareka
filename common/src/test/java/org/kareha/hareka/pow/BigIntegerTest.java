package org.kareha.hareka.pow;

import java.math.BigInteger;
import java.util.Arrays;

final class BigIntegerTest {

	private BigIntegerTest() {
		throw new AssertionError();
	}

	public static void main(final String[] args) {
		final BigInteger a = BigInteger.ONE.shiftLeft(256).subtract(BigInteger.ONE);
		System.out.println(a);
		System.out.println("bitCount: " + a.bitCount());
		System.out.println("bitLength: " + a.bitLength());

		final byte[] ba = new byte[32];
		Arrays.fill(ba, (byte) 0xff);
		final BigInteger b = new BigInteger(1, ba);
		System.out.println(b);
		System.out.println("bitCount: " + b.bitCount());
		System.out.println("bitLength: " + b.bitLength());
	}

}
