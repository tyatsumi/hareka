package org.kareha.hareka.pow;

import java.util.Arrays;

final class DevProofOfWorkTest {

	private static final byte[] data = { 0, 1, 2, 3, 4, 5, 6, 7 };

	private DevProofOfWorkTest() {
		throw new AssertionError();
	}

	public static void main(final String[] args) {
		// final SecureRandom random = new SecureRandom();
		// final byte[] data = new byte[32];
		// random.nextBytes(data);
		final byte[] target = new byte[256 / 8];
		Arrays.fill(target, (byte) 0xff);
		// 21 bits
		target[0] = (byte) 0x00;
		target[1] = (byte) 0x00;
		target[2] = (byte) 0x07;
		System.out.println("target:");
		printByteArray(target);
		final DevProofOfWork pow = new DevProofOfWork(target, data);
		final long start = System.currentTimeMillis();
		final byte[] nonce;
		try {
			nonce = pow.generate(new DevProofOfWork.Listener() {
				@Override
				public void progress(final int goal, final int current) {
					for (int i = 0; i < current; i++) {
						System.out.print("*");
					}
					for (int i = current; i < goal; i++) {
						System.out.print(".");
					}
					System.out.println();
				}
			});
		} catch (final InterruptedException e) {
			Thread.currentThread().interrupt();
			return;
		}
		final long end = System.currentTimeMillis();
		final float t = (end - start) / 1000f;
		System.out.println(t);

		final DevProofOfWork pow2 = new DevProofOfWork(target, data);
		final boolean result = pow2.verify(nonce);
		System.out.println(result);

		printByteArray(nonce);
		final byte[] hash = pow.naiveHash(nonce);
		printByteArray(hash);
	}

	private static void printByteArray(final byte[] b) {
		for (int i = 0; i < b.length; i++) {
			System.out.print(String.format("%02x", b[i] & 0xff));
		}
		System.out.println();
	}

}
