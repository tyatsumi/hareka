package org.kareha.hareka.util;

import static org.junit.Assert.assertEquals;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.junit.Test;
import org.kareha.hareka.logging.xml.XmlFragmentInput;

public class XmlFragmentInputTest {

	@Test
	public void testNext() throws JAXBException, IOException {
		final int n = 4;
		final byte[] xml = generateXml(n);
		final Collection<Entry> entries = load(new ByteArrayInputStream(xml));
		assertEquals(n, entries.size());
		int i = 0;
		for (final Entry entry : entries) {
			assertEquals(i, entry.value);
			i++;
		}

	}

	private static byte[] generateXml(final int n) throws JAXBException {
		final JAXBContext context = JAXBContext.newInstance(Entry.class);
		final ByteArrayOutputStream out = new ByteArrayOutputStream();
		for (int i = 0; i < n; i++) {
			final Entry entry = new Entry(i);
			final Marshaller marshaller = context.createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
			marshaller.marshal(entry, out);
		}
		return out.toByteArray();
	}

	private static Collection<Entry> load(final InputStream in) throws JAXBException, IOException {
		final JAXBContext context = JAXBContext.newInstance(Entry.class);
		final List<Entry> list = new ArrayList<>();
		final XmlFragmentInput logInput = new XmlFragmentInput(in);
		while (true) {
			final byte[] b = logInput.next();
			if (b == null) {
				break;
			}
			final Unmarshaller unmarshaller = context.createUnmarshaller();
			final Entry entry = (Entry) unmarshaller.unmarshal(new ByteArrayInputStream(b));
			list.add(entry);
		}
		return list;
	}

	@XmlRootElement(name = "entry")
	@XmlAccessorType(XmlAccessType.NONE)
	private static class Entry {

		@XmlElement
		int value;

		@SuppressWarnings("unused")
		private Entry() {
			// used by JAXB
		}

		Entry(final int value) {
			this.value = value;
		}

	}

}
