package org.kareha.hareka.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Test;

public final class CamelCaseTest {

	@Test
	public void testSnakeCaseToCamelCase() {
		assertNull(CamelCase.snakeToCamel(null));
		assertNull(CamelCase.snakeToCamel(""));
		assertNull(CamelCase.snakeToCamel("hello"));
		assertEquals("Hello", CamelCase.snakeToCamel("HELLO"));
		assertEquals("HelloWorld", CamelCase.snakeToCamel("HELLO_WORLD"));
	}

}
