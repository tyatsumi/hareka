package org.kareha.hareka.protocol;

import static org.junit.Assert.assertEquals;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Random;

import org.junit.Test;

public class CompactNumbersTest {

	private static final double SQRT3 = Math.sqrt(3);

	@Test
	public void testUInt() throws IOException, ProtocolException {
		for (long i = 2; i <= Integer.MAX_VALUE; i = (long) (i * SQRT3)) {
			final int size = (int) i;
			final ByteArrayOutputStream out = new ByteArrayOutputStream();
			CompactNumbers.writeUInt(out, size);
			final InputStream in = new ByteArrayInputStream(out.toByteArray());
			final int result = CompactNumbers.readUInt(in);
			assertEquals("size: " + Integer.toHexString(size), size, result);
		}
	}

	@Test
	public void testIntPositive() throws IOException, ProtocolException {
		for (long i = 2; i <= Integer.MAX_VALUE; i = (long) (i * SQRT3)) {
			final int size = (int) i;
			final ByteArrayOutputStream out = new ByteArrayOutputStream();
			CompactNumbers.writeInt(out, size);
			final InputStream in = new ByteArrayInputStream(out.toByteArray());
			final int result = CompactNumbers.readInt(in);
			assertEquals("size: " + Integer.toHexString(size), size, result);
		}
	}

	@Test
	public void testIntNegative() throws IOException, ProtocolException {
		for (long i = 2; i <= Integer.MAX_VALUE; i = (long) (i * SQRT3)) {
			final int size = -(int) i;
			final ByteArrayOutputStream out = new ByteArrayOutputStream();
			CompactNumbers.writeInt(out, size);
			final InputStream in = new ByteArrayInputStream(out.toByteArray());
			final int result = CompactNumbers.readInt(in);
			assertEquals("size: " + Integer.toHexString(size), size, result);
		}
	}

	@Test
	public void testULong() throws IOException, ProtocolException {
		final Random random = new Random();
		for (int i = 0; i <= Short.MAX_VALUE; i++) {
			final long v = random.nextLong();
			if (v < 0) {
				continue;
			}
			final ByteArrayOutputStream out = new ByteArrayOutputStream();
			CompactNumbers.writeULong(out, v);
			final InputStream in = new ByteArrayInputStream(out.toByteArray());
			final long result = CompactNumbers.readULong(in);
			assertEquals("value: " + Long.toHexString(v), v, result);
		}
	}

	@Test
	public void testLongPositive() throws IOException, ProtocolException {
		final Random random = new Random();
		for (int i = 0; i <= Short.MAX_VALUE; i++) {
			final long v = random.nextLong();
			if (v < 0) {
				continue;
			}
			final ByteArrayOutputStream out = new ByteArrayOutputStream();
			CompactNumbers.writeLong(out, v);
			final InputStream in = new ByteArrayInputStream(out.toByteArray());
			final long result = CompactNumbers.readLong(in);
			assertEquals("value: " + Long.toHexString(v), v, result);
		}
	}

	@Test
	public void testLongNegative() throws IOException, ProtocolException {
		final Random random = new Random();
		for (int i = 0; i <= Short.MAX_VALUE; i++) {
			final long v = random.nextLong();
			if (v >= 0) {
				continue;
			}
			final ByteArrayOutputStream out = new ByteArrayOutputStream();
			CompactNumbers.writeLong(out, v);
			final InputStream in = new ByteArrayInputStream(out.toByteArray());
			final long result = CompactNumbers.readLong(in);
			assertEquals("value: " + Long.toHexString(v), v, result);
		}
	}

}
