package org.kareha.hareka.protocol;

import static org.junit.Assert.assertEquals;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import org.junit.Test;

public class PacketInputStreamTest {

	private static final String TEST_STRING = "Hello World";

	@Test
	public void string() throws IOException, ProtocolException {
		final ByteArrayOutputStream out = new ByteArrayOutputStream();
		try (final ProtocolOutputStream pout = new ProtocolOutputStream()) {
			pout.writeString(TEST_STRING);
			pout.writeTo(out);
		}
		final String resultString;
		try (final ProtocolInputStream pin = new ProtocolInputStream(out.toByteArray())) {
			resultString = pin.readString();
		}
		assertEquals(TEST_STRING, resultString);
	}

}
