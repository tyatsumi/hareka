package org.kareha.hareka;

public interface ManuallyClosable {

	void close() throws Exception;

}
