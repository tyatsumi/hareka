package org.kareha.hareka.util;

public final class HtmlUtil {

	private HtmlUtil() {
		throw new AssertionError();
	}

	public static String escape(final String original) {
		if (original == null) {
			return null;
		}
		final StringBuilder builder = new StringBuilder(original.length());
		for (int index = 0; index < original.length(); index++) {
			final char c = original.charAt(index);
			switch (c) {
			default:
				builder.append(c);
				break;
			case '<':
				builder.append("&lt;");
				break;
			case '>':
				builder.append("&gt;");
				break;
			case '&':
				builder.append("&amp;");
				break;
			case '"':
				builder.append("&quot;");
				break;
			}
		}
		return builder.toString();
	}

}
