package org.kareha.hareka.util;

/**
 * A utility class for processing strings.
 */
public final class CamelCase {

	private CamelCase() {
		throw new AssertionError();
	}

	private static boolean isSnakeCase(final String s) {
		if (s == null) {
			return false;
		}
		return s.matches("^[A-Z](_?[A-Z]+[0-9]*)*$");
	}

	/**
	 * Returns camel cased string converted from underscored string.
	 * 
	 * @param s the underscored string
	 * @return the camel cased string
	 */
	public static String snakeToCamel(final String s) {
		if (!isSnakeCase(s)) {
			return null;
		}

		final StringBuilder sb = new StringBuilder();

		boolean head = true;
		for (int i = 0; i < s.length(); i++) {
			final char c = s.charAt(i);
			if (c == '_') {
				head = true;
				continue;
			}
			final char cc;
			if (head) {
				cc = Character.toUpperCase(c);
				head = false;
			} else {
				cc = Character.toLowerCase(c);
			}
			sb.append(cc);
		}

		return sb.toString();
	}

}
