package org.kareha.hareka.util;

import java.io.UnsupportedEncodingException;

public final class Utf8Util {

	private Utf8Util() {
		throw new AssertionError();
	}

	// should be in-lined?
	public static int length(final String string) {
		try {
			return string.getBytes("UTF-8").length;
		} catch (final UnsupportedEncodingException e) {
			// UTF-8 encoding is expected to be always supported
			throw new AssertionError();
		}
	}

}
