package org.kareha.hareka.util;

import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;

public final class KeyPairUtil {

	private KeyPairUtil() {
		throw new AssertionError();
	}

	public static String exportKeyPair(final KeyPair keyPair) {
		final StringBuilder sb = new StringBuilder();
		sb.append("-----BEGIN PRIVATE KEY-----\n");
		sb.append(split(Base64.getEncoder().encodeToString(keyPair.getPrivate().getEncoded())));
		sb.append("-----END PRIVATE KEY-----\n");
		sb.append("-----BEGIN PUBLIC KEY-----\n");
		sb.append(split(Base64.getEncoder().encodeToString(keyPair.getPublic().getEncoded())));
		sb.append("-----END PUBLIC KEY-----\n");
		return sb.toString();
	}

	private static String split(final String v) {
		final StringBuilder sb = new StringBuilder();
		int i = 0;
		while (i < v.length()) {
			final int end;
			if (i + 64 < v.length()) {
				end = i + 64;
			} else {
				end = v.length();
			}
			sb.append(v.substring(i, end));
			sb.append("\n");
			i = end;
		}
		return sb.toString();
	}

	public static KeyPair importKeyPair(final String v) {
		PrivateKey privateKey = null;
		PublicKey publicKey = null;
		final String[] lines = v.split("\n");
		int i = 0;
		while (i < lines.length) {
			if (lines[i].equals("-----BEGIN PRIVATE KEY-----")) {
				i++;
				final StringBuilder sb = new StringBuilder();
				while (i < lines.length) {
					if (lines[i].equals("-----END PRIVATE KEY-----")) {
						i++;
						break;
					}
					sb.append(lines[i]);
					i++;
				}
				final byte[] encoded = Base64.getDecoder().decode(sb.toString());
				final KeyFactory keyFactory;
				try {
					keyFactory = KeyFactory.getInstance("RSA");
				} catch (final NoSuchAlgorithmException e) {
					throw new AssertionError();
				}
				final KeySpec keySpec = new PKCS8EncodedKeySpec(encoded);
				try {
					privateKey = keyFactory.generatePrivate(keySpec);
				} catch (final InvalidKeySpecException e) {
					return null;
				}
			} else if (lines[i].equals("-----BEGIN PUBLIC KEY-----")) {
				i++;
				final StringBuilder sb = new StringBuilder();
				while (i < lines.length) {
					if (lines[i].equals("-----END PUBLIC KEY-----")) {
						i++;
						break;
					}
					sb.append(lines[i]);
					i++;
				}
				final byte[] encoded = Base64.getDecoder().decode(sb.toString());
				final KeyFactory keyFactory;
				try {
					keyFactory = KeyFactory.getInstance("RSA");
				} catch (final NoSuchAlgorithmException e) {
					throw new AssertionError();
				}
				final KeySpec keySpec = new X509EncodedKeySpec(encoded);
				try {
					publicKey = keyFactory.generatePublic(keySpec);
				} catch (final InvalidKeySpecException e) {
					return null;
				}
			}
		}
		if (publicKey == null || privateKey == null) {
			return null;
		}
		return new KeyPair(publicKey, privateKey);
	}

}
