package org.kareha.hareka.util;

import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

public final class LogUtil {

	private static final Logger logger = Logger.getLogger(LogUtil.class.getName());
	private static final String LOG_LEVEL_KEY = "org.kareha.hareka.loglevel";

	private static Logger rootLogger;
	@SuppressWarnings("unused")
	private static Logger appRootLogger;

	private LogUtil() {
		throw new AssertionError();
	}

	public static void loadLogLevel(final Logger appRootLogger) {
		final String levelStr = System.getProperty(LOG_LEVEL_KEY);
		if (levelStr == null) {
			return;
		}
		final Level level;
		try {
			level = Level.parse(levelStr);
		} catch (final IllegalArgumentException e) {
			logger.log(Level.WARNING, "", e);
			return;
		}
		rootLogger = Logger.getLogger("");
		for (final Handler handler : rootLogger.getHandlers()) {
			handler.setLevel(level);
		}
		LogUtil.appRootLogger = appRootLogger;
		appRootLogger.setLevel(level);
		for (final Handler handler : appRootLogger.getHandlers()) {
			handler.setLevel(level);
		}
	}

}
