package org.kareha.hareka.util;

import java.io.File;
import java.io.IOException;

import org.kareha.hareka.annotation.NotPure;

public final class KeyStoreUtil {

	private KeyStoreUtil() {
		throw new AssertionError();
	}

	@NotPure
	public static boolean generate(final File directory, final String keyStorePassword, final String keyPassword,
			final String commonName) {
		final String[] args = { System.getProperty("java.home") + File.separator + "bin" + File.separator + "keytool",
				"-genkey", "-keyalg", "RSA", "-keystore", "KeyStore", "-keysize", "2048", "-storepass",
				keyStorePassword, "-keypass", keyPassword, "-noprompt", "-dname", "CN=" + commonName, };
		try {
			return new ProcessBuilder(args).directory(directory).start().waitFor() == 0;
		} catch (final IOException e) {
			return false;
		} catch (final InterruptedException e) {
			return false;
		}
	}

}
