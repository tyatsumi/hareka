package org.kareha.hareka.util;

import java.util.ArrayList;
import java.util.List;

/**
 * The command tokenizer class split a command line text into tokens.
 */
public final class CommandLineTokenizer {

	private String s;

	/**
	 * Constructs a command tokenizer for the specified string.
	 * 
	 * @param s
	 *            the string to be tokenized
	 */
	public CommandLineTokenizer(final String s) {
		this.s = s;
	}

	private int i;

	private static final int END = -1;
	private static final int QUOTATION = -2;

	private int nextChar() {
		if (i >= s.length()) {
			return END;
		}
		char c = s.charAt(i);
		i++;
		if (c == '"') {
			return QUOTATION;
		} else if (c == '\\') {
			if (i >= s.length()) {
				return END;
			}
			c = s.charAt(i);
			i++;
			return c;
		}
		return c;
	}

	private String nextToken() {
		if (i >= s.length()) {
			return null;
		}

		final StringBuilder b = new StringBuilder();

		boolean quotation = false;
		int c;
		while ((c = nextChar()) != END) {
			if (quotation) {
				if (c == QUOTATION) {
					quotation = false;
				} else {
					b.append((char) c);
				}
			} else {
				if (c == QUOTATION) {
					quotation = true;
				} else if (c == ' ') {
					break;
				} else {
					b.append((char) c);
				}
			}
		}

		return b.toString();
	}

	/**
	 * Returns the array of tokenized strings.
	 * 
	 * @return the array of tokenized strings
	 */
	public String[] getTokens() {
		final List<String> list = new ArrayList<>();

		String a;
		while ((a = nextToken()) != null) {
			list.add(a);
		}

		final String[] args = new String[list.size()];
		for (int i = 0, n = list.size(); i < n; i++) {
			args[i] = list.get(i);
		}
		return args;
	}

}
