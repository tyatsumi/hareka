package org.kareha.hareka.ui.swing;

import java.awt.Color;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingUtilities;
import javax.swing.text.BadLocationException;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;

import org.kareha.hareka.annotation.Private;
import org.kareha.hareka.logging.SimpleLogger;

@SuppressWarnings("serial")
public class LogPanel extends JScrollPane {

	// keep strong reference to keep the root logger from being garbage
	// collected
	private static final Logger rootLogger = Logger.getLogger("");

	private final JTextPane textPane;
	private int maxLogSize = 4 * 1024 * 1024;
	private final Style severeStyle;
	private final Style warningStyle;
	private final Style infoStyle;
	private final Style configStyle;
	private final Style fineStyle;
	private final Style finerStyle;
	private final Style finestStyle;

	private final Handler handler;

	private LogPanel() {
		textPane = new JTextPane();
		textPane.setEditable(false);

		severeStyle = textPane.addStyle("severe", null);
		warningStyle = textPane.addStyle("warning", null);
		infoStyle = textPane.addStyle("info", null);
		configStyle = textPane.addStyle("config", null);
		fineStyle = textPane.addStyle("fine", null);
		finerStyle = textPane.addStyle("finer", null);
		finestStyle = textPane.addStyle("finest", null);

		StyleConstants.setForeground(severeStyle, Color.RED);
		StyleConstants.setForeground(warningStyle, Color.MAGENTA);
		StyleConstants.setForeground(infoStyle, Color.BLUE);
		StyleConstants.setForeground(configStyle, Color.BLACK);
		StyleConstants.setForeground(fineStyle, Color.DARK_GRAY);
		StyleConstants.setForeground(finerStyle, Color.GRAY);
		StyleConstants.setForeground(finestStyle, Color.LIGHT_GRAY);

		getViewport().setView(textPane);
		setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

		handler = new LogPanelHandler();
	}

	public static LogPanel newInstance() {
		final LogPanel logPanel = new LogPanel();
		rootLogger.addHandler(logPanel.handler);
		return logPanel;
	}

	private Style getLogStyle(final Level level) {
		if (level.intValue() >= Level.SEVERE.intValue()) {
			return severeStyle;
		} else if (level.intValue() >= Level.WARNING.intValue()) {
			return warningStyle;
		} else if (level.intValue() >= Level.INFO.intValue()) {
			return infoStyle;
		} else if (level.intValue() >= Level.CONFIG.intValue()) {
			return configStyle;
		} else if (level.intValue() >= Level.FINE.intValue()) {
			return fineStyle;
		} else if (level.intValue() >= Level.FINER.intValue()) {
			return finerStyle;
		} else {
			return finestStyle;
		}
	}

	public void setLogColor(final Level level, final Color color) {
		StyleConstants.setForeground(getLogStyle(level), color);
	}

	public Color getLogColor(final Level level) {
		return StyleConstants.getForeground(getLogStyle(level));
	}

	private void trimLogText() {
		final StyledDocument d = textPane.getStyledDocument();
		if (d.getLength() >= maxLogSize) {
			final int p0 = d.getLength() - maxLogSize / 2;
			try {
				final int p;
				final String text = d.getText(p0, d.getLength() - p0);
				final String ls = System.getProperty("line.separator");
				final int next = text.indexOf(ls);
				if (next == -1) {
					p = p0;
				} else {
					p = p0 + next + ls.length();
				}
				d.remove(0, p);
			} catch (final BadLocationException e) {
				SimpleLogger.INSTANCE.log(e);
			}
		}
	}

	private void scrollBottom() {
		final StyledDocument d = textPane.getStyledDocument();
		textPane.setCaretPosition(d.getLength());
	}

	private boolean isBottom() {
		final JScrollBar bar = getVerticalScrollBar();
		// * 3 / 2 : workaround
		return bar.getValue() + bar.getSize().height * 3 / 2 >= bar.getMaximum();
	}

	// To prevent loop, don't use Logger here. Use SimpleLogger instead.
	@Private
	void addLine(final Level level, final String s) {
		trimLogText();
		final boolean bottom = isBottom();

		final StyledDocument d = textPane.getStyledDocument();
		try {
			d.insertString(d.getLength(), s, getLogStyle(level));
		} catch (final BadLocationException e) {
			SimpleLogger.INSTANCE.log(e);
		}

		if (bottom) {
			scrollBottom();
		}
	}

	public void setMaxLogSize(final int maxLogSize) {
		if (maxLogSize < 0) {
			throw new IllegalArgumentException("maxLogSize must not be negative");
		}

		this.maxLogSize = maxLogSize;

		trimLogText();
		if (isBottom()) {
			scrollBottom(); // is this required?
		}
	}

	public int getMaxLogSize() {
		return maxLogSize;
	}

	private class LogPanelHandler extends Handler {

		@Private
		LogPanelHandler() {
			setFormatter(new SimpleFormatter());
		}

		@Override
		public void close() throws SecurityException {
			// nothing to do
		}

		@Override
		public void flush() {
			// nothing to do
		}

		@Override
		public void publish(final LogRecord record) {
			final String line = getFormatter().format(record);
			SwingUtilities.invokeLater(() -> addLine(record.getLevel(), line));
		}

	}

}
