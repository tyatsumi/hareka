package org.kareha.hareka.ui.swing;

import java.awt.BorderLayout;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ResourceBundle;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.WindowConstants;

import org.kareha.hareka.annotation.Private;

@SuppressWarnings("serial")
public class PasswordDialog extends JDialog {

	private enum BundleKey {
		Ok, Cancel,
	}

	private static final int COLUMNS = 24;

	private final JPasswordField field;
	private char[] password;

	public PasswordDialog(final JFrame owner, final String title, final String message) {
		super(owner);
		final ResourceBundle bundle = ResourceBundle.getBundle(PasswordDialog.class.getName());
		setTitle(title);
		setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(final WindowEvent e) {
				doCancel();
			}
		});
		setModal(true);

		field = new JPasswordField(COLUMNS);
		field.addActionListener(e -> doOk());
		field.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(final KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
					doCancel();
				}
			}
		});
		final JButton okButton = new JButton(bundle.getString(BundleKey.Ok.name()));
		okButton.addActionListener(e -> doOk());
		final JButton cancelButton = new JButton(bundle.getString(BundleKey.Cancel.name()));
		cancelButton.addActionListener(e -> doCancel());
		final JPanel buttonPanel = new JPanel();
		buttonPanel.add(okButton);
		buttonPanel.add(cancelButton);
		add(new JLabel(message), BorderLayout.NORTH);
		add(field, BorderLayout.CENTER);
		add(buttonPanel, BorderLayout.SOUTH);
		pack();
		setResizable(false);

		setLocationRelativeTo(owner);
		setVisible(true);
		field.requestFocus();
	}

	private void doOk() {
		password = field.getPassword();
		dispose();
	}

	@Private
	void doCancel() {
		dispose();
	}

	public char[] getPassword() {
		return password;
	}

}
