package org.kareha.hareka.ui.console;

import java.io.Console;
import java.util.function.Supplier;

public class ConsolePasswordReader implements Supplier<char[]> {

	private static final String DEFAULT_PROMPT = "Password: ";

	private final String prompt;

	public ConsolePasswordReader() {
		this(DEFAULT_PROMPT);
	}

	public ConsolePasswordReader(final String prompt) {
		this.prompt = prompt;
	}

	@Override
	public char[] get() {
		final Console console = System.console();
		if (console == null) {
			throw new IllegalStateException("No console found");
		}
		if (prompt != null) {
			System.out.print(prompt);
		}
		return console.readPassword();
	}

}
