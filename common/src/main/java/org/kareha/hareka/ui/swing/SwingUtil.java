package org.kareha.hareka.ui.swing;

import java.awt.Color;
import java.awt.Graphics;

public final class SwingUtil {

	private SwingUtil() {
		throw new AssertionError();
	}

	public static void drawString(final Graphics g, final String str, final int x, final int y) {
		drawString(g, str, x, y, Color.WHITE, Color.BLACK);
	}

	public static void drawString(final Graphics g, final String str, final int x, final int y, final Color color) {
		drawString(g, str, x, y, color, Color.BLACK);
	}

	public static void drawString(final Graphics g, final String str, final int x, final int y, final Color color,
			final Color borderColor) {
		g.setColor(borderColor);
		g.drawString(str, x - 1, y);
		g.drawString(str, x + 1, y);
		g.drawString(str, x, y - 1);
		g.drawString(str, x, y + 1);
		g.setColor(color);
		g.drawString(str, x, y);
	}

}
