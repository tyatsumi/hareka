package org.kareha.hareka.ui.swing;

import java.util.ResourceBundle;
import java.util.function.Supplier;

import javax.swing.JFrame;

import org.kareha.hareka.annotation.ConfinedTo;

public class PasswordReader implements Supplier<char[]> {

	private enum BundleKey {
		Title, Message,
	}

	private final JFrame owner;
	private final String title;
	private final String message;

	public PasswordReader(final JFrame owner) {
		this.owner = owner;
		final ResourceBundle bundle = ResourceBundle.getBundle(PasswordReader.class.getName());
		title = bundle.getString(BundleKey.Title.name());
		message = bundle.getString(BundleKey.Message.name());
	}

	public PasswordReader(final JFrame owner, final String title, final String message) {
		this.owner = owner;
		this.title = title;
		this.message = message;
	}

	@ConfinedTo("Swing")
	@Override
	public char[] get() {
		return new PasswordDialog(owner, title, message).getPassword();
	}

}
