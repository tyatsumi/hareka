package org.kareha.hareka.ui.swing;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Arrays;
import java.util.ResourceBundle;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.WindowConstants;
import javax.xml.bind.JAXBException;

import org.kareha.hareka.annotation.Private;
import org.kareha.hareka.key.EncryptionException;
import org.kareha.hareka.key.KeyStack;

@SuppressWarnings("serial")
public class PasswordChangerDialog extends JDialog {

	private enum BundleKey {
		NewPassword, EnterNewPassword, ChangePassword, EnterCurrentAndNewPasswords,

		CurrentPassword, NewPasswordAgain, Ok, Cancel, CurrentPasswordNotMatch, PasswordsNotMatch,

		EncryptionFailed, PasswordChanged,
	}

	private static final int COLUMNS = 24;

	private final KeyStack keyStack;
	private final boolean changing;
	private final JPasswordField currentField;
	private final JPasswordField newField;
	private final JPasswordField confirmField;

	public PasswordChangerDialog(final JFrame owner, final KeyStack keyStack) throws JAXBException {
		super(owner);

		this.keyStack = keyStack;

		setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(final WindowEvent e) {
				doCancel();
			}
		});
		setModal(true);

		final ResourceBundle bundle = ResourceBundle.getBundle(PasswordChangerDialog.class.getName());

		changing = keyStack.isProtected(0);

		final String title;
		final String headerText;
		if (!changing) {
			title = bundle.getString(BundleKey.NewPassword.name());
			headerText = bundle.getString(BundleKey.EnterNewPassword.name());
		} else {
			title = bundle.getString(BundleKey.ChangePassword.name());
			headerText = bundle.getString(BundleKey.EnterCurrentAndNewPasswords.name());
		}
		setTitle(title);

		currentField = new JPasswordField(COLUMNS);
		final JPanel currentPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
		currentPanel.add(new JLabel(bundle.getString(BundleKey.CurrentPassword.name())));
		currentPanel.add(currentField);

		newField = new JPasswordField(COLUMNS);
		final JPanel newPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
		newPanel.add(new JLabel(bundle.getString(BundleKey.NewPassword.name())));
		newPanel.add(newField);

		confirmField = new JPasswordField(COLUMNS);
		confirmField.addActionListener(e -> doOk());
		final JPanel confirmPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
		confirmPanel.add(new JLabel(bundle.getString(BundleKey.NewPasswordAgain.name())));
		confirmPanel.add(confirmField);

		final JPanel centerPanel = new JPanel();
		centerPanel.setLayout(new BoxLayout(centerPanel, BoxLayout.Y_AXIS));
		if (changing) {
			centerPanel.add(currentPanel);
		}
		centerPanel.add(newPanel);
		centerPanel.add(confirmPanel);

		final JButton okButton = new JButton(bundle.getString(BundleKey.Ok.name()));
		okButton.addActionListener(e -> doOk());
		final JButton cancelButton = new JButton(bundle.getString(BundleKey.Cancel.name()));
		cancelButton.addActionListener(e -> doCancel());
		final JPanel buttonPanel = new JPanel();
		buttonPanel.add(okButton);
		buttonPanel.add(cancelButton);
		add(new JLabel(headerText), BorderLayout.NORTH);
		add(centerPanel, BorderLayout.CENTER);
		add(buttonPanel, BorderLayout.SOUTH);
		pack();
		setResizable(false);

		setLocationRelativeTo(owner);
		if (changing) {
			currentField.requestFocus();
		} else {
			newField.requestFocus();
		}
	}

	private void doOk() {
		final ResourceBundle bundle = ResourceBundle.getBundle(PasswordChangerDialog.class.getName());
		try {
			if (changing) {
				keyStack.clearCache();
				final char[] currentPassword = currentField.getPassword();
				try {
					if (keyStack.get(0, currentPassword) == null) {
						JOptionPane.showMessageDialog(this, bundle.getString(BundleKey.CurrentPasswordNotMatch.name()));
						return;
					}
				} catch (final JAXBException e) {
					JOptionPane.showMessageDialog(this, e.getMessage());
					return;
				} finally {
					Arrays.fill(currentPassword, '\0');
				}
			}
			final char[] newPassword = newField.getPassword();
			final char[] confirmPassword = confirmField.getPassword();
			try {
				if (!Arrays.equals(newPassword, confirmPassword)) {
					JOptionPane.showMessageDialog(this, bundle.getString(BundleKey.PasswordsNotMatch.name()));
					return;
				}
				try {
					if (!keyStack.changePassword(0, newPassword)) {
						JOptionPane.showMessageDialog(this, bundle.getString(BundleKey.EncryptionFailed.name()));
						return;
					}
				} catch (final JAXBException e) {
					JOptionPane.showMessageDialog(this, e.getMessage());
					return;
				} catch (final EncryptionException e) {
					JOptionPane.showMessageDialog(this, bundle.getString(BundleKey.EncryptionFailed.name()));
					return;
				}
				JOptionPane.showMessageDialog(this, bundle.getString(BundleKey.PasswordChanged.name()));
				dispose();
			} finally {
				Arrays.fill(newPassword, '\0');
				Arrays.fill(confirmPassword, '\0');
			}
		} finally {
			// currentField.clear();
			// newField.clear();
			// confirmField.clear();
		}
	}

	@Private
	void doCancel() {
		dispose();
		// currentField.clear();
		// newField.clear();
		// confirmField.clear();
	}

}
