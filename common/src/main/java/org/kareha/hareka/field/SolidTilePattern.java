package org.kareha.hareka.field;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import org.kareha.hareka.annotation.Immutable;
import org.kareha.hareka.protocol.ProtocolOutput;

@Immutable
public class SolidTilePattern extends TilePattern {

	public static class Builder extends TilePattern.Builder<Builder> {

		protected Tile value = Tile.NULL_0;

		public Builder value(final Tile v) {
			value = v;
			return this;
		}

		@Override
		protected Builder self() {
			return this;
		}

		@Override
		public SolidTilePattern build() {
			return new SolidTilePattern(this);
		}

	}

	protected final Tile value;

	protected SolidTilePattern(final Builder builder) {
		super(builder);
		this.value = builder.value;
	}

	public SolidTilePattern(final Tile value) {
		this.value = value;
	}

	@Override
	public void writeTo(final ProtocolOutput out) {
		out.write(TilePatternType.SOLID);
		out.write(value);
	}

	@XmlType(name = "solidTilePattern")
	@XmlAccessorType(XmlAccessType.NONE)
	protected static class Adapted extends TilePattern.Adapted {

		@XmlElement
		protected Tile value;

		protected Adapted() {
			// used by JAXB
		}

		protected Adapted(final SolidTilePattern v) {
			super(v);
			value = v.value;
		}

		@Override
		protected SolidTilePattern unmarshal() {
			final Builder builder = new Builder();
			builder.value(value);
			return builder.build();
		}

	}

	@Override
	protected Adapted marshal() {
		return new Adapted(this);
	}

	@Override
	public Tile getTile(final Vector position) {
		return value;
	}

	public Tile getValue() {
		return value;
	}

}
