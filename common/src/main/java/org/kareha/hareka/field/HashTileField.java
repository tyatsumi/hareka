package org.kareha.hareka.field;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import org.kareha.hareka.annotation.Private;
import org.kareha.hareka.annotation.ThreadSafe;
import org.kareha.hareka.protocol.ProtocolInputStream;
import org.kareha.hareka.protocol.ProtocolOutputStream;
import org.kareha.hareka.protocol.ProtocolException;

@ThreadSafe
public class HashTileField extends AbstractTileField {

	protected final Map<Vector, Tile> map = new ConcurrentHashMap<>();

	public HashTileField(final boolean mutable, final TilePattern pattern) {
		super(mutable, pattern);
	}

	@Private
	void saveData(final OutputStream out) throws IOException {
		try (final ProtocolOutputStream dout = new ProtocolOutputStream()) {
			final Set<Map.Entry<Vector, Tile>> entries = map.entrySet();
			dout.writeCompactUInt(entries.size());
			for (final Map.Entry<Vector, Tile> entry : entries) {
				entry.getKey().writeTo(dout);
				entry.getValue().writeTo(dout);
			}
			dout.writeTo(out);
		}
	}

	@Private
	void loadData(final byte[] data, final Palette palette) throws IOException, ProtocolException {
		try (final ProtocolInputStream din = new ProtocolInputStream(data)) {
			map.clear();
			final int size = din.readCompactUInt();
			for (int i = 0; i < size; i++) {
				final Vector position = Vector.readFrom(din);
				final Tile tile = Tile.readFrom(din, palette);
				setTile(position, tile, true);
			}
		}
	}

	@XmlType(name = "hashTileField")
	@XmlAccessorType(XmlAccessType.NONE)
	protected static class Adapted extends AbstractTileField.Adapted {

		@XmlElement
		protected Palette palette;
		@XmlElement
		protected byte[] data;

		protected Adapted() {
			// used by JAXB
		}

		protected Adapted(final HashTileField v) {
			super(v);
			palette = new Palette(TileType.NULL);
			try (final ByteArrayOutputStream out = new ByteArrayOutputStream()) {
				v.saveData(out);
				data = out.toByteArray();
			} catch (final IOException e) {
				throw new RuntimeException(e);
			}
		}

		@Override
		protected HashTileField unmarshal() throws IOException, ProtocolException {
			final HashTileField hf = new HashTileField(mutable, pattern);
			if (regions != null) {
				hf.regions.addAll(regions);
			}
			hf.loadData(data, palette);
			return hf;
		}

	}

	@Override
	protected Adapted marshal() {
		return new Adapted(this);
	}

	@Override
	public Tile getTile(final Vector position) {
		final Tile tile = map.get(position);
		if (tile == null) {
			return getBackgroundTile(position);
		}
		return tile;
	}

	@Override
	public void setTile(final Vector position, final Tile tile, final boolean force) {
		if (!force && !isMutable(position)) {
			return;
		}
		if (tile == null || tile.equals(getBackgroundTile(position))) {
			map.remove(position);
		} else {
			map.put(position, tile);
		}
	}

	public void reduceToBackgroundTiles() {
		for (final Map.Entry<Vector, Tile> entry : map.entrySet()) {
			final Tile tile = entry.getValue();
			if (tile == null || tile.equals(getBackgroundTile(entry.getKey()))) {
				map.remove(entry.getKey());
			}
		}
	}

}
