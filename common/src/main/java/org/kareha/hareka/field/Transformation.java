package org.kareha.hareka.field;

import java.util.concurrent.ThreadLocalRandom;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.kareha.hareka.annotation.Immutable;
import org.kareha.hareka.annotation.PackagePrivate;
import org.kareha.hareka.annotation.Private;

@Immutable
@XmlJavaTypeAdapter(Transformation.Adapter.class)
public final class Transformation {

	public static final Transformation IDENTITY = valueOf(Vector.ZERO, 0, false, 0);

	@PackagePrivate
	final Vector translation;
	@PackagePrivate
	final int rotation;
	@PackagePrivate
	final boolean reflection;
	@PackagePrivate
	final int elevation;

	private Transformation(final Vector translation, final int rotation, final boolean reflection,
			final int elevation) {
		if (translation == null) {
			throw new IllegalArgumentException("null");
		}
		this.translation = translation;
		this.rotation = rotation;
		this.reflection = reflection;
		this.elevation = elevation;
	}

	public static Transformation valueOf(final Vector translation, final int rotation, final boolean reflection,
			final int elevation) {
		return new Transformation(translation, rotation, reflection, elevation);
	}

	@XmlType(name = "transformation")
	@XmlAccessorType(XmlAccessType.NONE)
	private static class Adapted {

		@XmlElement
		private Vector translation;
		@XmlElement
		private int rotation;
		@XmlElement
		private boolean reflection;
		@XmlElement
		private int elevation;

		@SuppressWarnings("unused")
		private Adapted() {
			// used by JAXB
		}

		@Private
		Adapted(final Transformation v) {
			translation = v.translation;
			rotation = v.rotation;
			reflection = v.reflection;
			elevation = v.elevation;
		}

		@Private
		Transformation unmarshal() {
			return valueOf(translation, rotation, reflection, elevation);
		}

	}

	@Private
	Adapted marshal() {
		return new Adapted(this);
	}

	static class Adapter extends XmlAdapter<Adapted, Transformation> {

		@Override
		public Adapted marshal(final Transformation v) throws Exception {
			if (v == null) {
				return null;
			}
			return v.marshal();
		}

		@Override
		public Transformation unmarshal(final Adapted v) throws Exception {
			if (v == null) {
				return null;
			}
			return v.unmarshal();
		}

	}

	public Transformation transform(final Transformation v) {
		final Vector vt = v.translation.reflect(reflection).rotate(rotation);
		final Vector t = translation.add(vt);
		final int ro;
		if (reflection) {
			ro = rotation - v.rotation;
		} else {
			ro = rotation + v.rotation;
		}
		final boolean re = reflection ^ v.reflection;
		final int el = elevation + v.elevation;
		return valueOf(t, ro, re, el);
	}

	public Transformation invert(final Transformation v) {
		// TODO invert is not implemented yet.
		throw new UnsupportedOperationException("Not implemented yet.");
	}

	// TODO This method should be moved to a utility class.
	public static Transformation random(final Vector translation) {
		return valueOf(translation, ThreadLocalRandom.current().nextInt(6), ThreadLocalRandom.current().nextBoolean(),
				0);
	}

	public Vector translation() {
		return translation;
	}

	public int rotation() {
		return rotation;
	}

	public boolean reflection() {
		return reflection;
	}

	public int elevation() {
		return elevation;
	}

}
