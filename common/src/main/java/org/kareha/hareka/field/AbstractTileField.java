package org.kareha.hareka.field;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.kareha.hareka.annotation.ThreadSafe;
import org.kareha.hareka.protocol.ProtocolException;

@ThreadSafe
@XmlJavaTypeAdapter(AbstractTileField.Adapter.class)
public abstract class AbstractTileField implements TileField {

	protected volatile boolean mutable;
	protected volatile TilePattern pattern;
	protected volatile List<Region> regions = new CopyOnWriteArrayList<>();

	protected AbstractTileField(final boolean mutable, final TilePattern pattern) {
		if (pattern == null) {
			throw new IllegalArgumentException("null");
		}
		this.mutable = mutable;
		this.pattern = pattern;
	}

	@XmlType(name = "tileField")
	@XmlSeeAlso({ HashTileField.Adapted.class, ArrayTileField.Adapted.class })
	@XmlAccessorType(XmlAccessType.NONE)
	protected static abstract class Adapted {

		@XmlElement
		protected boolean mutable;
		@XmlElement
		protected TilePattern pattern;
		@XmlElement(name = "region")
		protected List<Region> regions;

		protected Adapted() {
			// used by JAXB
		}

		protected Adapted(final AbstractTileField v) {
			mutable = v.mutable;
			pattern = v.pattern;
			regions = new ArrayList<>(v.regions);
		}

		protected abstract AbstractTileField unmarshal() throws IOException, ProtocolException;

	}

	protected abstract Adapted marshal();

	static class Adapter extends XmlAdapter<Adapted, AbstractTileField> {

		@Override
		public Adapted marshal(final AbstractTileField v) throws Exception {
			if (v == null) {
				return null;
			}
			return v.marshal();
		}

		@Override
		public AbstractTileField unmarshal(final Adapted v) throws Exception {
			if (v == null) {
				return null;
			}
			return v.unmarshal();
		}

	}

	public boolean isMutable() {
		return mutable;
	}

	public void setMutable(final boolean mutable) {
		this.mutable = mutable;
	}

	public TilePattern getPattern() {
		return pattern;
	}

	public void setPattern(final TilePattern pattern) {
		if (pattern == null) {
			throw new IllegalArgumentException("null");
		}
		this.pattern = pattern;
	}

	public List<Region> getRegions() {
		return regions;
	}

	public void setRegions(final Collection<Region> regions) {
		this.regions = new CopyOnWriteArrayList<>(regions);
	}

	public abstract void setTile(Vector position, Tile tile, boolean force);

	protected Tile getBackgroundTile(final Vector position) {
		for (final Region region : regions) {
			if (region.contains(position)) {
				return region.getTile(position);
			}
		}
		return pattern.getTile(position);
	}

	public boolean isMutable(final Vector position) {
		for (final Region region : regions) {
			if (region.contains(position)) {
				return region.isMutable();
			}
		}
		return mutable;
	}

}
