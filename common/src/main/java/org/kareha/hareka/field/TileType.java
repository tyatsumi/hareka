package org.kareha.hareka.field;

import java.io.IOException;

import org.kareha.hareka.protocol.ProtocolElement;
import org.kareha.hareka.protocol.ProtocolInput;
import org.kareha.hareka.protocol.ProtocolOutput;
import org.kareha.hareka.protocol.ProtocolException;

public enum TileType implements ProtocolElement {

	NULL(false, true, false, false),

	//

	AIR(true, false, false, true),

	// BRIDGE(true, false, false, true),

	DOWNSTAIRS(true, true, true, false),

	EARTH(false, false, false, false),

	FLOOR(true, false, false, true),

	FLOWER(true, false, false, true),

	GATE(true, true, true, false),

	GRASS(true, false, false, true),

	// ROCK(false, false, false, false),

	SAND(true, false, false, true),

	SNOW(true, false, false, true),

	TREE(false, false, false, false),

	UPSTAIRS(true, true, true, false),

	VOID(false, true, false, false),

	WALL(false, false, false, false),

	WATER(true, false, false, true),

	;

	private static final TileType[] values = values();

	public static TileType valueOf(final int ordinal) {
		if (ordinal < 0 || ordinal >= values.length) {
			throw new IllegalArgumentException("Out of bounds");
		}
		return values[ordinal];
	}

	private final boolean walkable;
	private final boolean special;
	private final boolean stackable;
	private final boolean attackable;

	TileType(final boolean walkable, final boolean special, final boolean stackable, final boolean attackable) {
		this.walkable = walkable;
		this.special = special;
		this.stackable = stackable;
		this.attackable = attackable;
	}

	public static TileType readFrom(final ProtocolInput in) throws IOException, ProtocolException {
		final int ordinal = in.readCompactUInt();
		return valueOf(ordinal);
	}

	public static TileType readFrom(final ProtocolInput in, final Palette palette)
			throws IOException, ProtocolException {
		final int ordinal = in.readCompactUInt();
		return palette.getTileType(ordinal);
	}

	@Override
	public void writeTo(final ProtocolOutput out) {
		out.writeCompactUInt(ordinal());
	}

	public boolean isWalkable() {
		return walkable;
	}

	public boolean isSpecial() {
		return special;
	}

	public boolean isStackable() {
		return stackable;
	}

	public boolean isAttackable() {
		return attackable;
	}

	public TileType transform(final Transformation transformation) {
		return this;
	}

	public TileType invert(final Transformation transformation) {
		return this;
	}

}
