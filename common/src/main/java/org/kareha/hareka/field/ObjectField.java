package org.kareha.hareka.field;

import java.util.List;

public interface ObjectField {

	int getViewSize();

	boolean addObject(FieldObject object);

	boolean removeObject(FieldObject object);

	boolean moveObject(FieldObject object);

	List<FieldObject> getObjects(Vector position);

	List<FieldObject> getInViewObjects(Vector center);

	boolean isExclusive(Vector position);

}
