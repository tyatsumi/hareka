package org.kareha.hareka.field;

import java.io.IOException;

import org.kareha.hareka.protocol.ProtocolElement;
import org.kareha.hareka.protocol.ProtocolInput;
import org.kareha.hareka.protocol.ProtocolOutput;
import org.kareha.hareka.protocol.ProtocolException;

/**
 * The direction on horizontal hexagonal grid. This enum contains the six
 * directions and a special direction indicating no direction.
 * 
 * @author Aki Goto
 * @since Hareka0.1
 */
public enum Direction implements ProtocolElement {

	/**
	 * Right, one of the six direction.
	 */
	RIGHT,

	/**
	 * Down-right, one of the six direction.
	 */
	DOWN_RIGHT,

	/**
	 * Down-left, one of the six direction.
	 */
	DOWN_LEFT,

	/**
	 * Left, one of the six direction.
	 */
	LEFT,

	/**
	 * Up-left, one of the six direction.
	 */
	UP_LEFT,

	/**
	 * Up-right, one of the six direction.
	 */
	UP_RIGHT,

	/**
	 * A special direction indicating no direction.
	 */
	NULL;

	/**
	 * The {@code Vector} object corresponding to the unit vector along the
	 * {@code Direction}.
	 */
	private final Vector vector;

	private Direction() {
		// Should pass unit vector by argument?
		if (ordinal() == 6) {
			vector = Vector.ZERO;
		} else {
			vector = Vector.valueOf(1, 0).rotate(ordinal());
		}
	}

	/**
	 * The permanent store of the array of enum values.
	 */
	private static final Direction[] values = values();

	/**
	 * Returns the {@code Direction} with the specified ordinal.
	 * 
	 * @param ordinal the ordinal of the {@code Direction} to be returned.
	 * @return the {@code Direction} of the specified ordinal.
	 */
	public static Direction valueOf(final int ordinal) {
		if (ordinal < 0 || ordinal >= values.length) {
			throw new IllegalArgumentException("Out of bounds");
		}
		return values[ordinal];
	}

	/**
	 * Reads the next {@code Direction} from the {@code PacketInput}.
	 * 
	 * @param in the {@code PacketInput} from which the {@code Direction} is read.
	 * @return the next {@code Direction}, or {@code null} if the data read is
	 *         invalid.
	 */
	public static Direction readFrom(final ProtocolInput in) throws IOException, ProtocolException {
		final int ordinal = in.readCompactUInt();
		return valueOf(ordinal);
	}

	@Override
	public void writeTo(final ProtocolOutput out) {
		out.writeCompactUInt(ordinal());
	}

	/**
	 * Returns the unit vector directed to this {@code Direction}.
	 * 
	 * @return the unit vector directed to this {@code Direction}.
	 */
	public Vector vector() {
		return vector;
	}

	/**
	 * Returns the {@code Direction} rotated by the specified amount.
	 * 
	 * @param amount the amount of rotation in pi/3 unit, clockwise.
	 * @return the rotated {@code Direction}.
	 */
	public Direction rotate(final int amount) {
		if (this == NULL) {
			return NULL;
		}
		return valueOf(Math.floorMod(ordinal() + amount, 6));
	}

	/**
	 * Returns the opposite {@code Direction}.
	 * 
	 * @return the opposite {@code Direction}.
	 */
	public Direction opposite() {
		if (this == NULL) {
			return NULL;
		}
		return valueOf((ordinal() + 3) % 6);
	}

	// * Currently, mirroring by the axis perpendicular to the x axis.
	// Is this OK?
	/**
	 * Returns the mirror image of this {@code Direction}.
	 * 
	 * @return the {@code Direction} mirrored by the axis perpendicular to the x
	 *         axis.
	 */
	public Direction reflect() {
		if (this == NULL) {
			return NULL;
		}
		return valueOf((9 - ordinal()) % 6);
	}

	/**
	 * Returns the mirror image of this {@code Direction} or the identical
	 * {@code Direction} to this, conditionally.
	 * 
	 * @param flag the flag indicating to reflect or not.
	 * @return if flag is true the {@code Direction} mirrored by the axis
	 *         perpendicular to the x axis, or the {@code Direction} identical to
	 *         this.
	 */
	public Direction reflect(final boolean flag) {
		if (!flag) {
			return this;
		}
		return reflect();
	}

	// * Should Transformation class be implemented by matrices?
	// * What order is best for rotate, reflect and translate?
	// * Should transform method be called invert?
	// * Should an interface Transformable be created?
	/**
	 * Returns the {@code Direction} transformed by the specified
	 * {@code Transformation} object.
	 * 
	 * @param transformation the {@code Transformation} object to be applied.
	 * @return the {@code Direction} transformed.
	 */
	public Direction transform(final Transformation transformation) {
		return rotate(-transformation.rotation).reflect(transformation.reflection);
	}

	// * Is invert method required? Transformation object can be inverted.
	// The invert method may exist for performance reason.
	/**
	 * Returns the {@code Direction} transformed by the specified
	 * {@code Transformation} object inversely.
	 * 
	 * @param transformation the {@code Transformation} object to be applied
	 *                       inversely.
	 * @return the {@code Direction} transformed inversely.
	 */
	public Direction invert(final Transformation transformation) {
		return reflect(transformation.reflection).rotate(transformation.rotation);
	}

}
