package org.kareha.hareka.field;

import java.io.IOException;

import org.kareha.hareka.annotation.Immutable;
import org.kareha.hareka.protocol.ProtocolElement;
import org.kareha.hareka.protocol.ProtocolInput;
import org.kareha.hareka.protocol.ProtocolOutput;
import org.kareha.hareka.protocol.ProtocolException;

@Immutable
public final class TilePiece implements ProtocolElement {

	private final Vector position;
	private final Tile tile;

	private TilePiece(final Vector position, final Tile tile) {
		if (position == null || tile == null) {
			throw new IllegalArgumentException("null");
		}
		this.position = position;
		this.tile = tile;
	}

	public static TilePiece valueOf(final Vector position, final Tile tile) {
		return new TilePiece(position, tile);
	}

	public static TilePiece readFrom(final ProtocolInput in) throws IOException, ProtocolException {
		final Vector position = Vector.readFrom(in);
		final Tile tile = Tile.readFrom(in);
		return valueOf(position, tile);
	}

	@Override
	public void writeTo(final ProtocolOutput out) {
		out.write(position);
		out.write(tile);
	}

	public Vector position() {
		return position;
	}

	public Tile tile() {
		return tile;
	}

	public TilePiece transform(final Transformation v) {
		return valueOf(position.transform(v), tile.transform(v));
	}

	public TilePiece invert(final Transformation v) {
		return valueOf(position.invert(v), tile.invert(v));
	}

}
