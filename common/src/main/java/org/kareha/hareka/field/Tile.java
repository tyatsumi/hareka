package org.kareha.hareka.field;

import java.io.IOException;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.kareha.hareka.annotation.Immutable;
import org.kareha.hareka.annotation.Private;
import org.kareha.hareka.protocol.ProtocolElement;
import org.kareha.hareka.protocol.ProtocolInput;
import org.kareha.hareka.protocol.ProtocolOutput;
import org.kareha.hareka.protocol.ProtocolException;

@Immutable
@XmlJavaTypeAdapter(Tile.Adapter.class)
public final class Tile implements ProtocolElement {

	public static final Tile NULL_0 = valueOf(TileType.NULL, 0);

	@Private
	final TileType type;
	@Private
	final int elevation;

	private Tile(final TileType type, final int elevation) {
		if (type == null) {
			throw new IllegalArgumentException("null");
		}
		this.type = type;
		this.elevation = elevation;
	}

	public static Tile valueOf(final TileType type, final int elevation) {
		return new Tile(type, elevation);
	}

	@Override
	public boolean equals(final Object obj) {
		if (!(obj instanceof Tile)) {
			return false;
		}
		final Tile tile = (Tile) obj;
		return tile.type == type && tile.elevation == elevation;
	}

	@Override
	public int hashCode() {
		return type.ordinal() ^ (elevation << 16 | elevation >>> 16);
	}

	public static Tile readFrom(final ProtocolInput in) throws IOException, ProtocolException {
		final TileType type = TileType.readFrom(in);
		final int elevation = in.readCompactInt();
		return valueOf(type, elevation);
	}

	public static Tile readFrom(final ProtocolInput in, final Palette palette) throws IOException, ProtocolException {
		final TileType type = TileType.readFrom(in, palette);
		final int elevation = in.readCompactInt();
		return valueOf(type, elevation);
	}

	@Override
	public void writeTo(final ProtocolOutput out) {
		type.writeTo(out);
		out.writeCompactInt(elevation);
	}

	@XmlType(name = "tile")
	@XmlAccessorType(XmlAccessType.NONE)
	private static class Adapted {

		@XmlElement
		private String type;
		@XmlElement
		private int elevation;

		@SuppressWarnings("unused")
		private Adapted() {
			// used by JAXB
		}

		@Private
		Adapted(final Tile v) {
			type = v.type.name();
			elevation = v.elevation;
		}

		@Private
		Tile unmarshal() {
			return valueOf(tileTypeValueOf(type), elevation);
		}

		private static TileType tileTypeValueOf(final String name) {
			if (name == null) {
				return TileType.NULL;
			}
			try {
				return TileType.valueOf(name);
			} catch (final IllegalArgumentException e) {
				return TileType.NULL;
			}
		}

	}

	@Private
	Adapted marshal() {
		return new Adapted(this);
	}

	static class Adapter extends XmlAdapter<Adapted, Tile> {

		@Override
		public Adapted marshal(final Tile v) throws Exception {
			if (v == null) {
				return null;
			}
			return v.marshal();
		}

		@Override
		public Tile unmarshal(final Adapted v) throws Exception {
			if (v == null) {
				return null;
			}
			return v.unmarshal();
		}

	}

	public Tile transform(final Transformation v) {
		return valueOf(type, elevation - v.elevation);
	}

	public Tile invert(final Transformation v) {
		return valueOf(type, elevation + v.elevation);
	}

	public TileType type() {
		return type;
	}

	public int elevation() {
		return elevation;
	}

}
