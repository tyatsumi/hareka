package org.kareha.hareka.field;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import org.kareha.hareka.annotation.Immutable;
import org.kareha.hareka.protocol.ProtocolOutput;

@Immutable
public class OneUniformTilePattern extends TilePattern {

	public static class Builder extends TilePattern.Builder<Builder> {

		protected Tile a = Tile.NULL_0;
		protected Tile b = Tile.NULL_0;
		protected Tile c = Tile.NULL_0;

		public Builder a(final Tile v) {
			a = v;
			return this;
		}

		public Builder b(final Tile v) {
			b = v;
			return this;
		}

		public Builder c(final Tile v) {
			c = v;
			return this;
		}

		@Override
		protected Builder self() {
			return this;
		}

		@Override
		public OneUniformTilePattern build() {
			return new OneUniformTilePattern(this);
		}

	}

	protected final Tile a;
	protected final Tile b;
	protected final Tile c;

	protected OneUniformTilePattern(final Builder builder) {
		this.a = builder.a;
		this.b = builder.b;
		this.c = builder.c;
	}

	public OneUniformTilePattern(final Tile a, final Tile b, final Tile c) {
		this.a = a;
		this.b = b;
		this.c = c;
	}

	@Override
	public void writeTo(final ProtocolOutput out) {
		out.write(TilePatternType.ONE_UNIFORM);
		out.write(a);
		out.write(b);
		out.write(c);
	}

	@XmlType(name = "oneUniformTilePattern")
	@XmlAccessorType(XmlAccessType.NONE)
	protected static class Adapted extends TilePattern.Adapted {

		@XmlElement
		protected Tile a;
		@XmlElement
		protected Tile b;
		@XmlElement
		protected Tile c;

		protected Adapted() {
			// used by JAXB
		}

		protected Adapted(final OneUniformTilePattern v) {
			super(v);
			a = v.a;
			b = v.b;
			c = v.c;
		}

		@Override
		protected OneUniformTilePattern unmarshal() {
			final Builder builder = new Builder();
			builder.a(a).b(b).c(c);
			return builder.build();
		}

	}

	@Override
	protected Adapted marshal() {
		return new Adapted(this);
	}

	@Override
	public Tile getTile(final Vector position) {
		switch (Math.floorMod(position.x() - position.y(), 3)) {
		default:
			throw new AssertionError();
		case 0:
			return a;
		case 1:
			return b;
		case 2:
			return c;
		}
	}

	public Tile getA() {
		return a;
	}

	public Tile getB() {
		return b;
	}

	public Tile getC() {
		return c;
	}

}
