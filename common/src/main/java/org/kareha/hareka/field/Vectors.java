package org.kareha.hareka.field;

import java.util.Random;

public final class Vectors {

	private static final Vector[] emptyArray = new Vector[0];

	private Vectors() {
		throw new AssertionError();
	}

	public static Vector[] range(final Vector center, final int size) {
		if (size < 0) {
			return emptyArray;
		}
		final Vector[] points = new Vector[1 + 3 * size * (size + 1)];
		int i = 0;
		for (int y = -size; y <= size; y++) {
			for (int x = Math.max(-size, -size - y), m = Math.min(size, size - y); x <= m; x++) {
				points[i] = Vector.valueOf(center.x() + x, center.y() + y);
				i++;
			}
		}
		return points;
	}

	private static Vector relative(final Vector v, final Direction direction, final int distance) {
		return v.add(direction.vector().multiply(distance));
	}

	public static Vector[] edge(final Vector center, final int size, final Direction direction) {
		return edge(center, size, direction, false);
	}

	public static Vector[] edge(final Vector center, final int size, final Direction direction, final boolean reflect) {
		if (size < 0) {
			return emptyArray;
		} else if (size == 0) {
			final Vector[] points = new Vector[1];
			points[0] = center;
			return points;
		}
		final Vector c = relative(center, direction, size);
		final Vector[] points = new Vector[1 + size * 2];
		points[0] = c;
		final Direction left = direction.rotate(-2);
		final Direction right = direction.rotate(2);
		if (!reflect) {
			for (int i = 0; i < size; i++) {
				points[1 + i] = relative(c, left, 1 + i);
			}
			for (int i = 0; i < size; i++) {
				points[1 + size + i] = relative(c, right, 1 + i);
			}
		} else {
			for (int i = 0; i < size; i++) {
				points[1 + i] = relative(c, right, 1 + i);
			}
			for (int i = 0; i < size; i++) {
				points[1 + size + i] = relative(c, left, 1 + i);
			}
		}
		return points;
	}

	@SuppressWarnings("unused")
	private static Vector heavyRandom(final Vector center, final int size, final Random random) {
		if (size <= 0) {
			throw new IllegalArgumentException("size must be positive");
		}
		final Vector[] points = range(center, size);
		return points[random.nextInt(points.length)];
	}

	public static Vector random(final Vector center, final int size, final Random random) {
		if (size < 0) {
			throw new IllegalArgumentException("size must not be negative");
		}
		final int total = 1 + 3 * size * (size + 1);
		final int i = random.nextInt(total);
		return byIndex(size, i).add(center);
	}

	private static Vector byIndex(final int size, final int v) {
		final int total = 1 + 3 * size * (size + 1);
		final Vector result;
		if (v < total / 2) {
			final int b = 2 * size + 1;
			final int k = (int) ((-b + Math.sqrt(b * b + 8 * v)) / 2);
			final int y2 = -size + k;
			final int base = size * k + k * (k + 3) / 2;
			final int x2 = v - base;
			result = Vector.valueOf(x2, y2);
		} else {
			final int j = total - v;
			final int b = 2 * size + 1;
			final int k = (int) Math.ceil((-b + Math.sqrt(b * b + 8 * j)) / 2);
			final int y2 = size + 1 - k;
			final int base = total / 2 - y2 * (y2 - 4 * size - 3) / 2;
			final int x2 = v - base;
			result = Vector.valueOf(x2, y2);
		}
		return result;
	}

	public static Vector[] ring(final Vector center, final int size) {
		if (size < 0) {
			return emptyArray;
		} else if (size == 0) {
			final Vector[] points = new Vector[1];
			points[0] = center;
			return points;
		}
		final Vector c = relative(center, Direction.RIGHT, size);
		final Vector[] points = new Vector[size * 6];
		Vector d = c;
		for (int i = 0; i < size; i++) {
			points[size * 0 + i] = d;
			d = d.neighbor(Direction.DOWN_LEFT);
		}
		for (int i = 0; i < size; i++) {
			points[size * 1 + i] = d;
			d = d.neighbor(Direction.LEFT);
		}
		for (int i = 0; i < size; i++) {
			points[size * 2 + i] = d;
			d = d.neighbor(Direction.UP_LEFT);
		}
		for (int i = 0; i < size; i++) {
			points[size * 3 + i] = d;
			d = d.neighbor(Direction.UP_RIGHT);
		}
		for (int i = 0; i < size; i++) {
			points[size * 4 + i] = d;
			d = d.neighbor(Direction.RIGHT);
		}
		for (int i = 0; i < size; i++) {
			points[size * 5 + i] = d;
			d = d.neighbor(Direction.DOWN_RIGHT);
		}
		return points;
	}

	private static Vector round(final float x, final float y, final float z) {
		final int ix = Math.round(x);
		final int iy = Math.round(y);
		final int iz = Math.round(z);

		final float dx = Math.abs(x - ix);
		final float dy = Math.abs(y - iy);
		final float dz = Math.abs(z - iz);

		final int rx;
		final int ry;
		if (dx > dy && dx > dz) {
			rx = -iy - iz;
			ry = iy;
		} else if (dy > dz) {
			rx = ix;
			ry = -iz - ix;
		} else {
			rx = ix;
			ry = iy;
		}

		return Vector.valueOf(rx, ry);
	}

	public static Vector[] line(final Vector a, final Vector b) {
		final int n = b.distance(a);
		final Vector[] line = new Vector[n + 1];
		for (int i = 0; i <= n; i++) {
			final float t;
			if (n == 0) {
				t = 0;
			} else {
				t = (float) i / n;
			}
			final float x = a.x() + (b.x() - a.x()) * t;
			final float y = a.y() + (b.y() - a.y()) * t;
			final float z = -x - y;
			line[i] = round(x, y, z);
		}
		return line;
	}

}
