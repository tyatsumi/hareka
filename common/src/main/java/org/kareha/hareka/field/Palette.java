package org.kareha.hareka.field;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.kareha.hareka.annotation.NotThreadSafe;
import org.kareha.hareka.annotation.Private;

@NotThreadSafe
@XmlJavaTypeAdapter(Palette.Adapter.class)
public class Palette {

	@Private
	final TileType defaultTileType;
	@Private
	final Map<Integer, TileType> map = new HashMap<>();

	public Palette(final TileType defaultTileType) {
		this.defaultTileType = defaultTileType;

		for (final TileType type : TileType.values()) {
			map.put(type.ordinal(), type);
		}
	}

	@XmlAccessorType(XmlAccessType.NONE)
	private static final class EnumEntry {

		@XmlAttribute
		@Private
		int ordinal;
		@XmlValue
		@Private
		String name;

		@SuppressWarnings("unused")
		private EnumEntry() {
			// used by JAXB
		}

		EnumEntry(final int ordinal, final String name) {
			this.ordinal = ordinal;
			this.name = name;
		}

	}

	@XmlType(name = "palette")
	@XmlAccessorType(XmlAccessType.NONE)
	private static class Adapted {

		@XmlElement(name = "default")
		private String defaultName;
		@XmlElement(name = "entry")
		private List<EnumEntry> entries;

		@SuppressWarnings("unused")
		private Adapted() {
			// used by JAXB
		}

		@Private
		Adapted(final Palette v) {
			defaultName = v.defaultTileType.name();

			entries = new ArrayList<>();
			for (final Map.Entry<Integer, TileType> entry : v.map.entrySet()) {
				entries.add(new EnumEntry(entry.getKey(), entry.getValue().name()));
			}
		}

		@Private
		Palette unmarshal() {
			final Palette palette = new Palette(tileTypeValueOf(defaultName));
			for (final EnumEntry entry : entries) {
				try {
					palette.putTileType(entry.ordinal, TileType.valueOf(entry.name));
				} catch (final IllegalArgumentException e) {
					continue;
				}
			}
			return palette;
		}

		private static TileType tileTypeValueOf(final String name) {
			if (name == null) {
				return TileType.NULL;
			}
			try {
				return TileType.valueOf(name);
			} catch (final IllegalArgumentException e) {
				return TileType.NULL;
			}
		}

	}

	@Private
	Adapted marshal() {
		return new Adapted(this);
	}

	static class Adapter extends XmlAdapter<Adapted, Palette> {

		@Override
		public Adapted marshal(final Palette v) throws Exception {
			if (v == null) {
				return null;
			}
			return v.marshal();
		}

		@Override
		public Palette unmarshal(final Adapted v) throws Exception {
			if (v == null) {
				return null;
			}
			return v.unmarshal();
		}

	}

	public TileType getTileType(final int ordinal) {
		final TileType tile = map.get(ordinal);
		if (tile == null) {
			return defaultTileType;
		}
		return tile;
	}

	public void putTileType(final int ordinal, final TileType tileType) {
		map.put(ordinal, tileType);
	}

}
