package org.kareha.hareka.field;

public interface TileField {

	Tile getTile(Vector position);

}
