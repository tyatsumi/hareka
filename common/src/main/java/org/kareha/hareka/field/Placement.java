package org.kareha.hareka.field;

import java.io.IOException;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.kareha.hareka.annotation.Immutable;
import org.kareha.hareka.annotation.Private;
import org.kareha.hareka.protocol.ProtocolElement;
import org.kareha.hareka.protocol.ProtocolInput;
import org.kareha.hareka.protocol.ProtocolOutput;
import org.kareha.hareka.protocol.ProtocolException;

@Immutable
@XmlJavaTypeAdapter(Placement.Adapter.class)
public final class Placement implements ProtocolElement {

	@Private
	final Vector position;
	@Private
	final Direction direction;

	private Placement(final Vector position, final Direction direction) {
		if (position == null || direction == null) {
			throw new IllegalArgumentException("null");
		}
		this.position = position;
		this.direction = direction;
	}

	public static Placement valueOf(final Vector position, final Direction direction) {
		return new Placement(position, direction);
	}

	@Override
	public boolean equals(final Object obj) {
		if (!(obj instanceof Placement)) {
			return false;
		}
		final Placement placement = (Placement) obj;
		return placement.position.equals(position) && placement.direction == direction;
	}

	@Override
	public int hashCode() {
		int result = 19;
		result = 31 * result + position.hashCode();
		result = 31 * result + direction.hashCode();
		return result;
	}

	@Override
	public String toString() {
		return position + "," + direction;
	}

	public static Placement readFrom(final ProtocolInput in) throws IOException, ProtocolException {
		final Vector position = Vector.readFrom(in);
		final Direction direction = Direction.readFrom(in);
		return valueOf(position, direction);
	}

	@Override
	public void writeTo(final ProtocolOutput out) {
		out.write(position);
		out.write(direction);
	}

	@XmlType(name = "placement")
	@XmlAccessorType(XmlAccessType.NONE)
	private static class Adapted {

		@XmlElement
		private Vector position;
		@XmlElement
		private Direction direction;

		@SuppressWarnings("unused")
		private Adapted() {
			// used by JAXB
		}

		@Private
		Adapted(final Placement v) {
			position = v.position;
			direction = v.direction;
		}

		@Private
		Placement unmarshal() {
			return valueOf(position, direction);
		}

	}

	@Private
	Adapted marshal() {
		return new Adapted(this);
	}

	static class Adapter extends XmlAdapter<Adapted, Placement> {

		@Override
		public Adapted marshal(final Placement v) throws Exception {
			if (v == null) {
				return null;
			}
			return v.marshal();
		}

		@Override
		public Placement unmarshal(final Adapted v) throws Exception {
			if (v == null) {
				return null;
			}
			return v.unmarshal();
		}

	}

	public Placement transform(final Transformation v) {
		return valueOf(position.transform(v), direction.transform(v));
	}

	public Placement invert(final Transformation v) {
		return valueOf(position.invert(v), direction.invert(v));
	}

	public Vector getPosition() {
		return position;
	}

	public Direction getDirection() {
		return direction;
	}

}
