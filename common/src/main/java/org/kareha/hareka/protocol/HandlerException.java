package org.kareha.hareka.protocol;

@SuppressWarnings("serial")
public class HandlerException extends Exception {

	public HandlerException(final String message) {
		super(message);
	}

	public HandlerException(final String message, final Throwable cause) {
		super(message, cause);
	}

}
