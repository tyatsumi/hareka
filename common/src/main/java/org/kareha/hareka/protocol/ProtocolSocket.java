package org.kareha.hareka.protocol;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketException;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.net.ssl.SSLHandshakeException;

import org.kareha.hareka.annotation.GuardedBy;
import org.kareha.hareka.annotation.Private;
import org.kareha.hareka.annotation.ThreadSafe;

@ThreadSafe
public abstract class ProtocolSocket<T extends Enum<?> & PacketType, S> implements AutoCloseable {

	@Private
	static final Logger logger = Logger.getLogger(ProtocolSocket.class.getName());

	@GuardedBy("this")
	@Private
	final Socket socket;
	@Private
	final ReaderTask reader;
	@Private
	final WriterTask writer;
	@Private
	volatile boolean disconnected;

	public ProtocolSocket(final Socket socket, final HandlerTable<T, S> table, final S session) {
		this.socket = socket;

		try {
			final InputStream in = new BufferedInputStream(socket.getInputStream());
			reader = new ReaderTask(in, table, session);
			final OutputStream out = new BufferedOutputStream(socket.getOutputStream());
			writer = new WriterTask(out);
		} catch (final IOException e) {
			throw new IllegalArgumentException(e.getMessage(), e);
		}
	}

	// multiple call causes IllegalThreadStateException
	public void start() {
		reader.start();
		writer.start();
	}

	@Override
	public void close() {
		reader.close();
	}

	public void write(final Packet packet) {
		writer.add(packet);
	}

	public boolean isDisconnected() {
		return disconnected;
	}

	public abstract void finish();

	public synchronized InetAddress getInetAddress() {
		return socket.getInetAddress();
	}

	private class ReaderTask extends Thread implements AutoCloseable {

		private final InputStream in;
		private final HandlerTable<T, S> table;
		private final S session;

		ReaderTask(final InputStream in, final HandlerTable<T, S> table, final S session) {
			this.in = in;
			this.table = table;
			this.session = session;
		}

		@Override
		public void close() {
			try {
				synchronized (ProtocolSocket.this) {
					socket.close();
				}
			} catch (final IOException e) {
				// ignore
			}
		}

		@Override
		public void run() {
			try (final ProtocolInputStream pin = new ProtocolInputStream(in)) {
				// when blocked by reading
				// actual termination may be caused by
				// socket.close
				while (!Thread.currentThread().isInterrupted()) {
					try {
						final int type = pin.readCompactUInt();

						final Handler<S> handler = table.get(type);
						if (handler == null) {
							logger.severe("Unknown type: " + type);
							break;
						}
						try {
							handler.handle(pin, session);
						} catch (final IOException e) {
							logger.log(Level.SEVERE, "", e);
							break;
						} catch (final ProtocolException e) {
							logger.log(Level.WARNING, "", e);
							break;
						} catch (final HandlerException e) {
							logger.log(Level.WARNING, "", e);
							break;
						}
					} catch (final SSLHandshakeException e) {
						logger.info(e.getMessage() + " (" + socket.getInetAddress() + ")");
						break;
					} catch (final SocketException e) {
						break;
					} catch (final EOFException e) {
						break;
					} catch (final IOException e) {
						logger.log(Level.SEVERE, "", e);
						break;
					} catch (final ProtocolException e) {
						logger.log(Level.WARNING, "", e);
						break;
					}
				}
			} catch (final Exception e) {
				logger.log(Level.SEVERE, "", e);
			} finally {
				writer.interrupt();
				finish();
				disconnected = true;
			}
		}

	}

	private class WriterTask extends Thread {

		private static final int QUEUE_CAPACITY = 0x10000;

		private final OutputStream out;

		// if bounded queue is used, canceling may suffer
		private final BlockingQueue<Packet> queue = new LinkedBlockingQueue<>(QUEUE_CAPACITY);

		WriterTask(final OutputStream out) {
			this.out = out;
		}

		void add(Packet packet) {
			if (!queue.offer(packet)) {
				logger.warning("Overflow in writing queue in PacketSocket");
				close();
				return;
			}
		}

		@Override
		public void run() {
			try {
				while (!Thread.currentThread().isInterrupted()) {
					final Packet packet = queue.take();
					try {
						packet.write(out);
						out.flush();
					} catch (final SSLHandshakeException e) {
						logger.info(e.getMessage() + " (" + socket.getInetAddress() + ")");
						break;
					} catch (final SocketException e) {
						break;
					} catch (final IOException e) {
						logger.log(Level.SEVERE, "", e);
						break;
					}
				}
			} catch (final InterruptedException e) {
				Thread.currentThread().interrupt();
			} catch (final Exception e) {
				logger.log(Level.SEVERE, "", e);
			} finally {
				reader.close();
			}
		}

	}

}
