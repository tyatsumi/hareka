package org.kareha.hareka.protocol;

import java.io.IOException;
import java.math.BigInteger;
import java.security.Key;
import java.util.Locale;

public interface ProtocolInput {

	boolean readBoolean() throws IOException;

	byte readByte() throws IOException;

	char readChar() throws IOException;

	double readDouble() throws IOException;

	float readFloat() throws IOException;

	int readInt() throws IOException;

	long readLong() throws IOException;

	short readShort() throws IOException;

	int readCompactUInt() throws IOException, ProtocolException;

	int readCompactInt() throws IOException, ProtocolException;

	long readCompactULong() throws IOException, ProtocolException;

	long readCompactLong() throws IOException, ProtocolException;

	String readString() throws IOException, ProtocolException;

	byte[] readByteArray() throws IOException, ProtocolException;

	BigInteger readBigInteger() throws IOException, ProtocolException;

	Locale readLocale() throws IOException, ProtocolException;

	Key readKey() throws IOException, ProtocolException;

}
