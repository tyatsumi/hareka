package org.kareha.hareka.protocol;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public final class CompactNumbers {

	private CompactNumbers() {
		throw new AssertionError();
	}

	public static int readUInt(final InputStream in) throws IOException, ProtocolException {
		int result = 0;
		int i = 0;
		int b0 = 0;
		while (true) {
			final int b = in.read();
			if (b == -1) {
				throw new ProtocolException("Unexpected EOF");
			}
			if (i == 0) {
				if (b == 0x80) {
					throw new ProtocolException("Invalid sequence");
				}
				b0 = b;
			}
			result <<= 7;
			result |= b & 0x7f;
			if ((b & 0x80) == 0) {
				if (i == 4 && (b0 & 0x70) != 0) {
					throw new ProtocolException("Overflow");
				}
				return result;
			}
			i++;
			if (i >= 5) {
				throw new ProtocolException("Sequence too long");
			}
		}
	}

	@SuppressWarnings("all") // parameter-assignment
	public static void writeUInt(final OutputStream out, int v) throws IOException {
		final int[] array = new int[5];
		int i = 0;
		while (true) {
			array[i] = v & 0x7f;
			i++;
			v >>>= 7;
			if (v == 0) {
				break;
			}
		}
		while (i > 1) {
			i--;
			out.write(0x80 | array[i]);
		}
		i--;
		out.write(array[i]);
	}

	public static int readInt(final InputStream in) throws IOException, ProtocolException {
		int result = 0;
		int i = 0;
		int b0 = 0;
		while (true) {
			final int b = in.read();
			if (b == -1) {
				throw new ProtocolException("Unexpected EOF");
			}
			if (i == 0) {
				if ((b & 0x40) != 0) {
					result = -1;
				}
				b0 = b;
			} else if (i == 1) {
				if ((b & 0x40) == 0 && result == 0) {
					throw new ProtocolException("Invalid sequence");
				} else if ((b & 0x40) != 0 && result == -1) {
					throw new ProtocolException("Invalid sequence");
				}
			}
			result <<= 7;
			result |= b & 0x7f;
			if (result >= 0) {
				if ((b & 0x80) == 0) {
					if (i == 4) {
						if ((b0 & 0x70) != 0) {
							throw new ProtocolException("Overflow");
						}
					}
					return result;
				}
			} else {
				if ((b & 0x80) == 0x80) {
					if (i == 4) {
						if ((b0 & 0x70) != 0x70) {
							throw new ProtocolException("Overflow");
						}
					}
					return result;
				}
			}
			i++;
			if (i >= 5) {
				throw new ProtocolException("Sequence too long");
			}
		}
	}

	@SuppressWarnings("all") // parameter-assignment
	public static void writeInt(final OutputStream out, int v) throws IOException {
		final int[] array = new int[5];
		int i = 0;
		while (true) {
			array[i] = v & 0x7f;
			i++;
			v >>= 7;
			if (v == 0) {
				if ((array[i - 1] & 0x40) != 0) {
					array[i] = 0x00;
					i++;
				}
				break;
			} else if (v == -1) {
				if ((array[i - 1] & 0x40) == 0) {
					array[i] = 0x7f;
					i++;
				}
				break;
			}
		}
		final int alpha;
		final int beta;
		if (v >= 0) {
			alpha = 0x80;
			beta = 0x00;
		} else {
			alpha = 0x00;
			beta = 0x80;
		}
		while (i > 1) {
			i--;
			out.write(alpha | array[i]);
		}
		i--;
		out.write(beta | array[i]);
	}

	public static long readULong(final InputStream in) throws IOException, ProtocolException {
		long result = 0;
		int i = 0;
		int b0 = 0;
		while (true) {
			final int b = in.read();
			if (b == -1) {
				throw new ProtocolException("Unexpected EOF");
			}
			if (i == 0) {
				if (b == 0x80) {
					throw new ProtocolException("Invalid sequence");
				}
				b0 = b;
			}
			result <<= 7;
			result |= b & 0x7f;
			if ((b & 0x80) == 0) {
				if (i == 9 && (b0 & 0x7e) != 0) {
					throw new ProtocolException("Overflow");
				}
				return result;
			}
			i++;
			if (i >= 10) {
				throw new ProtocolException("Sequence too long");
			}
		}
	}

	@SuppressWarnings("all") // parameter-assignment
	public static void writeULong(final OutputStream out, long v) throws IOException {
		final int[] array = new int[10];
		int i = 0;
		while (true) {
			array[i] = (int) (v & 0x7f);
			i++;
			v >>>= 7;
			if (v == 0) {
				break;
			}
		}
		while (i > 1) {
			i--;
			out.write(0x80 | array[i]);
		}
		i--;
		out.write(array[i]);
	}

	public static long readLong(final InputStream in) throws IOException, ProtocolException {
		long result = 0;
		int i = 0;
		int b0 = 0;
		while (true) {
			final int b = in.read();
			if (b == -1) {
				throw new ProtocolException("Unexpected EOF");
			}
			if (i == 0) {
				if ((b & 0x40) != 0) {
					result = -1;
				}
				b0 = b;
			} else if (i == 1) {
				if ((b & 0x40) == 0 && result == 0) {
					throw new ProtocolException("Invalid sequence");
				} else if ((b & 0x40) != 0 && result == -1) {
					throw new ProtocolException("Invalid sequence");
				}
			}
			result <<= 7;
			result |= b & 0x7f;
			if (result >= 0) {
				if ((b & 0x80) == 0) {
					if (i == 9) {
						if ((b0 & 0x7e) != 0) {
							throw new ProtocolException("Overflow");
						}
					}
					return result;
				}
			} else {
				if ((b & 0x80) == 0x80) {
					if (i == 9) {
						if ((b0 & 0x7e) != 0x7e) {
							throw new ProtocolException("Overflow");
						}
					}
					return result;
				}
			}
			i++;
			if (i >= 10) {
				throw new ProtocolException("Sequence too long");
			}
		}
	}

	@SuppressWarnings("all") // parameter-assignment
	public static void writeLong(final OutputStream out, long v) throws IOException {
		final int[] array = new int[10];
		int i = 0;
		while (true) {
			array[i] = (int) (v & 0x7f);
			i++;
			v >>= 7;
			if (v == 0) {
				if ((array[i - 1] & 0x40) != 0) {
					array[i] = 0x00;
					i++;
				}
				break;
			} else if (v == -1) {
				if ((array[i - 1] & 0x40) == 0) {
					array[i] = 0x7f;
					i++;
				}
				break;
			}
		}
		final int alpha;
		final int beta;
		if (v >= 0) {
			alpha = 0x80;
			beta = 0x00;
		} else {
			alpha = 0x00;
			beta = 0x80;
		}
		while (i > 1) {
			i--;
			out.write(alpha | array[i]);
		}
		i--;
		out.write(beta | array[i]);
	}

}
