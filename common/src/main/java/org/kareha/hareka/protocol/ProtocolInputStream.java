package org.kareha.hareka.protocol;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Locale;

import org.kareha.hareka.annotation.NotThreadSafe;
import org.kareha.hareka.key.KeyXml;

@NotThreadSafe
public class ProtocolInputStream extends DataInputStream implements ProtocolInput {

	public static final int MAX_STRING_LENGTH = Short.MAX_VALUE;
	public static final int MAX_BYTE_ARRAY_LENGTH = Short.MAX_VALUE;
	public static final int MAX_KEY_LENGTH = Short.MAX_VALUE;

	public ProtocolInputStream(final InputStream in) {
		super(in);
	}

	public ProtocolInputStream(final byte[] buf) {
		super(new ByteArrayInputStream(buf));
	}

	public ProtocolInputStream(final byte[] buf, final int offset, final int length) {
		super(new ByteArrayInputStream(buf, offset, length));
	}

	@Override
	public int readCompactUInt() throws IOException, ProtocolException {
		return CompactNumbers.readUInt(this);
	}

	@Override
	public int readCompactInt() throws IOException, ProtocolException {
		return CompactNumbers.readInt(this);
	}

	@Override
	public long readCompactULong() throws IOException, ProtocolException {
		return CompactNumbers.readULong(this);
	}

	@Override
	public long readCompactLong() throws IOException, ProtocolException {
		return CompactNumbers.readLong(this);
	}

	private String readString(final int maxLength) throws IOException, ProtocolException {
		final int length = readCompactUInt();
		if (length < 0) {
			throw new ProtocolException("Negative length");
		}
		if (length > maxLength) {
			throw new ProtocolException("Max length exceeded");
		}
		final byte[] b = new byte[length];
		for (int i = 0; i < length; i++) {
			b[i] = readByte();
		}
		try {
			return new String(b, 0, b.length, "UTF-8");
		} catch (final UnsupportedEncodingException e) {
			throw new AssertionError(e);
		}
	}

	@Override
	public String readString() throws IOException, ProtocolException {
		return readString(MAX_STRING_LENGTH);
	}

	private byte[] readByteArray(final int maxLength) throws IOException, ProtocolException {
		final int length = readCompactUInt();
		if (length < 0) {
			throw new ProtocolException("Negative length");
		}
		if (length > maxLength) {
			throw new ProtocolException("Max length exceeded");
		}
		final byte[] b = new byte[length];
		for (int i = 0; i < length; i++) {
			b[i] = readByte();
		}
		return b;
	}

	@Override
	public byte[] readByteArray() throws IOException, ProtocolException {
		return readByteArray(MAX_BYTE_ARRAY_LENGTH);
	}

	@Override
	public BigInteger readBigInteger() throws IOException, ProtocolException {
		return new BigInteger(readByteArray());
	}

	@Override
	public Locale readLocale() throws IOException, ProtocolException {
		final String tag = readString();
		return new Locale.Builder().setLanguageTag(tag).build();
	}

	private Key readKey(final int maxLength) throws IOException, ProtocolException {
		final String algorithm = readString();
		final String format = readString();
		final byte[] encoded = readByteArray(maxLength);
		final KeyXml xml = new KeyXml(algorithm, format, encoded);
		try {
			return xml.toObject();
		} catch (final NoSuchAlgorithmException | InvalidKeySpecException e) {
			throw new ProtocolException("Failed to reconstruct key", e);
		}
	}

	@Override
	public Key readKey() throws IOException, ProtocolException {
		return readKey(MAX_KEY_LENGTH);
	}

}
