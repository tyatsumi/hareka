package org.kareha.hareka.protocol;

@SuppressWarnings("serial")
public class ProtocolException extends Exception {

	public ProtocolException(final String message) {
		super(message);
	}

	public ProtocolException(final String message, final Throwable cause) {
		super(message, cause);
	}

}
