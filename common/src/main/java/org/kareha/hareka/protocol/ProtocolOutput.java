package org.kareha.hareka.protocol;

import java.math.BigInteger;
import java.security.Key;
import java.util.Locale;

public interface ProtocolOutput {

	// void write(byte[] b);

	// void write(byte[] b, int off, int len);

	// void write(int b);

	void writeBoolean(boolean v);

	void writeByte(int v);

	void writeChar(int v);

	void writeDouble(double v);

	void writeFloat(float v);

	void writeInt(int v);

	void writeLong(long v);

	void writeShort(int v);

	void writeCompactUInt(int v);

	void writeCompactInt(int v);

	void writeCompactULong(long v);

	void writeCompactLong(long v);

	void writeString(String v);

	void writeByteArray(byte[] v);

	void writeBigInteger(BigInteger v);

	void write(ProtocolElement v);

	void writeLocale(Locale v);

	void writeKey(Key v);

}
