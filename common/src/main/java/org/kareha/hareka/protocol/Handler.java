package org.kareha.hareka.protocol;

import java.io.IOException;

public interface Handler<S> {

	void handle(ProtocolInput in, S session) throws IOException, ProtocolException, HandlerException;

}
