package org.kareha.hareka.protocol;

import java.io.IOException;
import java.io.OutputStream;

public abstract class Packet {

	protected final ProtocolOutput out = new ProtocolOutputStream();

	protected abstract PacketType getType();

	public final void write(final OutputStream os) throws IOException {
		final ProtocolOutputStream s = (ProtocolOutputStream) out;
		CompactNumbers.writeUInt(os, getType().typeValue());
		s.writeTo(os);
	}

}
