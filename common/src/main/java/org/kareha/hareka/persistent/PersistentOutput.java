package org.kareha.hareka.persistent;

import java.io.DataOutput;
import java.io.IOException;

public interface PersistentOutput extends DataOutput {

	long getFilePointer();

	void flush() throws IOException;

	void writeCompactUInt(int v) throws IOException;

	void writeString(String v) throws IOException;

	void writeByteArray(byte[] v) throws IOException;

}
