package org.kareha.hareka.persistent;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;

import org.kareha.hareka.annotation.GuardedBy;
import org.kareha.hareka.protocol.ProtocolException;

public class FastPersistentHashTable<K, V> extends AbstractPersistentHashTable<K, V> {

	@GuardedBy("this")
	protected final RandomAccessFile file;

	public FastPersistentHashTable(final File file, final PersistentFormat<K> keyFormat,
			final PersistentFormat<V> valueFormat) throws IOException {
		super(keyFormat, valueFormat);
		final boolean exists = file.isFile();
		this.file = new RandomAccessFile(file, "rwd");

		if (!exists) {
			final byte[] b = new byte[BUCKET_SIZE * Long.BYTES];
			for (int i = 0; i < b.length; i++) {
				b[i] = -1;
			}
			this.file.write(b);
		}
	}

	@Override
	public synchronized V get(final K key) throws IOException, ProtocolException {
		final long bucketOffset = getHashCode(key) * Long.BYTES;
		file.seek(bucketOffset);
		long entryOffset = file.readLong();
		while (entryOffset != -1) {
			file.seek(entryOffset);
			final PersistentInput in = new BufferedPersistentInput(file);
			entryOffset = in.readLong();
			final K k = keyFormat.read(in);
			if (k.equals(key)) {
				return valueFormat.read(in);
			}
		}
		return null;
	}

	@Override
	public synchronized V put(final K key, final V value) throws IOException, ProtocolException {
		V v = null;
		long prevEntryOffset = getHashCode(key) * Long.BYTES;
		file.seek(prevEntryOffset);
		long entryOffset = file.readLong();
		while (entryOffset != -1) {
			file.seek(entryOffset);
			final long nextEntryOffset = file.readLong();
			if (v == null) {
				final PersistentInput in = new BufferedPersistentInput(file);
				final K k = keyFormat.read(in);
				if (k.equals(key)) {
					v = valueFormat.read(in);
					file.seek(prevEntryOffset);
					file.writeLong(nextEntryOffset);
				}
			}
			prevEntryOffset = entryOffset;
			entryOffset = nextEntryOffset;
		}
		file.seek(prevEntryOffset);
		final long length = file.length();
		file.writeLong(length);
		file.seek(length);
		final PersistentOutput out = new BufferedPersistentOutput(file);
		out.writeLong(-1);
		keyFormat.write(out, key);
		valueFormat.write(out, value);
		out.flush();
		return v;
	}

	@Override
	public synchronized V remove(final K key) throws IOException, ProtocolException {
		long prevEntryOffset = getHashCode(key) * Long.BYTES;
		file.seek(prevEntryOffset);
		long entryOffset = file.readLong();
		while (entryOffset != -1) {
			file.seek(entryOffset);
			final long nextEntryOffset = file.readLong();
			final PersistentInput in = new BufferedPersistentInput(file);
			final K k = keyFormat.read(in);
			if (k.equals(key)) {
				final V v = valueFormat.read(in);
				file.seek(prevEntryOffset);
				file.writeLong(nextEntryOffset);
				return v;
			}
			prevEntryOffset = entryOffset;
			entryOffset = nextEntryOffset;
		}
		return null;
	}

	@Override
	public synchronized void close() throws Exception {
		file.close();
	}

}
