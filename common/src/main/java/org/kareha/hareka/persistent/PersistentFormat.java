package org.kareha.hareka.persistent;

import java.io.IOException;

import org.kareha.hareka.protocol.ProtocolException;

public interface PersistentFormat<T> {

	T read(PersistentInput in) throws IOException, ProtocolException;

	void write(PersistentOutput out, T v) throws IOException;

}
