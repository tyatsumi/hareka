package org.kareha.hareka.persistent;

import java.io.IOException;

import org.kareha.hareka.ManuallyClosable;
import org.kareha.hareka.protocol.ProtocolException;

public interface PersistentHashTable<K, V> extends ManuallyClosable {

	V get(K key) throws IOException, ProtocolException;

	V put(K key, V value) throws IOException, ProtocolException;

	V remove(K key) throws IOException, ProtocolException;

}
