package org.kareha.hareka.persistent;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.io.UnsupportedEncodingException;

import org.kareha.hareka.annotation.NotThreadSafe;
import org.kareha.hareka.protocol.CompactNumbers;

@NotThreadSafe
public class BufferedPersistentOutput implements PersistentOutput {

	protected static final int DEFAULT_BUFFER_SIZE = 8192;

	protected final RandomAccessFile out;
	protected long filePointer;
	protected final ByteArrayOutputStream bout = new ByteArrayOutputStream();
	protected final DataOutputStream dout = new DataOutputStream(bout);
	protected final int bufferSize;

	public BufferedPersistentOutput(final RandomAccessFile out) throws IOException {
		this(out, DEFAULT_BUFFER_SIZE);
	}

	public BufferedPersistentOutput(final RandomAccessFile out, final int bufferSize) throws IOException {
		this.out = out;
		filePointer = out.getFilePointer();
		this.bufferSize = bufferSize;
	}

	@Override
	public long getFilePointer() {
		return filePointer + bout.size();
	}

	@Override
	public void flush() throws IOException {
		out.write(bout.toByteArray());
		filePointer += bout.size();
		bout.reset();
	}

	private void checkBuffer() throws IOException {
		if (bout.size() >= bufferSize) {
			flush();
		}
	}

	@Override
	public void write(final int b) throws IOException {
		dout.write(b);
		checkBuffer();
	}

	@Override
	public void write(final byte[] b) throws IOException {
		dout.write(b);
		checkBuffer();
	}

	@Override
	public void write(final byte[] b, final int off, final int len) throws IOException {
		dout.write(b, off, len);
		checkBuffer();
	}

	@Override
	public void writeBoolean(final boolean v) throws IOException {
		dout.writeBoolean(v);
		checkBuffer();
	}

	@Override
	public void writeByte(final int v) throws IOException {
		dout.writeByte(v);
		checkBuffer();
	}

	@Override
	public void writeBytes(final String s) throws IOException {
		dout.writeBytes(s);
		checkBuffer();
	}

	@Override
	public void writeChar(final int v) throws IOException {
		dout.writeChar(v);
		checkBuffer();
	}

	@Override
	public void writeChars(final String v) throws IOException {
		dout.writeChars(v);
		checkBuffer();
	}

	@Override
	public void writeDouble(final double v) throws IOException {
		dout.writeDouble(v);
		checkBuffer();
	}

	@Override
	public void writeFloat(final float v) throws IOException {
		dout.writeFloat(v);
		checkBuffer();
	}

	@Override
	public void writeInt(final int v) throws IOException {
		dout.writeInt(v);
		checkBuffer();
	}

	@Override
	public void writeLong(final long v) throws IOException {
		dout.writeLong(v);
		checkBuffer();
	}

	@Override
	public void writeShort(final int v) throws IOException {
		dout.writeShort(v);
		checkBuffer();
	}

	@Override
	public void writeUTF(final String s) throws IOException {
		dout.writeUTF(s);
		checkBuffer();
	}

	@Override
	public synchronized void writeCompactUInt(final int v) throws IOException {
		CompactNumbers.writeUInt(dout, v);
		checkBuffer();
	}

	@Override
	public synchronized void writeString(final String v) throws IOException {
		byte[] b;
		try {
			b = v.getBytes("UTF-8");
		} catch (final UnsupportedEncodingException e) {
			// UTF-8 encoding is expected to be always supported
			throw new AssertionError(e);
		}
		writeCompactUInt(b.length);
		write(b);
	}

	@Override
	public synchronized void writeByteArray(final byte[] v) throws IOException {
		writeCompactUInt(v.length);
		write(v);
	}
}
