package org.kareha.hareka.persistent;

import java.io.DataInput;
import java.io.IOException;

import org.kareha.hareka.protocol.ProtocolException;

public interface PersistentInput extends DataInput {

	long getFilePointer();

	int readCompactUInt() throws IOException, ProtocolException;

	String readString() throws IOException, ProtocolException;

	byte[] readByteArray() throws IOException, ProtocolException;

}
