package org.kareha.hareka.wait;

import java.io.IOException;

import org.kareha.hareka.annotation.Immutable;
import org.kareha.hareka.protocol.ProtocolElement;
import org.kareha.hareka.protocol.ProtocolInput;
import org.kareha.hareka.protocol.ProtocolOutput;
import org.kareha.hareka.protocol.ProtocolException;

@Immutable
public class Wait implements ProtocolElement {

	private final long id;
	private final WaitType type;
	private final int duration;
	private final long timestamp;

	Wait(final long id, final WaitType type, final int duration) {
		this(id, type, duration, System.currentTimeMillis());
	}

	private Wait(final long id, final WaitType type, final int duration, final long timestamp) {
		this.id = id;
		this.type = type;
		this.duration = duration;
		this.timestamp = timestamp;
	}

	public static Wait readFrom(final ProtocolInput in) throws IOException, ProtocolException {
		final long id = in.readCompactULong();
		final WaitType type = WaitType.readFrom(in);
		final int duration = in.readCompactInt();
		return new Wait(id, type, duration);
	}

	@Override
	public void writeTo(final ProtocolOutput out) {
		out.writeCompactULong(id);
		out.write(type);
		out.writeCompactInt(getRemaining());
	}

	public long getId() {
		return id;
	}

	public WaitType getType() {
		return type;
	}

	public boolean isExpiredWithDelay(final int delay) {
		final int d;
		if (delay < 0) {
			d = 0;
		} else if (delay > duration) {
			d = duration;
		} else {
			d = delay;
		}
		return System.currentTimeMillis() >= timestamp + duration - d;
	}

	public boolean isExpiredWithPenalty(final int penalty) {
		return System.currentTimeMillis() >= timestamp + duration + penalty;
	}

	public int getRemaining() {
		return (int) (timestamp + duration - System.currentTimeMillis());
	}

	public Wait penalize(final int penalty) {
		return new Wait(id, type, duration + penalty, timestamp);
	}

}
