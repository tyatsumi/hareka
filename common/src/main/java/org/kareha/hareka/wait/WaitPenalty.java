package org.kareha.hareka.wait;

import java.util.EnumMap;
import java.util.Map;

import org.kareha.hareka.annotation.GuardedBy;
import org.kareha.hareka.annotation.ThreadSafe;

@ThreadSafe
public class WaitPenalty {

	@GuardedBy("this")
	private Map<WaitType, Integer> map;

	public synchronized void put(final WaitType type, final int value) {
		if (map == null) {
			map = new EnumMap<>(WaitType.class);
		}
		map.put(type, value);
	}

	public synchronized int get(final WaitType type) {
		if (map == null) {
			return 0;
		}
		final Integer value = map.get(type);
		if (value == null) {
			return 0;
		}
		return value;
	}

	public synchronized void remove(final WaitType type) {
		if (map == null) {
			return;
		}
		map.remove(type);
	}

}
