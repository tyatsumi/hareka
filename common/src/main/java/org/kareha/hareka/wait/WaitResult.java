package org.kareha.hareka.wait;

public class WaitResult {

	private final boolean success;
	private final Wait wait;

	WaitResult(final boolean success, final Wait wait) {
		this.success = success;
		this.wait = wait;
	}

	public boolean isSuccess() {
		return success;
	}

	public Wait getWait() {
		return wait;
	}

}
