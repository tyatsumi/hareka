package org.kareha.hareka.wait;

import java.util.EnumMap;
import java.util.Map;

import org.kareha.hareka.annotation.GuardedBy;
import org.kareha.hareka.annotation.ThreadSafe;

@ThreadSafe
public class WaitTable {

	@GuardedBy("this")
	private final Map<WaitType, Wait> map = new EnumMap<>(WaitType.class);
	@GuardedBy("this")
	private int delay;

	public synchronized void put(final Wait v) {
		map.put(v.getType(), v);
	}

	public synchronized Wait get(final WaitType type) {
		final Wait wait = map.get(type);
		if (wait == null) {
			return null;
		}
		if (!wait.isExpiredWithDelay(delay)) {
			return null;
		}

		map.remove(type);

		return wait;
	}

	public synchronized int getDelay() {
		return delay;
	}

	public synchronized void setDelay(final int v) {
		delay = v;
	}

}
