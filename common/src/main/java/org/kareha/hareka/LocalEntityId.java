package org.kareha.hareka;

import java.io.IOException;
import java.util.Arrays;

import javax.xml.bind.DatatypeConverter;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.kareha.hareka.annotation.Immutable;
import org.kareha.hareka.annotation.Private;
import org.kareha.hareka.protocol.ProtocolElement;
import org.kareha.hareka.protocol.ProtocolInput;
import org.kareha.hareka.protocol.ProtocolOutput;
import org.kareha.hareka.protocol.ProtocolException;

@Immutable
@XmlJavaTypeAdapter(LocalEntityId.Adapter.class)
public final class LocalEntityId implements ProtocolElement {

	private final byte[] value;

	private LocalEntityId(final byte[] value) {
		if (value == null) {
			throw new IllegalArgumentException("null");
		}
		if (value.length != Constants.ID_CIPHER_KEY_LENGTH) {
			throw new IllegalArgumentException("value.length is not " + Constants.ID_CIPHER_KEY_LENGTH);
		}
		this.value = value.clone();
	}

	private LocalEntityId(final String hexString) {
		if (hexString == null) {
			throw new IllegalArgumentException("null");
		}
		// DatatypeConverter.parseHexBinary throws IllegalArgumentException
		value = DatatypeConverter.parseHexBinary(hexString);
	}

	public static LocalEntityId valueOf(final byte[] value) {
		return new LocalEntityId(value);
	}

	public static LocalEntityId valueOf(final String hexString) {
		return new LocalEntityId(hexString);
	}

	@Override
	public boolean equals(final Object obj) {
		if (!(obj instanceof LocalEntityId)) {
			return false;
		}
		final LocalEntityId localId = (LocalEntityId) obj;
		return Arrays.equals(localId.value, value);
	}

	@Override
	public int hashCode() {
		return Arrays.hashCode(value);
	}

	@Override
	public String toString() {
		return DatatypeConverter.printHexBinary(value).toLowerCase();
	}

	public static LocalEntityId readFrom(final ProtocolInput in) throws IOException, ProtocolException {
		final byte[] value = in.readByteArray();
		return valueOf(value);
	}

	@Override
	public void writeTo(final ProtocolOutput out) {
		out.writeByteArray(value);
	}

	@XmlType(name = "localEntityId")
	@XmlAccessorType(XmlAccessType.NONE)
	private static class Adapted {

		@XmlValue
		private String value;

		@SuppressWarnings("unused")
		private Adapted() {
			// used by JAXB
		}

		@Private
		Adapted(final LocalEntityId v) {
			value = v.toString();
		}

		@Private
		LocalEntityId unmarshal() {
			return valueOf(value);
		}

	}

	@Private
	Adapted marshal() {
		return new Adapted(this);
	}

	static class Adapter extends XmlAdapter<Adapted, LocalEntityId> {

		@Override
		public Adapted marshal(final LocalEntityId v) throws Exception {
			if (v == null) {
				return null;
			}
			return v.marshal();
		}

		@Override
		public LocalEntityId unmarshal(final Adapted v) throws Exception {
			if (v == null) {
				return null;
			}
			return v.unmarshal();
		}

	}

	public byte[] value() {
		return value.clone();
	}

}
