package org.kareha.hareka.user;

import java.util.Collection;
import java.util.Collections;

class EmptyRoleSet implements RoleSet {

	@Override
	public Collection<Role> getRoles() {
		return Collections.emptyList();
	}

	@Override
	public boolean isAbleTo(final Permission permission) {
		if (permission == null) {
			throw new IllegalArgumentException("null");
		}
		return false;
	}

	@Override
	public Role getHighestRole(final Permission permission) {
		if (permission == null) {
			throw new IllegalArgumentException("null");
		}
		return null;
	}

	@Override
	public int getHighestRank() {
		return 0;
	}

}
