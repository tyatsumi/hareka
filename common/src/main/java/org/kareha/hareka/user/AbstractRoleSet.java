package org.kareha.hareka.user;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.kareha.hareka.annotation.GuardedBy;

public abstract class AbstractRoleSet implements RoleSet {

	@GuardedBy("this")
	protected final Set<Role> roles = new HashSet<>();

	public AbstractRoleSet() {
	}

	public AbstractRoleSet(final RoleSet original) {
		roles.addAll(original.getRoles());
	}

	@Override
	public synchronized Collection<Role> getRoles() {
		return new ArrayList<>(roles);
	}

	@Override
	public synchronized boolean isAbleTo(final Permission permission) {
		if (permission == null) {
			throw new IllegalArgumentException("null");
		}
		for (final Role role : roles) {
			if (role.isAbleTo(permission)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public synchronized Role getHighestRole(final Permission permission) {
		if (permission == null) {
			throw new IllegalArgumentException("null");
		}
		Role highest = null;
		for (final Role role : roles) {
			if (role.isAbleTo(permission)) {
				if (highest == null || role.getRank() > highest.getRank()) {
					highest = role;
				}
			}
		}
		return highest;
	}

	@Override
	public synchronized int getHighestRank() {
		int rank = 0;
		for (final Role role : roles) {
			if (role.getRank() > rank) {
				rank = role.getRank();
			}
		}
		return rank;
	}

}
