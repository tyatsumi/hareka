package org.kareha.hareka.user;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.kareha.hareka.annotation.Immutable;
import org.kareha.hareka.annotation.Private;

@Immutable
@XmlJavaTypeAdapter(UserId.Adapter.class)
public final class UserId {

	@Private
	final long value;

	private UserId(final long value) {
		this.value = value;
	}

	public static UserId valueOf(final long value) {
		return new UserId(value);
	}

	public static UserId valueOf(final String s) {
		// Long.parseLong throws NumberFormatException
		final long value = Long.parseLong(s, 16);
		return valueOf(value);
	}

	@Override
	public boolean equals(final Object obj) {
		if (!(obj instanceof UserId)) {
			return false;
		}
		final UserId id = (UserId) obj;
		return id.value == value;
	}

	@Override
	public int hashCode() {
		return (int) (value >>> 32 ^ value);
	}

	@Override
	public String toString() {
		return Long.toHexString(value);
	}

	@XmlType(name = "userId")
	@XmlAccessorType(XmlAccessType.NONE)
	private static class Adapted {

		@XmlValue
		private String value;

		@SuppressWarnings("unused")
		private Adapted() {
			// used by JAXB
		}

		@Private
		Adapted(final UserId v) {
			value = v.toString();
		}

		@Private
		UserId unmarshal() {
			return valueOf(value);
		}

	}

	@Private
	Adapted marshal() {
		return new Adapted(this);
	}

	static class Adapter extends XmlAdapter<Adapted, UserId> {

		@Override
		public Adapted marshal(final UserId v) throws Exception {
			if (v == null) {
				return null;
			}
			return v.marshal();
		}

		@Override
		public UserId unmarshal(final Adapted v) throws Exception {
			if (v == null) {
				return null;
			}
			return v.unmarshal();
		}

	}

	public long value() {
		return value;
	}

}
