package org.kareha.hareka.user;

import java.util.Collection;

import org.kareha.hareka.protocol.ProtocolElement;
import org.kareha.hareka.protocol.ProtocolOutput;

public interface Role extends ProtocolElement {

	Role SUPER = new SuperRole();

	String getId();

	int getRank();

	boolean isAbleTo(Permission permission);

	Collection<Permission> getPermissions();

	@Override
	default void writeTo(final ProtocolOutput out) {
		out.writeString(getId());
		out.writeCompactUInt(getRank());
		final Collection<Permission> permissions = getPermissions();
		out.writeCompactUInt(permissions.size());
		for (final Permission permission : permissions) {
			out.write(permission);
		}
	}

}
