package org.kareha.hareka.user;

import java.util.Collection;

public interface RoleSet {

	RoleSet EMPTY = new EmptyRoleSet();

	Collection<Role> getRoles();

	boolean isAbleTo(Permission permission);

	Role getHighestRole(Permission permission);

	int getHighestRank();

}
