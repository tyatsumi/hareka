package org.kareha.hareka.user;

import java.io.IOException;

import org.kareha.hareka.protocol.ProtocolElement;
import org.kareha.hareka.protocol.ProtocolInput;
import org.kareha.hareka.protocol.ProtocolOutput;
import org.kareha.hareka.protocol.ProtocolException;

public enum UserRegistrationMode implements ProtocolElement {

	INVITED_ONLY, ACCEPT_ALL;

	private static final UserRegistrationMode[] values = values();

	public static UserRegistrationMode valueOf(final int ordinal) {
		if (ordinal < 0 || ordinal >= values.length) {
			throw new IllegalArgumentException("Out of bounds");
		}
		return values[ordinal];
	}

	public static UserRegistrationMode readFrom(final ProtocolInput in) throws IOException, ProtocolException {
		final int ordinal = in.readCompactUInt();
		return valueOf(ordinal);
	}

	@Override
	public void writeTo(final ProtocolOutput out) {
		out.writeCompactUInt(ordinal());
	}

}
