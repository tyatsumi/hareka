package org.kareha.hareka.user;

public class SingleRoleSet extends AbstractRoleSet {

	public SingleRoleSet(final Role role) {
		roles.add(role);
	}

}
