package org.kareha.hareka.user;

import java.io.IOException;

import org.kareha.hareka.persistent.PersistentFormat;
import org.kareha.hareka.persistent.PersistentInput;
import org.kareha.hareka.persistent.PersistentOutput;

public class UserIdPersistentFormat implements PersistentFormat<UserId> {

	@Override
	public UserId read(final PersistentInput in) throws IOException {
		return UserId.valueOf(in.readLong());
	}

	@Override
	public void write(final PersistentOutput out, final UserId v) throws IOException {
		out.writeLong(v.value());
	}

}
