package org.kareha.hareka.key;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Signature;
import java.security.SignatureException;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import org.kareha.hareka.annotation.GuardedBy;
import org.kareha.hareka.annotation.ThreadSafe;

@ThreadSafe
public class KeyGrabber {

	public interface Handler {

		void handle(int version, PublicKey publicKey);

	}

	public static class Entry {

		private static final long DEFAULT_TIME_LIMIT = 60 * 1000;
		private static final Random random = new SecureRandom();

		private final int id;
		private final Handler handler;
		@GuardedBy("this")
		private byte[] nonce;
		@GuardedBy("this")
		private long timestamp;

		Entry(final int id, final Handler handler) {
			this.id = id;
			this.handler = handler;
		}

		public int getId() {
			return id;
		}

		public void handle(final int version, final PublicKey publicKey) {
			handler.handle(version, publicKey);
		}

		public byte[] newNonce(final int length) {
			final byte[] n = new byte[length];
			random.nextBytes(n);
			synchronized (this) {
				nonce = n;
				timestamp = System.currentTimeMillis();
			}
			return n;
		}

		public boolean verify(final PublicKey publicKey, final String algorithm, final byte[] signature) {
			return verify(publicKey, algorithm, signature, DEFAULT_TIME_LIMIT);
		}

		public boolean verify(final PublicKey publicKey, final String algorithm, final byte[] signature,
				final long timeLimit) {
			try {
				final byte[] n;
				synchronized (this) {
					if (nonce == null) {
						return false;
					}
					if (System.currentTimeMillis() > timestamp + timeLimit) {
						return false;
					}
					n = nonce;
				}
				final Signature s;
				try {
					s = Signature.getInstance(algorithm);
				} catch (final NoSuchAlgorithmException e) {
					return false;
				}
				try {
					s.initVerify(publicKey);
				} catch (final InvalidKeyException e) {
					return false;
				}
				try {
					s.update(n);
				} catch (final SignatureException e) {
					return false;
				}
				final boolean result;
				try {
					result = s.verify(signature);
				} catch (final SignatureException e) {
					return false;
				}
				if (!result) {
					return false;
				}
				return true;
			} finally {
				synchronized (this) {
					nonce = null;
				}
			}
		}

	}

	private final int capacity;
	@GuardedBy("this")
	private int count;
	@GuardedBy("this")
	private final Map<Integer, Entry> entryMap;
	@GuardedBy("this")
	private PublicKey latest;

	public KeyGrabber(final int capacity) {
		this.capacity = capacity;
		entryMap = new HashMap<>(Math.min(16, (int) Math.ceil(capacity / 0.75)));
	}

	public KeyGrabber() {
		this(1);
	}

	public synchronized Entry add(final Handler v) {
		if (entryMap.size() >= capacity) {
			return null;
		}
		final int id = count++;
		final Entry entry = new Entry(id, v);
		entryMap.put(id, entry);
		return entry;
	}

	public synchronized Entry get(final int id) {
		return entryMap.remove(id);
	}

	public synchronized PublicKey getLatest() {
		return latest;
	}

	public synchronized void setLatest(final PublicKey latest) {
		this.latest = latest;
	}

}
