package org.kareha.hareka.key;

import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Signature;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.kareha.hareka.annotation.NotThreadSafe;

@NotThreadSafe
@XmlRootElement(name = "keyPair")
@XmlAccessorType(XmlAccessType.NONE)
public class KeyPairXml {

	private static final int RSA_NONCE_BYTE_LENGTH = 32;
	private static final String RSA_SIGNATURE_ALGORITHM = "SHA256withRSA";

	@XmlElement(name = "public")
	private KeyXml publicKey;
	@XmlElement(name = "private")
	private KeyXml privateKey;

	@SuppressWarnings("unused")
	private KeyPairXml() {
		// used by JAXB
	}

	public KeyPairXml(final KeyPair keyPair) {
		publicKey = new KeyXml(keyPair.getPublic());
		privateKey = new KeyXml(keyPair.getPrivate());
	}

	public KeyPairXml(final KeyPair keyPair, final PasswordProtection protection, final char[] password)
			throws EncryptionException {
		publicKey = new KeyXml(keyPair.getPublic());
		privateKey = new KeyXml(keyPair.getPrivate(), protection, password);
	}

	public boolean isProtected() {
		return privateKey.isProtected();
	}

	public KeyPair toObject() throws NoSuchAlgorithmException, InvalidKeySpecException {
		return new KeyPair((PublicKey) publicKey.toObject(), (PrivateKey) privateKey.toObject());
	}

	public KeyPair toObject(final char[] password)
			throws NoSuchAlgorithmException, InvalidKeySpecException, DecryptionException {
		final KeyPair keyPair = new KeyPair((PublicKey) publicKey.toObject(),
				(PrivateKey) privateKey.toObject(password));

		// verify the key pair

		final String algorithm = keyPair.getPublic().getAlgorithm();
		if (!keyPair.getPrivate().getAlgorithm().equals(algorithm)) {
			throw new IllegalStateException("Algorithms not match");
		}

		if (algorithm.equals("RSA")) {
			// generate a random nonce
			final SecureRandom random = new SecureRandom();
			final byte[] nonce = new byte[RSA_NONCE_BYTE_LENGTH];
			random.nextBytes(nonce);

			// create a signature for the random nonce with private key
			final Signature signature;
			try {
				signature = Signature.getInstance(RSA_SIGNATURE_ALGORITHM);
			} catch (final NoSuchAlgorithmException e) {
				throw new AssertionError(e);
			}
			try {
				signature.initSign(keyPair.getPrivate());
			} catch (final InvalidKeyException e) {
				throw new DecryptionException(e);
			}
			try {
				signature.update(nonce);
			} catch (final SignatureException e) {
				throw new DecryptionException(e);
			}
			final byte[] s;
			try {
				s = signature.sign();
			} catch (final SignatureException e) {
				throw new DecryptionException(e);
			}

			// verify the signature with public key
			final Signature signature2;
			try {
				signature2 = Signature.getInstance(RSA_SIGNATURE_ALGORITHM);
			} catch (final NoSuchAlgorithmException e) {
				throw new AssertionError();
			}
			try {
				signature2.initVerify(keyPair.getPublic());
			} catch (final InvalidKeyException e) {
				throw new DecryptionException(e);
			}
			try {
				signature2.update(nonce);
			} catch (final SignatureException e) {
				throw new DecryptionException(e);
			}
			final boolean result;
			try {
				result = signature2.verify(s);
			} catch (final SignatureException e) {
				throw new DecryptionException(e);
			}
			if (!result) {
				throw new DecryptionException("Signature verification failed");
			}

			// now key pair is verified
			return keyPair;
		}
		throw new UnsupportedOperationException("Algorithm not supported yet");
	}

}
