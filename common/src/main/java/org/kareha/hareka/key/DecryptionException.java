package org.kareha.hareka.key;

@SuppressWarnings("serial")
public class DecryptionException extends Exception {

	public DecryptionException() {
		super();
	}

	public DecryptionException(final String message) {
		super(message);
	}

	public DecryptionException(final String message, final Throwable cause) {
		super(message, cause);
	}

	protected DecryptionException(final String message, final Throwable cause, final boolean enableSuppression,
			final boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public DecryptionException(final Throwable cause) {
		super(cause);
	}

}
