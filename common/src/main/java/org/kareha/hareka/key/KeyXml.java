package org.kareha.hareka.key;

import java.security.Key;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.kareha.hareka.annotation.NotThreadSafe;

/**
 * This class implements XML representation for Key as JAXB object. Optionally,
 * encryption for encoded key with password is supported.
 * 
 * This class is not thread-safe.
 * 
 * @see java.security.Key
 * 
 * @author Aki Goto
 */
@NotThreadSafe
@XmlRootElement(name = "key")
@XmlAccessorType(XmlAccessType.NONE)
public class KeyXml {

	@XmlElement
	private PasswordProtection protection;
	@XmlElement
	private String algorithm;
	@XmlElement
	private String format;
	@XmlElement
	private byte[] encoded;

	@SuppressWarnings("unused")
	private KeyXml() {
		// used by JAXB
	}

	/**
	 * Constructs a JAXB object from the given key.
	 * 
	 * @param key
	 *            the key.
	 */
	public KeyXml(final Key key) {
		algorithm = key.getAlgorithm();
		format = key.getFormat();
		encoded = key.getEncoded();
	}

	/**
	 * Constructs a JAXB object from the given parameters for key.
	 * 
	 * @param algorithm
	 *            the name of the algorithm.
	 * @param format
	 *            the encoding format.
	 * @param encoded
	 *            the encoded key.
	 */
	public KeyXml(final String algorithm, final String format, final byte[] encoded) {
		this.algorithm = algorithm;
		this.format = format;
		this.encoded = encoded.clone();
	}

	/**
	 * Constructs a JAXB object from the given key, the protection parameters and
	 * the password.
	 * 
	 * @param key
	 *            the key.
	 * @param protection
	 *            the protection parameters.
	 * @param password
	 *            the password.
	 * @throws EncryptionException
	 *             - if encryption failed.
	 */
	public KeyXml(final Key key, final PasswordProtection protection, final char[] password)
			throws EncryptionException {
		this(key.getAlgorithm(), key.getFormat(), key.getEncoded(), protection, password);
	}

	/**
	 * Constructs a JAXB object from the given parameters for key, the protection
	 * parameters and the password.
	 * 
	 * @param algorithm
	 *            the name of the algorithm.
	 * @param format
	 *            the encoding format.
	 * @param encoded
	 *            the encoded key.
	 * @param protection
	 *            the protection parameters.
	 * @param password
	 *            the password.
	 * @throws EncryptionException
	 *             - if encryption failed.
	 */
	public KeyXml(final String algorithm, final String format, final byte[] encoded,
			final PasswordProtection protection, final char[] password) throws EncryptionException {
		this.protection = protection;
		this.algorithm = algorithm;
		this.format = format;
		this.encoded = protection.encrypt(encoded, password);
	}

	/**
	 * Returns the copy of the protection parameters for this object.
	 * 
	 * @return the copy of the protection parameters for this object.
	 */
	public PasswordProtection getProtection() {
		return new PasswordProtection(protection);
	}

	/**
	 * Returns the name of the algorithm.
	 * 
	 * @return the name of the algorithm.
	 */
	public String getAlgorithm() {
		return algorithm;
	}

	/**
	 * Returns the encoding format.
	 * 
	 * @return the encoding format.
	 */
	public String getFormat() {
		return format;
	}

	/**
	 * Return the copy of the encoded key.
	 * 
	 * @return the copy of the encoded key.
	 */
	public byte[] getEncoded() {
		if (encoded == null) {
			return null;
		}
		return encoded.clone();
	}

	/**
	 * Tests whether this object is protected with password.
	 * 
	 * @return true if this object is protected with password, otherwise false.
	 */
	public boolean isProtected() {
		return protection != null;
	}

	/**
	 * Creates a newly allocated Key for this JAXB object. The object must not be
	 * protected with password for using this method. PKCS#8 and X.509 formats are
	 * supported. UnsupportedOperationException will be thrown for other formats.
	 * 
	 * @return a newly allocated Key for this JAXB object.
	 * @throws NoSuchAlgorithmException
	 *             - if the algorithm is not supported by the library.
	 * @throws InvalidKeySpecException
	 *             - if key spec is invalid.
	 */
	public Key toObject() throws NoSuchAlgorithmException, InvalidKeySpecException {
		if (isProtected()) {
			// InvalidKeySpecException may be appropriate for this case.
			throw new InvalidKeySpecException("This key is password protected");
		}
		final KeyFactory keyFactory = KeyFactory.getInstance(algorithm);
		if ("PKCS#8".equals(format)) {
			final KeySpec keySpec = new PKCS8EncodedKeySpec(encoded);
			// If encrypted, InvalidKeySpecException may have been thrown.
			return keyFactory.generatePrivate(keySpec);
		} else if ("X.509".equals(format)) {
			final KeySpec keySpec = new X509EncodedKeySpec(encoded);
			return keyFactory.generatePublic(keySpec);
		} else {
			throw new UnsupportedOperationException("Unsupported format");
		}
	}

	/**
	 * Creates a newly allocated Key for this JAXB object. The object must be
	 * protected with password for using this method. PKCS#8 and X.509 formats are
	 * supported. UnsupportedOperationException will be thrown for other formats.
	 * 
	 * @param password
	 *            the password for decryption.
	 * @return a newly allocated Key for this JAXB object.
	 * @throws NoSuchAlgorithmException
	 *             - if the algorithm is not supported by the library.
	 * @throws DecryptionException
	 *             - if decryption failed.
	 */
	public Key toObject(final char[] password) throws NoSuchAlgorithmException, DecryptionException {
		if (!isProtected()) {
			// DecryptionException may be appropriate for this case.
			throw new DecryptionException("This key is not password protected");
		}
		// If not encrypted, decryption may fail, and DecryptionException will
		// be thrown.
		final byte[] decrypted = protection.decrypt(encoded, password);

		final KeyFactory keyFactory = KeyFactory.getInstance(algorithm);
		if ("PKCS#8".equals(format)) {
			final KeySpec keySpec = new PKCS8EncodedKeySpec(decrypted);
			try {
				// If not encrypted, InvalidKeySpecException may have been
				// thrown, then wrapped in DecryptionException and rethrown.
				return keyFactory.generatePrivate(keySpec);
			} catch (final InvalidKeySpecException e) {
				throw new DecryptionException(e);
			}
		} else if ("X.509".equals(format)) {
			final KeySpec keySpec = new X509EncodedKeySpec(decrypted);
			try {
				return keyFactory.generatePublic(keySpec);
			} catch (final InvalidKeySpecException e) {
				throw new DecryptionException(e);
			}
		} else {
			throw new UnsupportedOperationException("Unsupported format");
		}
	}

}
