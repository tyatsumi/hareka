package org.kareha.hareka.key;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;

public class SimpleKeyStore {

	// password can't be null when storing KeyStore
	// zero size password is OK
	private static final char[] password = new char[0];

	private final File file;
	private final KeyStore keyStore;

	// dataDirectory can be null for fake
	public SimpleKeyStore(final File file) throws IOException {
		try {
			keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
		} catch (final KeyStoreException e) {
			throw new RuntimeException(e);
		}
		this.file = file;
		try {
			if (file.exists()) {
				try (final InputStream in = new BufferedInputStream(new FileInputStream(file))) {
					keyStore.load(in, password);
				}
			} else {
				keyStore.load(null, null);
			}
		} catch (final NoSuchAlgorithmException e) {
			throw new RuntimeException(e);
		} catch (final CertificateException e) {
			throw new RuntimeException(e);
		}
	}

	public Certificate get(final String alias) {
		try {
			return keyStore.getCertificate(alias);
		} catch (final KeyStoreException e) {
			throw new RuntimeException(e);
		}
	}

	public void put(final String alias, final Certificate cert) throws IOException {
		try {
			keyStore.setCertificateEntry(alias, cert);
		} catch (final KeyStoreException e) {
			throw new RuntimeException(e);
		}
		save();
	}

	private void save() throws IOException {
		try (final OutputStream out = new BufferedOutputStream(new FileOutputStream(file))) {
			keyStore.store(out, password);
		} catch (final KeyStoreException e) {
			throw new RuntimeException(e);
		} catch (final NoSuchAlgorithmException e) {
			throw new RuntimeException(e);
		} catch (final CertificateException e) {
			throw new RuntimeException(e);
		}
	}

}
