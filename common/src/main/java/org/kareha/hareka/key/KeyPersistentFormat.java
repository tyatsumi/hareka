package org.kareha.hareka.key;

import java.io.IOException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import org.kareha.hareka.persistent.PersistentFormat;
import org.kareha.hareka.persistent.PersistentInput;
import org.kareha.hareka.persistent.PersistentOutput;
import org.kareha.hareka.protocol.ProtocolException;

public class KeyPersistentFormat implements PersistentFormat<Key> {

	@Override
	public Key read(final PersistentInput in) throws IOException, ProtocolException {
		final String algorithm = in.readString();
		final String format = in.readString();
		final byte[] encoded = in.readByteArray();
		final KeyXml xml = new KeyXml(algorithm, format, encoded);
		try {
			return xml.toObject();
		} catch (final NoSuchAlgorithmException | InvalidKeySpecException e) {
			throw new IOException(e);
		}
	}

	@Override
	public void write(final PersistentOutput out, final Key v) throws IOException {
		out.writeString(v.getAlgorithm());
		out.writeString(v.getFormat());
		out.writeByteArray(v.getEncoded());
	}

}
