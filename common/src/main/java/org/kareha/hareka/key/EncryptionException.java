package org.kareha.hareka.key;

@SuppressWarnings("serial")
public class EncryptionException extends Exception {

	public EncryptionException() {
		super();
	}

	public EncryptionException(final String message) {
		super(message);
	}

	public EncryptionException(final String message, final Throwable cause) {
		super(message, cause);
	}

	protected EncryptionException(final String message, final Throwable cause, final boolean enableSuppression,
			final boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public EncryptionException(final Throwable cause) {
		super(cause);
	}

}
