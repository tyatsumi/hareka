package org.kareha.hareka.logging.xml;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.io.UnsupportedEncodingException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.kareha.hareka.annotation.GuardedBy;

public abstract class AbstractXmlLogger<T extends XmlLogRecord> implements XmlLogger<T> {

	protected static final int INDEX_RECORD_SIZE = Long.BYTES * 2;

	protected final File directory;
	protected final String filename;
	protected final JAXBContext context;
	protected final boolean format;
	@GuardedBy("this")
	protected long nextId;

	public AbstractXmlLogger(final File directory, final String filename, final Class<?> clazz, final boolean format)
			throws IOException, JAXBException {
		this.directory = directory;
		this.filename = filename;
		context = JAXBContext.newInstance(clazz);
		this.format = format;
		nextId = loadSize();
	}

	protected long loadSize() throws IOException, JAXBException {
		try (final RandomAccessFile file = new RandomAccessFile(getIndexFile(), "r")) {
			return file.length() / INDEX_RECORD_SIZE;
		} catch (final FileNotFoundException e) {
			return 0;
		}
	}

	protected byte[] marshal(final T object) throws JAXBException {
		try (final ByteArrayOutputStream out = new ByteArrayOutputStream()) {
			final Marshaller marshaller = context.createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
			if (format) {
				marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			}
			marshaller.marshal(object, out);
			if (format) {
				try {
					final String s = out.toString("UTF-8").trim() + "\n";
					return s.getBytes("UTF-8");
				} catch (final UnsupportedEncodingException e) {
					throw new AssertionError();
				}
			}
			out.write('\n');
			return out.toByteArray();
		} catch (final IOException e) {
			throw new JAXBException(e);
		}
	}

	protected T unmarshal(final byte[] b) throws JAXBException {
		final Unmarshaller unmarshaller = context.createUnmarshaller();
		@SuppressWarnings("unchecked")
		final T object = (T) unmarshaller.unmarshal(new ByteArrayInputStream(b));
		return object;
	}

	protected File getIndexFile() {
		return new File(directory, filename + ".index");
	}

	protected File getBodyFile() {
		return new File(directory, filename + ".body");
	}

	@Override
	public synchronized long size() {
		return nextId;
	}

	protected abstract long getDate(long id) throws IOException;

	@Override
	public long find(final long date, final boolean reverse) throws IOException, JAXBException {
		final long count = size();
		if (count < 1) {
			return -1;
		}
		if (reverse) {
			final long last = getDate(count - 1);
			if (last < date) {
				return count - 1;
			}
			final long first = getDate(0);
			if (first >= date) {
				return -1;
			}
		} else {
			final long first = getDate(0);
			if (first >= date) {
				return 0;
			}
			final long last = getDate(count - 1);
			if (last < date) {
				return -1;
			}
		}
		long top = 0;
		long bottom = count - 1;
		while (true) {
			final long center = (top + bottom) / 2;
			final long d = getDate(center);
			if (d < date) {
				top = center;
			} else {
				bottom = center;
			}
			if (bottom - top <= 1) {
				return reverse ? top : bottom;
			}
		}
	}

}
