package org.kareha.hareka.logging.xml;

import java.io.IOException;
import java.util.Collection;

import javax.xml.bind.JAXBException;

import org.kareha.hareka.ManuallyClosable;

public interface XmlLogger<T extends XmlLogRecord> extends ManuallyClosable {

	Collection<T> get(long id, int count) throws IOException, JAXBException;

	void add(T object) throws IOException, JAXBException;

	long size();

	long find(long date, boolean reverse) throws IOException, JAXBException;

}
