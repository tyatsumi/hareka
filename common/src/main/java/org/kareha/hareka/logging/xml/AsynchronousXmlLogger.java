package org.kareha.hareka.logging.xml;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.bind.JAXBException;

import org.kareha.hareka.annotation.GuardedBy;
import org.kareha.hareka.annotation.Private;

public class AsynchronousXmlLogger<T extends XmlLogRecord> extends FastXmlLogger<T> {

	@Private
	static final Logger logger = Logger.getLogger(AsynchronousXmlLogger.class.getName());
	protected static final int DEFAULT_BUFFER_SIZE = 32;

	@GuardedBy("this")
	protected long size;
	protected final Buffer buffer;
	protected final WriterTask writer;
	protected final Future<Void> writerFuture;

	public AsynchronousXmlLogger(final File directory, final String filename, final Class<?> clazz,
			final boolean format, final ExecutorService executorService) throws IOException, JAXBException {
		this(directory, filename, clazz, format, executorService, DEFAULT_BUFFER_SIZE);
	}

	public AsynchronousXmlLogger(final File directory, final String filename, final Class<?> clazz,
			final boolean format, final ExecutorService executorService, final int bufferSize)
			throws IOException, JAXBException {
		super(directory, filename, clazz, format);
		size = nextId;
		buffer = new Buffer(bufferSize);
		writer = new WriterTask();
		writerFuture = executorService.submit(writer);
	}

	private class WriterTask implements Callable<Void> {

		private final BlockingQueue<T> queue = new LinkedBlockingQueue<>();

		@Private
		WriterTask() {

		}

		void add(T object) {
			if (!queue.offer(object)) {
				logger.severe("Overflow in writing queue");
			}
		}

		@Override
		public Void call() {
			try {
				while (!Thread.currentThread().isInterrupted()) {
					final T object = queue.take();
					try {
						buffer.addObject(object);
					} catch (final IOException | JAXBException e) {
						logger.log(Level.SEVERE, "", e);
					}
				}
			} catch (final InterruptedException e) {
				Thread.currentThread().interrupt();
			}
			return null;
		}

	}

	private class Buffer implements AutoCloseable {

		private final int size;
		@GuardedBy("this")
		private final List<T> objects = new ArrayList<>();
		@GuardedBy("this")
		private boolean closed;

		Buffer(final int size) {
			this.size = size;
		}

		synchronized void addObject(final T object) throws IOException, JAXBException {
			if (closed) {
				return;
			}
			objects.add(object);
			if (objects.size() >= size) {
				flush();
			}
		}

		// synchronized by Buffer then by AsynchronousXmlLogger (addObjects)
		synchronized void flush() throws IOException, JAXBException {
			if (objects.isEmpty()) {
				return;
			}
			addObjects(objects);
			objects.clear();
		}

		@Override
		public synchronized void close() throws Exception {
			flush();
			closed = true;
		}

	}

	@GuardedBy("this")
	@Private
	synchronized void addObjects(final Collection<T> objects) throws IOException, JAXBException {
		final ByteArrayOutputStream indexOut = new ByteArrayOutputStream();
		final ByteArrayOutputStream bodyOut = new ByteArrayOutputStream();
		final long bodyLength = bodyFile.length();
		long address = bodyLength;
		final DataOutputStream indexDataOut = new DataOutputStream(indexOut);
		for (final T object : objects) {
			final byte[] b = marshal(object);
			indexDataOut.writeLong(address);
			indexDataOut.writeLong(object.getDate());
			bodyOut.write(b);
			address += b.length;
		}
		indexFile.seek(indexFile.length());
		indexFile.write(indexOut.toByteArray());
		bodyFile.seek(bodyLength);
		bodyFile.write(bodyOut.toByteArray());
		size += objects.size();
	}

	@Override
	public Collection<T> get(final long id, final int count) throws IOException, JAXBException {
		synchronized (buffer) {
			buffer.flush();
			return super.get(id, count);
		}
	}

	@Override
	public synchronized void add(final T object) throws IOException, JAXBException {
		final long date = System.currentTimeMillis();
		object.setId(nextId);
		object.setDate(date);
		writer.add(object);
		nextId++;
	}

	@Override
	public void close() throws Exception {
		writerFuture.cancel(true);
		// should wait writer to finish?
		buffer.close();
		super.close();
	}

	@Override
	public synchronized long size() {
		return size;
	}

}
