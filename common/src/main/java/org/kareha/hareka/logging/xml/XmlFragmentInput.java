package org.kareha.hareka.logging.xml;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;

public class XmlFragmentInput {

	private static final int BUFFER_SIZE = 8192;

	private enum State {

		BODY, TAG_START, OPEN_TAG, CLOSE_TAG,

	}

	private interface Source {

		int read() throws IOException;

	}

	private static class InputStreamSource implements Source {

		private final InputStream in;

		InputStreamSource(final InputStream in) {
			this.in = in;
		}

		@Override
		public int read() throws IOException {
			return in.read();
		}

	}

	private static class RandomAccessFileSource implements Source {

		private final RandomAccessFile in;
		private final byte[] buffer = new byte[BUFFER_SIZE];
		private int bytesRead;
		private int index;

		RandomAccessFileSource(final RandomAccessFile in) {
			this.in = in;
		}

		@Override
		public int read() throws IOException {
			if (index >= bytesRead) {
				bytesRead = in.read(buffer);
				if (bytesRead == -1) {
					return -1;
				}
				index = 0;
			}
			final int b = buffer[index];
			index++;
			return b & 0xff;
		}

	}

	private final Source in;

	public XmlFragmentInput(final InputStream in) {
		this.in = new InputStreamSource(in);
	}

	public XmlFragmentInput(final RandomAccessFile in) {
		this.in = new RandomAccessFileSource(in);
	}

	public byte[] next() throws IOException {
		final ByteArrayOutputStream out = new ByteArrayOutputStream();
		State state = State.BODY;
		int level = 0;
		int prev = 0;
		while (true) {
			final int c = in.read();
			if (c == -1) {
				return null;
			}
			if (state == State.TAG_START) {
				if (c == '/') {
					state = State.CLOSE_TAG;
				} else {
					state = State.OPEN_TAG;
				}
			} else if (c == '<') {
				state = State.TAG_START;
			} else if (c == '>') {
				if (prev == '/') {
					state = State.CLOSE_TAG;
					level++;
				}
				if (state == State.OPEN_TAG) {
					state = State.BODY;
					level++;
				} else if (state == State.CLOSE_TAG) {
					state = State.BODY;
					level--;
					if (level == 0) {
						out.write(c);
						return out.toByteArray();
					}
				}
			}
			out.write(c);
			prev = c;
		}
	}

}
