package org.kareha.hareka.logging.xml;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import javax.xml.bind.JAXBException;

import org.kareha.hareka.annotation.GuardedBy;

public class FastXmlLogger<T extends XmlLogRecord> extends AbstractXmlLogger<T> {

	@GuardedBy("this")
	protected final RandomAccessFile indexFile;
	@GuardedBy("this")
	protected final RandomAccessFile bodyFile;

	public FastXmlLogger(final File directory, final String filename, final Class<?> clazz, final boolean format)
			throws IOException, JAXBException {
		super(directory, filename, clazz, format);
		indexFile = new RandomAccessFile(getIndexFile(), "rw");
		bodyFile = new RandomAccessFile(getBodyFile(), "rw");
	}

	@Override
	public synchronized Collection<T> get(final long id, final int count) throws IOException, JAXBException {
		final long indexOffset = id * INDEX_RECORD_SIZE;
		final long bodyBlockOffset;
		if (indexFile.length() < indexOffset + INDEX_RECORD_SIZE) {
			return Collections.emptyList();
		}
		indexFile.seek(indexOffset);
		bodyBlockOffset = indexFile.readLong();

		bodyFile.seek(bodyBlockOffset);
		final XmlFragmentInput input = new XmlFragmentInput(bodyFile);
		final List<T> list = new ArrayList<>();
		for (int i = 0; i < count; i++) {
			final byte[] b = input.next();
			if (b == null) {
				return list;
			}
			list.add(unmarshal(b));
		}
		return list;
	}

	@Override
	public synchronized void add(final T object) throws IOException, JAXBException {
		final long date = System.currentTimeMillis();
		object.setId(nextId);
		object.setDate(date);
		final long bodyLength = bodyFile.length();
		indexFile.seek(indexFile.length());
		indexFile.writeLong(bodyLength);
		indexFile.writeLong(date);
		bodyFile.seek(bodyLength);
		bodyFile.write(marshal(object));
		nextId++;
	}

	@Override
	public void close() throws Exception {
		Exception ex = null;
		try {
			indexFile.close();
		} catch (final Exception e) {
			ex = e;
		}
		try {
			bodyFile.close();
		} catch (final Exception e) {
			ex = e;
		}
		if (ex != null) {
			throw ex;
		}
	}

	@Override
	public synchronized long getDate(final long id) throws IOException {
		final long indexOffset = id * INDEX_RECORD_SIZE;
		if (indexFile.length() < indexOffset + INDEX_RECORD_SIZE) {
			throw new IOException();
		}
		indexFile.seek(indexOffset + Long.BYTES);
		return indexFile.readLong();
	}

}
