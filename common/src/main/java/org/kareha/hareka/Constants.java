package org.kareha.hareka;

import java.math.BigInteger;

public final class Constants {

	private Constants() {
		throw new AssertionError();
	}

	public static final Version VERSION = new Version("0.1.0-alpha.14");

	public static final String KEY_PAIR_ALGORITHM = "RSA";
	public static final int KEY_PAIR_BIT_LENGTH = 2048;

	public static final String SIGNATURE_ALGORITHM = "SHA256withRSA";

	// should be bit length?
	public static final int NONCE_BYTE_LENGTH = 32;

	private static final String[] ACCEPTABLE_PUBLIC_KEY_ALGORITHMS = { "RSA", };
	private static final String[] ACCEPTABLE_PUBLIC_KEY_FORMATS = { "X.509", };

	public static String[] getAcceptablePublicKeyAlgorithms() {
		return ACCEPTABLE_PUBLIC_KEY_ALGORITHMS.clone();
	}

	public static String[] getAcceptablePublicKeyFormats() {
		return ACCEPTABLE_PUBLIC_KEY_FORMATS.clone();
	}

	public static final int PASSWORD_PROTECTION_ITERATION_COUNT = 100000;
	public static final int PASSWORD_PROTECTION_KEY_LENGTH = 128;
	public static final String PASSWORD_PROTECTION_KEY_ALGORITHM = "PBKDF2WithHmacSHA1";
	public static final String PASSWORD_PROTECTION_CIPHER_ALGORITHM = "AES/CBC/PKCS5Padding";

	public static final BigInteger NEW_USER_POW_TARGET = BigInteger.ONE.shiftLeft(256 - 22);
	public static final int POW_DATA_BYTE_LENGTH = 32;

	public static final int ID_CIPHER_KEY_LENGTH = 128 / 8;

	public static final int MIN_NAME_LENGTH = 3;
	public static final int MAX_NAME_LENGTH = 24;

}
