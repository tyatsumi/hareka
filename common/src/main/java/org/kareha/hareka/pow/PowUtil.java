package org.kareha.hareka.pow;

import java.math.BigInteger;

public class PowUtil {

	private PowUtil() {
		throw new AssertionError();
	}

	public static long estimatedTime(final byte[] nonce, final long elapsedTime, final BigInteger target) {
		return Pow.bound.multiply(BigInteger.valueOf(elapsedTime)).divide(target).divide(new BigInteger(1, nonce))
				.longValue();
	}

}
