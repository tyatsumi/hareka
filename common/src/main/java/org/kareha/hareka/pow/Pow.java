package org.kareha.hareka.pow;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.kareha.hareka.annotation.Private;

// SHA_d-256: m -> SHA-256(SHA-256(0^512 || m))
public class Pow {

	public static class Result {

		private final byte[] nonce;
		private final long elapsedTime;

		public Result(final byte[] nonce, final long elapsedTime) {
			this.nonce = nonce.clone();
			this.elapsedTime = elapsedTime;
		}

		public byte[] nonce() {
			return nonce.clone();
		}

		public long elapsedTime() {
			return elapsedTime;
		}

	}

	private static final String HASH_ALGORITHM = "SHA-256";
	static final BigInteger bound = BigInteger.ONE.shiftLeft(256);

	@Private
	final BigInteger target;
	@Private
	final byte[] data;
	private final int threads;

	public Pow(final BigInteger target, final byte[] data, final int threads) {
		this.target = target;
		this.data = data.clone();
		this.threads = threads;
	}

	public Pow(final BigInteger target, final byte[] data) {
		this(target, data, Runtime.getRuntime().availableProcessors());
	}

	private class Task implements Callable<byte[]> {

		private final int offset;
		private final BigInteger step;
		private final MessageDigest md0;

		Task(final int offset, final int step) {
			this.offset = offset;
			this.step = BigInteger.valueOf(step);

			final MessageDigest md;
			try {
				md = MessageDigest.getInstance(HASH_ALGORITHM);
			} catch (final NoSuchAlgorithmException e) {
				throw new AssertionError(e);
			}
			boolean clonable = true;
			try {
				md.clone();
			} catch (final CloneNotSupportedException e) {
				clonable = false;
			}
			if (clonable) {
				md.update(new byte[64]);
				md.update(data);
				md0 = md;
			} else {
				md0 = null;
			}
		}

		@Override
		public byte[] call() throws InterruptedException {
			BigInteger nonce = BigInteger.valueOf(offset);
			while (!Thread.currentThread().isInterrupted()) {
				final byte[] b = nonce.toByteArray();
				final BigInteger current = new BigInteger(1, hash(b));
				if (current.compareTo(target) == -1) {
					return b;
				}
				nonce = nonce.add(step);
			}
			throw new InterruptedException();
		}

		private byte[] hash(final byte[] nonce) {
			final MessageDigest md;
			if (md0 == null) {
				try {
					md = MessageDigest.getInstance(HASH_ALGORITHM);
				} catch (final NoSuchAlgorithmException e) {
					throw new AssertionError(e);
				}
				md.update(new byte[64]);
				md.update(data);
			} else {
				try {
					md = (MessageDigest) md0.clone();
				} catch (final CloneNotSupportedException e) {
					throw new AssertionError();
				}
			}
			return md.digest(md.digest(nonce));
		}

	}

	public Result perform() throws InterruptedException {
		final long startTime = System.currentTimeMillis();
		final ExecutorService executorService = Executors.newFixedThreadPool(threads);
		final CompletionService<byte[]> completionService = new ExecutorCompletionService<>(executorService);
		try {
			for (int i = 0; i < threads; i++) {
				completionService.submit(new Task(i, threads));
			}
			final Future<byte[]> future = completionService.take();
			try {
				return new Result(future.get(), System.currentTimeMillis() - startTime);
			} catch (final ExecutionException e) {
				throw new RuntimeException(e);
			}
		} finally {
			executorService.shutdownNow();
		}
	}

	byte[] naiveHash(final byte[] nonce) {
		final MessageDigest md;
		try {
			md = MessageDigest.getInstance(HASH_ALGORITHM);
		} catch (final NoSuchAlgorithmException e) {
			throw new AssertionError(e);
		}
		md.update(new byte[64]);
		md.update(data);
		return md.digest(md.digest(nonce));
	}

	public boolean verify(final byte[] nonce) {
		return new BigInteger(1, naiveHash(nonce)).compareTo(target) == -1;
	}

}
