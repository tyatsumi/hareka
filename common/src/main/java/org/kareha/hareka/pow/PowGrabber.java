package org.kareha.hareka.pow;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import org.kareha.hareka.annotation.GuardedBy;
import org.kareha.hareka.annotation.ThreadSafe;

@ThreadSafe
public class PowGrabber {

	public interface Handler {

		void handle(boolean result);

		void canceled();

	}

	public static class Entry {

		private static final Random random = new SecureRandom();

		private final int id;
		private final Handler handler;
		@GuardedBy("this")
		private BigInteger target;
		@GuardedBy("this")
		private byte[] data;

		Entry(final int id, final Handler handler, final BigInteger target) {
			this.id = id;
			this.handler = handler;
			this.target = target;
		}

		public int getId() {
			return id;
		}

		public void handle(boolean result) {
			handler.handle(result);
		}

		public void cancel() {
			handler.canceled();
		}

		public byte[] newData(final int length) {
			final byte[] b = new byte[length];
			random.nextBytes(b);
			synchronized (this) {
				data = b;
			}
			return b;
		}

		public boolean verify(final byte[] nonce) {
			final Pow pow = new Pow(target, data);
			return pow.verify(nonce);
		}

	}

	private final int capacity;
	@GuardedBy("this")
	private int count;
	@GuardedBy("this")
	private final Map<Integer, Entry> entryMap;

	public PowGrabber(final int capacity) {
		this.capacity = capacity;
		entryMap = new HashMap<>(Math.min(16, (int) Math.ceil(capacity / 0.75)));
	}

	public PowGrabber() {
		this(1);
	}

	public synchronized Entry add(final Handler v, final BigInteger target) {
		if (entryMap.size() >= capacity) {
			return null;
		}
		final int id = count++;
		final Entry entry = new Entry(id, v, target);
		entryMap.put(id, entry);
		return entry;
	}

	public synchronized Entry get(final int id) {
		return entryMap.remove(id);
	}

}
