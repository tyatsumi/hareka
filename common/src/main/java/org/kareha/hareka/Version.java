package org.kareha.hareka;

import java.io.IOException;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlAdapter;

import org.kareha.hareka.annotation.Immutable;
import org.kareha.hareka.annotation.Private;
import org.kareha.hareka.protocol.ProtocolElement;
import org.kareha.hareka.protocol.ProtocolException;
import org.kareha.hareka.protocol.ProtocolInput;
import org.kareha.hareka.protocol.ProtocolOutput;

@Immutable
public class Version implements ProtocolElement {

	@Private
	final String value;

	public Version(final String value) {
		if (value == null) {
			throw new IllegalArgumentException("null");
		}
		this.value = value;
	}

	@Override
	public boolean equals(final Object obj) {
		if (!(obj instanceof Version)) {
			return false;
		}
		final Version version = (Version) obj;
		return version.value.equals(value);
	}

	@Override
	public int hashCode() {
		return value.hashCode();
	}

	@Override
	public String toString() {
		return value;
	}

	public static Version readFrom(final ProtocolInput in) throws IOException, ProtocolException {
		final String value = in.readString();
		return new Version(value);
	}

	@Override
	public void writeTo(final ProtocolOutput out) {
		out.writeString(value);
	}

	@XmlType(name = "version")
	@XmlAccessorType(XmlAccessType.NONE)
	private static class Adapted {

		@XmlElement
		private String value;

		@SuppressWarnings("unused")
		private Adapted() {
			// used by JAXB
		}

		@Private
		Adapted(final Version v) {
			value = v.value;
		}

		@Private
		Version unmarshal() {
			return new Version(value);
		}

	}

	@Private
	Adapted marshal() {
		return new Adapted(this);
	}

	static class Adapter extends XmlAdapter<Adapted, Version> {

		@Override
		public Adapted marshal(final Version v) throws Exception {
			if (v == null) {
				return null;
			}
			return v.marshal();
		}

		@Override
		public Version unmarshal(final Adapted v) throws Exception {
			if (v == null) {
				return null;
			}
			return v.unmarshal();
		}

	}

	public String getValue() {
		return value;
	}

}
