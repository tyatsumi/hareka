package org.kareha.hareka.game;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.kareha.hareka.annotation.GuardedBy;
import org.kareha.hareka.annotation.Private;
import org.kareha.hareka.annotation.ThreadSafe;
import org.kareha.hareka.protocol.ProtocolElement;
import org.kareha.hareka.protocol.ProtocolException;
import org.kareha.hareka.protocol.ProtocolInput;
import org.kareha.hareka.protocol.ProtocolOutput;

@ThreadSafe
@XmlJavaTypeAdapter(Name.Adapter.class)
public class Name implements ProtocolElement {

	private enum BundleKey {
		DefaultLanguage, DefaultTranslation,
	}

	private static final String DEFAULT_LANGUAGE;
	private static final String DEFAULT_TRANSLATION;

	static {
		final ResourceBundle bundle = ResourceBundle.getBundle(Name.class.getName());
		DEFAULT_LANGUAGE = bundle.getString(BundleKey.DefaultLanguage.name());
		DEFAULT_TRANSLATION = bundle.getString(BundleKey.DefaultTranslation.name());
	}

	@GuardedBy("this")
	@Private
	final Map<String, String> translations = new HashMap<>();

	public Name() {
		// do nothing
	}

	@Private
	Name(final Collection<Translation> translations) {
		for (final Translation i : translations) {
			if (i.language == null || i.language.isEmpty()) {
				throw new IllegalArgumentException("language must not be null nor null string");
			}
			if (i.value == null || i.value.isEmpty()) {
				throw new IllegalArgumentException("value must not be null nor null string");
			}
			this.translations.put(i.language, i.value);
		}
	}

	public Name(final Name original) {
		synchronized (original) {
			translations.putAll(original.translations);
		}
	}

	public static Name readFrom(final ProtocolInput in) throws IOException, ProtocolException {
		final int size = in.readCompactUInt();
		final List<Translation> list = new ArrayList<>();
		for (int i = 0; i < size; i++) {
			final String language = in.readString();
			if (language.isEmpty()) {
				throw new ProtocolException("language must not be null string");
			}
			final String translation = in.readString();
			if (translation.isEmpty()) {
				throw new ProtocolException("translation must not be null string");
			}
			list.add(new Translation(language, translation));
		}
		return new Name(list);
	}

	@Override
	public synchronized void writeTo(final ProtocolOutput out) {
		out.writeCompactUInt(translations.size());
		for (final Map.Entry<String, String> entry : translations.entrySet()) {
			out.writeString(entry.getKey());
			out.writeString(entry.getValue());
		}
	}

	@XmlType(name = "nameTranslation")
	@XmlAccessorType(XmlAccessType.NONE)
	private static class Translation {

		@XmlAttribute
		@Private
		String language;
		@XmlValue
		@Private
		String value;

		@SuppressWarnings("unused")
		private Translation() {
			// used by JAXB
		}

		@Private
		Translation(final String language, final String value) {
			this.language = language;
			this.value = value;
		}

	}

	@XmlType(name = "name")
	@XmlAccessorType(XmlAccessType.NONE)
	private static class Adapted {

		@XmlElement(name = "translation")
		private List<Translation> translations;

		@SuppressWarnings("unused")
		private Adapted() {
			// used by JAXB
		}

		@Private
		Adapted(final Name v) {
			translations = new ArrayList<>();
			synchronized (v) {
				for (final Map.Entry<String, String> entry : v.translations.entrySet()) {
					translations.add(new Translation(entry.getKey(), entry.getValue()));
				}
			}
		}

		@Private
		Name unmarshal() {
			return new Name(translations);
		}

	}

	@Private
	Adapted marshal() {
		return new Adapted(this);
	}

	static class Adapter extends XmlAdapter<Adapted, Name> {

		@Override
		public Adapted marshal(final Name v) throws Exception {
			if (v == null) {
				return null;
			}
			return v.marshal();
		}

		@Override
		public Name unmarshal(final Adapted v) throws Exception {
			if (v == null) {
				return null;
			}
			return v.unmarshal();
		}

	}

	public synchronized boolean isEmpty() {
		// value != null && !value.isEmpty()
		return translations.isEmpty();
	}

	public synchronized String get(final String language) {
		final String translation = translations.get(language);
		if (translation != null) {
			return translation;
		}
		final String defaultTranslation = translations.get(DEFAULT_LANGUAGE);
		if (defaultTranslation != null) {
			return defaultTranslation;
		}
		for (final String anyTranslation : translations.values()) {
			// anyTranslation != null
			return anyTranslation;
		}
		return DEFAULT_TRANSLATION;
	}

	public synchronized void set(final String language, final String translation) {
		if (translation == null || translation.isEmpty()) {
			translations.remove(language);
			return;
		}
		translations.put(language, translation);
	}

	public synchronized Map<String, String> getMap() {
		return new HashMap<>(translations);
	}

}
