package org.kareha.hareka.game;

import java.io.IOException;

import org.kareha.hareka.protocol.ProtocolElement;
import org.kareha.hareka.protocol.ProtocolInput;
import org.kareha.hareka.protocol.ProtocolOutput;
import org.kareha.hareka.protocol.ProtocolException;

public enum ItemType implements ProtocolElement {

	ACORN,

	DEW,

	LEAF0, LEAF1, LEAF2, LEAF3, LEAF4, LEAF5, LEAF6, LEAF7, LEAF8, LEAF9, LEAF10, LEAF11, LEAF12, LEAF13, LEAF14,
	LEAF15, LEAF16, LEAF17, LEAF18, LEAF19, LEAF20, LEAF21, LEAF22, LEAF23, LEAF24, LEAF25, LEAF26, LEAF27,

	GRAPEFRUIT,

	CHERRY,

	APPLE, APPLE1,

	STRAWBERRY,

	PEACH,

	MELON,

	RASPBERRY,

	WATERMELON,

	;

	private static final ItemType[] values = values();

	public static ItemType valueOf(final int ordinal) {
		if (ordinal < 0 || ordinal >= values.length) {
			throw new IllegalArgumentException("Out of bounds");
		}
		return values[ordinal];
	}

	public static ItemType readFrom(final ProtocolInput in) throws IOException, ProtocolException {
		final int ordinal = in.readCompactUInt();
		return valueOf(ordinal);
	}

	@Override
	public void writeTo(final ProtocolOutput out) {
		out.writeCompactUInt(ordinal());
	}

}
