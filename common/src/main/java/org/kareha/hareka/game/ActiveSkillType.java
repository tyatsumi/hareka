package org.kareha.hareka.game;

import java.io.IOException;

import org.kareha.hareka.protocol.ProtocolElement;
import org.kareha.hareka.protocol.ProtocolException;
import org.kareha.hareka.protocol.ProtocolInput;
import org.kareha.hareka.protocol.ProtocolOutput;

public enum ActiveSkillType implements ProtocolElement {

	TELEPORT_RANDOMLY(SkillTargetType.NULL),

	ATTACK(SkillTargetType.FIELD_ENTITY),

	GREET(SkillTargetType.FIELD_ENTITY),

	PICK_UP(SkillTargetType.FIELD_ENTITY),

	POSSESS(SkillTargetType.FIELD_ENTITY),

	PULL(SkillTargetType.FIELD_ENTITY),

	PUSH(SkillTargetType.FIELD_ENTITY),

	REVIVE(SkillTargetType.FIELD_ENTITY),

	BREAK_WALL(SkillTargetType.TILE),

	;

	private final SkillTargetType targetType;

	private ActiveSkillType(final SkillTargetType targetType) {
		this.targetType = targetType;
	}

	public SkillTargetType getTargetType() {
		return targetType;
	}

	private static final ActiveSkillType[] values = values();

	public static ActiveSkillType valueOf(final int ordinal) {
		if (ordinal < 0 || ordinal >= values.length) {
			throw new IllegalArgumentException("Out of bounds");
		}
		return values[ordinal];
	}

	public static ActiveSkillType readFrom(final ProtocolInput in) throws IOException, ProtocolException {
		final int ordinal = in.readCompactUInt();
		return valueOf(ordinal);
	}

	@Override
	public void writeTo(final ProtocolOutput out) {
		out.writeCompactUInt(ordinal());
	}

}
