package org.kareha.hareka.sound;

import java.io.IOException;
import java.io.InputStream;

import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.MidiUnavailableException;
import javax.sound.midi.Sequence;
import javax.sound.midi.Sequencer;

final class MultiSequencerTest {

	private static final int count = 8;
	private static final String path = "org/kareha/hareka/sound/resource/music/mutopiaproject.org/Sheep.mid";

	private MultiSequencerTest() {
		throw new AssertionError();
	}

	public static void main(final String[] args)
			throws MidiUnavailableException, InvalidMidiDataException, IOException, InterruptedException {
		final Sequencer[] sequencers = new Sequencer[count];
		for (int i = 0; i < count; i++) {
			sequencers[i] = MidiSystem.getSequencer();
		}
		for (int i = 0; i < count; i++) {
			sequencers[i].open();
		}

		final Sequence sequence;
		try (final InputStream in = MultiSequencerTest.class.getClassLoader().getResourceAsStream(path)) {
			sequence = MidiSystem.getSequence(in);
		}

		for (int i = 0; i < count; i++) {
			sequencers[i].setSequence(sequence);
			sequencers[i].start();
			Thread.sleep(2000);
		}
	}

}
