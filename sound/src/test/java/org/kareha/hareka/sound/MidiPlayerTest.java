package org.kareha.hareka.sound;

final class MidiPlayerTest {

	private MidiPlayerTest() {
		throw new AssertionError();
	}

	public static void main(final String[] args) throws Exception {
		final MidiPlayer player = new MidiPlayer();
		final MusicPlayer music = new MusicPlayer(player);
		music.play("field", false);

		Thread.sleep(4000);
		music.stop();
		Thread.sleep(4000);
		music.start();
		Thread.sleep(4000);
		music.stop();
		Thread.sleep(4000);

		music.start();
		Thread.sleep(4000);
		player.setEnabled(false);
		Thread.sleep(4000);
		player.setEnabled(true);
		Thread.sleep(4000);
		music.stop();
		Thread.sleep(4000);

		player.close();
	}

}
