package org.kareha.hareka.sound;

import java.io.File;

import javax.xml.bind.JAXB;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.kareha.hareka.annotation.GuardedBy;
import org.kareha.hareka.annotation.Private;
import org.kareha.hareka.annotation.ThreadSafe;

@ThreadSafe
public class SoundSettings {

	private static final String filename = "SoundSettings.xml";

	private final File file;
	@GuardedBy("this")
	@Private
	boolean musicEnabled;
	@GuardedBy("this")
	@Private
	String midiDevice;

	public SoundSettings(final File dataDirectory) {
		file = new File(dataDirectory, filename);
		if (!load()) {
			setupDefault();
			helperSave();
		}
	}

	private void setupDefault() {
		synchronized (this) {
			musicEnabled = isDefaultMusicEnabled();
			midiDevice = getDefaultMidiDevice();
		}
	}

	@XmlRootElement(name = "soundSettings")
	@XmlAccessorType(XmlAccessType.NONE)
	private static class Adapted {

		@XmlElement
		@Private
		Boolean musicEnabled;
		@XmlElement
		@Private
		String midiDevice;

		@SuppressWarnings("unused")
		private Adapted() {
			// used by JAXB
		}

		@Private
		Adapted(final SoundSettings v) {
			synchronized (v) {
				musicEnabled = v.musicEnabled;
				midiDevice = v.midiDevice;
			}
		}

	}

	private boolean load() {
		if (!file.isFile()) {
			return false;
		}
		final Adapted adapted = JAXB.unmarshal(file, Adapted.class);
		synchronized (this) {
			musicEnabled = adapted.musicEnabled != null ? adapted.musicEnabled : isDefaultMusicEnabled();
			midiDevice = adapted.midiDevice != null ? adapted.midiDevice : getDefaultMidiDevice();
		}
		return true;
	}

	private synchronized void helperSave() {
		JAXB.marshal(new Adapted(this), file);
	}

	public void save() {
		helperSave();
	}

	private static boolean isDefaultMusicEnabled() {
		return true;
	}

	public synchronized boolean isMusicEnabled() {
		return musicEnabled;
	}

	public synchronized void setMusicEnabled(final boolean v) {
		musicEnabled = v;
	}

	private static String getDefaultMidiDevice() {
		return MidiPlayer.getDefaultDeviceName();
	}

	public synchronized String getMidiDevice() {
		return midiDevice;
	}

	public synchronized void setMidiDevice(final String v) {
		midiDevice = v;
	}

}
