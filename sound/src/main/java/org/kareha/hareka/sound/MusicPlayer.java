package org.kareha.hareka.sound;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.Sequence;
import javax.xml.bind.JAXB;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.kareha.hareka.annotation.GuardedBy;

public class MusicPlayer {

	private static final Logger logger = Logger.getLogger(MusicPlayer.class.getName());

	private static final String resourceHead = "org/kareha/hareka/sound/resource/music/";
	private static final String tableFilename = "table.xml";

	private final MidiPlayer midiPlayer;
	@GuardedBy("this")
	private final Map<String, String> map = new HashMap<>();
	@GuardedBy("this")
	private final Map<String, Sequence> cache = new HashMap<>();
	@GuardedBy("this")
	private String currentId;

	public MusicPlayer(final MidiPlayer midiPlayer) {
		this.midiPlayer = midiPlayer;

		try (final InputStream in = getClass().getClassLoader().getResourceAsStream(resourceHead + tableFilename)) {
			final MusicTableEntry entry = JAXB.unmarshal(in, MusicTableEntry.class);
			for (final MusicEntry me : entry.list) {
				map.put(me.id, me.path);
			}
		} catch (final IOException e) {
			logger.log(Level.SEVERE, "", e);
		}
	}

	private String getPath(final String id) {
		final String p = map.get(id);
		if (p == null) {
			return null;
		}
		return resourceHead + p;
	}

	public synchronized boolean play(final String id, final boolean loop) {
		if (id.equals(currentId)) {
			return true;
		}
		currentId = id;
		Sequence sequence = cache.get(id);
		if (sequence == null) {
			final String path = getPath(id);
			if (path == null) {
				return false;
			}
			try (final InputStream in = getClass().getClassLoader().getResourceAsStream(path)) {
				if (in == null) {
					return false;
				}
				sequence = MidiSystem.getSequence(in);
			} catch (final IOException e) {
				logger.log(Level.SEVERE, "", e);
				return false;
			} catch (final InvalidMidiDataException e) {
				logger.log(Level.SEVERE, "", e);
				return false;
			}
			cache.put(id, sequence);
		}
		if (midiPlayer != null) {
			try {
				midiPlayer.setSequence(sequence, loop);
			} catch (final InvalidMidiDataException e) {
				logger.log(Level.SEVERE, "", e);
				return false;
			}
			midiPlayer.start();
		}
		return true;
	}

	public synchronized boolean play(final File file, final boolean loop) {
		final Sequence sequence;
		try (final InputStream in = new BufferedInputStream(new FileInputStream(file))) {
			sequence = MidiSystem.getSequence(in);
		} catch (final IOException e) {
			logger.log(Level.SEVERE, "", e);
			return false;
		} catch (final InvalidMidiDataException e) {
			logger.log(Level.SEVERE, "", e);
			return false;
		}
		if (midiPlayer != null) {
			try {
				midiPlayer.setSequence(sequence, loop);
			} catch (final InvalidMidiDataException e) {
				logger.log(Level.SEVERE, "", e);
				return false;
			}
			midiPlayer.start();
		}
		return true;
	}

	public synchronized void start() {
		if (midiPlayer != null) {
			midiPlayer.start();
		}
	}

	public synchronized void stop() {
		if (midiPlayer != null) {
			midiPlayer.stop();
		}
	}

	@XmlRootElement(name = "musicTable")
	@XmlAccessorType(XmlAccessType.NONE)
	private static final class MusicTableEntry {

		@XmlElement(name = "entry")
		List<MusicEntry> list = new ArrayList<>();

	}

	@XmlAccessorType(XmlAccessType.NONE)
	private static final class MusicEntry {

		@XmlElement
		String id;
		@XmlElement
		String path;

	}

}
